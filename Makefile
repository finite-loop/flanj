# Manage the flutils package
# Run the following for more information:
#	make help

PROJECT := flanj
PROJECT_HOME := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

.PHONY: validate-activated
validate-activated:
ifndef VIRTUAL_ENV
	$(error "Must be in the $(PROJECT) activated virtualenv.")
else
ifeq (,$(findstring $(PROJECT), ${VIRTUAL_ENV}))
	$(error "In the wrong activated environment. Must be in the $(PROJECT) activated virtualenv.")
endif
endif

.PHONY: validate-not-activate
ifndef VIRTUAL_ENV
	$(error "Can NOT be in an activated virtualenv.")
endif


.PHONY: help
## Show help
help:
# The following help code was modified from https://bit.ly/2D2e0oP
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${GREEN}%s${RESET}\n    %s\n\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)


.PHONY: mypy-file
## Run mypy type checking on code to the given path to file.
mypy-file: validate-activated
	mypy --config-file=./.mypy.ini $(filter-out $@,$(MAKECMDGOALS))


.PHONY: mypy
## Run mypy type checking on all of the flanj code.
mypy: validate-activated
	mypy --config-file=./.mypy.ini -p flanj


.PHONY: tests
## Run the tests for flanj.
tests: validate-activated
	@cd ./tests && ./manage.py test


.PHONY: coverage
## Run tests, coverage and show a general terminal report
coverage: validate-activated
	cd $(PROJECT_HOME)tests && \
	coverage run --rcfile=../.coveragerc ./manage.py test && \
	coverage report --rcfile=../.coveragerc

.PHONY: coverage-html
## Run tests, coverage, and show the html report.
coverage-html: validate-activated
	cd $(PROJECT_HOME)tests && \
	coverage run --rcfile=$(PROJECT_HOME).coveragerc ./manage.py test && \
	coverage report --rcfile=$(PROJECT_HOME).coveragerc && \
	coverage html --rcfile=$(PROJECT_HOME).coveragerc && \
	python -m webbrowser -t "file://$(PROJECT_HOME)tests/htmlcov/index.html"

.PHONY: clean
## Clean up all test and build artifacts.
clean:
	rm -rf ./tests/htmlcov
	rm -rf ./tests/.coverage
	rm -rf ./dist
	rm -rf ./.mypy_cache


.PHONY: build
## Build flanj
build:
	rm -rf ./dist && \
	poetry build -vv
	@echo "don't forget to run 'make deploy'"

.PHONY: deploy
## deploy flanj
deploy:
	twine upload --repository gitlab-flanj dist/*

