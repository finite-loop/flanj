from django.db import models
from django.contrib.auth.models import Group


class GroupIdentifier(models.Model):
    group = models.OneToOneField(
        Group,
        models.CASCADE
    )
    identifier = models.CharField(
        max_length=150,
        unique=True,
        db_index=True
    )
