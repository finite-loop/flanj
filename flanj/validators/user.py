from functools import cached_property
from typing import (
    Any,
    Optional,
    Union,
    Type,
    cast,
)
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db.models import Model
from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from django.utils.translation import (
    gettext_lazy,
    ngettext_lazy,
)
from .general import (
    MinLengthValidator,
    MaxLengthValidator,
)


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class UsernameMinLengthValidator(MinLengthValidator):
    # noinspection Mypy
    message = ngettext_lazy(
        'Username must have, at least, %(limit_value)d character '
        '(it has %(show_value)d).',
        'Username, must have, at least, %(limit_value)d characters '
        '(it has %(show_value)d).',
        'limit_value'  # type: ignore
    )
    code = 'invalid'
    flags = 0


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class UsernameMaxLengthValidator(MaxLengthValidator):
    # noinspection Mypy
    message = ngettext_lazy(
        'Username can have, at most, a maximum of %(limit_value)d '
        'character (it has %(show_value)d).',
        'Username can have, at most,  a maximum of %(limit_value)d '
        'characters (it has %(show_value)d).',
        'limit_value'  # type: ignore
    )
    code = 'invalid'
    flags = 0


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class UsernameUnicodeValidator(UnicodeUsernameValidator):
    message = gettext_lazy(
        'Username may contain only letters, numbers, and @.+-_ characters.'
    )


# noinspection PyUnusedClass,PyUnusedName,PyUnusedFunction
@deconstructible
class BaseUserValidator:
    code = 'invalid'
    message = gettext_lazy(
        'User already exists.'
    )

    def __init__(
            self,
            message: Optional[str] = None,
            code: Optional[str] = None,
    ) -> None:
        if message is not None:
            self.message = message
        if code is not None:
            self.code = code
        self._user: Optional[AbstractBaseUser] = None

    @cached_property
    def _model(self) -> Type[AbstractBaseUser]:
        # noinspection PyTypeChecker,Mypy
        out: Union[Type[Model], Type[AbstractBaseUser]] = get_user_model()
        # noinspection PyTypeChecker
        out = cast(Type[AbstractBaseUser], out)
        return out

    def set_user(self, instance: AbstractBaseUser) -> None:
        self._user = instance

    @property
    def _user_id(self) -> Union[int, None]:
        out = None
        if self._user is not None:
            if hasattr(self._user, 'id'):
                try:
                    out = int(getattr(self._user, 'id'))
                except (TypeError, ValueError):     # pragma: no cover
                    pass                            # pragma: no cover
        return out

    def __eq__(self, other: Any) -> bool:
        return (  # pragma: no cover
                isinstance(other, self.__class__) and
                self.message == other.message and
                self.code == other.code
        )

    def __call__(self, value: str) -> None:
        raise NotImplementedError(  # pragma: no cover
            '%s must be a callable.' % self.__class__.__name__
        )


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class UsernameUniqueValidator(BaseUserValidator):
    message = gettext_lazy(
        'Username already exists.'
    )

    def __call__(self, value: str) -> None:
        queryset = self._model.objects.filter(username=value)
        if self._user_id is not None:
            queryset = queryset.exclude(pk=self._user_id)
        if queryset.exists():
            raise ValidationError(self.message, code=self.code)


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class UserEmailUniqueValidator(BaseUserValidator):
    message = gettext_lazy(
        'Email address already exists.'
    )

    def __call__(self, value: str) -> None:
        queryset = self._model.objects.filter(email=value)
        if self._user_id is not None:
            queryset = queryset.exclude(pk=self._user_id)
        if queryset.exists():
            raise ValidationError(self.message, code=self.code)
