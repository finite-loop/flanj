from typing import (
    Any,
    Optional,
    cast,
)
from django.core.exceptions import ValidationError
from django.core.validators import BaseValidator
from django.core.validators import (
    DecimalValidator as _DecimalValidator,
    EmailValidator as _EmailValidator,
)

from django.utils.deconstruct import deconstructible
from django.utils.translation import (
    gettext_lazy,
    ngettext_lazy,
)
from flanj.utils.objs import is_tz_name


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class MaxValueValidator(BaseValidator):
    message = gettext_lazy(
        'This value must be less than or equal to %(limit_value)s.'
    )
    code = 'max_value'

    def compare(self, a: Any, b: Any) -> bool:
        return a > b


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class MinValueValidator(BaseValidator):
    message = gettext_lazy(
        'This value must be greater than or equal to %(limit_value)s.'
    )
    code = 'min_value'

    def compare(self, a: Any, b: Any) -> bool:
        return a < b


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class MinLengthValidator(BaseValidator):
    message = ngettext_lazy(
        (
            'This value must have, at least, %(limit_value)d character '
            '(it has %(show_value)d).'
        ),
        (
            'This value must have, at least, %(limit_value)d characters '
            '(it has %(show_value)d).'
        ),
        'limit_value'  # type: ignore
    )

    code = 'min_length'

    def compare(self, a: Any, b: Any) -> bool:
        return a < b

    def clean(self, x: Any) -> int:
        return len(x)


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class MaxLengthValidator(BaseValidator):
    message = ngettext_lazy(
        (
            'This value can have, at most, %(limit_value)d character '
            '(it has %(show_value)d).'
        ),
        (
            'This value can have, at most, %(limit_value)d characters '
            '(it has %(show_value)d).'
        ),
        'limit_value'  # type: ignore
    )
    code = 'max_length'

    def compare(self, a: Any, b: Any) -> bool:
        return a > b

    def clean(self, x: Any) -> int:
        return len(x)


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class DecimalValidator(_DecimalValidator):
    messages = {
        'invalid': gettext_lazy('Enter a number.'),
        'max_digits': ngettext_lazy(
            'This value must have, no more than, %(max)s digit in total.',
            'This value must have, no more than, %(max)s digits in total.',
            'max'  # type: ignore
        ),
        'max_decimal_places': ngettext_lazy(
            'This value must have, no more than, %(max)s decimal place.',
            'This value must have, no more than, %(max)s decimal places.',
            'max'  # type: ignore
        ),
        'max_whole_digits': ngettext_lazy(
            (
                'This value must have, no more than, %(max)s digit before the '
                'decimal point.'
            ),
            (
                'This value must have, no more than, %(max)s digits before '
                'the decimal point.'
            ),
            'max'  # type: ignore
        ),
    }


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class EmailValidator(_EmailValidator):
    message = gettext_lazy('This email address must be valid.')


# noinspection PyUnusedClass,PyUnusedName
@deconstructible
class TimeZoneValidator:
    message = gettext_lazy(
        "This value must must be a valid timezone name. A valid timezone "
        "name can be found at https://tinyurl.com/h6edjuq in the "
        "'TZ database name' column."
    )
    code = 'invalid_timezone'

    def __init__(
            self,
            message: Optional[str] = None,
            code: Optional[str] = None,
    ) -> None:
        if hasattr(message, 'encode') and message:
            message = cast(str, message)
            self.message = message
        if hasattr(code, 'encode') and code:
            code = cast(str, code)
            self.code = code

    def __call__(self, value: Any) -> None:
        if hasattr(value, 'encode'):
            value = cast(str, value)
            if is_tz_name(value) is True:
                return
        raise ValidationError(self.message, code=self.code)
