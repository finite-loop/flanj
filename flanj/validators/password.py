import re
import gzip
from os import PathLike
from string import (
    ascii_lowercase,
    ascii_uppercase,
    digits,
    punctuation,
    whitespace,
)
from typing import (
    Optional,
    Set,
    Tuple,
)


from difflib import SequenceMatcher
from django.contrib.auth.password_validation import (
    CommonPasswordValidator as CommonPwValidator
)
from django.core.exceptions import (
    FieldDoesNotExist,
    ValidationError,
)
from django.utils.translation import (
    gettext,
    ngettext,
    ngettext_lazy
)


from django.contrib.auth.models import AbstractBaseUser


DEFAULT_PASSWORD_LIST_PATH = CommonPwValidator.DEFAULT_PASSWORD_LIST_PATH


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordMinimumLengthValidator:

    """Validate whether the password is of a minimum length."""
    LENGTH_MESSAGE_SINGULAR = (
        'Password currently has %(length)s character.'
    )
    LENGTH_MESSAGE_PLURAL = (
        'Password currently has %(length)s characters.'
    )

    ERROR_MESSAGE_SINGULAR = (
        'Password is too short; it must contain at least %(min_length)d '
        'character.  %(length_message)s'
    )
    ERROR_MESSAGE_PLURAL = (
        'Password is too short; it must contain at least %(min_length)d '
        'characters.'
    )
    HELP_MESSAGE_SINGULAR = (
        'Password must contain at least %(min_length)d character.'
    )
    HELP_MESSAGE_PLURAL = (
        'Password must contain at least %(min_length)d characters.'
    )

    def __init__(
            self,
            min_length: int = 8,
            length_message_singular: str = LENGTH_MESSAGE_SINGULAR,
            length_message_plural: str = LENGTH_MESSAGE_PLURAL,
            error_message_singular: str = ERROR_MESSAGE_SINGULAR,
            error_message_plural: str = ERROR_MESSAGE_PLURAL,
            help_message_singular: str = HELP_MESSAGE_SINGULAR,
            help_message_plural: str = HELP_MESSAGE_PLURAL,
    ) -> None:
        self.min_length: int = min_length
        self.length_message_singular: str = length_message_singular
        self.length_message_plural: str = length_message_plural
        self.error_message_singular: str = error_message_singular
        self.error_message_plural: str = error_message_plural
        self.help_message_singular: str = help_message_singular
        self.help_message_plural: str = help_message_plural

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        password_length: int = len(password)
        if len(password) < self.min_length:
            length_message = ngettext_lazy(
                self.length_message_singular,
                self.length_message_plural,
                password_length
            )
            raise ValidationError(
                ngettext(
                    self.error_message_singular,
                    self.error_message_plural,
                    self.min_length
                ),
                code='password_too_short',
                params={
                    'min_length': self.min_length,
                    'length_message': length_message,
                },
            )

    def get_help_text(self) -> str:
        return ngettext(
            self.help_message_singular,
            self.help_message_plural,
            self.min_length
        ) % {'min_length': self.min_length}


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordMaximumLengthValidator:

    """Validate whether the password length is of less than or equal
    to a maximum length.
    """

    LENGTH_MESSAGE_SINGULAR = (
        'Password currently has %(length)s character.'
    )
    LENGTH_MESSAGE_PLURAL = (
        'Password currently has %(length)s characters.'
    )

    ERROR_MESSAGE_SINGULAR = (
        'Password is too long; it must contain, at most, %(max_length)d '
        'character.  %(length_message)s'
    )
    ERROR_MESSAGE_PLURAL = (
        'Password is too long; it must contain, at most, %(max_length)d '
        'characters.'
    )
    HELP_MESSAGE_SINGULAR = (
        'Password must contain, at most, %(max_length)d character.'
    )
    HELP_MESSAGE_PLURAL = (
        'Password must contain, at most, %(max_length)d characters.'
    )

    def __init__(
            self,
            max_length: int = 150,
            length_message_singular: str = LENGTH_MESSAGE_SINGULAR,
            length_message_plural: str = LENGTH_MESSAGE_PLURAL,
            error_message_singular: str = ERROR_MESSAGE_SINGULAR,
            error_message_plural: str = ERROR_MESSAGE_PLURAL,
            help_message_singular: str = HELP_MESSAGE_SINGULAR,
            help_message_plural: str = HELP_MESSAGE_PLURAL,
    ) -> None:
        self.max_length: int = max_length
        self.length_message_singular: str = length_message_singular
        self.length_message_plural: str = length_message_plural
        self.error_message_singular: str = error_message_singular
        self.error_message_plural: str = error_message_plural
        self.help_message_singular: str = help_message_singular
        self.help_message_plural: str = help_message_plural

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        password_length: int = len(password)
        if len(password) > self.max_length:
            length_message = ngettext_lazy(
                self.length_message_singular,
                self.length_message_plural,
                password_length
            )
            raise ValidationError(
                ngettext(
                    self.error_message_singular,
                    self.error_message_plural,
                    self.max_length
                ),
                code='password_too_short',
                params={
                    'max_length': self.max_length,
                    'length_message': length_message,
                },
            )

    def get_help_text(self) -> str:
        return ngettext(
            self.help_message_singular,
            self.help_message_plural,
            self.max_length
        ) % {'max_length': self.max_length}


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordUserAttributeSimilarityValidator:

    """Validate whether the password is sufficiently different from the
    user's attributes.

    If no specific attributes are provided, look at a sensible list of
    defaults. Attributes that don't exist are ignored. Comparison is made to
    not only the full attribute value, but also its components, so that, for
    example, a password is validated against either part of an email address,
    as well as the full address.
    """
    DEFAULT_USER_ATTRIBUTES: Tuple[str, ...] = (
        'username',
        'first_name',
        'last_name',
        'email'
    )
    ERROR_MESSAGE = (
        'Password is too similar to the %(verbose_name)s associated '
        'with this account.'
    )
    HELP_MESSAGE = (
        'Password cannot be too similar to other personal information.'
    )

    def __init__(
            self,
            user_attributes: Tuple[str, ...] = DEFAULT_USER_ATTRIBUTES,
            max_similarity: float = 0.7,
            error_message: str = ERROR_MESSAGE,
            help_message: str = HELP_MESSAGE
    ) -> None:
        self.user_attributes: Tuple[str, ...] = user_attributes
        self.max_similarity: float = max_similarity
        self.error_message: str = error_message
        self.help_message: str = help_message

    # noinspection PyUnresolvedReferences,PyProtectedMember
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        if not user:
            return

        for attribute_name in self.user_attributes:
            value = getattr(user, attribute_name, None)
            if not value or not isinstance(value, str):
                continue  # pragma: no cover
            value_parts = re.split(r'\W+', value) + [value]
            for value_part in value_parts:
                matcher = SequenceMatcher(
                    a=password.lower(),
                    b=value_part.lower()
                )
                if matcher.quick_ratio() >= self.max_similarity:
                    try:
                        verbose_name = str(
                            user._meta.get_field(attribute_name).verbose_name
                        )
                    except FieldDoesNotExist:           # pragma: no cover
                        verbose_name = attribute_name   # pragma: no cover
                    raise ValidationError(
                        gettext(self.error_message),
                        code='password_too_similar',
                        params={'verbose_name': verbose_name},
                    )

    def get_help_text(self) -> str:
        return gettext(self.help_message)


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordCommonValidator:
    """Validate whether the password is a common password.

    The password is rejected if it occurs in a provided list of passwords,
    which may be gzipped. The list Django ships with contains 20000 common
    passwords (lowercased and deduplicated), created by Royce Williams:
    https://gist.github.com/roycewilliams/281ce539915a947a23db17137d91aeb7
    The password list must be lowercased to match the comparison in
    validate().
    """
    ERROR_MESSAGE = 'Password is too common.'
    HELP_MESSAGE = 'Password cannot be a commonly used password.'

    def __init__(
            self,
            password_list_path: PathLike = DEFAULT_PASSWORD_LIST_PATH,
            error_message: str = ERROR_MESSAGE,
            help_message: str = HELP_MESSAGE
    ) -> None:
        try:
            with gzip.open(str(password_list_path)) as f:
                common_passwords_lines = f.read().decode().splitlines()
        except IOError:  # pragma: no cover
            with open(str(password_list_path)) as f:       # pragma: no cover
                common_passwords_lines = f.readlines()     # pragma: no cover

        self.passwords: Set[str] = {p.strip() for p in common_passwords_lines}
        self.error_message: str = error_message
        self.help_message: str = help_message

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        if password.lower().strip() in self.passwords:
            raise ValidationError(
                gettext(self.error_message),
                code='password_too_common',
            )

    def get_help_text(self) -> str:
        return gettext(self.help_message)


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordDifferentValidator:

    """Validate whether the password is different than the current password. """

    ERROR_MESSAGE = 'Password cannot be the same as the current password.'
    HELP_MESSAGE = 'Password cannot be the same as the current password.'

    def __init__(
            self,
            error_message: str = ERROR_MESSAGE,
            help_message: str = HELP_MESSAGE
    ) -> None:
        self.error_message: str = error_message
        self.help_message: str = help_message

    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        if user is not None:
            if user.check_password(password):
                raise ValidationError(
                    gettext(self.error_message)
                )

    def get_help_text(self) -> str:
        return gettext(self.help_message)


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordNumericValidator:

    """Validate whether the password is alphanumeric."""

    ERROR_MESSAGE = 'Password is entirely numeric.'
    HELP_MESSAGE = 'Password cannot be entirely numeric.'

    def __init__(
            self,
            error_message: str = ERROR_MESSAGE,
            help_message: str = HELP_MESSAGE
    ) -> None:
        self.error_message: str = error_message
        self.help_message: str = help_message

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        if password.isdigit():
            raise ValidationError(
                gettext(self.error_message),
                code='password_entirely_numeric',
            )

    def get_help_text(self) -> str:
        return gettext(self.help_message)


# noinspection PyUnusedFunction,PyUnusedClass
class _CharacterValidator:

    ERROR_MESSAGE_SINGULAR = ''
    ERROR_MESSAGE_PLURAL = ''
    HELP_MESSAGE_SINGULAR = ''
    HELP_MESSAGE_PLURAL = ''

    def __init__(
            self,
            min_chars: int = 1,
            error_message_singular: Optional[str] = None,
            error_message_plural: Optional[str] = None,
            help_message_singular: Optional[str]= None,
            help_message_plural: Optional[str] = None

    ) -> None:
        self.min_chars: int = min_chars
        if (hasattr(error_message_singular, 'capitalize') is False
                or not error_message_singular):
            error_message_singular = self.ERROR_MESSAGE_SINGULAR
        self.error_message_singular: str = error_message_singular

        if (hasattr(error_message_plural, 'capitalize') is False
                or not error_message_plural):
            error_message_plural = self.ERROR_MESSAGE_PLURAL
        self.error_message_plural: str = error_message_plural

        if (hasattr(help_message_singular, 'capitalize') is False
                or not help_message_singular):
            help_message_singular = self.HELP_MESSAGE_SINGULAR
        self.help_message_singular: str = help_message_singular

        if (hasattr(help_message_plural, 'capitalize') is False
                or not help_message_plural):
            help_message_plural = self.HELP_MESSAGE_PLURAL
        self.help_message_plural: str = help_message_plural

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        raise NotImplementedError()  # pragma: no cover

    def get_help_text(self) -> str:
        return ngettext(
            self.help_message_singular,
            self.help_message_plural,
            self.min_chars
        ) % {'min_chars': self.min_chars}


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordUppercaseCharacterValidator(_CharacterValidator):

    """Validate whether the password contains a given number of
    uppercase characters.
    """

    ERROR_MESSAGE_SINGULAR = (
        'Password is missing, at least, %(min_chars)d uppercase '
        'character. (e.g. {})'.format(ascii_uppercase)
    )
    ERROR_MESSAGE_PLURAL = (
        'Password is missing, at least, %(min_chars)d uppercase '
        'characters. (e.g. {})'.format(ascii_uppercase)
    )
    HELP_MESSAGE_SINGULAR = (
        'Password must have, at least, %(min_chars)d uppercase character. '
        '(e.g. {})'.format(ascii_uppercase)
    )
    HELP_MESSAGE_PLURAL = (
        'Password must have, at least, %(min_chars)d uppercase characters. '
        '(e.g. {})'.format(ascii_uppercase)

    )

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        count: int = 0
        for c in password:
            if c.isupper():
                count += 1
                if count >= self.min_chars:
                    break

        if count < self.min_chars:

            raise ValidationError(
                ngettext(
                    self.error_message_singular,
                    self.error_message_plural,
                    self.min_chars
                ),
                code='password_missing_uppercase_character',
                params={
                    'min_chars': self.min_chars,
                },
            )


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordLowercaseCharacterValidator(_CharacterValidator):

    """Validate whether the password contains a given number of
    lowercase characters.
    """

    ERROR_MESSAGE_SINGULAR = (
        'Password is missing, at least, %(min_chars)d lowercase '
        'character. (e.g. {})'.format(ascii_lowercase)
    )
    ERROR_MESSAGE_PLURAL = (
        'Password is missing, at least, %(min_chars)d lowercase '
        'characters. (e.g. {})'.format(ascii_lowercase)
    )
    HELP_MESSAGE_SINGULAR = (
        'Password must have, at least, %(min_chars)d lowercase character. '
        '(e.g. {})'.format(ascii_lowercase)
    )
    HELP_MESSAGE_PLURAL = (
        'Password must have, at least, %(min_chars)d lowercase characters. '
        '(e.g. {})'.format(ascii_lowercase)

    )

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        count: int = 0
        for c in password:
            if c.islower():
                count += 1
                if count >= self.min_chars:
                    break

        if count < self.min_chars:
            raise ValidationError(
                ngettext(
                    self.error_message_singular,
                    self.error_message_plural,
                    self.min_chars
                ),
                code='password_missing_lowercase_character',
                params={
                    'min_chars': self.min_chars,
                },
            )


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordNumericCharacterValidator(_CharacterValidator):

    """Validate whether the password contains a given number of
    numeric characters.
    """

    ERROR_MESSAGE_SINGULAR = (
        'Password is missing, at least, %(min_chars)d numeric '
        'character. (e.g. {})'.format(digits)
    )
    ERROR_MESSAGE_PLURAL = (
        'Password is missing, at least, %(min_chars)d numeric '
        'characters. (e.g. {})'.format(digits)
    )
    HELP_MESSAGE_SINGULAR = (
        'Password must have, at least, %(min_chars)d numeric character. '
        '(e.g. {})'.format(digits)
    )
    HELP_MESSAGE_PLURAL = (
        'Password must have, at least, %(min_chars)d numeric characters. '
        '(e.g. {})'.format(digits)
    )

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        count: int = 0
        for c in password:
            if c.isdigit():
                count += 1
                if count >= self.min_chars:
                    break

        if count < self.min_chars:
            raise ValidationError(
                ngettext(
                    self.error_message_singular,
                    self.error_message_plural,
                    self.min_chars
                ),
                code='password_missing_lowercase_character',
                params={
                    'min_chars': self.min_chars,
                },
            )


# noinspection PyUnusedFunction,PyUnusedClass
class PasswordSpecialCharacterValidator(_CharacterValidator):

    """Validate whether the password contains a given number of
    special characters.
    """

    ERROR_MESSAGE_SINGULAR = (
        'Password is missing, at least, %(min_chars)d special '
        'character. (e.g. {})'.format(punctuation.replace('%', '%%'))
    )
    ERROR_MESSAGE_PLURAL = (
        'Password is missing, at least, %(min_chars)d special '
        'characters. (e.g. {})'.format(punctuation.replace('%', '%%'))
    )
    HELP_MESSAGE_SINGULAR = (
        'Password must have, at least, %(min_chars)d special character. '
        '(e.g. {})'.format(punctuation.replace('%', '%%'))
    )
    HELP_MESSAGE_PLURAL = (
        'Password must have, at least, %(min_chars)d special characters. '
        '(e.g. {})'.format(punctuation.replace('%', '%%'))
    )

    # noinspection PyUnusedLocal
    def validate(
            self,
            password: str,
            user: Optional[AbstractBaseUser] = None
    ) -> None:
        count: int = 0
        for c in password:
            if (c.isdigit() is False and
                    c.islower() is False and
                    c.isupper() is False and
                    c not in whitespace):
                count += 1
                if count >= self.min_chars:
                    break

        if count < self.min_chars:
            raise ValidationError(
                ngettext(
                    self.error_message_singular,
                    self.error_message_plural,
                    self.min_chars
                ),
                code='password_missing_special_character',
                params={
                    'min_chars': self.min_chars,
                },
            )

