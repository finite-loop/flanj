from functools import cached_property
from io import StringIO
from types import SimpleNamespace
from typing import (
    Any,
    Dict,
    Optional,
    Tuple,
    Union,
)
from django.db import (
    connection,
    connections,
    DEFAULT_DB_ALIAS,
)

from django.core.management.base import (
    BaseCommand as _BaseCommand,
    CommandParser,
    OutputWrapper,
)
from django.core.management.color import no_style
from django.db.utils import ProgrammingError
from django.db.models.query import QuerySet
from django.db.migrations.recorder import MigrationRecorder

from .prnt import (
    FormatText,
)
from .fsys import Fsys
from .prnt import (
    Prnt,
    PrntObj,
    Txt,
    Spin,
)


class BaseCommand(_BaseCommand):

    ADD_DRY_RUN_ARGUMENT = False

    def __init__(
            self,
            stdout: Optional[Union[StringIO]] = None,
            stderr: Optional[Union[StringIO]] = None,
            no_color: bool = False
    ) -> None:
        # Set default values before calling super
        self._dry_run: bool = False
        self._verbose: bool = False
        self._verbosity: int = 1
        self._no_color: bool = True
        self.cmd_vals: SimpleNamespace = SimpleNamespace()
        super().__init__(
            stdout=stdout,
            stderr=stderr,
            no_color=no_color
        )
        if hasattr(self.stdout, 'ending'):
            self.stdout.ending = ''
        Spin.STDOUT = self.stdout
        if hasattr(self.stderr, 'ending'):
            self.stderr.ending = ''
        Spin.STDERR = self.stderr

    @cached_property
    def prnt(self) -> Prnt:
        if self.stdout:
            return Prnt(file=self.stdout, no_color=self.no_color)
        return Prnt(no_color=self.no_color)

    # noinspection PyUnusedFunction
    @cached_property
    def prnt_obj(self) -> PrntObj:
        if self.stdout:
            return PrntObj(file=self.stdout, no_color=self.no_color)
        return PrntObj(no_color=self.no_color)

    @cached_property
    def dry_run(self) -> bool:
        """True means do NOT execute anything."""
        return self._dry_run

    @cached_property
    def verbosity(self) -> int:
        """The verbosity value."""
        return self._verbosity

    # noinspection PyUnusedFunction
    @cached_property
    def verbose(self) -> bool:
        """True if the verbosity is > 1."""
        return self._verbosity > 1

    @cached_property
    def no_color(self) -> bool:
        """True means don't show any screen output in color."""
        return self._no_color

    # noinspection PyUnusedFunction
    @cached_property
    def fsys(self) -> Fsys:
        """Object used for managing files."""
        return Fsys(
            verbosity=self.verbosity,
            dry_run=self.dry_run,
            no_color=self.no_color
        )

    @cached_property
    def migration_queryset(self) -> QuerySet:
        """The migration queryset."""
        obj = MigrationRecorder(connection)
        return obj.migration_qs

    # noinspection PyUnusedFunction
    def has_migrations(self) -> bool:
        """Return True if any migrations have been applied."""
        try:
            num = len(self.migration_queryset.all())
        except ProgrammingError:
            return False
        else:
            if num > 0:
                return True
            else:
                return False

    def create_parser(
            self,
            prog_name: str,
            subcommand: str,
            **kwargs: Any,
    ) -> CommandParser:
        parser = super().create_parser(prog_name, subcommand, **kwargs)
        if self.ADD_DRY_RUN_ARGUMENT is True:
            parser.add_argument(
                '--dry-run',
                action='store_true',
                dest='dry_run',
                default=False,
                help=(
                    'Run this command but do nothing but echo out '
                    'information.'
                )
            )

        return parser

    def set_options(self, options: Dict[str, Any]) -> None:
        pass

    def set_args(self, args: Tuple[Any]) -> None:
        pass

    def prepare(
            self,
            args: Tuple[Any],
            options: Dict[str, Any],
    ) -> None:
        pass

    def _set_options(self, options: Dict[str, Any]) -> None:
        """Used to set various properties on this object."""
        self._verbosity = options.get('verbosity', 1)
        self._dry_run = options.get('dry_run', False)
        self._no_color = options.get('no_color', False)
        FormatText.no_color = self._no_color
        self.set_options(options)

    def execute(self, *args: Any, **options: Any) -> Any:
        """
        Try to execute this command, performing system checks if needed (as
        controlled by the ``requires_system_checks`` attribute, except if
        force-skipped).
        """
        if options['no_color']:
            self.style = no_style()
            self.stderr.style_func = None
        if options.get('stdout'):
            self.stdout = OutputWrapper(options['stdout'])
        if options.get('stderr'):
            self.stderr = OutputWrapper(
                options['stderr'],
                self.stderr.style_func
            )

        if self.requires_system_checks and not options.get('skip_checks'):
            self.check()
        if self.requires_migrations_checks:
            self.check_migrations()

        self._set_options(options)
        self.set_args(args)  # type: ignore
        self.prepare(args, options)  # type: ignore

        output = self.handle(*args, **options)
        if output:
            if self.output_transaction:
                conn = connections[options.get(
                    'database',
                    DEFAULT_DB_ALIAS)
                ]
                output = '%s\n%s\n%s' % (
                    self.style.SQL_KEYWORD(
                        conn.ops.start_transaction_sql()
                    ),
                    output,
                    self.style.SQL_KEYWORD(
                        conn.ops.end_transaction_sql()
                    ),
                )
            self.stdout.write(output)
        return output
