from typing import (
    Any,
    Dict,
    Optional
)
from flanj.utils.strs import contains_html
from .format_text import FormatText


class Txt:

    def __init__(
            self,
            no_color: Optional[bool] = None,
            style: Optional[Dict[str, str]] = None,
    ) -> None:
        self.fmt: FormatText = FormatText(
            no_color=no_color,
            style=style
        )

    def format_heading(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            **kwargs_: Any
    ) -> str:
        if contains_html(txt_) is False:
            txt_ = '<b><u><info>%s</info></u></b>' % txt_
        return self.fmt(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )

    def format_heading_results(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            **kwargs_: Any
    ) -> str:
        if contains_html(txt_) is False:
            txt_ = '<b><info>%s</info></b>\n' % txt_
        return self.fmt(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )

    def format_finished(
            self,
            *args_: Any,
            txt_: Optional[str] = None,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            **kwargs_: Any
    ) -> str:
        head = '{ICON_STAR} <green>Finished</green>'
        if txt_ is None:
            txt_ = ''
        if txt_:
            txt_ = '%s\n%s' % (head, txt_)
        else:
            txt_ = head

        if indent_ is None:
            indent_ = 0

        if subsequent_indent_ is None:
            subsequent_indent_ = indent_ + 3

        return self.fmt(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )

    def format_success(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            **kwargs_: Any
    ) -> str:
        if indent_ is None:
            indent_ = 0

        txt_ = '{ICON_SUCCESS}  %s' % txt_
        if subsequent_indent_ is None:
            subsequent_indent_ = indent_ + 2

        return self.fmt(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )

    def format_warning(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            **kwargs_: Any,
    ) -> str:
        if indent_ is None:
            indent_ = 0

        if contains_html(txt_) is False:
            txt_ = '<warn>%s</warn>' % txt_
        txt_ = '{ICON_WARN} %s' % txt_
        if subsequent_indent_ is None:
            subsequent_indent_ = indent_ + 3

        return self.fmt(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )

    def format_info(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            **kwargs_: Any,
    ) -> str:
        if indent_ is None:
            indent_ = 0

        if contains_html(txt_) is False:
            txt_ = '<info>%s</info>' % txt_
        txt_ = '{ICON_INFO} %s' % txt_
        if subsequent_indent_ is None:
            subsequent_indent_ = indent_ + 3

        return self.fmt(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )

    def format_error(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            **kwargs_: Any,
    ) -> str:
        if indent_ is None:
            indent_ = 0

        if contains_html(txt_) is False:
            txt_ = '<error>%s</error>' % txt_
        txt_ = '{ICON_ERROR} %s' % txt_
        if subsequent_indent_ is None:
            subsequent_indent_ = indent_ + 3

        return self.fmt(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
