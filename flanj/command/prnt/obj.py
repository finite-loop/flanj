import sys
from datetime import datetime
from functools import singledispatch
from types import SimpleNamespace
from collections import OrderedDict
from collections.abc import (
    Mapping,
    MutableSequence,
    Sequence,
    Set,
)
from typing import (
    Any,
    Dict,
    List,
    Optional,
    TextIO,
    Union,
    cast,
)
from io import TextIOBase
from .format_text import FormatText

from django.core.management.base import OutputWrapper


EMPTY = '__notused__'


def build_keys(
        obj: Any,
        hide_keys: Optional[List[str]]
) -> List[str]:
    sort = True
    if isinstance(obj, OrderedDict):
        sort = False
    if hasattr(obj, '_asdict') and hasattr(obj, '_fields'):
        sort = False
        # noinspection PyProtectedMember
        obj = obj._asdict()

    if sort is True:
        keys = list(sorted(obj.keys()))
    else:
        keys = list(obj.keys())
    if hide_keys:
        keys = list(filter(lambda a: a not in hide_keys, keys))  # type: ignore
    try:
        index = keys.index('name')
    except ValueError:
        pass
    else:
        keys.pop(index)
        keys.insert(0, 'name')

    try:
        index = keys.index('id')
    except ValueError:
        pass
    else:
        keys.pop(index)
        keys.insert(0, 'id')
    return keys


def _append(
        lines: List[str],
        indent: int,
        fmt: FormatText,
        key: Optional[Union[str, int]] = None,
        val: Any = EMPTY,
        torepr: bool = False,
        comma: bool = False,
        key_val_sep: str = ': ',
        key_color: Optional[str] = None,
        val_color: Optional[str] = None
) -> None:
    if key is None:
        key = ''
    else:
        if key_color:
            key = fmt(
                '<b><{color}>{value}</{color}></b>',
                color=key_color,
                value=key,
                end_='',
                to_screen_=False
            )

        if val is EMPTY:
            if comma is True:
                key = '%s,' % key
            else:
                key = '%s' % key
        else:
            if key:
                key = '%s%s' % (key, key_val_sep)

    if val is EMPTY:
        val = ''
    else:
        if torepr is True:
            val = '%r' % val
        if val_color:
            val = fmt(
                '<{color}>{value}</{color}>',
                color=val_color,
                value=val,
                end_='',
                to_screen_=False
            )
        if comma is True:
            val = '%s,' % val

    out = '{: <{indent}}{key}{val}'.format(
        '',
        indent=indent,
        key=key,
        val=val
    )
    lines.append(out)


@singledispatch
def _to_lines(
        obj: Any,
        lines: List[str],
        indent: int,
        fmt: FormatText,
        key: Optional[str] = None,
        comma: bool = False,
        hide_keys: Optional[List[str]] = None,
        key_color: Optional[str] = None,
        val_color: Optional[str] = None
) -> None:
    _append(
        lines,
        indent,
        fmt,
        key=key,
        val=obj,
        torepr=True,
        comma=comma,
        key_color=key_color,
        val_color=val_color
    )


# noinspection PyProtectedMember
@_to_lines.register(Sequence)
def _to_lines_sequence(
        obj: Sequence,
        lines: List[str],
        indent: int,
        fmt: FormatText,
        key: Optional[Union[str, int]] = None,
        comma: bool = False,
        hide_keys: Optional[List[str]] = None,
        key_color: Optional[str] = None,
        val_color: Optional[str] = None
) -> None:
    # Process strings and bytes
    if hasattr(obj, 'capitalize'):
        _append(
            lines,
            indent,
            fmt,
            key=key,
            val=obj,
            torepr=True,
            comma=comma,
            key_color=key_color,
            val_color=val_color
        )
        return

    if hasattr(obj, '_asdict') is True:
        sep = '.'
    elif isinstance(obj, MutableSequence):
        sep = '['
    else:
        sep = '('

    # Process empty Sequences
    if not obj:
        if sep == '.':
            v = 'NamedTuple()'
        else:
            v = '%s()' % type(obj).__name__

        _append(
            lines,
            indent,
            fmt,
            key=key,
            val=v,
            comma=comma,
            key_color=key_color,
            val_color=val_color
        )
        return

    # Process NamedTuple
    if sep == '.':
        keys = build_keys(obj, hide_keys=hide_keys)
        if not keys:
            # Process empty NamedTuple after keys were filtered
            if len(keys) == 0:
                _append(
                    lines,
                    indent,
                    fmt,
                    key=key,
                    val='NamedTuple()',
                    comma=comma,
                    key_color=key_color,
                    val_color=val_color
                )
                return

        new_indent = indent
        if key:
            _append(
                lines,
                indent,
                fmt,
                key=key,
                val='NamedTuple(',
                comma=False,
                key_color=key_color,
                val_color=val_color
            )
        else:
            _append(
                lines,
                indent,
                fmt,
                key=key,
                val='(',
                comma=False,
                key_color=key_color,
                val_color=val_color
            )
        new_indent = indent + 2

        for k in keys:
            if key:
                new_key = '.%s' % k
            else:
                new_key = k
            v = getattr(obj, k)
            _to_lines(
                v,
                lines,
                new_indent,
                fmt,
                key=new_key,
                comma=False,
                hide_keys=hide_keys,
                key_color=key_color,
                val_color=val_color
            )
        _append(
            lines,
            indent,
            fmt,
            val=')',
            comma=comma,
            key_color=key_color,
            val_color=val_color
        )

    else:
        if isinstance(key, int):
            key = None
        # Process list and tuple
        _append(
            lines,
            indent,
            fmt,
            key=key,
            val=sep,
            comma=False,
            key_color=key_color,
            val_color=val_color
        )
        if sep == '[':
            sep = ']'
        else:
            sep = ')'

        last = len(obj) - 1
        new_indent = indent + 2
        new_comma = True

        for x, v in enumerate(obj):
            if x == last:
                new_comma = False
            _to_lines(
                v,
                lines,
                new_indent,
                fmt,
                key=None,
                comma=new_comma,
                hide_keys=hide_keys,
                key_color=key_color,
                val_color=val_color
            )

        _append(
            lines,
            indent,
            fmt,
            key=None,
            val=sep,
            comma=comma,
            key_color=key_color,
            val_color=val_color
        )


@_to_lines.register(Set)
def _to_lines_set(
        obj: Set,
        lines: List[str],
        indent: int,
        fmt: FormatText,
        key: Union[str, int, None] = None,
        comma: bool = False,
        hide_keys: Optional[List[str]] = None,
        key_color: Optional[str] = None,
        val_color: Optional[str] = None
) -> None:
    if isinstance(key, int):
        key = None
    key = cast(Optional[str], key)
    name = '%s(' % obj.__class__.__name__
    if len(obj) == 0:
        _append(
            lines,
            indent,
            fmt,
            key=key,
            val='%s)' % name,
            comma=comma,
            key_color=key_color,
            val_color=val_color
        )
    else:
        last = len(obj) - 1
        _append(
            lines,
            indent,
            fmt,
            key=key,
            val='%s(' % name,
            comma=False,
            key_color=key_color,
            val_color=val_color
        )
        new_indent = indent + 2
        new_comma = True
        for x, val in enumerate(sorted(obj)):
            if x == last:
                new_comma = False
            _to_lines(
                val,
                lines,
                new_indent,
                fmt,
                key=None,
                comma=new_comma,
                hide_keys=hide_keys,
                key_color=key_color,
                val_color=val_color
            )
        _append(
            lines,
            indent,
            fmt,
            key=None,
            val='))',
            comma=comma,
            key_color=key_color,
            val_color=val_color
        )


# noinspection PyUnusedLocal
@_to_lines.register(Mapping)
def _to_lines_mapping(
        obj: Mapping,
        lines: List[str],
        indent: int,
        fmt: FormatText,
        key: Union[str, int, None] = None,
        comma: bool = False,
        parent_type: Optional[str] = None,
        hide_keys: Optional[List[str]] = None,
        key_color: Optional[str] = None,
        val_color: Optional[str] = None
) -> None:
    if isinstance(key, int):
        key = None
    keys = build_keys(obj, hide_keys=hide_keys)
    # Handle an empty obj
    if len(keys) == 0:
        val = '%s()' % obj.__class__.__name__
        _append(
            lines,
            indent,
            fmt,
            key=key,
            val=val,
            comma=comma,
            key_color=key_color,
            val_color=val_color
        )
        return

    # Handle everything else
    last = len(keys) - 1
    sep = ':'

    new_indent = indent

    if key or indent > 0:
        _append(
            lines,
            indent,
            fmt,
            key=key,
            val='{',
            comma=False,
            key_color=key_color,
            val_color=val_color
        )
        new_indent = indent + 2

    for x, k in enumerate(keys):
        if sep == '.':
            new_key = '%s%s%s' % (key, sep, k)
        else:
            if indent == 0:
                new_key = str(k)
            else:
                new_key = repr(k)

        if x < last:
            new_comma = True
        else:
            new_comma = False

        v = obj[k]

        _to_lines(
            v,
            lines,
            new_indent,
            fmt,
            key=new_key,
            comma=new_comma,
            hide_keys=hide_keys,
            key_color=key_color,
            val_color=val_color
        )
    if key or indent > 0:
        _append(
            lines,
            indent,
            fmt,
            key=None,
            val='}',
            comma=comma,
            key_color=key_color,
            val_color=val_color
        )

@_to_lines.register(datetime)
def _to_lines_datetime(
        obj: Any,
        lines: List[str],
        indent: int,
        fmt: FormatText,
        key: Union[str, int, None] = None,
        comma: bool = False,
        hide_keys: Optional[List[str]] = None,
        key_color: Optional[str] = None,
        val_color: Optional[str] = None
) -> None:
    if isinstance(key, int):
        key = None
    name = obj.strftime('%Y-%m-%d %H:%M:%S %Z')
    _append(
        lines,
        indent,
        fmt,
        key=key,
        val=name,
        torepr=True,
        comma=comma,
        key_color=key_color,
        val_color=val_color
    )


@_to_lines.register(SimpleNamespace)
def _to_lines_simplenamespace(
        obj: SimpleNamespace,
        lines: List[str],
        indent: int,
        fmt: FormatText,
        key: Optional[str] = None,
        comma: bool = False,
        hide_keys: Optional[List[str]] = None,
        key_color: Optional[str] = None,
        val_color: Optional[str] = None
) -> None:
    keys = build_keys(obj.__dict__, hide_keys=hide_keys)
    if len(keys) == 0:
        name = '%s()' % obj.__class__.__name__
        _append(
            lines,
            indent,
            fmt,
            key=key,
            val=name,
            comma=comma,
            key_color=key_color,
            val_color=val_color,
        )
        return
    for x, k in enumerate(sorted(obj.__dict__.keys())):
        if hide_keys is not None and k in hide_keys:
            continue
        if key:
            new_key = '%s.%s' % (key, k)
        else:
            new_key = k
        v = obj.__dict__[k]
        _to_lines(
            v,
            lines,
            indent,
            fmt,
            key=new_key,
            comma=False,
            hide_keys=hide_keys,
            key_color=key_color,
            val_color=val_color
        )


def prnt_obj(
        obj: Any,
        hide_keys: Optional[List[str]] = None,
        key_color: str = 'cyan',
        val_color: Optional[str] = None,
        to_screen: bool = True,
        fmt: Optional[FormatText] = None,
        no_color: Optional[bool] = None,
        file: Optional[Union[TextIO, OutputWrapper]] = None
) -> str:
    if fmt is None:
        fmt = FormatText(no_color=no_color, no_wrap_override=True)
    lines: List[str] = []
    _to_lines(
        obj,
        lines,
        0,
        fmt,
        hide_keys=hide_keys,
        key_color=key_color,
        val_color=val_color
    )
    out = '\n'.join(lines)

    if to_screen is True:
        if file is None:
            file = sys.stdout
        file.write(out)
        file.flush()
    return out


class PrntObj:

    def __init__(
            self,
            no_color: Optional[bool] = None,
            style: Optional[Dict[str, str]] = None,
            file: Optional[Union[TextIO, OutputWrapper]] = None
    ) -> None:
        self.format_text: FormatText = FormatText(
            no_color=no_color,
            style=style,
            no_wrap_override=True
        )
        if file is None:
            self.file: Union[TextIO, OutputWrapper] = sys.stdout
        else:
            self.file = file

    def __call__(
            self,
            obj: Any,
            hide_keys: Optional[List[str]] = None,
            key_color: str = 'cyan',
            val_color: Optional[str] = None,
            to_screen: bool = True,
    ) -> str:
        return prnt_obj(
            obj,
            hide_keys=hide_keys,
            key_color=key_color,
            val_color=val_color,
            to_screen=to_screen,
            fmt=self.format_text,
            file=self.file
        )
