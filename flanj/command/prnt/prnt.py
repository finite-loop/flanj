import sys
from io import StringIO
from typing import (
    Any,
    Dict,
    IO,
    Optional,
    TextIO,
    Union,
    cast,
)
from django.core.management.base import OutputWrapper

from .txt import Txt


class Prnt(Txt):

    def __init__(
            self,
            no_color: Optional[bool] = None,
            style: Optional[Dict[str, str]] = None,
            file: Optional[Union[IO, OutputWrapper, StringIO, TextIO]] = None
    ) -> None:
        super().__init__(no_color=no_color, style=style)

        if file is None:
            file = sys.stdout
        if not isinstance(file, OutputWrapper):
            file = cast(StringIO, file)
            file = OutputWrapper(file)
        file = cast(OutputWrapper, file)
        self._file: OutputWrapper = file

    def _show(self, txt_: str, end_: str) -> None:
        txt_ = '%s%s' % (txt_, end_)
        self._file.write(txt_)
        self._file.flush()

    def show(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        txt_ = self.fmt(
            txt_,
            *args_,
            now_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
        self._show(txt_, end_)

    def heading(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        txt_ = self.format_heading(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
        self._show(txt_, end_)

    # noinspection PyUnusedFunction
    def heading_results(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        txt_ = self.format_heading_results(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
        self._show(txt_, end_)

    def finished(
            self,
            *args_: Any,
            txt_: Optional[str] = None,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        txt_ = self.format_finished(
            *args_,
            txt_=txt_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
        txt_ = cast(str, txt_)
        self._show(txt_, end_)

    def success(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        txt_ = self.format_success(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
        self._show(txt_, end_)

    def warning(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        txt_ = self.format_warning(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
        self._show(txt_, end_)

    def info(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            width_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        txt_ = self.format_info(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
        self._show(txt_, end_)

    def error(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        txt_ = self.format_error(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            **kwargs_
        )
        self._show(txt_, end_)

    def __call__(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            end_: str = '\n',
            **kwargs_: Any
    ) -> None:
        self.show(
            txt_,
            *args_,
            no_wrap_=no_wrap_,
            indent_=indent_,
            subsequent_indent_=subsequent_indent_,
            width_=width_,
            end_=end_,
            **kwargs_
        )
