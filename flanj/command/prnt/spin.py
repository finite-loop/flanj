import sys
import threading
import itertools
from collections.abc import Mapping
from functools import cached_property
from io import TextIOBase
from queue import Queue
from types import TracebackType
from typing import (
    Any,
    AnyStr,
    Dict,
    List,
    Optional,
    TextIO,
    Tuple,
    Type,
    Union,
    cast,
)
from .prnt import Prnt
from .txt import Txt
from .format_text import DEFAULT_CHARS
from .format_text import FormatText
from flanj.utils.objs import is_list_like
from flanj.utils.paths import (
    contract_paths_in_args,
    contract_paths_in_kwargs,
)

from django.core.management.base import OutputWrapper


class SpinWarn(Warning):
    """A generic warning when raised the message will be used as the Spin.wrn
    value.
    """
    pass


class SpinInfo(Warning):
    """A generic warning when raised the message will be used as the Spin.inf
    value.
    """
    pass


class SpinError(SystemExit):
    pass


class Spin:

    CHARS = "\u280b\u2819\u2839\u2838\u283c\u2834\u2826\u2827\u2807\u280f"

    # The following are set in flutils.command.BaseCommand
    STDOUT: Union[OutputWrapper, TextIO] = sys.stdout
    STDERR: Union[OutputWrapper, TextIO] = sys.stderr

    def __init__(
            self,
            text: str,
            *args: Any,
            suc: Optional[str] = None,
            **kwargs: Any
    ) -> None:
        self._text: str = ''
        self._text_raw: str = ''
        self._success: str = ''
        self._warning: str = ''
        self._info: str = ''
        self._error: str = ''
        self._args: Tuple[Any, ...] = ()
        self._kwargs: Dict[str, Any] = {}
        self._stop_spin: threading.Event = threading.Event()
        self._hide_spin: threading.Event = threading.Event()
        self._spin_thread: Optional[threading.Thread] = None
        self._queue: Queue = Queue()
        self.started: bool = False

        self.args: Tuple[Any, ...] = tuple(args) or ()
        self.kwargs = kwargs or {}
        self.text = text
        self.success = suc or ''

    @cached_property
    def txt(self) -> Txt:
        return Txt()

    @cached_property
    def fmt(self) -> FormatText:
        return self.txt.fmt

    # noinspection PyUnusedFunction
    @cached_property
    def prnt(self) -> Prnt:
        return Prnt(
            file=self.STDOUT
        )

    @property
    def args(self) -> Tuple[Any, ...]:
        return self._args

    @args.setter
    def args(self, value: Tuple[Any, ...]) -> None:
        if is_list_like(value) is False:
            raise AttributeError(
                "%s.args must be a list or a tuple. Got: %r" % (
                    self.__class__.__name__,
                    type(value).__name__
                )
            )
        # contract any paths that are in the args
        value = contract_paths_in_args(value, escape_html=True)
        self._args = value

    @property
    def kwargs(self) -> Dict[str, Any]:
        return self._kwargs.copy()

    @kwargs.setter
    def kwargs(self, value: Dict[str, Any]) -> None:
        if isinstance(value, Mapping) is False:
            raise AttributeError(
                "%s.kwargs must be a 'Mapping'. Got: %r" % (
                    self.__class__.__name__,
                    type(value).__name__
                )
            )
        # contract any paths that are in the kwargs
        # ... the following is pass by object.
        contract_paths_in_kwargs(value, escape_html=True)
        out = DEFAULT_CHARS.copy()
        out.update(value)
        self._kwargs = out

    @property
    def text(self) -> str:
        return self._text

    @text.setter
    def text(self, value: str) -> None:
        self.stop(finished=False)
        self._text_raw = value.format(*self.args, *self.kwargs)
        self._text = self.fmt(value, *self.args, **self.kwargs)

    @property
    def success(self) -> str:
        if not self._success:
            self._success = self.txt.format_success(self._text_raw)
        return self._success

    @success.setter
    def success(self, value: str) -> None:
        self._success = self.txt.format_success(
            value,
            *self.args,
            **self.kwargs
        )

    @property
    def error(self) -> str:
        if not self._error:
            self._error = self.txt.fmt(
                '{ICON_ERROR} <red>ERROR WHEN:</red> %s' % self._text_raw,
                )
        return self._error

    @error.setter
    def error(self, value: str) -> None:
        self._error = self.txt.format_error(
            value,
            *self.args,
            **self.kwargs
        )

    @property
    def warning(self) -> str:
        return self._warning

    @warning.setter
    def warning(self, value: str) -> None:
        self._warning = self.txt.format_warning(
            value,
            *self.args,
            *self.kwargs
        )

    @property
    def info(self) -> str:
        return self._info

    @info.setter
    def info(self, value: str) -> None:
        self._info = self.txt.format_info(
            value,
            *self.args,
            *self.kwargs
        )

    # noinspection PyUnusedFunction
    def set_percent(self, val: int) -> None:
        self.stop(finished=False)
        text = '<green>{}%</green> {}'.format(val, self._text_raw)
        self._text = self.fmt(text, *self.args, **self.kwargs)
        self.start()

    def _hide_cursor(self) -> None:
        self.STDOUT.write("\033[?25l")
        self.STDOUT.flush()

    def _clear_line(self) -> None:
        self.STDOUT.write("\033[K")

    def _show_cursor(self) -> None:
        self.STDOUT.write("\033[?25h")
        self.STDOUT.flush()

    @cached_property
    def _cycle(self) -> itertools.cycle:
        out = []
        for c in self.CHARS:
            out.append(self.fmt('<green>{}</green>', c, no_wrap_=True))
        return itertools.cycle(tuple(out))

    @staticmethod
    def _spin(
            hide_spinner_event: threading.Event,
            stop_spinner_event: threading.Event,
            cycle: itertools.cycle,
            stdout: TextIO,
            text: str
    ) -> None:
        while True:
            while hide_spinner_event.wait(0.06) is not True:
                # Move the cursor to the beginning of the line
                stdout.write('\r')
                stdout.flush()

                # Move the cursor up one line
                # See https://unix.stackexchange.com/a/26592
                # stdout.write('\033[1A')
                # stdout.flush()

                # Clear line
                stdout.write("\033[K")
                stdout.flush()

                # Write line
                stdout.write('\r%s %s' % (next(cycle), text))
                stdout.flush()

            if stop_spinner_event.is_set():
                stop_spinner_event.wait()

                # Move the cursor to the beginning of the line
                stdout.write('\r')
                stdout.flush()

                # Move the cursor up one line
                # See https://unix.stackexchange.com/a/26592
                # stdout.write('\033[1A')
                # stdout.flush()

                # Clear line
                stdout.write("\033[K")
                stdout.flush()
                break

    def start(self) -> None:
        if self.STDOUT.isatty():
            self._hide_cursor()

        self._spin_thread = threading.Thread(
            name='spinner',
            target=self._spin,
            args=(
                self._hide_spin,
                self._stop_spin,
                self._cycle,
                self.STDOUT,
                self.text,
            ),
            daemon=True
        )
        self._spin_thread.start()

    def stop(self, finished: bool = True) -> None:
        if self._spin_thread:
            self._hide_spin.set()
            self._stop_spin.set()
            while self._spin_thread.is_alive():
                try:
                    self._spin_thread.join()
                except RuntimeError:
                    pass

        self.STDOUT.write('\r')
        self._clear_line()

        if finished is True:
            if self.STDOUT.isatty():
                self._show_cursor()

    def hide(self) -> None:
        thread_is_alive = False
        if self._spin_thread:
            self._spin_thread = cast(threading.Thread, self._spin_thread)
            if self._spin_thread.is_alive():
                thread_is_alive = True

        if thread_is_alive is not True:
            return

        if not self._hide_spin.is_set():
            # set the hidden spinner flag
            self._hide_spin.set()

            # clear the current line
            self.STDOUT.write('\r')
            self._clear_line()

            # flush the stdout buffer so the current line can be
            # rewritten
            self.STDOUT.flush()

    def show(self) -> None:
        """Show the hidden spinner."""
        thread_is_alive = False
        if self._spin_thread:
            self._spin_thread = cast(threading.Thread, self._spin_thread)
            if self._spin_thread.is_alive():
                thread_is_alive = True

        if thread_is_alive is not True:
            return

        if self._hide_spin:
            self._hide_spin = cast(threading.Event, self._hide_spin)
            if self._hide_spin.is_set():
                # clear the hidden spinner flag
                self._hide_spin.clear()

                # clear the current line so the spinner is not appended to it
                self.STDOUT.write("\r")
                self._clear_line()

    def __enter__(self) -> 'Spin':
        self.start()
        return self

    def __exit__(
            self,
            exc_type: Optional[Type[TracebackType]],
            exc_value: Optional[BaseException],
            traceback: Optional[TracebackType],
    ) -> bool:
        self.stop()

        # Process spin errors.
        if isinstance(exc_value, SpinError):
            self.error = str(exc_value)
            self.STDOUT.write('\r%s\n' % self.error)
            self.STDOUT.flush()
            sys.exit(1)

        # extract SpinWarn message.
        if isinstance(exc_value, SpinWarn):
            self.warning = str(exc_value)
            self.STDOUT.write('\r%s\n' % self.warning)
            self.STDOUT.flush()
            return True

        # extract SpinInfo message
        if isinstance(exc_value, SpinInfo):
            self.info = str(exc_value)
            self.STDOUT.write('\r%s\n' % self.info)
            self.STDOUT.flush()
            return True

        if isinstance(exc_value, SystemExit):
            self.STDOUT.write('\r%s\n' % self.error)
            self.STDOUT.flush()
            msg = self.txt.format_error(str(exc_value))
            self.STDOUT.write('\r%s\n' % msg)
            self.STDOUT.flush()
            sys.exit(1)

        if isinstance(exc_value, KeyboardInterrupt):
            self.STDOUT.write('\r%s\n' % self.error)
            self.STDOUT.flush()
            self.STDOUT.write('%s\n' % self.txt.format_error(
                'Keyboard Interrupt'
            ))
            self.STDOUT.flush()
            sys.exit(1)

        # All other exceptions
        if isinstance(exc_value, Exception):
            self.STDOUT.write('\r%s\n' % self.error)
            self.STDOUT.flush()
            return False

        if self.warning:
            self.STDOUT.write('\r%s\n' % self.warning)
            self.STDOUT.flush()
            return True

        if self.info:
            self.STDOUT.write('\r%s\n' % self.info)
            self.STDOUT.flush()
            return True

        if self.success:
            self.STDOUT.write('\r%s\n' % self.success)
            self.STDOUT.flush()
        return True
