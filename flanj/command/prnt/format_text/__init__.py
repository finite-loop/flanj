import sys
from typing import (
    Any,
    Dict,
    Optional,
)
from prompt_toolkit import print_formatted_text
from prompt_toolkit.output.base import Output as PromptToolkitOutput
from prompt_toolkit.output.defaults import create_output as _get_output
from prompt_toolkit.utils import is_windows
from prompt_toolkit.styles import Style
from prompt_toolkit.formatted_text import HTML
from xml.parsers.expat import ExpatError
from flanj.utils.paths import (
    contract_paths_in_args,
    contract_paths_in_kwargs,
)
from flanj.utils.strs import strip_html
from .wrap import wrap_line



DEFAULT_STYLE: Dict[str, str] = {
    'default': 'ansidefault',
    'black': 'ansiblack',
    'red': 'ansired',
    'green': 'ansigreen',
    'yellow': 'ansiyellow',
    'blue': 'ansiblue',
    'magenta': 'ansimagenta',
    'cyan': 'ansicyan',
    'white': 'ansiwhite',
    'orange': '#F57C00',
    'grey': '#B0BEC5',
    'brown': '#8D6E63',
    'purple': '#AB47BC',
    'brightblack': 'ansibrightblack',
    'brightred': 'ansibrightred',
    'brightgreen': 'ansibrightgreen',
    'brightyellow': 'ansibrightyellow',
    'brightblue': '#42A5F5',
    'brightmagenta': 'ansibrightmagenta',
    'brightcyan': 'ansibrightcyan',
    'brightorange': '#FFB74D',
    'brightpurple': '#CE93D8',
    'done': 'ansigreen',
    'warn': 'ansiyellow',
    'info': '#42A5F5',  # bright blue
    'error': 'ansired',
}


DEFAULT_CHARS = {
    'ICON_STAR': '\u2b50\ufe0f',                # ⭐️ Star
    'ICON_ERROR': '\U0001f534',                 # 🔴 Red Circle
    'ICON_WARN': '\u26a0\ufe0f\u200b',          # ⚠️ Warning
    'ICON_INFO': '\U0001f535',                  # 🔵 Blue Circle
    'ICON_SUCCESS': '<green>\u2714</green>',    # ✔︎  Green Heavy Check Mark
    'ICON_MEGAPHONE': '\U0001f4e3',             # 📣 Megaphone
    'MAC_COMMAND': '\u2318',                    # ⌘ Mac Command
    'MAC_OPTION': '\u2325',                     # ⌥ Mac Option
    'MUSIC_FLAT': '\u266d',                     # ♭ Music Flat Sign
    'MUSIC_NATURAL': '\u266e',                  # ♮ Music Natural Sign
    'MUSIC_SHARP': '\u266f',                    # ♯ Music Sharp Sign
    'DEGREE': '\u00b0',                         # ° Degree Sign
    'DEGREE_C': '\u00b0C',                      # °C Degree Celsius
    'DEGREE_F': '\u00b0F',                      # °F Degree Fahrenheit
    'MICRO': '\u00b5',                          # µ Micro Sign
    'OHM': '\u2126',                            # Ω Ohm Sign
}


def _get_default_output() -> PromptToolkitOutput:

    output: PromptToolkitOutput = _get_output()
    if is_windows():
        from prompt_toolkit.output.windows10 import Windows10_Output
        if isinstance(output, Windows10_Output):
            from .windows10 import Windows10Output
            return Windows10Output(sys.__stdout__)

        from prompt_toolkit.output.conemu import ConEmuOutput as ConEmu_Output
        if isinstance(output, ConEmu_Output):
            from .conemu import ConEmuOutput
            return ConEmuOutput(sys.__stdout__)

        from .win32 import Win32Output
        return Win32Output(
            output.stdout,
            use_complete_width=output.use_complete_width
        )
    else:
        from .vt100 import Vt100Output
        return Vt100Output(
            output.stdout,
            output.get_size,
        )


_OUTPUT = _get_default_output()


def _format_text(
        txt_: str,
        end_: str = '',
        style_: Optional[Dict[str, str]] = None
) -> str:
    if style_ is None:
        style: Style = Style.from_dict(DEFAULT_STYLE)
    else:
        style = Style.from_dict(style_)
    output = _OUTPUT
    try:
        text = HTML(txt_)
    except ExpatError as e:
        raise ValueError(
            '%s\nGot: %r' % (str(e), txt_)
        )
    print_formatted_text(text, end=end_, style=style, output=output)
    return output.read()


class FormatText:

    NO_COLOR: bool = False
    STYLE: Dict[str, str] = {}

    def __init__(
            self,
            no_color: Optional[bool] = None,
            style: Optional[Dict[str, str]] = None,
            no_wrap_override: Optional[bool] = None
    ) -> None:
        if isinstance(no_color, bool):
            self.no_color: bool = no_color
        else:
            self.no_color = self.NO_COLOR
        self.style: Dict[str, str] = DEFAULT_STYLE.copy()

        if style is None:
            self.style.update(self.STYLE)
        else:
            self.style.update(style)
        self.no_wrap_override: Optional[bool] = no_wrap_override

    def __call__(
            self,
            txt_: str,
            *args_: Any,
            no_wrap_: bool = False,
            indent_: Optional[int] = None,
            subsequent_indent_: Optional[int] = None,
            width_: Optional[int] = None,
            **kwargs_: Any
    ) -> str:
        args_ = contract_paths_in_args(args_)
        contract_paths_in_kwargs(kwargs_)
        kwargs = DEFAULT_CHARS.copy()
        kwargs.update(kwargs_)
        txt_ = txt_.format(*args_, **kwargs)
        if isinstance(self.no_wrap_override, bool):
            no_wrap_ = self.no_wrap_override
        if no_wrap_ is False:
            out = []
            for x, line in enumerate(txt_.splitlines()):
                if subsequent_indent_ is None:
                    out.append(
                        wrap_line(
                            line,
                            indent=indent_,
                            subsequent_indent=subsequent_indent_,
                            width=width_
                        )
                    )
                else:
                    if x == 0:
                        out.append(
                            wrap_line(
                                line,
                                indent=indent_,
                                subsequent_indent=subsequent_indent_,
                                width=width_
                            )
                        )
                    else:
                        out.append(
                            wrap_line(
                                line,
                                indent=subsequent_indent_,
                                width=width_
                            )
                        )
            txt_ = '\n'.join(out)
        if self.no_color is True:
            return strip_html(txt_)
        else:
            return _format_text(txt_, style_=self.style)
