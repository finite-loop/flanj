from typing import (
    Any,
    List,
)
from prompt_toolkit.output.vt100 import Vt100_Output


# noinspection PyAbstractClass
class Vt100Output(Vt100_Output):

    def flush(self) -> None:
        return

    def read(self, *args: Any) -> str:
        out = ''.join(self._buffer)
        self._buffer: List[Any] = []
        return out
