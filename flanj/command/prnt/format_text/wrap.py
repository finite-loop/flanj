import shutil
from functools import cached_property
from html import escape
from html.parser import HTMLParser
from typing import (
    Any,
    Callable,
    List,
    Tuple,
    Optional,
    cast,
)
from urwid.util import str_util


# noinspection PyAbstractClass
class _LineLexer(HTMLParser):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        if 'convert_charrefs' not in kwargs:
            kwargs['convert_charrefs'] = True
        super().__init__(*args, **kwargs)
        self._callback: Optional[Callable[[str, str], None]] = None

    def handle_starttag(
            self,
            tag: str,
            attrs: List[Tuple[str, Optional[str]]],
    ) -> None:
        out = ['<', tag]
        for key, val in attrs:
            if val is None:
                out.append(key)
            else:
                out.append('{}="{}"'.format(key, val))
        out.append('>')
        # noinspection PyTypeChecker
        self._callback = cast(Callable[[str, str], None], self._callback)
        self._callback('ver-start', ''.join(out))

    def handle_endtag(self, tag: str) -> None:
        # noinspection PyTypeChecker
        self._callback = cast(Callable[[str, str], None], self._callback)
        if tag == 'sp':
            self._callback('space', ' ')
        else:
            self._callback('ver-end', '</{}>'.format(tag))

    def handle_startendtag(
            self, tag: str,
            attrs: List[Tuple[str, Optional[str]]]
    ) -> None:
        pass

    def handle_data(self, data: str) -> None:
        # noinspection PyTypeChecker
        self._callback = cast(Callable[[str, str], None], self._callback)
        if data.startswith(' '):
            self._callback('space', ' ')
        words = data.split()
        last = len(words) - 1
        for x, word in enumerate(words):
            self._callback('word', word)
            if x != last:
                self._callback('space', ' ')
        if data.endswith(' '):
            self._callback('space', ' ')

    def handle_comment(self, data: str) -> None:
        pass

    def handle_decl(self, decl: str) -> None:
        pass

    def handle_pi(self, data: str) -> None:
        pass

    def unknown_decl(self, data: str) -> None:
        pass

    def __call__(
            self,
            line: str,
            callback: Callable[[str, str], None]
    ) -> None:
        self._callback = callback
        self.feed(line)


_line_lexer = _LineLexer()


class _WrapLine:

    def __init__(
            self,
            indent: Optional[int] = None,
            subsequent_indent: Optional[int] = None,
            width: Optional[int] = None
    ) -> None:
        if isinstance(indent, int):
            indent = abs(indent)
        self._indent: int = indent or 0
        self.__indent = 0

        if isinstance(subsequent_indent, int):
            subsequent_indent = abs(subsequent_indent)
        self._subsequent_indent: Optional[int] = subsequent_indent

        if isinstance(width, int):
            width = abs(width)
        self._width: int = width or 0
        self._is_first_line: bool = True
        self._current_length: int = self.indent
        self._out: List[str] = [self.indent_text]
        self._is_new_line: bool = True

    @cached_property
    def term_width(self) -> int:
        return shutil.get_terminal_size((70, 24, )).columns

    @cached_property
    def width(self) -> int:
        if self._width > self.term_width:
            return self.term_width

        if self._width == 0:
            return self.term_width

        return self._width

    @cached_property
    def indent_amount(self) -> int:
        if self._indent >= self.width:
            return 0
        return self._indent

    @cached_property
    def subsequent_indent_amount(self) -> int:
        if self._subsequent_indent is None:
            return self.indent_amount
        if self._subsequent_indent >= self.width:
            return self.indent_amount
        return self._subsequent_indent

    @property
    def indent(self) -> int:
        if self._is_first_line:
            return self.indent_amount
        return self.subsequent_indent_amount

    @property
    def indent_text(self) -> str:
        return ' ' * self.indent

    @staticmethod
    def get_length(data: str) -> int:
        out = 0
        for c in data:
            out += str_util.get_width(ord(c))
        return out

    def process_text(self, lex_data: str) -> None:
        lex_data_length = self.get_length(lex_data)
        length_new = self._current_length + lex_data_length

        if length_new < self.width:
            if self._is_new_line is True and lex_data == ' ':
                return
            self._out.append(escape(lex_data))
            self._current_length = length_new
            self._is_new_line = False
            return

        if length_new == self.width:
            if lex_data == ' ':
                self._out.append('\n')
                self._is_first_line = False
                self._out.append(self.indent_text)
                self._current_length = self.indent
                self._is_new_line = True
                return
            self._out.append(escape(lex_data))
            self._out.append('\n')
            self._is_first_line = False
            self._out.append(self.indent_text)
            self._current_length = self.indent
            self._is_new_line = True
            return

        # previous_out_index = len(self._out) - 1
        if length_new > self.width:
            if lex_data_length < self.width - self.indent:
                if lex_data == ' ':
                    self._out.append('\n')
                    self._is_first_line = False
                    self._out.append(self.indent_text)
                    self._current_length = self.indent
                    self._is_new_line = True
                    return
                previous_out_index = len(self._out) - 1
                previous_out_data = self._out[previous_out_index]
                if previous_out_data == ' ':
                    self._out[previous_out_index] = '\n'
                    self._is_first_line = False
                    self._out.append(self.indent_text)
                    self._out.append(escape(lex_data))
                    self._current_length = self.indent + lex_data_length
                    self._is_new_line = False
                    return
                self._out.append('\n')
                self._is_first_line = False
                self._out.append(self.indent_text)
                self._out.append(escape(lex_data))
                self._current_length = self.indent + lex_data_length
                self._is_new_line = False
                return

        while len(lex_data) > 0:
            new_data = lex_data[:self.width - self._current_length]
            self.process_text(new_data)
            lex_data = lex_data[len(new_data):]

    def process(self, lex_token: str, lex_data: str) -> None:
        if lex_token.startswith('ver-'):
            self._out.append(lex_data)
            return
        self.process_text(lex_data)

    def __call__(self, line: str) -> str:
        self._is_first_line = True
        self._is_new_line = True
        self._out = [self.indent_text]
        self._current_length = self.indent
        _line_lexer(line, self.process)
        return ''.join(self._out)


def wrap_line(
        line: str,
        indent: Optional[int] = None,
        subsequent_indent: Optional[int] = None,
        width: Optional[int] = None,
) -> str:
    _wrap_line = _WrapLine(
        indent=indent,
        subsequent_indent=subsequent_indent,
        width=width
    )
    return _wrap_line(line)
