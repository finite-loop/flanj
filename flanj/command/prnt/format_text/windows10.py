from prompt_toolkit.output.windows10 import Windows10_Output


class Windows10Output(Windows10_Output):

    def flush(self) -> None:
        return

    def read(self, *args: int) -> str:
        out = ''.join(self.vt100_output._buffer)
        self.vt100_output._buffer = []
        return out
