from typing import Any
from prompt_toolkit.output.conemu import ConEmuOutput as ConEmu_Output


class ConEmuOutput(ConEmu_Output):

    def flush(self) -> None:
        return

    def read(self, *args: Any) -> str:
        out = ''.join(self.vt100_output._buffer)
        self.vt100_output._buffer = []
        return out
