from typing import (
    Any,
    List,
)
from prompt_toolkit.output.win32 import Win32Output as Win32_Output


class Win32Output(Win32_Output):

    def flush(self) -> None:
        return

    def read(self, *args: Any) -> str:
        out = ''.join(self._buffer)
        self._buffer: List[Any] = []
        return out
