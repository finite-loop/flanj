from typing import Any
from django.core.management.base import CommandError as _CommandError
from flanj.utils.paths import (
    contract_paths_in_args,
    contract_paths_in_kwargs,
)


class CommandError(_CommandError):

    def __init__(self, msg: str, *args: Any, **kwargs: Any) -> None:
        args = contract_paths_in_args(args)
        contract_paths_in_kwargs(kwargs)
        if args or kwargs:
            msg = msg.format(*args, **kwargs)
        if not msg.startswith("\n"):
            msg = "\n{}".format(msg)
        super().__init__(msg)


