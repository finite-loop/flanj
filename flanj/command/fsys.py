import os
from typing import (
    Optional
)
from .prnt import Spin


class Fsys(object):

    def __init__(
            self,
            verbosity: int = 1,
            dry_run: bool = False,
            no_color: bool = False,
    ) -> None:
        self.verbosity: int = verbosity
        self.dry_run: bool = dry_run
        self.no_color: bool = no_color

    # noinspection PyUnusedFunction
    def makedirs(
            self,
            path: str,
            mode: Optional[int] = None,
            quiet: bool = False
    ) -> None:
        """Create all directories within the given Path.

        No errors are thrown if the given `path` already exists.

        Default mode is 700.

        Args:
            path (str): The path of the directory to create.
            mode (int, optional): The mode of the directories to create.
                Defaults to: ``0o700``.
            quiet (bool, optional): A value of ``True`` will hide the spinner
                when creating the directory.  Defaults to: ``False``.
        """
        if mode is None:
            mode = 0o700

        if quiet is True:
            if self.dry_run is False:
                os.makedirs(path, mode=mode, exist_ok=True)
            return

        if self.dry_run is True:
            txt = 'DRY RUN creating directory {}'
            suc = 'DRY RUN created directory {}'
        else:
            txt = 'Creating directory {}'
            suc = 'Created directory {}'

        with Spin(txt, path, suc=suc) as _:
            if self.dry_run is False:
                os.makedirs(path, mode=mode, exist_ok=True)

    @staticmethod
    def has_files(dir_path: str) -> bool:
        """Return ``True`` if the given ``dir_path`` is a directory and
        contains files.

        Args:
            dir_path (str): The full path of the directory.
        """
        if os.path.exists(dir_path) and os.path.isdir(dir_path):
            for name in os.listdir(dir_path):
                path = os.path.join(dir_path, name)
                if os.path.isfile(path):
                    return True
        return False

    # noinspection PyUnusedFunction
    def delete_files(
            self,
            dir_path: str,
            quiet: bool = False
    ) -> None:
        """Delete all files in the given dir_path.

        This will not delete any folders within the given directory.

        Args:
            dir_path (str): The full path of the directory
            quiet (bool, optional): A value of ``True`` will hide the spinner
                when creating the directory.  Defaults to: ``False``.
        """
        paths = []
        if self.has_files(dir_path) is True:
            for name in os.listdir(dir_path):
                path = os.path.join(dir_path, name)
                if os.path.isfile(path):
                    paths.append(path)

        if quiet is True and self.dry_run is True:
            return

        for path in paths:
            if quiet is True:
                if self.dry_run is False:
                    os.unlink(path)
            else:
                if self.dry_run is True:
                    txt = 'DRY RUN deleting {}'
                    suc = 'DRY RUN deleted {}'
                else:
                    txt = 'Deleting {}'
                    suc = 'Deleted {}'

                with Spin(txt, path, suc=suc) as _:
                    if self.dry_run is False:
                        os.unlink(path)
