import os
from datetime import (
    datetime,
    tzinfo,
)
from typing import (
    Optional,
    Union,
    cast,
)

import pytz

# noinspection PyProtectedMember
from pytz.tzinfo import BaseTzInfo
from dateutil.tz import tzfile
from tzlocal import get_localzone


DEFAULT_TIMESTAMP = datetime(1970, 1, 1, 0, 0, 0, tzinfo=pytz.UTC)
NOW = datetime.utcnow().replace(tzinfo=pytz.UTC, microsecond=0)
LOCAL_TZINFO: tzinfo = get_localzone()


def as_tzinfo(
        tz_info: Union[str, tzinfo, None] = None,
        default: Union[str, tzinfo, None] = LOCAL_TZINFO,
) -> tzinfo:
    """Convert the given obj (str) into a ``pytz`` ``tzinfo`` object for use
    with :obj:`datetime <datetime.datetime>`.

    Args:
        tz_info (str or :obj:`tzinfo <datetime.datetime.tzinfo>, optional): A
            string containing a valid
            `TZ Database Name <https://tinyurl.com/h6edjuq>`_. Or this can
            be a :obj:`tzinfo <datetime.datetime.tzinfo>` object that was
            created by ``pytz``, ``dateutil``, ``pendulum`` or ``zoneinfo``.
            Any :obj:`tzinfo <datetime.datetime.tzinfo>` object given will
            be normalized to a ``pytz`` ``tzinfo`` object. Defaults to:
            :obj:`None` which will use the value of ``default``.
        default (str or :obj:`tzinfo <datetime.datetime.tzinfo>, optional): The
            default value to use if the given ``tz_info`` is :obj:`None` or
            is invalid.  The value can be a string containing a valid
            `TZ Database Name <https://tinyurl.com/h6edjuq>`_. Or the value
            can be a :obj:`tzinfo <datetime.datetime.tzinfo>` object that was
            created by ``pytz``, ``dateutil``, ``pendulum`` or ``zoneinfo``.
            Any :obj:`tzinfo <datetime.datetime.tzinfo>` object given will
            be normalized to a ``pytz`` ``tzinfo`` object. Defaults to
            :obj:`~flanj.utils.timestamps.LOCAL_TZINFO` which is the
            system's local timezone.

    Raises:
        ValueError: If the given ``default`` is of type :obj:`str` and
            has a value that is not a proper tz database name.
        TypeError: If the given ``default`` is of type
            :obj:`tzinfo <datetime.datetime.tzinfo>` and cannot be converted
            to a ``pytz.BaseTzInfo`` object.
        TypeError: If the given ``default`` is not of type
            :obj:`tzinfo <datetime.datetime.tzinfo>`, :obj:`str` or
            :obj:`None`.
        ValueError: If the given ``tz_info`` and ``default`` are both
            :obj:`None`
        ValueError: If the given ``tz_info`` is of type :obj:`str` with a
            value that is not a valid
            `TZ Database Name <https://tinyurl.com/h6edjuq>`_
        TypeError: If the given ``tz_info`` is of type
            :obj:`tzinfo <datetime.datetime.tzinfo>` and cannot be converted
            to a ``pytz.BaseTzInfo`` object.
    """
    if default is not None:
        if hasattr(default, 'encode'):
            default = cast(str, default)
            try:
                default = pytz.timezone(default)
            except pytz.UnknownTimeZoneError:
                raise ValueError(
                    f"The given 'default' value, {default!r}, is not "
                    f"a valid TZ Database Name. See: "
                    f"https://tinyurl.com/h6edjuq"
                )
        elif isinstance(default, tzinfo) is True:
            default = cast(tzinfo, default)
            if not is_pytz_tzinfo(default):
                try:
                    tz_database_name = get_tz_database_name(default)
                except TypeError:
                    raise TypeError(
                        "Unable to create a 'pytz' 'tzinfo' object from "
                        "the given 'default' value. The value for 'default' "
                        "can be a 'tzinfo' object created from 'pytz', "
                        "'dateutil', 'pendulum' or 'zoneinfo'. Or, the "
                        "value for 'default' can be a 'str' containing a "
                        "valid TZ Database Name (https://tinyurl.com/h6edjuq)."
                        " Or, the value for 'default' can be None."
                    )
                default = pytz.timezone(tz_database_name)
        else:
            raise TypeError(
                "The given 'default' value, %r, must be of type "
                "datetime.datetime.tzinfo, str or None. Got: %r" % (
                    default,
                    type(default).__name__
                )
            )

    error_msg = (
        "Unable to create a 'pytz' 'tzinfo' object from the given "
        "'tz_info' value. The value for 'tz_info' can be a 'tzinfo' "
        "object created from 'pytz', 'dateutil', 'pendulum' or "
        "'zoneinfo'. Or, the value for 'tz_info' can be a 'str' "
        "containing a valid TZ Database Name "
        "(https://tinyurl.com/h6edjuq). Or, the value for 'tz_info' can "
        "be None."
    )

    if tz_info is None:
        if default is None:
            raise ValueError(
                "Unable to return a 'datetime.datetime.tzinfo object "
                "because both the given 'tz_info' and 'default' values "
                "are set to None."
            )
        default = cast(tzinfo, default)
        return default

    if hasattr(tz_info, 'encode'):
        tz_info = cast(str, tz_info)
        try:
            return pytz.timezone(tz_info)
        except pytz.UnknownTimeZoneError:
            if default is None:
                raise ValueError(
                    f"The given value for 'tz_info', {tz_info!r}, is not "
                    f"a valid TZ Database Name. See: "
                    f"https://tinyurl.com/h6edjuq"
                )
            default = cast(tzinfo, default)
            return default

    if isinstance(tz_info, tzinfo):
        tz_info = cast(tzinfo, tz_info)
        if is_pytz_tzinfo(tz_info):
            return tz_info
        try:
            tz_database_name = get_tz_database_name(tz_info)
        except TypeError:
            if default is None:
                raise TypeError(error_msg)
            default = cast(tzinfo, default)
            return default
        else:
            return pytz.timezone(tz_database_name)

    raise TypeError(error_msg)


def is_dateutil_tzinfo(tz_info: Union[tzinfo, tzfile, None]) -> bool:
    """Return :obj:`True` if the given ``tz_info`` is from the ``dateutil``
    module, :obj:`False` otherwise.
    """
    try:
        filename = getattr(tz_info, '_filename')
    except AttributeError:
        pass
    else:
        if not callable(filename) and hasattr(filename, 'encode'):
            return True
    return False


def is_pytz_tzinfo(tz_info: Union[tzinfo, BaseTzInfo, None]) -> bool:
    """Return :obj:`True` if the given ``tz_info`` is from the ``pytz``
    module, :obj:`False` otherwise.
    """
    if hasattr(tz_info, 'zone'):
        tz_info = cast(BaseTzInfo, tz_info)
        if not callable(tz_info.zone):
            return True
    return False


def is_pendulum_tzinfo(tz_info: Optional[tzinfo]) -> bool:
    """Return :obj:`True` if the given ``tz_info`` is from the
    ``pendulum`` module, :obj:`False` otherwise."""
    if hasattr(tz_info, 'name'):
        name = getattr(tz_info, 'name')
        if not callable(name) and hasattr(name, 'encode'):
            return True
    return False


def is_valid_tz_database_name(name: str) -> bool:
    """Return :obj:`True` if the given ``name`` is a valid TZ Database Name,
    :obj:`False` otherwise.
    """
    try:
        pytz.timezone(name)
    except pytz.UnknownTimeZoneError:
        return False
    else:
        return True


def get_tz_database_name(
        tz_info: Union[tzinfo, tzfile, BaseTzInfo, None]
) -> str:
    """Return the TZ Database Name from the given ``tz_info`` object.

    Args:
         tz_info (:obj:`datetime <datetime.datetime>` or :obj:`None`): The
            object from which the TZ Database Name will be extracted. A value
            of :obj:`None` will return an empty :obj:`str`.

    Raises:
        TypeError: If the given ``tz_info`` was not created from ``pytz``,
            ``dateutil``, ``pendulum`` or ``zoneinfo``.
    """
    if tz_info is None:
        return ''
    if is_dateutil_tzinfo(tz_info) is True:
        tz_info = cast(tzfile, tz_info)
        path: str = getattr(tz_info, '_filename')
        # Credit goes to https://tinyurl.com/y32n49bs for the following
        # code that finds the tz database name from the path.
        start = path.find(os.path.sep) + 1
        while start != 0:
            path = path[start:]
            if is_valid_tz_database_name(path):
                return path
            start = path.find(os.path.sep) + 1

    if is_pytz_tzinfo(tz_info):
        tz_info = cast(BaseTzInfo, tz_info)
        return getattr(tz_info, 'zone')

    if is_pendulum_tzinfo(tz_info):
        return getattr(tz_info, 'name')

    # Last ditch effort. This will also work with a ZoneInfo object that
    # is available as of Python 3.9.
    tz_info = cast(tzinfo, tz_info)
    fullname = str(tz_info)
    if is_valid_tz_database_name(fullname):
        return fullname

    raise TypeError(
        "The given 'tz_info' object must be created from 'pytz', 'dateutil', "
        "'pendulum' or 'zoneinfo'."
    )


def normalize_datetime_tzinfo(
        timestamp: datetime,
        default_tz_info: Union[str, tzinfo, None] = LOCAL_TZINFO,
) -> datetime:
    """Normalize the given ``timestamp.tzinfo`` to a ``pytz`` ``tzinfo``
    object.

    :obj:`datetime <datetime.datetime>` objects are immutable meaning the
    returned object is a copy.

    Args:
        timestamp (datetime):  The datetime object which will have it's
            ``tzinfo`` replaced with the equivalent ``pytz`` object.
            If ``timestamp.tzinfo`` is :obj:`None`, it's ``tzinfo`` will
            be replaced with ``pytz.UTC``.
        default_tz_info (str or :obj:`tzinfo <datetime.datetime.tzinfo>,
        optional): The default value to use if the given
            ``timestamp.tzinfo`` is :obj:`None`. The ``default_tz_info`` value
            can be a string containing a valid
            ``TZ Database Name <https://tinyurl.com/h6edjuq>``_. Or the value
            can be a :obj:`tzinfo <datetime.datetime.tzinfo>` object that was
            created by ``pytz``, ``dateutil``, ``pendulum`` or ``zoneinfo``.
            Any :obj:`tzinfo <datetime.datetime.tzinfo>` object given will
            be normalized to a ``pytz`` ``tzinfo`` object. Defaults to
            :obj:`~flanj.utils.timestamps.LOCAL_TZINFO` which is the
            system's local timezone.

    Raises:
        AttributeError: If the given ``timestamp.tzinfo`` cannot be converted
            into a ``pytz.BaseTzInfo`` object.
        ValueError: If the given ``default_tz_info`` is of type :obj:`str` and
            has a value that is not a proper tz database name.
        TypeError: If the given ``default_tz_info`` is of type
            :obj:`tzinfo <datetime.datetime.tzinfo>` and cannot be converted
            to a ``pytz.BaseTzInfo`` object.
        TypeError: If the given ``default_tz_info`` is not of type
            :obj:`tzinfo <datetime.datetime.tzinfo>`, :obj:`str` or
            :obj:`None`.
    """
    if is_pytz_tzinfo(timestamp.tzinfo) is True:
        return timestamp

    tz_database_name = get_tz_database_name(timestamp.tzinfo)
    if tz_database_name:
        ts = timestamp.astimezone(pytz.UTC)
        tz_info = pytz.timezone(tz_database_name)
        return ts.astimezone(tz_info)

    if default_tz_info is not None:
        default_tz_info = as_tzinfo(default_tz_info)
        default_tz_info = cast(tzinfo, default_tz_info)
        return timestamp.replace(tzinfo=default_tz_info)

    raise AttributeError(
        f"Unable to convert the given 'timestamp.tzinfo', "
        f"{timestamp.tzinfo!r} object to a 'pytz.BaseTzInfo object."
    )


# noinspection PySameParameterValue
def as_iso8601(
        timestamp: datetime,
        force_tz_info: Union[tzinfo, str, None] = None,
) -> str:
    """Return the given ``timestamp`` as a string in iso8601 format.

    Args:
        timestamp (:obj:`datetime <datetime.datetime>`): The object
            from which the iso8601 formatted string will be returned.
            The ``timestamp.tzinfo`` value must have been created
            from ``pytz``.
        force_tz_info (str or :obj:`tzinfo <datetime.datetime.tzinfo>,
        optional): This will convert the given ``timestamp.tzinfo``
            (time zone) to this value (time zone) before returning the
            iso8061 formatted string. The ``force_tz_info`` value can be a
            string containing a valid
            ``TZ Database Name <https://tinyurl.com/h6edjuq>``_. Or the value
            can be a :obj:`tzinfo <datetime.datetime.tzinfo>` object that was
            created by ``pytz``, ``dateutil``, ``pendulum`` or ``zoneinfo``.
            Any :obj:`tzinfo <datetime.datetime.tzinfo>` object given will
            be normalized to a ``pytz`` ``tzinfo`` object. Defaults to
            :obj:`None` which will not convert the given ``timestamp``.

    Raises:
        AttributeError: If the given ``timestamp.tzinfo`` is no of type
            ``pytz.BaseTzInfo``
        ValueError: If the given ``force_tz_info`` is of type :obj:`str` and
            has a value that is not a proper tz database name.
        TypeError: If the given ``force_tz_info`` is of type
            :obj:`tzinfo <datetime.datetime.tzinfo>` and cannot be converted
            to a ``pytz.BaseTzInfo`` object.
        TypeError: If the given ``force_tz_info`` is not of type
            :obj:`tzinfo <datetime.datetime.tzinfo>`, :obj:`str` or
            :obj:`None`.

    """
    if not is_pytz_tzinfo(timestamp.tzinfo):
        raise AttributeError(
            f"The given 'timestamp.tzinfo', {timestamp.tzinfo!r}, must be "
            f"a 'pytz.BaseTzInfo' object."
        )
    if force_tz_info is not None:
        new_tz_info = as_tzinfo(force_tz_info)
        timestamp = timestamp.astimezone(new_tz_info)

    if get_tz_database_name(timestamp.tzinfo).lower() == 'utc':
        return timestamp.strftime('%Y-%m-%dT%H:%M:%SZ')
    return timestamp.isoformat()
