import os
from html import escape
from typing import (
    Any,
    Dict,
    Tuple,
    Union,
    cast,
)


def contract_path(
        path: Union[str, os.PathLike]
) -> str:
    if isinstance(path, os.PathLike):
        path = cast(os.PathLike, path)
        path = str(path)
    if not hasattr(path, 'encode'):
        raise TypeError('The given path must be of type str or os.PathLike')
    path = cast(str, path)
    home = os.path.expanduser('~')
    if path.startswith(home):
        path = '~%s' % path[len(home):]
    return path


def contract_paths_in_args(
        args: Tuple[Any, ...],
        escape_html: bool = False,
) -> Tuple[Any, ...]:
    sep = os.path.sep
    out = list(args)
    for index, item in enumerate(out):
        if hasattr(item, 'encode'):
            item = cast(str, item)
            if item.startswith(sep):
                out[index] = contract_path(item)
            if escape_html is True:
                out[index] = escape(item)
        elif isinstance(item, os.PathLike):
            out[index] = contract_path(item)
    return tuple(out)


def contract_paths_in_kwargs(
        kwargs: Dict[str, Any],
        escape_html: bool = False,
) -> None:
    sep = os.path.sep
    for key, val in kwargs.items():
        if hasattr(val, 'encode'):
            val = cast(str, val)
            if val.startswith(sep):
                kwargs[key] = contract_path(val)
            if escape_html is True:
                kwargs[key] = escape(val)
        elif isinstance(val, os.PathLike):
            kwargs[key] = contract_path(val)
