from collections.abc import Sequence
from locale import getpreferredencoding
from html.parser import HTMLParser
from typing import (
    List,
    cast,
)


# noinspection PyAbstractClass
class _MLStripper(HTMLParser):

    def __init__(self) -> None:
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed: List[str] = []
        super().__init__()

    def handle_data(self, d: str) -> None:
        self.fed.append(d)

    def get_data(self) -> str:
        return ''.join(self.fed)


def strip_html(text: str) -> str:
    """Remove any html in the given text."""
    s = _MLStripper()
    s.feed(text)
    return s.get_data()


def contains_html(text: str) -> bool:
    """Return True if the given text contains html; False otherwise."""
    stripped = strip_html(text)
    if stripped == text:
        return False
    return True


# noinspection PyRedeclaration
def sequence_to_str(seq: Sequence) -> str:
    """Convert the given Sequence of items into string of readable text."""
    if not hasattr(seq, 'index'):
        raise TypeError('Only Sequences can be converted to strings.')
    if hasattr(seq, 'capitalize'):
        if hasattr(seq, 'decode'):
            seq = cast(bytes, seq)
            return seq.decode(getpreferredencoding())
        seq = cast(str, seq)
        return seq
    hold = list(map(repr, seq))
    if len(hold) > 1:
        end = 'or %s' % hold.pop(-1)
        index = len(hold) - 1
        hold[index] = '%s %s' % (hold[index], end)
    return ', '.join(hold)
