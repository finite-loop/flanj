import socket
from socket import gaierror
from collections import UserString
from collections.abc import (
    Sequence,
    MutableSequence,
    Mapping,
    ByteString,
)
from ipaddress import ip_address
from typing import Any
try:
    from zoneinfo import available_timezones
except ImportError:
    available_timezones = None
    from pytz import timezone
    from pytz.exceptions import UnknownTimeZoneError


def has_attrs(obj: Any, *attrs: str) -> bool:
    """Return True if all the given attrs exist on the given obj.  False
    otherwise.
    """
    for attr in attrs:
        if hasattr(obj, attr) is False:
            return False
    return True


def is_dict(*objs: Any) -> bool:
    """Return True if all of the given are dictionary-like objects.  False
    otherwise.
    """
    for obj in objs:
        if isinstance(obj, Mapping) is False:
            return False
    return True


def is_list(*objs: Any) -> bool:
    """Return True if all of the given are list-like objects.  False
    otherwise.
    """
    for obj in objs:
        if isinstance(obj, MutableSequence) is False:
            return False
    return True


def is_tuple(*objs: Any) -> bool:
    """Return True if all of the given are tuples.  False otherwise.

    Tuples are:
        - a 'Sequence'; and,
        - not a 'MutableSequence' which would be a list; and,
        - not a 'str' or 'UserString'; and,
        - not a 'BytesString'.
    """
    for obj in objs:
        if isinstance(obj, Sequence) is False:
            return False
        if isinstance(obj, MutableSequence) is True:
            return False
        if isinstance(obj, ByteString) is True:
            return False
        if isinstance(obj, (str, UserString)) is True:
            return False
    return True


def is_list_like(*objs: Any, namedtuple_ok: bool = True) -> bool:
    """Return ``True`` if the given ``obj`` is a list or a tuple.

    If the obj is string-like or bytes-like the value of ``False`` will be
    returned.

    Args:
        *objs (Any): The object to test.
        namedtuple_ok (bool, optional):  If ``True`` and the given ``obj`` is a
            namedtuple, this function will return ``True``.  Default: ``True``
    """
    for obj in objs:
        if obj is None:
            return False
        if isinstance(obj, Sequence):
            if hasattr(obj, 'capitalize'):
                return False
            if namedtuple_ok is False:
                if hasattr(obj, '_fields'):
                    return False
        else:
            return False
    return True


def is_str(*objs: Any) -> bool:
    """Test the given ``*objs``, making sure all are 'string-like'.

    This will return ``True`` if every object in the given ``*objs`` has the
    ``obj.capitalize()`` and ``obj.encode()`` methods; ``False`` otherwise.

    Args:
        *objs (Any): The objects to test.
    """
    for obj in objs:
        if not hasattr(obj, 'encode'):
            return False
    return True


def is_ip_address(*objs: Any) -> bool:
    """Test the given ``*objs``, making sure all are valid ip-addresses.

    Args:
        *objs (Any): The objects to test.
    """
    for obj in objs:
        try:
            ip_address(obj)
        except ValueError:
            return False
    return True


# noinspection PyUnusedFunction
def is_host(*objs: Any) -> bool:
    """Test the given ``*objs``, making sure all are valid hosts.

    Args:
        *objs (Any): The objects to test.
    """
    for obj in objs:
        if is_ip_address(obj) is True:
            continue
        if is_str(obj) is False:
            return False

        if socket.gethostbyname(obj):
            continue
        try:
            socket.getaddrinfo(obj, 22, type=socket.SOCK_STREAM)
        except gaierror:
            return False
    return True


def is_tz_name(*objs: Any) -> bool:
    """Test the given ``*objs``, making sure all are a valid TZ database
    name.

    A list of valid TZ database names can be found at: https://bit.ly/2glGdNY

    Args:
         *objs (Any): The objects to test.
    """
    for obj in objs:
        if not is_str(obj):
            return False
        if available_timezones is None:
            try:
                timezone(obj)
            except UnknownTimeZoneError:
                return False
        else:
            if obj not in available_timezones():
                return False
    return True


def is_hash(*objs: Any) -> bool:
    """Test the given ``*objs``, making sure all are a valid hashes.

    Args:
         *objs (Any): The objects to test.
    """
    for obj in objs:
        try:
            int(obj, 16)
        except (ValueError, TypeError):
            return False
    return True


def is_port(*objs: Any) -> bool:
    """Test the given ``*objs``, making sure all are a valid port numbers.

    Tests non int types as well.

    Args:
         *objs (Any): The objects to test.
    """
    for obj in objs:
        if not isinstance(obj, int):
            try:
                obj = int(obj)
            except (ValueError, TypeError):
                return False
        if not (1 <= obj <= 65535):
            return False
    return True
