# type: ignore
from typing import (
    cast,
    get_type_hints,
)
import asyncio


# noinspection PyPep8Naming
class cached_property:
    """A property that is only computed once per instance and then replaces
    itself with an ordinary attribute.

    Deleting the attribute resets the property.

    This class is Copyright (c) 2015 Daniel Greenfeld Under the BSD license.
    Source: https://bit.ly/2Jjdkuk and https://bit.ly/1pQ8R0f
    """

    def __init__(self, func) -> None:
        self.__doc__ = getattr(func, "__doc__")
        self.type_hints = get_type_hints(func)
        self.func = func

    def __get__(self, obj, cls):
        if obj is None:
            return self

        if asyncio and asyncio.iscoroutinefunction(self.func):
            return self._wrap_in_coroutine(obj)

        value = self.func(obj)

        try:
            hint = self.type_hints['return']
        except KeyError:
            pass
        else:
            value = cast(hint, value)

        obj.__dict__[self.func.__name__] = value
        return value

    def _wrap_in_coroutine(self, obj):

        async def wrapper():
            future = asyncio.ensure_future(self.func(obj))
            obj.__dict__[self.func.__name__] = future
            return future

        return wrapper()
