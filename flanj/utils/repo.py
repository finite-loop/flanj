import os
from datetime import datetime
from functools import cached_property
from typing import (
    Generator,
    NamedTuple,
    Optional,
    Tuple,
    cast,
)

import packaging.version
from git import (
    Commit,
    Repo as _Repo,
    TagReference,
)
from git.refs.head import Head
from git.objects.util import from_timestamp
from packaging.version import Version
from pytz import UTC


def find_git_home(
        path: Optional[str] = None
) -> str:
    """Find the ``.git`` directory by walking up the given ``path``'s
    parent directories.

    Args:
         path (str, optional): The starting path. Defaults to the current
            working directory.

    This function will return an empty string, ``''`` if NO ``.git``
    directory is found; or, if the search has reached the user's home
    directory; or, the search has reached any directory directory below
    the system root directory of ``/``.
    """
    home = os.path.expanduser('~')
    if hasattr(path, 'encode') is False:
        path = ''
    path = cast(str, path)
    path = path.strip()
    if not path:
        path = os.getcwd()

    path = os.path.abspath(path)
    if not os.path.isdir(path):
        path = os.path.dirname(path)
    path = os.path.join(path, '.git')
    while os.path.isdir(path) is False:
        path = os.path.dirname(path)
        path = os.path.dirname(path)
        if path == home or path == os.path.sep:
            return ''
        if len(path.split(os.path.sep)) <= 2:
            return ''
        path = os.path.join(path, '.git')
    return path


class CommitInfo(NamedTuple):
    sha: str
    author: str
    message: str
    timestamp: datetime


def _build_commit_info(commit: Commit) -> CommitInfo:
    kwargs = {
        'sha': str(commit.hexsha),
        'author': str(commit.author.name),
        'message': str(commit.message),
        'timestamp': commit.committed_datetime.astimezone(UTC)
    }
    return CommitInfo(**kwargs)


class TagInfo(NamedTuple):
    name: str
    path: str
    sha: str
    author: str
    message: str
    timestamp: datetime
    is_lightweight: bool
    version: Version
    commits: Tuple[CommitInfo, ...]
    commit_sha_tuple: Tuple[str, ...]


def _build_tag_info(
        tag_ref: TagReference,
        commits: Tuple[CommitInfo, ...],
) -> TagInfo:
    kwargs = {
        'name': str(tag_ref.name),
        'path':  str(tag_ref.path),
        'sha': str(tag_ref.commit.hexsha),
        'author': str(tag_ref.commit.author.name),
        'message': str(tag_ref.commit.message),
        'timestamp': tag_ref.commit.committed_datetime.astimezone(UTC),
        'is_lightweight': True,
        'version': packaging.version.parse(tag_ref.name),
        'commits': commits,
        'commit_sha_tuple': _build_commit_sha_tuple(commits),
    }
    if tag_ref.tag is not None:
        kwargs['sha'] = str(tag_ref.tag.hexsha)
        kwargs['author'] = str(tag_ref.tag.tagger.name)
        kwargs['message'] = str(tag_ref.tag.message)
        ts = from_timestamp(
            tag_ref.tag.tagged_date,
            tag_ref.tag.tagger_tz_offset,
        )
        kwargs['timestamp'] = ts.astimezone(UTC)
        kwargs['is_lightweight'] = False
    return TagInfo(**kwargs)


def _build_commit_sha_tuple(
        commits: Tuple[CommitInfo, ...]
) -> Tuple[str, ...]:
    out = []
    for c in commits:
        out.append(c.sha)
    return tuple(out)


def _each_head_commit(head: Head) -> Generator[CommitInfo, None, None]:
    if not hasattr(head, 'commit') or head.commit is None:
        return
    yield _build_commit_info(head.commit)
    for c in head.commit.iter_parents():
        yield _build_commit_info(c)


class BranchInfo(NamedTuple):
    name: str
    path: str
    tracking_branch: str
    is_dirty: bool
    has_untracked_files: bool
    commit: Optional[CommitInfo]
    commits_since_tag: Tuple[CommitInfo, ...]
    tags: Tuple[TagInfo, ...]


def _build_branch_info(
        branch: Head,
        commit: Optional[CommitInfo],
        commits_since_tag: Tuple[CommitInfo, ...],
        tags: Tuple[TagInfo, ...],
        is_dirty: bool = False,
        has_untracked_files: bool = False,
) -> BranchInfo:
    kwargs = {
        'name': str(branch.name),
        'path': str(branch.path),
        'tracking_branch': '',
        'is_dirty': is_dirty,
        'has_untracked_files': has_untracked_files,
        'commit': commit,
        'commits_since_tag': commits_since_tag,
        'tags': tags,
    }
    tracking_branch = branch.tracking_branch()
    if tracking_branch:
        kwargs['tracking_branch'] = str(tracking_branch.path)
    return BranchInfo(**kwargs)  # type: ignore


class Repo:

    def __init__(
            self,
            repo_home: str,
    ) -> None:
        if hasattr(repo_home, 'encode') is False:
            raise TypeError(
                "The given 'repo_home' must be of type 'str' got %r" %
                type(repo_home).__name__
            )
        if os.path.basename(repo_home) != '.git':
            repo_home = os.path.join(repo_home, '.git')

        if not os.path.isdir(repo_home):
            raise ValueError(
                f"The given value for 'repo_home', {repo_home!r} does not "
                f"exist or is not a directory"
            )
        self._repo_home: str = os.path.dirname(repo_home)

    @cached_property
    def repo_home(self) -> str:
        return self._repo_home

    @cached_property
    def _repo(self) -> _Repo:
        return _Repo(self.repo_home)

    @cached_property
    def _tag_refs(self) -> Tuple[TagReference, ...]:
        hold = {}
        out = []
        for tag in self._repo.tags:
            tag = cast(TagReference, tag)
            hold[packaging.version.parse(tag.name)] = tag
        for key in sorted(hold.keys()):
            out.append(hold[key])
        return tuple(out)

    @cached_property
    def tags(self) -> Tuple[TagInfo, ...]:
        out = []
        previous_tag_commit_sha = ''
        for tag_ref in self._tag_refs:
            commits = self._get_tag_commits(
                tag_ref.name,
                previous_tag_commit_sha,
            )
            tag_info = _build_tag_info(tag_ref, commits)
            out.append(tag_info)
            previous_tag_commit_sha = str(tag_ref.commit.hexsha)
        out.sort(key=lambda x: x.version, reverse=True)
        return tuple(reversed(out))

    def _get_tag_commits(
            self,
            tag_name: str,
            previous_tag_commit_sha: str
    ) -> Tuple[CommitInfo, ...]:
        out = []
        for commit in self._repo.iter_commits(tag_name):
            if commit.hexsha == previous_tag_commit_sha:
                break
            commit_info = _build_commit_info(commit)
            out.append(commit_info)
        out.sort(key=lambda x: x.timestamp, reverse=True)
        return tuple(out)

    def _find_tag(self, sha: str) -> Optional[TagInfo]:
        for tag_info in self.tags:
            if sha == tag_info.sha:
                return tag_info
            if sha in tag_info.commit_sha_tuple:
                return tag_info
        return None

    @cached_property
    def active_branch(self) -> BranchInfo:
        head: Head = self._repo.active_branch
        commit: Optional[CommitInfo] = None
        if hasattr(head, 'commit') and head.commit is not None:
            commit = _build_commit_info(head.commit)
        commits_since_tag_set = set()
        tag_set = set()
        tag_is_set = False
        for commit_info in _each_head_commit(head):
            tag_info = self._find_tag(commit_info.sha)
            if tag_info:
                tag_set.add(tag_info)
                tag_is_set = True
            else:
                if tag_is_set is False:  # pragma: no cover
                    commits_since_tag_set.add(commit_info)  # pragma: no cover
        commit_since_tag_list = list(commits_since_tag_set)
        commit_since_tag_list.sort(key=lambda x: x.timestamp, reverse=True)
        commits_since_tag = tuple(commit_since_tag_list)
        tag_list = list(tag_set)
        tag_list.sort(key=lambda x: x.version, reverse=True)
        tags = tuple(tag_list)
        is_dirty = self._repo.is_dirty()
        has_untracked_files = self._repo.is_dirty(
            index=False,
            untracked_files=True
        )
        return _build_branch_info(
            head,
            commit,
            commits_since_tag,
            tags,
            is_dirty=is_dirty,
            has_untracked_files=has_untracked_files
        )


def get_active_branch_info(repo_home: str) -> BranchInfo:
    git_home = find_git_home(repo_home)
    repo = Repo(git_home)
    return repo.active_branch
