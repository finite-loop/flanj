import json
from collections.abc import Sequence
from copy import copy
from datetime import (
    datetime,
    tzinfo,
)
from http.client import HTTPMessage
from itertools import product
from typing import (
    Any,
    Dict,
    Generator,
    List,
    Optional,
    NamedTuple,
    Tuple,
    Union,
    cast,
)
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.request import (
    Request,
    urlopen,
)

import dateutil.parser
import pytz
from flanj.utils.timestamps import (
    as_iso8601,
    as_tzinfo,
    normalize_datetime_tzinfo,
)
from tzlocal import get_localzone


_MultiType = Optional[Union[List[str], Tuple[str, ...]]]


def _prep_multi_type(value: _MultiType, name: str) -> Tuple[str, ...]:
    if value is None:
        return ()
    error = False
    if not isinstance(value, Sequence):
        error = True
    if hasattr(value, 'encode') or hasattr(value, 'decode'):
        error = True
    if error is True:
        raise TypeError(
            "The value for %r can only be of type: None, List[str] or "
            "Tuple[str, ...]. Got: %r." % (name, type(value).__name__)
        )
    return tuple(value)


class Label(NamedTuple):
    id: int
    project_id: int
    name: str
    created_at: Optional[datetime]
    updated_at: Optional[datetime]


def _build_label(row: Dict[str, Any]) -> Label:
    created_at_str = row.get('created_at', '')
    created_at: Optional[datetime] = None
    if created_at_str:
        created_at = dateutil.parser.parse(created_at_str).astimezone(
            pytz.UTC
        )

    updated_at_str = row.get('updated_at', '')
    updated_at: Optional[datetime] = None
    if updated_at_str:
        updated_at = dateutil.parser.parse(updated_at_str).astimezone(
            pytz.UTC
        )

    return Label(
        id=row['id'],
        project_id=row['project_id'],
        name=row['name'],
        created_at=created_at,
        updated_at=updated_at,
    )


class Story(NamedTuple):
    id: int
    created_at: datetime
    updated_at: Optional[datetime]
    accepted_at: Optional[datetime]
    estimate: int
    story_type: str
    name: str
    description: str
    current_state: str
    url: str
    project_id: str
    labels: Tuple[Label, ...]


def _build_story(row: Dict[str, Any]) -> Optional[Story]:
    if row['kind'] != 'story':
        return None

    labels: Tuple[Label, ...] = ()
    if hasattr(row['labels'], 'append') and row['labels']:
        hold = []
        for label_row in row['labels']:
            hold.append(_build_label(label_row))
        labels = tuple(hold)

    created_at = dateutil.parser.parse(row['created_at'])

    updated_at_str = row.get('updated_at', '')
    updated_at: Optional[datetime] = None
    if updated_at_str:
        updated_at = dateutil.parser.parse(
            updated_at_str
        ).astimezone(pytz.UTC)

    accepted_at_str = row.get('accepted_at', '')
    accepted_at: Optional[datetime] = None
    if accepted_at_str:
        accepted_at = dateutil.parser.parse(accepted_at_str).astimezone(
            pytz.UTC
        )

    return Story(
        id=row['id'],
        created_at=created_at.astimezone(pytz.UTC),
        updated_at=updated_at,
        accepted_at=accepted_at,
        estimate=row.get('estimate', -1),
        story_type=row['story_type'],
        name=row['name'],
        description=row.get('description', ''),
        current_state=row['current_state'],
        url=row['url'],
        project_id=row['project_id'],
        labels=labels,
    )


class PivotalTracker:

    """Use to query data from Pivotal Tracker.

    This class uses the
    `Pivotal Tracker API <https://www.pivotaltracker.com/help/api#top>`_
    to query information about stories.

    The idea behind this class is to query general story information which
    may be written into a change long or manifest file.

    """

    URL: str = 'https://www.pivotaltracker.com/services/'

    STATES: Tuple[str, ...] = (
        'accepted',
        'delivered',
        'finished',
        'started',
        'rejected',
        'planned',
        'unstarted',
        'unscheduled',
    )

    STORY_TYPES: Tuple[str, ...] = (
        'feature',
        'bug',
        'chore',
        'release',
    )

    # noinspection PySameParameterValue
    def __init__(
            self,
            token: str,
            project_id: Union[int, str],
            time_zone: Union[tzinfo, str, None] = None,
            version: Union[int, str] = 5,
    ) -> None:
        """

        Args:
            token (str): This is the API token found on this page:
                https://www.pivotaltracker.com/profile
            project_id (int or str): This can be found in the URL of a
                project or under the 'Access' section of the project's
                setting page
            time_zone (:obj:`datetime.tzinfo` or str): The time zone to
                convert all datetime objects. This time zone will also be used
                to replace missing the tzinfo in any
                :obj:`datetime <datetime.datetime>` object given as an
                argument to the ``fetch_stories`` method. This can be a
                string containing a value from the `TZ database name` column
                from:
                `List of tz database time zones <https://tinyurl.com/h6edjuq>`_
                Defaults to: :obj:`None` which will use the local time zone.
            version (int, optional): This only works with version 5 of the
                Pivotal Tracker API. Defaults to: ``5``.
        """
        if hasattr(token, 'encode') is False:
            raise TypeError(
                "The value for 'token' must be of type str."
            )
        token = token.strip()
        if not token:
            raise ValueError(
                "The value for 'token' cannot be empty."
            )
        self.__initial_header: Dict[str, str] = {
            'X-TrackerToken': token
        }

        try:
            project_id = int(project_id)
        except TypeError:
            raise TypeError(
                "The value for 'project_id' must be of type int or str."
            )
        except ValueError:
            raise TypeError(
                "The value for 'project_id' cannot be converted into an int."
            )
        else:
            project_id = cast(int, project_id)
        self.__project_id: int = project_id

        try:
            version = int(version)
        except TypeError:
            raise TypeError(
                "The value for 'version' must be of type int or str."
            )
        except ValueError:
            raise TypeError(
                "The value for 'version' cannot be converted into an int."
            )
        else:
            version = cast(int, version)
        if version != 5:
            raise ValueError(
                "The value for 'version' can only be 5."
            )
        self.tz_info: tzinfo = as_tzinfo(
            time_zone,
            default=get_localzone(),
        )
        self.__version = version
        self.__url = f'{self.URL}/v{version}/projects/{project_id}'
        self.stories: Dict[int, Story] = {}
        self.data: List[Any] = []
        self.total: int = 0

    # noinspection PyUnusedFunction
    @property
    def project_id(self) -> int:
        return self.__project_id

    @property
    def version(self) -> int:
        return self.__project_id

    @property
    def url(self) -> str:
        return self.__url

    def _add_story_type(
            self,
            story_type: str,
            params: Dict[str, Any],
            is_data: bool = False
    ) -> None:
        if story_type.lower() in self.STORY_TYPES:
            if is_data is True:
                params['story_type'] = story_type.lower()
            else:
                params['with_story_type'] = story_type.lower()
            return
        values = ', '.join(map(lambda x: f"'{x}'", self.STORY_TYPES))
        raise ValueError(
            "The value for 'story_type' must be one of: %s. "
            "Got: %r" % (values, story_type)
        )

    def _add_current_state(
            self,
            state: str,
            params: Dict[str, Any],
            is_data: bool = False
    ) -> None:
        if state.lower() in self.STATES:
            if is_data is True:
                params['state'] = state.lower()
            else:
                params['with_state'] = state.lower()
            return
        values = ', '.join(map(lambda x: f"'{x}'", self.STATES))
        raise ValueError(
            "The value for 'state' must be one of: %s" % values
        )

    def _build_request(self, path: str, params: Dict[str, Any]) -> Request:
        parameters = urlencode(params)
        url = f'{self.url}/{path}'
        if parameters:
            url = f'{url}?{parameters}'
        return Request(url, headers=self.__initial_header)

    def _each_fetched_row(
            self,
            path: str,
            params: Dict[str, Any],
    ) -> Generator[Dict[str, Any], None, None]:
        total_fetched = 0
        request = self._build_request(path, params)
        with urlopen(request) as response:
            info: HTTPMessage = response.info()
            total = int(info.get('X-Tracker-Pagination-Total'))
            limit = int(info.get('X-Tracker-Pagination-Limit'))
            json_rows = json.load(response)
        self.total = total
        total_fetched += len(json_rows)
        for row in json_rows:
            yield row

        while total_fetched < total:
            params['limit'] = limit
            params['offset'] = total_fetched
            request = self._build_request(path, params)
            with urlopen(request) as response:
                info = response.info()
                total = int(info.get('X-Tracker-Pagination-Total'))
                limit = int(info.get('X-Tracker-Pagination-Limit'))
                json_rows = json.load(response)
            total_fetched += len(json_rows)
            for row in json_rows:
                yield row

    def _load_stories(self, params: Dict[str, str]) -> None:
        path = 'stories'
        for row in self._each_fetched_row(path, params):
            self.data.append(row)
            obj = _build_story(row)
            if obj:
                self.stories[obj.id] = obj

    # noinspection PyUnusedFunction
    def fetch_stories(
            self,
            with_states: _MultiType = None,
            with_story_types: _MultiType = None,
            accepted_before: Optional[datetime] = None,
            accepted_after: Optional[datetime] = None,
            created_before: Optional[datetime] = None,
            created_after: Optional[datetime] = None,
            updated_before: Optional[datetime] = None,
            updated_after: Optional[datetime] = None,
            limit: int = 1_000_000,
            offset: Optional[int] = None
    ) -> None:
        with_states = _prep_multi_type(
            with_states,
            'with_states'
        )
        with_story_types = _prep_multi_type(
            with_story_types,
            'with_story_types'
        )
        states_types: Tuple[Tuple[str, str], ...] = ()
        if with_states or with_story_types:
            if with_states and with_story_types:
                states_types = tuple(product(with_states, with_story_types))
            else:
                if with_states:
                    states_types = tuple(product(with_states, ('', )))
                else:
                    states_types = tuple(product(('', ), with_story_types))

        base_params: Dict[str, Any] = {
            'limit': limit,
        }
        if offset is not None:
            base_params['offset'] = offset

        if accepted_before is not None:
            accepted_before = normalize_datetime_tzinfo(
                accepted_before,
                default_tz_info=self.tz_info,
            )
            base_params['accepted_before'] = as_iso8601(
                accepted_before,
                force_time_zone=True,
                time_zone=pytz.UTC,
            )

        if accepted_after is not None:
            accepted_after = normalize_datetime_tzinfo(
                accepted_after,
                default_tz_info=self.tz_info,
            )
            base_params['accepted_after'] = as_iso8601(
                accepted_after,
                force_time_zone=True,
                time_zone=pytz.UTC,
            )
        if created_before is not None:
            created_before = normalize_datetime_tzinfo(
                created_before,
                default_tz_info=self.tz_info,
            )
            base_params['created_before'] = as_iso8601(
                created_before,
                force_time_zone=True,
                time_zone=pytz.UTC,
            )

        if created_after is not None:
            created_after = normalize_datetime_tzinfo(
                created_after,
                default_tz_info=self.tz_info,
            )
            base_params['created_after'] = as_iso8601(
                created_after,
                force_time_zone=True,
                time_zone=pytz.UTC,
            )

        if updated_before is not None:
            updated_before = normalize_datetime_tzinfo(
                updated_before,
                default_tz_info=self.tz_info,
            )
            base_params['updated_before'] = as_iso8601(
                updated_before,
                force_time_zone=True,
                time_zone=pytz.UTC,
            )

        if updated_after is not None:
            updated_after = normalize_datetime_tzinfo(
                updated_after,
                default_tz_info=self.tz_info,
            )
            base_params['updated_after'] = as_iso8601(
                updated_after,
                force_time_zone=True,
                time_zone=pytz.UTC,
            )

        if states_types:
            for state, story_type in states_types:
                params = copy(base_params)
                if state:
                    self._add_current_state(state, params)
                if story_type:
                    self._add_story_type(story_type, params)
                self._load_stories(params)
                print('count: %s' % len(self.stories))
        else:
            self._load_stories(base_params)

    def fetch_story(
            self,
            story_id: Union[str, int],
    ) -> bool:
        path = f'stories/{story_id}'
        request = self._build_request(path, {})
        try:
            with urlopen(request) as response:
                row = json.load(response)
        except HTTPError as e:
            if e.code == 404:
                return False
        obj = _build_story(row)
        if obj:
            self.stories[obj.id] = obj
            return True
        return False
