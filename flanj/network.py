from typing import (
    NamedTuple,
    Union,
    Tuple,
    Dict,
)
import netifaces
import ipaddress as _ip


class IpV4(NamedTuple):
    address: _ip.IPv4Address
    network: _ip.IPv4Network
    cidr: int
    with_cidr: str
    netmask: str
    with_netmask: str
    hostmask: str
    with_hostmask: str
    interface_name: str
    gateway: Union[_ip.IPv4Address, None]
    broadcast: Union[_ip.IPv4Address, None]


class IpV6(NamedTuple):
    address: _ip.IPv6Address
    network: _ip.IPv6Network
    cidr: int
    with_cidr: str
    netmask: str
    with_netmask: str
    hostmask: str
    with_hostmask: str
    interface_name: str
    gateway: Union[_ip.IPv6Address, None]
    flags: int


class Gateway(NamedTuple):
    address: Union[_ip.IPv4Address, _ip.IPv6Address]
    interface_name: str

def system_ipv4_addresses() -> Tuple[IpV4]:
    gateway: Gateway = system_ipv4_gateway()
    if not gateway:
        return tuple()
    try:
        data = netifaces.ifaddresses(gateway.interface_name)[
            netifaces.AF_INET
        ]
    except KeyError:
        return tuple()
    out = list()
    for row in data:
        addr = "%s/%s" % (row['addr'], row['netmask'])
        try:
            interface = _ip.IPv4Interface(addr)
        except ValueError:
            continue
        try:
            broadcast = _ip.ip_address(row.get('broadcast'))
        except (TypeError, ValueError):
            broadcast = None
        out.append(
            IpV4(
                interface.ip,
                interface.network,
                int(interface.with_prefixlen.split('/')[-1]),
                interface.with_prefixlen,
                interface.with_netmask.split('/')[-1],
                interface.with_netmask,
                interface.with_hostmask.split('/')[-1],
                interface.with_hostmask,
                gateway.interface_name,
                gateway.address,
                broadcast
            )
        )
    return tuple(out)


def system_ipv6_addresses() -> Tuple[IpV4]:
    gateway: Gateway = system_ipv4_gateway()
    if not gateway:
        return tuple()
    try:
        data = netifaces.ifaddresses(gateway.interface_name)[
            netifaces.AF_INET
        ]
    except KeyError:
        return tuple()
    out = list()
    for row in data:
        addr = "%s/%s" % (row['addr'], row['netmask'].split('/')[-1])
        try:
            interface = _ip.IPv6Interface(addr)
        except ValueError:
            continue

        out.append(
            IpV6(
                interface.ip,
                interface.network,
                int(interface.with_prefixlen.split('/')[-1]),
                interface.with_prefixlen,
                interface.with_netmask.split('/')[-1],
                interface.with_netmask,
                interface.with_hostmask.split('/')[-1],
                interface.with_hostmask,
                gateway.interface_name,
                gateway.address,
                row.get('flags', 0)
            )
        )
    return tuple(out)


def system_addresses() -> Tuple[Union[IpV4, IpV6]]:
    out = list(system_ipv4_addresses()) + list(system_ipv6_addresses())
    return tuple(out)


def system_ipv4_gateway() -> Union[Gateway, tuple]:
    default_gw: Dict[int, Tuple[str, str]] = netifaces.gateways().get(
        'default', dict()
    )
    if netifaces.AF_INET in default_gw:
        try:
            return Gateway(
                _ip.ip_address(default_gw[netifaces.AF_INET][0]),
                default_gw[netifaces.AF_INET][1]

            )
        except ValueError:
            pass
    return tuple()


def system_ipv6_gateway() -> Union[Gateway, tuple]:
    default_gw: Dict[int, Tuple[str, str]] = netifaces.gateways().get(
        'default', dict()
    )
    if netifaces.AF_INET6 in default_gw:
        try:
            return Gateway(
                _ip.ip_address(default_gw[netifaces.AF_INET6][0]),
                default_gw[netifaces.AF_INET6][1]
            )
        except ValueError:
            pass
    return tuple()

