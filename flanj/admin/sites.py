import logging
from typing import (
    Any,
    Dict,
    List,
    Optional,
    Union,
    Sequence,
    Tuple,
    cast,
)

from django.apps import apps
from django.conf import settings
from django.contrib.admin import AdminSite as DjangoAdminSite
from django.http import (
    Http404,
    HttpRequest,
)
from django.template.response import TemplateResponse
from django.urls import (
    NoReverseMatch,
    reverse,
)
from django.urls.resolvers import (
    URLResolver,
    URLPattern,
)
from django.utils.text import capfirst
from django.utils.translation import (
    gettext as _,
    gettext_lazy
)

AUTH_USER_MODEL: str = getattr(
    settings,
    'AUTH_USER_MODEL',
    ''
)

# Text to put at the end of each admin page's <title>
ADMIN_SITE_TITLE: str = getattr(
    settings,
    'ADMIN_SITE_TITLE',
    'Django site admin',
)

# Text to put in each admin page's <h1>
ADMIN_SITE_HEADER: str = getattr(
    settings,
    'ADMIN_SITE_HEADER',
    'Django administration',
)

# Text to put at the top of the admin index page.
ADMIN_INDEX_TITLE: str = getattr(
    settings,
    'ADMIN_INDEX_TITLE',
    'Site administration',
)

# URL for the "View site" link at the top of each admin page.
ADMIN_VIEW_SITE_URL: str = getattr(
    settings,
    'ADMIN_VIEW_SITE_URL',
    '/',
)

logger = logging.getLogger(__name__)


class AdminSite(DjangoAdminSite):

    """This is an override of the django admin site object that adds
    additional features:

    #. Automatically display the custom ``User`` model, as set in
       ``settings.AUTH_USER_MODEL``, to display under the
       "AUTHENTICATION AND AUTHORIZATION" app section.
    #. Ability to set the order of the various models under each app section.
    #. Add values in ``settings.py`` to set the text:
       * at the end of each admin page's <title>
       * in each admin page's <h1>
       * at the top of the admin index page
    #. Add a value in ``settings.py`` to set the URL for the "View site"
       link at the top of each admin page.

    SETUP:

    To use this class, edit``settings.py`` and find ``INSTALLED_APPS`` to
    replace (or add) the value ``django.contrib.admin`` with
    ``flanj.admin.AdminConfig``::

        INSTALLED_APPS = [
            ...
            # 'django.contrib.admin',
            'flanj.admin.AdminConfig',
            ...
        ]

    MODEL ORDERING UNDER EACH APP:

    Setting the order of the models displayed on the admin pages is done by
    setting a position, ``_app_position = n`` in the ``ModelAdmin`` or
    ``_admin_app_position = n`` in the ``Model``. Positions set on the
    ``ModelAdmin`` will override any positions set on the ``Model``.

    Examples:
        To set the position on a ``ModelAdmin`` set the ``_app_position``
        property to the desired sort position::

            from django.contrib import admin
            from origin.admin import site
            from . import models

            @admin.register(models.MyTableForSomething, site=site)
            class MyTableForSomethingAdmin(admin.ModelAdmin):
                _app_position = 1
                ...

            @admin.register(models.MyTableForAnotherThing, site=site)
            class MyTableForAnotherThingAdmin(admin.ModelAdmin):
                _app_position = 2
                ...

        To set the position on a ``Model`` set the ``_admin_app_position``
        property to the desired sort position:

            from django.db import models

            class MyTableForSomething(models.Model):
                _admin_app_position = 1
                ...

            class MyTableForAnotherThing(models.Model):
                _admin_app_position = 2
                ...

        To create a custom ``User`` model (``core/models.py``)::

            from django.db import models
            from django.conf import settings
            from django.contrib.auth.models import AbstractUser


            class User(AbstractUser):

                supervisor = models.ForeignKey(
                    settings.AUTH_USER_MODEL,
                    models.SET_NULL,
                    related_name='subordinates',
                    blank=True,
                    null=True
                )
                ...

        Make sure to set AUTH_USER_MODEL in ``settings.py``::

            ...
            AUTH_USER_MODEL = 'core.User'
            ...

        To create a custom ``User`` admin::

            from django.contrib.auth.models import Group
            from django.contrib.auth.admin import GroupAdmin
            from django.contrib.auth.admin import UserAdmin as _UserAdmin
            from django.contrib.auth.forms import UserCreationForm as _Form
            from django.contrib.auth import get_user_model

            User = get_user_model()

            class UserCreationForm(_Form):
                class Meta(_Form.Meta):
                    model = User

                def clean_username(self):
                    # username is unique anyway
                    # Base class is too clever and touches auth.User
                    return self.cleaned_data["username"]

            @admin.register(User, site=site)
            class UserAdmin(_UserAdmin):
                _app_position = 1
                add_form = UserCreationForm

                # add_fieldsets is not a standard ModelAdmin attribute.
                # UserAdmin overrides get_fieldsets to use this attribute when
                # creating a user.
                fieldsets = list(_UserAdmin.fieldsets)
                # Add supervisor to Personal Info
                fieldsets[1][1]['fields'] = fieldsets[1][1]['fields'] +
                        ('supervisor', )

            @admin.register(Group, site=site)
            class GroupAdmin(GroupAdmin):
                _app_position = 2

    REFERENCES:
    * `Overriding the default admin site <https://bit.ly/2P9Cr8l>`_
    """

    # Text to put at the end of each page's <title>.
    site_title = gettext_lazy(ADMIN_SITE_TITLE)

    # Text to put in each page's <h1>.
    site_header = gettext_lazy(ADMIN_SITE_HEADER)

    # Text to put at the top of the admin index page.
    index_title = gettext_lazy(ADMIN_INDEX_TITLE)

    # URL for the "View site" link at the top of each admin page.
    site_url = ADMIN_VIEW_SITE_URL

    @staticmethod
    def _normalize_position(position: Any) -> str:
        if hasattr(position, 'capitalize') is False:
            if isinstance(position, int):
                return '{:0>20}'.format(position)
            else:
                return str(position)
        return position

    def _build_app_dict(
            self,
            request: HttpRequest,
            label: Optional[str] = None
    ) -> Dict[str, Any]:
        """ Release the app dictionary. The optional `label` parameter
        filters models of a specific app.
        """
        app_dict = {}

        if label and label != 'auth':
            models = {
                m: m_a for m, m_a in self._registry.items()
                if m._meta.app_label == label
            }
        else:
            models = self._registry

        models = self._registry

        for model, model_admin in models.items():
            app_label = model._meta.app_label

            has_module_perms = model_admin.has_module_permission(request)
            logger.debug(
                'has_module_perms=%r' % has_module_perms
            )
            if not has_module_perms:
                continue

            perms = model_admin.get_model_perms(request)

            # Check whether user has any perm for this module.
            # If so, add the module to the model_list.
            if True not in perms.values():
                continue

            info = (app_label, model._meta.model_name)
            model_name_parts = model.__module__.split('.')[:-1]
            model_name_parts.append(model.__name__)
            model_name = '.'.join(model_name_parts)
            model_dict = {
                'name': capfirst(model._meta.verbose_name_plural),
                'object_name': model._meta.object_name,
                'perms': perms,
                'model_name': model_name,
            }
            # Get the sorting position from the model
            app_position = self._normalize_position(
                getattr(model, '_admin_app_position', '')
            )

            # If the model does NOT have the position get it from the admin
            if not app_position:
                app_position = self._normalize_position(
                    getattr(model_admin, '_app_position', model_dict['name'])
                )

            # Add the field to use for sorting the models in the app section
            model_dict['app_position'] = app_position

            if perms.get('change') or perms.get('view'):
                model_dict['view_only'] = not perms.get('change')
                try:
                    model_dict['admin_url'] = reverse(
                        'admin:%s_%s_changelist' % info, current_app=self.name
                    )
                except NoReverseMatch:
                    pass
            if perms.get('add'):
                try:
                    model_dict['add_url'] = reverse(
                        'admin:%s_%s_add' % info, current_app=self.name
                    )
                except NoReverseMatch:
                    pass

            if app_label in app_dict:
                app_dict[app_label]['models'].append(model_dict)
            else:
                app_dict[app_label] = {
                    'name': apps.get_app_config(app_label).verbose_name,
                    'app_label': app_label,
                    'app_url': reverse(
                        'admin:app_list',
                        kwargs={'app_label': app_label},
                        current_app=self.name,
                    ),
                    'has_module_perms': has_module_perms,
                    'models': [model_dict],
                }

        self._move_user(app_dict)
        for key in app_dict.keys():
            app_dict[key]['models'].sort(key=lambda x: x['app_position'])

        if label:
            return app_dict.get(label)
        return app_dict

    def get_urls(self) -> List[Union[URLResolver, URLPattern]]:
        urls = super().get_urls()
        if not AUTH_USER_MODEL:
            return urls
        for x, url in enumerate(urls):
            name = url.pattern.describe()
            name = name.strip("'")
            name = name.replace('/', '.')[:-1]
            # Change the route pattern so that User exists under auth.
            if name == AUTH_USER_MODEL.lower():
                route_parts = url.pattern._route.split('/')
                route_parts[0] = 'auth'
                new_route = '/'.join(route_parts)
                urls[x].pattern._route = new_route
        return urls

    @staticmethod
    def _find_group_positions(
            app_dict: Dict[str, Any]
    ) -> Tuple[Optional[str], Optional[int]]:
        """Find the description of the Group model.
        """
        if not AUTH_USER_MODEL:
            return None, None
        app = app_dict.get('auth', dict())
        if not app:
            return None, None
        for group_index, model in enumerate(app['models']):
            if model['name'].lower() == 'groups':
                return 'auth', group_index
        return None, None

    @staticmethod
    def _find_user_positions(
            app_dict: Dict[str, Any]
    ) -> Tuple[Optional[str], Optional[int]]:
        """Find the description of the User model.
        """
        if not AUTH_USER_MODEL:
            return None, None
        key = AUTH_USER_MODEL.split('.')[0]
        app = app_dict.get(key, dict())
        if not app:
            return None, None
        for model_index, model in enumerate(app['models']):
            if model['model_name'] == AUTH_USER_MODEL:
                return key, model_index
        return None, None

    def _move_user(self, app_dict: Dict[str, Any]) -> None:
        if not AUTH_USER_MODEL:
            return None
        group_pos = self._find_group_positions(app_dict)
        user_pos = self._find_user_positions(app_dict)
        for pos in group_pos:
            if pos is None:
                return None
        for pos in user_pos:
            if pos is None:
                return None

        group_pos = cast(Tuple[str, int], group_pos)
        user_pos = cast(Tuple[str, int], user_pos)

        # pop off the user model description
        user = app_dict[user_pos[0]]['models'].pop(user_pos[1])

        # set the user app_position
        # user['app_position'] = 1

        # set the user urls
        # user['admin_url'] = "/admin/auth/user/"
        # user['add_url'] = "/admin/auth/user/add/"

        # set the group app_position
        # app_dict[group_pos[0]]['models'][group_pos[1]]['app_position'] = 2

        # add the User model description to the app that contains the Group
        app_dict[group_pos[0]]['models'].insert(group_pos[1], user)

        # Remove the User app description if there are no longer any models
        if len(app_dict[user_pos[0]]['models']) < 1:
            del app_dict[user_pos[0]]

    def get_app_list(self, request: HttpRequest) -> Sequence[str]:
        """
        Return a sorted list of all the installed apps that have been
        registered in this site.
        """
        app_dict = self._build_app_dict(request)

        # Sort the apps alphabetically.
        app_list = sorted(app_dict.values(), key=lambda x: x['name'].lower())

        return app_list

    def each_context(self, request: HttpRequest) -> Dict[str, Any]:
        """
        Return a dictionary of variables to put in the template context for
        *every* page in the admin site.

        For sites running on a sub-path, use the SCRIPT_NAME value if site_url
        hasn't been customized.
        """
        script_name = request.META['SCRIPT_NAME']
        if self.site_url == '/' and script_name:
            site_url = script_name
        else:
            site_url = self.site_url
        return {
            'site_title': self.site_title,
            'site_header': self.site_header,
            'site_url': site_url,
            'has_permission': self.has_permission(request),
            'available_apps': self.get_app_list(request),
            'apps_dict': self._build_app_dict(request),
        }

    def app_index(
            self,
            request: HttpRequest,
            app_label: str,
            extra_context: Optional[Dict[str, Any]] = None
    ) -> TemplateResponse:
        app_dict = self._build_app_dict(request, app_label)
        print(app_dict)
        if not app_dict:
            raise Http404('The requested admin page does not exist.')
        app_name = apps.get_app_config(app_label).verbose_name
        context = {
            **self.each_context(request),
            'title': _('%(app)s administration') % {'app': app_name},
            'app_list': [app_dict],
            'app_label': app_label,
            'apps_dict': app_dict,
            ** (extra_context or {}),
        }

        request.current_app = self.name

        return TemplateResponse(request, self.app_index_template or [
            'admin/%s/app_index.html' % app_label,
            'admin/app_index.html'
        ], context)


site = AdminSite()
