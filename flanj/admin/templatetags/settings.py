from collections.abc import Sequence
from typing import (
    Any,
    Dict,
    NamedTuple,
    Optional,
    Tuple,
)

from django import template
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.template import Context
# noinspection Mypy
from django.template.base import (
    Parser,
    Token,
    TemplateSyntaxError,
    Node,
)
from django.utils import timezone

# noinspection Mypy
from flutils.namedtupleutils import to_namedtuple
# noinspection Mypy
from flutils.validators import validate_identifier


register = template.Library()
User = get_user_model()

# noinspection PyProtectedMember
_AUTH_GROUP_APP_VERBOSE_NAME = Group._meta.app_config.verbose_name

# noinspection PyProtectedMember
_AUTH_GROUP_APP_LABEL = Group._meta.app_label

# noinspection PyProtectedMember
_AUTH_USER_APP_LABEL = User._meta.app_label

# noinspection PyProtectedMember
_AUTH_USER_VERBOSE_NAME_PLURAL = User._meta.verbose_name_plural

_MANDATORY_SETTINGS_ATTRIBUTES = (
    'COPYRIGHT_YEAR',
    'AUTH_GROUP_APP_VERBOSE_NAME',
    'AUTH_GROUP_APP_LABEL',
    'AUTH_USER_APP_LABEL',
    'AUTH_USER_VERBOSE_NAME_PLURAL',
)


def _build_allowed_settings_attributes() -> Tuple[str, ...]:
    items = getattr(
        settings,
        'TEMPLATE_ALLOWED_SETTINGS_ATTRIBUTES',
        _MANDATORY_SETTINGS_ATTRIBUTES,
    )
    if isinstance(items, Sequence):
        if hasattr(items, 'capitalize'):
            items = [items]
    else:
        items = _MANDATORY_SETTINGS_ATTRIBUTES

    items = set(items)
    for item in _MANDATORY_SETTINGS_ATTRIBUTES:
        items.add(item)
    return tuple(sorted(items))


_ALLOWED_SETTINGS_ATTRIBUTES = _build_allowed_settings_attributes()


def _build_settings_for_templates() -> NamedTuple:
    hold: Dict[str, Any] = {}
    for attr_name in _ALLOWED_SETTINGS_ATTRIBUTES:
        val = getattr(settings, attr_name, '___no_value___')
        if val != '___no_value___':
            hold[attr_name] = val
        else:
            if attr_name == 'COPYRIGHT_YEAR':
                hold[attr_name] = timezone.now().strftime('%Y')
            elif attr_name == 'AUTH_GROUP_APP_VERBOSE_NAME':
                key = 'AUTH_GROUP_APP_VERBOSE_NAME'
                hold[key] = _AUTH_GROUP_APP_VERBOSE_NAME
            elif attr_name == 'AUTH_GROUP_APP_LABEL':
                key = 'AUTH_GROUP_APP_LABEL'
                hold[key] = _AUTH_GROUP_APP_LABEL
            elif attr_name == 'AUTH_USER_APP_LABEL':
                key = 'AUTH_USER_APP_LABEL'
                hold[key] = _AUTH_USER_APP_LABEL
            elif attr_name == 'AUTH_USER_VERBOSE_NAME_PLURAL':
                key = 'AUTH_USER_VERBOSE_NAME_PLURAL'
                hold[key] = _AUTH_USER_VERBOSE_NAME_PLURAL
    out = to_namedtuple(hold)
    return out


# noinspection PySameParameterValue
class GetSettingsNode(Node):
    def __init__(
            self,
            variable: str,
            name: Optional[str] = None
    ) -> None:
        self.variable = variable
        if name is None:
            self.name = variable
        else:
            self.name = name

    def render(self, context: Context) -> str:
        context[self.name] = _build_settings_for_templates()
        return ''


# noinspection PyUnusedFunction,PyUnusedLocal
@register.tag
def get_settings(
        parser: Parser,
        token: Token,
) -> GetSettingsNode:
    # token.split_contents() isn't useful here because this ver
    # does NOT accept variables as arguments.
    args = token.contents.split()
    if len(args) == 1:
        name = 'settings'
    elif len(args) == 3 and args[1].lower() == 'as' and args[2]:
        name = args[2].strip('"\'')
    else:
        raise TemplateSyntaxError(
            "get_settings [as IDENTIFIER] (got %r)"
            % token.contents
        )
    try:
        validate_identifier(name)
    except SyntaxError:
        raise TemplateSyntaxError(
            '%r invalid identifier %r' % (
                token.contents,
                name
            )
        )

    return GetSettingsNode('settings', name=name)


class GetAuthUserModelNode(Node):
    def __init__(
            self,
            variable: str,
            name: Optional[str] = None
    ) -> None:
        self.variable = variable
        if name is None:
            self.name = variable
        else:
            self.name = name

    def render(self, context: Context) -> str:
        model_name = getattr(settings, 'AUTH_USER_MODEL', '')
        model_name_parts = model_name.split('.')
        hold = {}
        if len(model_name_parts) > 1:
            hold['user_app_label'] = model_name_parts[0].lower()
            hold['user_model_name'] = model_name_parts[1].lower()

        context[self.name] = hold.get(self.variable, '')
        return ''


# noinspection PyUnusedFunction,PyUnusedLocal
@register.tag('get_auth_user_model')
def get_auth_user_model(
        parser: Parser,
        token: Token
) -> GetAuthUserModelNode:

    # token.split_contents() isn't useful here because this ver does NOT
    # accept variables as arguments
    args = token.contents.split()
    if len(args) != 2 and len(args) != 4:
        raise TemplateSyntaxError(
            "'get_auth_user_model' VARIABLE_NAME [as NAME] (got %r)" % args
        )
    if len(args) == 4 and args[2] != 'as':
        raise TemplateSyntaxError(
            "'get_auth_user_model' VARIABLE_NAME [as NAME] (got %r)" % args
        )
    variable = args[1]
    name = None
    if len(args) == 4:
        name = args[3]
    return GetAuthUserModelNode(variable, name=name)
