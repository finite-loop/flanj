import operator
from collections.abc import Sequence
from functools import reduce
from typing import (
    Any,
    Dict,
    Generator,
    List,
    Optional,
    Tuple,
    Union,
)

from django.contrib.admin import ModelAdmin
from django.contrib.admin.filters import (
    BooleanFieldListFilter,
    RelatedFieldListFilter,
    ChoicesFieldListFilter,
)
from django.contrib.admin.utils import lookup_needs_distinct
from django.contrib.admin.views.main import ChangeList
from django.contrib.admin.views.main import SEARCH_VAR
from django.core.exceptions import FieldDoesNotExist
from django.db.models import (
    Model,
    Q,
)
from django.db.models.constants import LOOKUP_SEP
from django.db.models.fields import Field
from django.db.models.query import QuerySet
from django.http import HttpRequest
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _


def build_filter_is_last(
        field_path: str,
        admin_filter_fields: Tuple[str, ...]
) -> bool:
    last = len(admin_filter_fields) - 1
    return admin_filter_fields.index(field_path) == last


# noinspection PyUnresolvedReferences
def get_empty_choice_display(
        obj: '_Mixin',
        model_admin: ModelAdmin
) -> str:
    try:
        out = obj.empty_choice_display
    except AttributeError:
        pass
    else:
        if hasattr(out, 'capitalize') and out:
            return out
    try:
        out = model_admin.empty_choice_display
    except AttributeError:
        pass
    else:
        if hasattr(out, 'capitalize') and out:
            return out
    out = model_admin.get_empty_value_display()
    if out == '-':
        return '(NONE)'
    return out


# noinspection PyUnresolvedReferences
def get_non_empty_choice_display(
        obj: '_Mixin',
        model_admin: ModelAdmin,
        empty_choice_display: Optional[str] = None
) -> str:
    try:
        out = obj.non_empty_choice_display
    except AttributeError:
        pass
    else:
        if hasattr(out, 'capitalize') and out:
            return out
    try:
        out = model_admin.non_empty_choice_display
    except AttributeError:
        pass
    else:
        if hasattr(out, 'capitalize') and out:
            return out

    if hasattr(empty_choice_display, 'capitalize') and empty_choice_display:
        out = empty_choice_display
    else:
        out = get_empty_choice_display(obj, model_admin)

    surround_vals = (
        ('(', ')'),
        ('[', ']'),
        ('{', '}'),
        None,
    )
    surround = None
    for surround in surround_vals:
        if surround:
            if out.startswith(surround[0]) and out.endswith(surround[1]):
                break
    if surround:
        out = out.lstrip(surround[0]).rstrip(surround[1])

    if out.istitle():
        out = 'Not %s' % out
    elif out.isupper():
        out = 'NOT %s' % out
    else:
        out = 'not %s' % out
    if surround:
        out = '{}%s{}' % out
        out = out.format(*surround)
    return mark_safe(out)


def build_search_field(field_name: str, queryset: QuerySet) -> str:
    if field_name.startswith('^'):
        return "%s__istartswith" % field_name[1:]
    elif field_name.startswith('='):
        return "%s__iexact" % field_name[1:]
    elif field_name.startswith('@'):
        return "%s__search" % field_name[1:]
    # Use field_name if it includes a lookup.
    # noinspection PyProtectedMember
    opts = queryset.model._meta
    lookup_fields = field_name.split(LOOKUP_SEP)
    # Go through the fields, following all relations.
    prev_field = None
    for path_part in lookup_fields:
        if path_part == 'pk':
            path_part = opts.pk.name
        try:
            field = opts.get_field(path_part)
        except FieldDoesNotExist:
            # Use valid query lookups.
            if prev_field and prev_field.get_lookup(path_part):
                return field_name
        else:
            prev_field = field
            if hasattr(field, 'get_path_info'):
                # Update opts to follow the relation.
                opts = field.get_path_info()[-1].to_opts
    # Otherwise, use the field with icontains.
    return "%s__icontains" % field_name


def build_search_fields(
        request: HttpRequest,
        model_admin: ModelAdmin,
        queryset: QuerySet,
        search: Optional[str] = None
) -> Tuple[str, ...]:
    if search is None:
        search = request.GET.get(SEARCH_VAR, '')
    if not search:
        return ()
    search_fields = model_admin.get_search_fields(request)
    if not search_fields:
        return ()
    return tuple([
        build_search_field(str(field_name), queryset)
        for field_name in search_fields
    ])


def _choices_filter_search(
        request: HttpRequest,
        model_admin: ModelAdmin,
        queryset: Optional[QuerySet] = None,
        use_distinct: bool = False
) -> Tuple[QuerySet, bool]:
    if queryset is None:
        queryset = model_admin.get_queryset(request)
    search = request.GET.get(SEARCH_VAR, '')
    if not search:
        return queryset, use_distinct
    fields = build_search_fields(request, model_admin, queryset, search)
    for bit in search.split():
        or_queries = [Q(**{field: bit}) for field in fields]
        queryset = queryset.filter(reduce(operator.or_, or_queries))
    # noinspection PyProtectedMember
    opts = queryset.model._meta
    use_distinct |= any(
        lookup_needs_distinct(opts, field) for field in fields
    )
    return queryset, use_distinct


def get_filter_names(
        request: HttpRequest,
        model_admin: ModelAdmin,
) -> Tuple[str, ...]:
    out = []
    for name, _ in model_admin.get_list_filter(request):
        out.append(name)
    if out:
        return tuple(out)
    return ()


def _each_field(name: str, request: HttpRequest) -> Generator[str, None, None]:
    for key in request.GET.keys():
        if key.startswith(str(name)):
            yield key


def _get_val(field: str, request: HttpRequest) -> Union[str, None]:
    val = request.GET.get(field, None)
    if not isinstance(val, Sequence):
        return None
    if not hasattr(val, 'capitalize'):
        if len(val):
            val = val[0]
        else:
            return None
    val = val.strip()
    if not val:
        return None
    return val


def _choices_filter(
        request: HttpRequest,
        model_admin: ModelAdmin,
        queryset: Optional[QuerySet] = None,
        use_distinct: bool = False
) -> Tuple[QuerySet, bool]:
    if queryset is None:
        queryset = model_admin.get_queryset(model_admin)
    fields = set()
    names = get_filter_names(request, model_admin)
    for name in names:
        for field in _each_field(name, request):
            val = _get_val(field, request)
            if val is None:
                continue
            if val.lower() == 'true':
                val = True
            elif val.lower() == 'false':
                val = False
            fields.add(field)
            queryset = queryset.filter(**{field: val})
    # noinspection PyProtectedMember
    opts = queryset.model._meta
    use_distinct |= any(
        lookup_needs_distinct(opts, field) for field in list(fields)
    )
    return queryset, use_distinct


def get_choices_queryset(
        request: HttpRequest,
        model_admin: ModelAdmin,
        queryset: Optional[QuerySet] = None,
        use_distinct: bool = False
) -> Tuple[QuerySet, bool]:
    # import ipdb
    # ipdb.set_trace()
    queryset, use_distinct = _choices_filter_search(
        request,
        model_admin,
        queryset=queryset,
        use_distinct=use_distinct
    )
    queryset, use_distinct = _choices_filter(
        request,
        model_admin,
        queryset=queryset,
        use_distinct=use_distinct
    )
    return queryset, use_distinct


# noinspection PyUnusedFunction,PyUnusedName
class _Mixin:

    template = 'admin/filter_dropdown.html'
    empty_choice_display = ''
    non_empty_choice_display = ''

    def __init__(
            self,
            field: Field,
            request: HttpRequest,
            params: Dict[str, Any],
            model: Model,
            model_admin: ModelAdmin,
            field_path: str
    ) -> None:

        self.admin_filter_names: Tuple[str, ...] = get_filter_names(
            request,
            model_admin
        )
        self.filter_index = self.admin_filter_names.index(field_path)
        self.filter_is_last: bool = build_filter_is_last(
            field_path,
            self.admin_filter_names
        )
        super().__init__(field, request, params, model, model_admin, field_path)
        self.lookup_null_choices: List[Tuple[str, str]] = \
            self.field_null_choices(field, request, model_admin)

        if hasattr(self, '_post_init'):
            self._post_init(
                field,
                request,
                params,
                model,
                model_admin,
                field_path
            )

    # noinspection PyUnresolvedReferences,PyUnusedLocal
    def field_null_choices(
            self,
            field: Field,
            request: HttpRequest,
            model_admin: ModelAdmin
    ) -> List[Tuple[str, str]]:
        out = []
        if self.include_empty_choice is True:
            empty_choice_display = get_empty_choice_display(
                self,
                model_admin
            )
            non_empty_choice_display = get_non_empty_choice_display(
                self,
                model_admin,
                empty_choice_display=empty_choice_display
            )
            qs, use_distinct = get_choices_queryset(request, model_admin)
            if use_distinct is True:
                qs = qs.distinct()
            if self.value() == 'True':
                out.append(('True', empty_choice_display))
            elif self.value() == 'False':
                out.append(('False', non_empty_choice_display))
            else:
                key = '%s__isnull' % self.field_path
                if qs.filter(**{key: True}).count():
                    out.append(('True', empty_choice_display))
                if qs.filter(**{key: False}).count():
                    out.append(('False', non_empty_choice_display))
        return out

    # noinspection PyMethodMayBeStatic
    def has_output(self) -> bool:
        return True

    # noinspection PyUnresolvedReferences
    def value(self) -> Union[str, None]:
        if self.include_empty_choice is True:
            if self.lookup_val_isnull is not None:
                return str(self.lookup_val_isnull)
        if self.lookup_val is not None:
            return str(self.lookup_val)
        return None

    # noinspection PyUnresolvedReferences
    def choices(
            self,
            changelist: ChangeList
    ) -> Generator[Dict[str, Any], None, None]:
        selected_value = self.value()
        yield {
            'selected': selected_value is None,
            'query_string': changelist.get_query_string(
                remove=[
                    self.lookup_kwarg,
                    self.lookup_kwarg_isnull
                ]
            ),
            'display': _('All'),
        }
        if selected_value in ('True', 'False', None):
            for val_str, display in self.lookup_null_choices:
                yield {
                    'selected': selected_value == val_str,
                    'query_string': changelist.get_query_string(
                        {self.lookup_kwarg_isnull: val_str},
                        [self.lookup_kwarg]
                    ),
                    'display': display,
                }
        if selected_value != 'True':
            for pk, display in self.lookup_choices:
                yield {
                    'selected': selected_value == str(pk),
                    'query_string': changelist.get_query_string(
                        {self.lookup_kwarg: pk},
                        [self.lookup_kwarg_isnull]
                    ),
                    'display': display,
                }


# noinspection PyUnusedClass, PyUnusedFunction
class RelatedChainDropdownFilter(_Mixin, RelatedFieldListFilter):

    def field_choices(
            self,
            field: Field,
            request: HttpRequest,
            model_admin: ModelAdmin
    ) -> List[Tuple[int, str]]:
        name = '%s__pk' % self.field_path
        qs, __ = get_choices_queryset(request, model_admin)
        qs = qs.values_list(name, flat=True).distinct()
        pks = tuple(set(qs))
        ordering = ()
        # noinspection PyProtectedMember
        related_admin = model_admin.admin_site._registry.get(
            field.remote_field.model
        )
        if related_admin is not None:
            ordering = related_admin.get_ordering(request)
        # noinspection PyArgumentList
        return field.get_choices(
            include_blank=False,
            limit_choices_to={'pk__in': pks},
            ordering=ordering
        )


# noinspection PyUnusedClass,PyUnusedFunction
class ChoicesChainDropdownFilter(_Mixin, ChoicesFieldListFilter):

    # noinspection PyUnusedLocal
    def _post_init(
            self,
            field: Field,
            request: HttpRequest,
            params: Dict[str, Any],
            model: Model,
            model_admin: ModelAdmin,
            field_path: str
    ) -> None:
        self.lookup_choices = self.field_choices(field, request, model_admin)

    @property
    def include_empty_choice(self) -> bool:
        return self.field.null

    def field_choices(
            self,
            field: Field,
            request: HttpRequest,
            model_admin: ModelAdmin
    ) -> List[Tuple[int, str]]:
        out = []
        name = '%s' % self.field_path
        qs, __ = get_choices_queryset(request, model_admin)
        qs = qs.values_list(name, flat=True).distinct()
        ids = tuple(set(qs))

        choices = field.choices
        for choice in sorted(choices, key=lambda x: x[1]):
            if choice[0] in ids:
                out.append(choice)
        return out



# noinspection PyUnusedClass,PyUnusedFunction
class BooleanChainDropdownFilter(_Mixin, BooleanFieldListFilter):

    # noinspection PyUnusedLocal
    def _post_init(
            self,
            field: Field,
            request: HttpRequest,
            params: Dict[str, Any],
            model: Model,
            model_admin: ModelAdmin,
            field_path: str
    ) -> None:
        self.lookup_choices = self.field_choices(field, request, model_admin)

    @property
    def lookup_kwarg_isnull(self) -> str:
        return self.lookup_kwarg2

    @property
    def lookup_val_isnull(self) -> Any:
        return self.lookup_val2

    @property
    def include_empty_choice(self) -> bool:
        return self.field.null

    # noinspection PyUnusedLocal
    def field_choices(
            self,
            field: Field,
            request: HttpRequest,
            model_admin: ModelAdmin
    ) -> List[Tuple[str, str]]:
        out = []
        qs, used_distinct = get_choices_queryset(request, model_admin)
        qs = qs.values_list(self.field_path, flat=True)
        if used_distinct is False:
            qs = qs.distinct()
        vals = set(qs)

        if True in vals:
            out.append(('1', _('Yes')))
        if False in vals:
            out.append(('0', _('No')))
        return out



