import os
from typing import (
    Union,
)
from django.contrib.admin import ModelAdmin
from django.contrib.admin.views.main import ChangeList
from django.db.models import Model
from django.http import HttpRequest


templates_dir = os.path.join(
    os.path.dirname(__file__),
    'templates'
)


class MatviewChangeList(ChangeList):

    def get_list_model(self) -> Union[Model, None]:
        try:
            return self.model_admin.list_model
        except AttributeError:
            return None

    def get_filters(self, request: HttpRequest):
        list_model = self.get_list_model()
        if list_model is not None:
            model = self.model
            self.model = list_model
            out = super().get_filters(request)
            self.model = model
            return out
        return super().get_filters(request)


# noinspection PyUnusedClass
class MatviewModelAdmin(ModelAdmin):

    def get_list_model(self) -> Union[Model, None]:
        try:
            return self.list_model
        except AttributeError:
            return None

    def get_changelist(self, request: HttpRequest, **kwargs):
        """
        Return the ChangeList class for use on the changelist page.
        """
        return MatviewChangeList


default_app_config = 'flanj.admin.apps.AdminConfig'
