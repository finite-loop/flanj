from typing import (
    Any,
    Union,
    cast,
)
from collections.abc import (
    Iterable,
    Iterator,
    Mapping,
)


from django.conf import settings
from django.contrib.auth.models import (
    Group,
    Permission,
)
from flanj.models import GroupIdentifier
from .types import (
    GroupSetting,
)


_PERMISSION_ERROR = (
    "Permission must be in the format <app_label>.<codename> Got: {!r} "
    "To see a list of available permissions run: "
    "./manage.py show_permissions"
)


def _split_permission(permission: str) -> tuple[str, str]:
    permission = permission.strip()
    parts = tuple(map(lambda x: x.strip(), permission.split('.')))
    if len(parts) != 2:
        raise ValueError(_PERMISSION_ERROR.format(permission))
    if not parts[0] or not parts[1]:
        raise ValueError(_PERMISSION_ERROR.format(permission))
    return parts[0], parts[1]


def _validate_permissions(
        settings_key: str,
        i: int,
        permissions: Iterable[str],
        simple_validate: bool = False,
) -> None:
    if (not isinstance(permissions, Iterable) or
            hasattr(permissions, 'capitalize')):
        raise TypeError(
            f'settings.{settings_key}[{i}][permissions]'
            f' must be a list or a tuple'
        )

    for j, permission in enumerate(permissions):
        if not hasattr(permission, 'capitalize'):
            raise TypeError(
                f'settings.{settings_key}[{i}][permissions][{j}] '
                f'must be a str'
            )
        try:
            app_label, codename = _split_permission(permission)
        except ValueError as e:
            raise ValueError(
                f'settings.{settings_key}[{i}][permissions][{j}] '
                f'{e!s}'
            )

        if simple_validate is not True:
            try:
                Permission.objects.get(
                    content_type__app_label=app_label,
                    codename=codename,
                )
            except Permission.DoesNotExist:
                raise ValueError(
                    f'settings.{settings_key}[{i}][permissions][{j}] '
                    f'the permission of {permission!r} does NOT exist.'
                )


def _validate_str(
        settings_key: str,
        i: int,
        setting: dict[str, Any],
        key: str,
        current_values: Union[list[str], None] = None,
) -> None:
    if key not in setting.keys():
        raise ValueError(
            f'settings.{settings_key}[{i}][{key}]'
            f' must be set'
        )
    value = setting[key].strip()
    if not value:
        raise ValueError(
            f'settings.{settings_key}[{i}][{key}]'
            f' cannot be empty'
        )
    if current_values is not None:
        current_values = cast(list[str], current_values)
        if value.lower() in current_values:
            raise ValueError(
                f'settings.{settings_key}[{i}][{key}] = {value!r}'
                f' must be unique'
            )
        else:
            current_values.append(value.lower())


def validate_groups_settings(
        settings_key: str = 'GROUPS',
        settings_val: Union[Iterable[dict[str, Any]], None] = None,
        simple_validate: bool = False
) -> None:
    if settings_val is None:
        settings_val = getattr(settings, settings_key, ())
    if (not isinstance(settings_val, Iterable) or
            hasattr(settings_val, 'capitalize')):
        raise TypeError(
            f'settings.{settings_key} must be a list or a tuple'
        )
    settings_val = cast(Iterable[dict[str, Any]], settings_val)
    current_identifiers = []
    current_group_names = list(map(
        lambda x: x.name.lower(),
        Group.objects.filter(groupidentifier=None).all()
    ))
    for i, setting in enumerate(settings_val):
        if not isinstance(setting, Mapping):
            raise TypeError(
                f'settings.{settings_key}[{i}] must be a dict'
            )
        _validate_str(
            settings_key,
            i,
            setting,
            'name',
            current_values=current_group_names
        )
        _validate_str(
            settings_key,
            i,
            setting,
            'identifier',
            current_values=current_identifiers
        )
        if 'permissions' not in setting.keys():
            raise ValueError(
                f'settings.{settings_key}[{i}][permissions]'
                f' must be set'
            )
        _validate_permissions(
            settings_key,
            i,
            setting['permissions'],
            simple_validate=simple_validate
        )


def _as_permission_object(permission: str) -> Permission:
    app_label, codename = _split_permission(permission)
    return Permission.objects.get(
        content_type__app_label=app_label,
        codename=codename,
    )


def each_group_setting(
        settings_key: str = 'GROUPS',
        settings_val: Union[Iterable[dict[str, Any]], None] = None
) -> Iterable[GroupSetting]:
    if settings_val is None:
        settings_val = getattr(settings, settings_key, ())
    settings_val = cast(Iterable[dict[str, Any]], settings_val)
    for setting in settings_val:
        kwargs = {
            'name': setting['name'].strip(),
            'identifier': setting['identifier'].strip(),
        }
        permissions: tuple[Permission, ...] = tuple(
            map(lambda x: _as_permission_object(x), setting['permissions'])
        )
        kwargs['permissions'] = permissions
        yield GroupSetting(**kwargs)
