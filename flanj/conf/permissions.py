from collections.abc import (
    Iterator,
    Iterable,
    Mapping,
)
from typing import (
    Any,
    Type,
    Union,
    cast,
)

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission

from .types import (
    PermissionSetting,
    PermissionSettingDict,
)
from django.conf import settings

BUILTIN_SENTINELS: tuple[str, ...] = (
    '',
    '_existing_',
    '_builtin_'
)


def _validate_content_type(
        settings_key: str,
        i: int,
        app_label: str,
        model: str,
) -> None:
    try:
        ContentType.objects.get_by_natural_key(app_label, model)
    except ContentType.DoesNotExist:
        raise ValueError(
            f"settings.{settings_key}[{i}][content_type_app_label] with a "
            f"value of {app_label!r} and " 
            f"settings.{settings_key}[{i}][content_type_model] with a "
            f"value of {model!r}, are invalid. Unable to find a ContentType " 
            f"having an app_label of {app_label!r} and a model of {model!r} "
            f"Run: './manage.py show_content_types' and choose a valid "
            f"APP_LABEL, model pair."
        )


def _validate_builtin_permissions(
        settings_key: str,
        i: int,
        setting: PermissionSettingDict,
) -> None:
    if setting['permission_name'] not in BUILTIN_SENTINELS:
        return
    app_label = setting['content_type_app_label']
    model = setting['content_type_model']
    content_type = ContentType.objects.get_by_natural_key(app_label, model)
    exists = Permission.objects.filter(
        codename=setting['permission_codename'],
        content_type=content_type,
    ).exists()
    if exists is not True:
        raise ValueError(
            f'settings.{settings_key}[{i}] is set to be a pre-existing '
            f'built-in permission. However this permission does not '
            f'exist. Double check these values against the output of '
            f'manage.py show_permissions'
        )


def _validate_unique(
        settings_key: str,
        i: int,
        setting: dict[str, Any],
        key: str,
        current_values: Union[list[str], list[int]],
) -> None:
    value: Union[str, int] = setting[key]
    if hasattr(value, 'capitalize'):
        value = cast(str, value)
        current_values = cast(list[str], current_values)
        if key == 'permission_codename':
            app_label: str = setting['content_type_app_label']
            val = f'{app_label}.{value}'
            if val in current_values:
                raise ValueError(
                    f'settings.{settings_key}[{i}][{key}] has the value '
                    f'{value!r}. When combined with content_type_app_label '
                    f'the value is {val!r}. This combined value must be '
                    f'unique.'
                )
            current_values.append(val)
            return
        if key == 'permission_name' and value in BUILTIN_SENTINELS:
            return
        if value in current_values:
            raise ValueError(
                f'settings.{settings_key}[{i}][{key}] the value, '
                f'{value!r}, must be unique.'
            )
        current_values.append(value)
        return

    value = cast(int, value)
    current_values = cast(list[int], current_values)
    if value in current_values:
        raise ValueError(
            f'settings.{settings_key}[{i}][{key}] the value, '
            f'{value}, must be unique.'
        )


def validate_permissions_settings(
        settings_key: str = 'PERMISSIONS',
        settings_val: Union[Iterable[dict[str, Any]], None] = None
) -> None:
    if settings_val is None:
        settings_val = getattr(settings, settings_key, ())
    if (not isinstance(settings_val, Iterable) or
            hasattr(settings_val, 'capitalize')):
        raise TypeError(
            f'settings.{settings_key} must be a list or a tuple'
        )
    settings_val = cast(Iterable[dict[str, Any]], settings_val)
    current_unique_values = {
        'frontend_id': [],
        'permission_name': [],
        'permission_codename': [],
    }

    for i, setting in enumerate(settings_val):
        if not isinstance(setting, Mapping):
            raise TypeError(
                f'settings.{settings_key}[{i}] must be a dict'
            )

        setting_keys: tuple[str, ...] = tuple(setting.keys())
        permission_annotations = PermissionSettingDict.__annotations__
        permission_key: str
        permission_type: Type
        for permission_key, permission_type in permission_annotations.items():
            if permission_key not in setting_keys:
                raise ValueError(
                    f'settings.{settings_key}[{i}][{permission_key}]'
                    f' must be set'
                )
            setting_val: Union[str, int, bool] = setting[permission_key]
            if not isinstance(setting_val, permission_type):
                permission_type_name: str = permission_type.__name__
                raise TypeError(
                    f'settings.{settings_key}[{i}][{permission_key}]'
                    f' must be of type {permission_type_name!r}.'
                )
            if hasattr(setting_val, 'capitalize'):
                setting_val = cast(str, setting_val)
                setting_val = setting_val.strip()
                if not setting_val:
                    if permission_key != 'permission_codename':
                        raise ValueError(
                            f'settings.{settings_key}[{i}][{permission_key}]'
                            f' cannot be empty.'
                        )
        for permission_key in current_unique_values.keys():
            _validate_unique(
                settings_key,
                i,
                setting,
                permission_key,
                current_unique_values[permission_key],
            )
        _validate_content_type(
            settings_key,
            i,
            setting['content_type_app_label'],
            setting['content_type_model']
        )
        _validate_builtin_permissions(
            settings_key,
            i,
            setting
        )


def each_permission_setting(
        settings_key: str = 'PERMISSIONS',
        settings_val: Union[Iterable[dict[str, Any]], None] = None
) -> Iterator[PermissionSetting]:
    if settings_val is None:
        settings_val = getattr(settings, settings_key, ())
    settings_val = cast(Iterable[dict[str, Any]], settings_val)
    for setting in settings_val:
        kwargs = setting.copy()
        kwargs['content_type'] = ContentType.objects.get_by_natural_key(
            kwargs['content_type_app_label'],
            kwargs['content_type_model']
        )
        yield PermissionSetting(**kwargs)
