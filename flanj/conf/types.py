from collections.abc import Iterable
from typing import (
    NamedTuple,
    TypedDict,
)

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission


class PermissionSettingDict(TypedDict):
    frontend_id: int
    permission_name: str
    permission_codename: str
    content_type_app_label: str
    content_type_model: str


class PermissionSetting(NamedTuple):
    frontend_id: int
    permission_name: str
    permission_codename: str
    content_type_app_label: str
    content_type_model: str
    content_type: ContentType


class GroupSettingDict(TypedDict):
    identifier: str
    name: str
    permissions: Iterable[str]


class GroupSetting(NamedTuple):
    identifier: str
    name: str
    permissions: tuple[Permission, ...]
