import os
import logging
from datetime import (
    datetime,
    tzinfo,
)
from typing import (
    Optional,
    cast,
)

import time
import pytz
from logging import LogRecord

from django.core.management.color import color_style

try:
    from functools import cached_property
except ImportError:
    from flutils import cached_property


class Formatter(logging.Formatter):

    def __init__(
            self,
            fmt: Optional[str] = None,
            datefmt: Optional[str] = None,
            style: Optional[str] = None,
            timezone: Optional[str] = None,
    ) -> None:

        self._timezone: str = timezone or 'UTC'
        # noinspection Mypy
        style: str = style or '{'
        # noinspection Mypy
        super().__init__(fmt=fmt, datefmt=datefmt, style=style)

    # noinspection PyUnusedName
    converter = time.gmtime
    default_time_format = '%Y-%m-%d %H:%M:%S,%f %z'

    @cached_property
    def _tz(self) -> tzinfo:
        return pytz.timezone(self._timezone)

    def formatTime(
            self,
            record: LogRecord,
            datefmt: Optional[str] = None
    ) -> str:
        # The record.created property is a float generated from
        # time.time() which could localtime. This needs to converted
        # into UTC.
        #
        utc = datetime.fromtimestamp(record.created, pytz.UTC)
        if str(self._tz) == 'UTC':
            loc = utc
        else:
            loc = utc.astimezone(self._tz)

        if datefmt:
            if datefmt == 'iso':
                out = loc.isoformat()
            else:
                out = loc.strftime(datefmt)
        else:
            out = loc.strftime(self.default_time_format)
        return out


# noinspection PyUnusedClass
class ServerFormatter(Formatter):
    def __init__(self, *args, **kwargs) -> None:
        self.style = color_style()
        super().__init__(*args, **kwargs)

    def format(self, record) -> str:
        msg = record.msg
        status_code = getattr(record, 'status_code', None)

        if status_code:
            if 200 <= status_code < 300:
                # Put 2XX first, since it should be the common case
                msg = self.style.HTTP_SUCCESS(msg)
            elif 100 <= status_code < 200:
                msg = self.style.HTTP_INFO(msg)
            elif status_code == 304:
                msg = self.style.HTTP_NOT_MODIFIED(msg)
            elif 300 <= status_code < 400:
                msg = self.style.HTTP_REDIRECT(msg)
            elif status_code == 404:
                msg = self.style.HTTP_NOT_FOUND(msg)
            elif 400 <= status_code < 500:
                msg = self.style.HTTP_BAD_REQUEST(msg)
            else:
                # Any 5XX, or any other status code
                msg = self.style.HTTP_SERVER_ERROR(msg)

        # Normally the record.server_time is set and formatted in
        # django.core.servers.basehttp.WSGIRequestHandler.log_message
        # However, the following will force formatting on record.server_time
        # giving control back to the formatters-datefmt as set in
        # settings.LOGGING
        if self.uses_server_time is True:
            record.server_time = self.formatTime(record, self.datefmt)

        record.msg = msg
        return super().format(record)

    @cached_property
    def uses_server_time(self) -> bool:
        # Changed this to a cached property to make life a little easier.
        if hasattr(self._fmt, 'capitalize') is True:
            self._fmt = cast(str, self._fmt)
            return self._fmt.find('server_time') >= 0
        return False
