import os
import toml


__all__ = [
    '__version__',
    'templatetags',
    'env',
]


def _version_pyproject() -> str:
    path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    # noinspection PyUnresolvedReferences
    path = os.path.join(path, 'pyproject.toml')
    try:
        info = toml.load(path)
    except FileNotFoundError:
        return ''

    try:
        name = info['tool']['poetry']['name']
    except KeyError:
        return ''

    if name != __name__:
        return ''

    try:
        return info['tool']['poetry']['version']
    except KeyError:
        return ''


def _version_pkg_resources() -> str:
    try:
        from pkg_resources import (
            get_distribution,
            DistributionNotFound,
            Distribution,
        )
    except ImportError:
        return ''

    try:
        info: Distribution = get_distribution(__name__)
    except DistributionNotFound:
        return ''

    return info.version


__version__ = _version_pyproject() or _version_pkg_resources()
