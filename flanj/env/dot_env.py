import os
from copy import copy
from typing import (
    Dict,
    Generator,
    List,
    Optional,
    Sequence,
    Set,
    Tuple,
    Union,
)
from dotenv import dotenv_values


def _process_name(
        hold: Set[str],
        name: str,
        is_dirname: bool = False,
) -> None:
    """Add the given ``name`` to the given ``hold`` if the given
    name is not empty.  If ``is_dirname`` is ``True`` then validate
    that the given name exists as a directory and add it to ``hold``.
    """
    name = name.strip()
    if name:
        if is_dirname is True:
            name = os.path.expanduser(name)
            name = os.path.expandvars(name)
            name = os.path.normpath(name)
            if name.startswith(os.sep) and os.path.isdir(name):
                hold.add(name)
        else:
            hold.add(name)


def _process_names(
        hold: Set[str],
        value: Sequence,
        is_dirname: bool = False,
) -> None:
    """Adds the given sequence of strings to the given hold"""
    if isinstance(value, Sequence):
        if hasattr(value, 'capitalize'):
            _process_name(hold, value, is_dirname=is_dirname)
        else:
            for item in value:
                if hasattr(item, 'capitalize'):
                    _process_name(hold, item, is_dirname=is_dirname)


def _each_name(
        value: Optional[Sequence] = None
) -> Generator[str, None, None]:
    """Yield each sorted unique str from given value"""
    hold = set()
    if isinstance(value, Sequence):
        _process_names(hold, value)
    for name in sorted(hold):
        yield name


DOTENV_DEFAULT_BASENAMES = (
    '.env',
    '.env.django',
    'env',
    'env.django',
)


def _basenames(
        basenames: Optional[Sequence] = None,
) -> Tuple[str, ...]:
    """Return each possible dotenv filename as a tuple of strings."""
    hold = set()
    if not isinstance(basenames, Sequence):
        return DOTENV_DEFAULT_BASENAMES
    _process_names(hold, basenames)
    return tuple(sorted(hold))


def _process_path(hold: Set[str], value: Sequence) -> None:
    """Adds the given sequence of path strings to hold after splitting
    on ``:``
    """
    if isinstance(value, Sequence):
        if hasattr(value, 'capitalize'):
            _process_names(hold, value.split(':'), is_dirname=True)
        else:
            for item in value:
                if hasattr(item, 'capitalize'):
                    _process_names(hold, item.split(':'), is_dirname=True)


def _each_path_dirname(
        paths: Optional[Sequence] = None,
        env_varnames: Optional[Sequence] = None,
) -> Generator[str, None, None]:
    """Yield the full path each possible directory that may contain
    a dotenv file.
    """
    hold = set()
    for name in _each_name(env_varnames):
        _process_path(
            hold,
            os.environ.get(name, ''),
        )
    _process_path(hold, paths)
    for dirname in sorted(hold):
        yield dirname


def _each_dotenv_filepath(
        env_varnames: Optional[Sequence] = None,
        paths: Optional[Sequence] = None,
        basenames: Optional[Sequence] = None,
) -> Generator[str, None, None]:
    dotenv_basenames = _basenames(basenames=basenames)
    for dirname in _each_path_dirname(paths=paths, env_varnames=env_varnames):
        for name in dotenv_basenames:
            path = os.path.join(dirname, name)
            if (os.path.isfile(path) and
                os.path.getsize(path) and
                os.access(path, os.R_OK)
            ):
                yield path


def values(
        env_varnames: Optional[Sequence] = None,
        paths: Optional[Sequence] = None,
        basenames: Optional[Sequence] = None,
) -> Dict[str, str]:
    """Get a dictionary of all the dotenv values."""
    out = {}
    for filepath in _each_dotenv_filepath(
        env_varnames=env_varnames,
        paths=paths,
        basenames=basenames,
    ):
        out.update(dotenv_values(dotenv_path=filepath))
    return out

