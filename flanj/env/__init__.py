import json
import os
import shutil
import sys
from collections.abc import (
    Iterable,
    Mapping,
    Sequence,
)
from distutils.version import LooseVersion
from functools import (
    cached_property,
    singledispatch,
)
from importlib import import_module
from site import getsitepackages
from types import (
    ModuleType,
    SimpleNamespace,
)
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Optional,
    Tuple,
    Union,
    cast,
)

# noinspection Mypy
import appdirs
# noinspection PyPep8Naming
from django import __version__ as _DJANGO_VERSION
from flutils.validators import validate_identifier

from flanj.utils.objs import is_str
from flanj.utils.strs import sequence_to_str
from . import dot_env
from . import rnd
from .exceptions import (
    ConfigurationKeyError,
    ConfigurationTypeError,
    ConfigurationValueError,
    SettingsTypeError,
    SettingsValueError,
)
from .homes import get_home
from .manifests import (
    Manifest,
    build_manifest,
)
from .url import (
    ParseUrl,
    Url,
)
from .utils import data_size
from .validators import (
    validate_admins,
    validate_default_sequence,
    validate_directory_is_read_write,
    validate_file_is_read_write,
    validate_groups,
    validate_time_zone,
    validate_url,
)

__all__ = [
    '_Environment',
    'Manifest',
    'quote_value',
    'Env',
    'manifests'
]


_ver_tup = Tuple[Union[int, str], ...]

DJANGO_FULL_VERSION: _ver_tup = tuple(LooseVersion(_DJANGO_VERSION).version)

# noinspection PyUnusedName
DJANGO_MINOR_VERSION: _ver_tup = DJANGO_FULL_VERSION[:2]

# noinspection DuplicatedCode
REQUIRED: str = '__required__'

# noinspection DuplicatedCode
EMPTY: str = '__empty__'

# noinspection DuplicatedCode
_DEFAULT: str = '__default__'

_BOOL_TRUE = ('true', '1', 'on')
_BOOL_FALSE = ('false', '0', 'off')
_BOOL_VALS = tuple(list(_BOOL_TRUE) + list(_BOOL_FALSE))
_CHARS = ('{', '[', '(', '}', ']', ')', ',', ':')

LOG_LEVELS: Tuple[str, ...] = (
    'CRITICAL',
    'ERROR',
    'WARNING',
    'INFO',
    'DEBUG',
    'NOTSET',
)

@singledispatch
def _deep_tuple(value: Any) -> Any:
    return value


# noinspection PyUnusedFunction
@_deep_tuple.register(Sequence)
def _deep_tuple_sequence(value: Sequence) -> Union[str, tuple]:
    if hasattr(value, 'encode'):
        value = cast(str, value)
        return value
    out: List[Any] = []
    for val in value:
        out.append(_deep_tuple(val))
    return tuple(out)


# noinspection PyUnusedFunction
@_deep_tuple.register(Mapping)
def _deep_tuple_mapping(value: Mapping) -> Mapping:
    out = {}
    for key, val in value.items():
        out[key] = _deep_tuple(val)
    return out


def func_name() -> str:
    """Return the name of the function that is calling this function."""
    # noinspection PyUnresolvedReferences,PyProtectedMember
    hold = sys._getframe(2).f_code.co_names
    return '.'.join(hold[-2:])


class EmailValueError(ValueError):
    pass


def _prev_is_backslash(value: str) -> bool:
    count = 0
    for c in reversed(value):
        if c == '\\':
            count += 1
        else:
            break
    if count % 2:
        return True
    return False


def _convert_word(word: str) -> Any:
    try:
        val_int = int(word)
    except (TypeError, ValueError):
        pass
    else:
        return str(val_int)

    try:
        val_float = float(word)
    except (TypeError, ValueError):
        pass
    else:
        return str(val_float)

    if word.lower() == 'false':
        return 'false'
    elif word.lower() == 'true':
        return 'true'
    elif word.lower() == 'none':
        return 'null'
    elif word.lower() == 'null':
        return 'null'
    else:
        word = word.replace('"', '\\"')
        word = '"{}"'.format(word)
        return word


def quote_value(value: str) -> str:
    out = ''
    word = ''
    for c in value:
        prev_is_backslash = _prev_is_backslash(word)
        if c in _CHARS and prev_is_backslash is False:
            if c in ('(', ')'):
                if c == '(':
                    c = '['
                else:
                    c = ']'
            word = word.strip(' ')
            if word:
                out += _convert_word(word)
                out += c
                word = ''
                continue
            else:
                out += c
                continue
        else:
            if prev_is_backslash is True:
                if c in _CHARS or c in ('"',):
                    word = word[:-1]
            else:
                if not word and out[-1:] in (',', ':'):
                    out += ' '
            word += c
    word = word.strip(' ')
    if word:
        out += _convert_word(word)

    out = out.strip()
    return out


_OPTIONAL_MODULE = Union[ModuleType, SimpleNamespace, None]
_UNION_MODULE = Union[ModuleType, SimpleNamespace]


class _Environment:

    REQUIRED: str = REQUIRED
    EMPTY: str = EMPTY

    # noinspection PySameParameterValue
    def __init__(
            self,
            package_name: str,
            settings_module: _OPTIONAL_MODULE = None,
            dotenv_values: Optional[Dict[str, str]] = None,
            settings_file: str = '',
            package_home: Optional[str] = None,
            package_data_home: Optional[str] = None,
            package_cache_home: Optional[str] = None,
            package_config_home: Optional[str] = None,
            package_cert_home: Optional[str] = None,
            package_cert_private_home: Optional[str] = None,
    ) -> None:
        self.package_name: str = package_name
        if settings_module:
            self.settings_module: _UNION_MODULE = settings_module
            if settings_file:
                self.settings_file: str = settings_file
            else:
                self.settings_file = _get_settings_file_path(
                    self.settings_module
                )
        else:
            self.settings_module = SimpleNamespace()
            self.settings_file = 'settings.py'
        self.dotenv: Dict[str, str] = dotenv_values or {}
        self._package_home: str = package_home or ''
        self._package_data_home: str = package_data_home or ''
        self._package_cache_home: str = package_cache_home or ''
        self._package_config_home: str = package_config_home or ''
        self._package_cert_home: str = package_cert_home or ''
        self._package_cert_private_home: str = package_cert_private_home or ''
        self._caller_name: str = self._get_caller_name()

    @cached_property
    def package_home(self) -> str:
        if self._package_home:
            self._package_home = os.path.expanduser(self._package_home)
            self._package_home = os.path.expandvars(self._package_home)
            if os.path.isdir(self._package_home):
                return self._package_home
        package_module = import_module(self.package_name)
        return os.path.dirname(os.path.abspath(package_module.__file__))

    # noinspection PyUnusedFunction
    @cached_property
    def package_is_installed(self) -> bool:
        for path in getsitepackages():
            if self.package_home.startswith(path):
                return True
        return False

    @cached_property
    def package_is_in_home(self) -> bool:
        return self.package_home.startswith(os.path.expanduser('~'))

    @cached_property
    def package_data_home_env_var(self) -> str:
        return '%s_DATA_HOME' % self.package_name.upper()

    @cached_property
    def package_data_home(self) -> str:
        default = '/var/opt/%s' % self.package_name
        if self.package_is_in_home is True:
            default = appdirs.user_data_dir(self.package_name)
        return get_home(
            'package_data_home',
            self._package_data_home,
            self.package_data_home_env_var,
            self.settings_file,
            default
        )

    @cached_property
    def package_config_home_env_var(self) -> str:
        return '%s_CONFIG_HOME' % self.package_name.upper()

    @cached_property
    def package_config_home(self) -> str:
        default = '/etc/opt/%s' % self.package_name
        if self.package_is_in_home is True:
            default = appdirs.user_config_dir(self.package_name)
        return get_home(
            'package_config_home',
            self._package_config_home,
            self.package_config_home_env_var,
            self.settings_file,
            default
        )

    @cached_property
    def package_cert_home_env_var(self) -> str:
        return '%s_CERT_HOME' % self.package_name.upper()

    @cached_property
    def package_cert_home(self) -> str:
        default = os.path.join(self.package_config_home, 'cert')
        return get_home(
            'package_cert_home',
            self._package_cert_home,
            self.package_cert_home_env_var,
            self.settings_file,
            default
        )

    @cached_property
    def package_cert_private_home_env_var(self) -> str:
        return '%s_CERT_PRIVATE_HOME' % self.package_name.upper()

    # noinspection PyUnusedFunction
    @cached_property
    def package_cert_private_home(self) -> str:
        default = os.path.join(self.package_cert_home, 'private')
        return get_home(
            'package_cert_private_home',
            self._package_cert_private_home,
            self.package_cert_private_home_env_var,
            self.settings_file,
            default
        )

    @cached_property
    def rnd_chars(self) -> Callable[..., str]:
        return rnd.chars

    @cached_property
    def parse_url(self) -> ParseUrl:
        return ParseUrl(
            self.package_data_home,
            self.package_config_home,
            self.package_cert_home,
            self._package_cert_private_home,
            package_name=self.package_name,
        )

    # noinspection PyUnusedFunction
    @cached_property
    def combined(self) -> Dict[str, str]:
        out = self.dotenv.copy()
        out.update(os.environ.copy())
        return out

    def _get_caller_name(self) -> str:
        """The variable name used to instantiate this class"""
        out = ''
        class_name = self.__class__.__name__

        for x in range(0, 5):
            # noinspection PyUnresolvedReferences,PyProtectedMember
            names = sys._getframe(x).f_code.co_names
            try:
                names.index(class_name)
            except ValueError:
                continue

            names = tuple(reversed(names))
            index = names.index(class_name)
            if index == 0:
                continue
            out = names[index - 1]
            break
        return out

    def _validate_key(
            self,
            key: str,
            function_name: str,
            arguments: Dict,
            keyword_arguments: Dict,
    ) -> None:

        try:
            validate_identifier(key)
        except TypeError as e:
            raise SettingsTypeError(
                function_name,
                arguments,
                keyword_arguments,
                'key',
                'str',
                self._caller_name,
                self.settings_file,
                has_empty=False,
                has_required=False,
                value=key,
                value_type=type(key).__name__,
                template_override=(
                   'When calling {signature}, the value of '
                   '{problem_arg_name!r} is an invalid identifier. '
                   'This value must be of type {expected_type_name!r}; '
                   'Got {value_type!r}. Please fix in {settings_file!r}.'
                )
            )
        except SyntaxError as e:
            raise SettingsValueError(
                function_name,
                arguments,
                keyword_arguments,
                'key',
                'str',
                self._caller_name,
                self.settings_file,
                has_empty=False,
                has_required=False,
                value=key,
                msg=str(e),
                template_override=(
                    'When calling {signature}, the value of '
                    '{problem_arg_name!r} is an invalid identifier. '
                    '{msg}. Please fix in {settings_file!r}.'
                )
            )

    def _validate_default(
            self,
            default: Any,
            method_name: str,
            arguments: Dict,
            keyword_arguments: Dict,
            expected_type: str,
            has_empty: bool = True,
            has_required: bool = True,
    ) -> None:
        types = (
            'str',
            'url',
            'bool',
            'int',
            'list',
            'tuple',
            'admins',
            'time_zone',
            'log_level',
            'json',
            'data_size',
            'groups',
        )
        if expected_type not in types:
            types_str = ', '.join(map(lambda x: f"'{x}'", types))
            raise ValueError(f"expected_type must be one of: {types_str}")

        if default in (EMPTY, REQUIRED):
            return

        if expected_type in (
                'str',
                'url',
                'time_zone',
                'log_level',
                'json',
                'data_size'
        ):
            if not is_str(default):
                raise SettingsTypeError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'str',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                )
            if expected_type == 'url':
                try:
                    validate_url(
                        default,
                        public=keyword_arguments.get('only_public', False)
                    )
                except ValueError as e:
                    raise SettingsValueError(
                        method_name,
                        arguments,
                        keyword_arguments,
                        'default',
                        'str',
                        self._caller_name,
                        self.settings_file,
                        has_empty=has_empty,
                        has_required=has_required,
                        additional_message=str(e)
                    )
            elif expected_type == 'time_zone':
                try:
                    validate_time_zone(default)
                except ValueError as e:
                    raise SettingsValueError(
                        method_name,
                        arguments,
                        keyword_arguments,
                        'default',
                        'str',
                        self._caller_name,
                        self.settings_file,
                        has_empty=has_empty,
                        has_required=has_required,
                        additional_message=str(e)
                    )
            elif expected_type == 'log_level':
                if default not in LOG_LEVELS:
                    raise SettingsValueError(
                        method_name,
                        arguments,
                        keyword_arguments,
                        'default',
                        'str',
                        self._caller_name,
                        self.settings_file,
                        has_empty=has_empty,
                        has_required=has_required,
                        value=default,
                        levels=', '.join(map(lambda x: f"'{x}'", LOG_LEVELS)),
                        additional_message=(
                            'The value {value!r} must be one of: {levels}'
                        )
                    )
            elif expected_type == 'json':
                if default.startswith(os.sep):
                    return
                try:
                    json.loads(default)
                except json.JSONDecodeError:
                    if not default.startswith(os.sep):
                        raise SettingsValueError(
                            method_name,
                            arguments,
                            keyword_arguments,
                            'default',
                            'str',
                            self._caller_name,
                            self.settings_file,
                            has_empty=has_empty,
                            has_required=has_required,
                            value=default,
                            additional_message=(
                                'The value, {value!r}, must be a string of '
                                'JSON or a string containing a full path to '
                                'a json file'
                            ),
                        )
            elif expected_type == 'data_size':
                try:
                    data_size(default)
                except ValueError as e:
                    raise SettingsValueError(
                        method_name,
                        arguments,
                        keyword_arguments,
                        'default',
                        'str',
                        self._caller_name,
                        self.settings_file,
                        has_empty=has_empty,
                        has_required=has_required,
                        value=default,
                        additional_message=str(e),
                    )

        elif expected_type == 'groups':
            try:
                validate_groups(default)
            except (ValueError, json.JSONDecodeError) as e:
                raise SettingsValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'str',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                    value=default,
                    additional_message=str(e),
                )
            except TypeError as e:
                raise SettingsTypeError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'str',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                    additional_message=str(e),
                )
        elif expected_type == 'bool':
            if not isinstance(default, bool):
                raise SettingsTypeError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'str',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                )
        elif expected_type == 'int':
            if not isinstance(default, int):
                raise SettingsTypeError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'int',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                )
        elif expected_type == 'list':
            try:
                validate_default_sequence(default, 'list')
            except TypeError as e:
                raise SettingsTypeError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'list',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                    additional_message=str(e),
                )
            except ValueError as e:
                raise SettingsValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'list',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                    additional_message=str(e)
                )
        elif expected_type == 'tuple':
            try:
                validate_default_sequence(default, 'tuple')
            except TypeError as e:
                raise SettingsTypeError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'tuple',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                    additional_message=str(e),
                )
            except ValueError as e:
                raise SettingsValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'tuple',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                    additional_message=str(e)
                )
        elif expected_type == 'admins':
            try:
                validate_admins(default)
            except TypeError as e:
                raise SettingsTypeError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'list or tuple',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                    additional_message=str(e),
                )
            except ValueError as e:
                raise SettingsValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'list or tuple',
                    self._caller_name,
                    self.settings_file,
                    has_empty=has_empty,
                    has_required=has_required,
                    additional_message=str(e)
                )

    def _validate_time_zone(
            self,
            time_zone: str,
            method_name: str,
            arguments: Dict,
            keyword_arguments: Dict,
    ) -> None:
        if time_zone is None:
            return
        if not is_str(time_zone):
            raise SettingsTypeError(
                method_name,
                arguments,
                keyword_arguments,
                'time_zone',
                'str',
                self._caller_name,
                self.settings_file,
                has_empty=False,
                has_required=False,
            )
        if time_zone:
            try:
                validate_time_zone(time_zone)
            except ValueError as e:
                raise SettingsValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'time_zone',
                    'str',
                    self._caller_name,
                    self.settings_file,
                    additional_message=str(e)
                )

    def get(
            self,
            key: str,
            default: str = EMPTY,
            _function_name: Optional[str] = None,
            _arguments: Optional[Dict] = None,
            _keyword_arguments: Optional[Dict] = None,
    ) -> str:
        """Return the str value for environment/dotenv key.

        Args:
            key (str): The environment or dotenv key.
            default (Any, optional): The default value to return, as a str.
                (A non string value will be converted to a string.) The value
                can also be ``EMPTY`` or ``REQUIRED``.  A value of ``EMPTY``
                will return ``''`` if the environment/dotenv value is not set.
                A value of ``REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set. Defaults to: ``EMPTY``.
            _function_name (str, optional): The name of the function calling
                this function. Defaults to ``None`` which assume that
                this function is called from ``settings.py``
            _arguments (Dict, optional): A dictionary containing the
                argument names and their values passed in.  Defaults to
                ``None``.
            _keyword_arguments (Dict, optional): A dictionary containing the
                keyword argument names and their values passed in.
                Defaults to ``None``.
        """
        if hasattr(_function_name, 'capitalize') and _function_name:
            _function_name = cast(str, _function_name)
            function_name: str = _function_name
            if hasattr(_arguments, 'items'):
                _arguments = cast(Dict, _arguments)
                arguments = _arguments
            else:
                arguments = {}
            if hasattr(_keyword_arguments, 'items'):
                _keyword_arguments = cast(Dict, _keyword_arguments)
                keyword_arguments = _keyword_arguments
            else:
                keyword_arguments = {}
        else:
            function_name = 'get'
            arguments = {'key': key}
            keyword_arguments = {'default': default}

        self._validate_key(
            key,
            function_name,
            arguments,
            keyword_arguments,
        )

        self._validate_default(
            default,
            function_name,
            arguments,
            keyword_arguments,
            'str',
        )

        if default == REQUIRED:
            val = self.dotenv.get(key, REQUIRED)
            if not val:
                val = REQUIRED
            val = os.environ.get(key, val)
            if not val:
                val = REQUIRED
            if val == REQUIRED:
                raise ConfigurationKeyError(
                    function_name,
                    arguments,
                    keyword_arguments,
                    'key',
                    'Any',
                    self._caller_name,
                    self.settings_file,
                    value=val,
                )
            return val

        elif default == EMPTY:
            val = self.dotenv.get(key, '')
            if not hasattr(val, 'capitalize'):
                val = ''
            return os.environ.get(key, val)

        else:
            val = self.dotenv.get(key, default)
            val = os.environ.get(key, val)
            if not is_str(val):
                val = str(val)
            return val.strip()

    def get_bool(
            self, key: str,
            default: Union[str, bool] = EMPTY
    ) -> bool:
        """Return the environment/dotenv bool value for the given key.

        Args:
            key (str): the environment or dotenv key.
            default (str or bool, optional): The default value to return if
                the environment/dotenv value is not set. The value can also be
                ``EMPTY`` or ``REQUIRED``.  A value of ``EMPTY`` will return
                ``False`` if the environment/dotenv value is not set.
                A value of ``REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set. Defaults to: ``EMPTY``.
        """
        method_name = 'get_bool'
        arguments = {'key': key}
        keyword_arguments = {'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'bool',
        )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        if not val:
            return False
        if val == _DEFAULT:
            default = cast(bool, default)
            return default
        if val in _BOOL_VALS:
            if val in _BOOL_TRUE:
                return True
            return False

        raise ConfigurationValueError(
            method_name,
            arguments,
            keyword_arguments,
            'None',
            'str',
            self._caller_name,
            self.settings_file,
            value=val,
            values=sequence_to_str(_BOOL_VALS),
            additional_message='The value must be one of: {values}'
        )

    def get_int(
            self,
            key: str,
            default: Union[str, bool] = EMPTY
    ) -> int:
        """Return the environment/dotenv int value for the given key.

        Args:
            key (str): the environment or dotenv key.
            default (str or int, optional): The default value to return if
                the environment/dotenv value is not set. The value can also be
                ``EMPTY`` or ``REQUIRED``.  A value of ``EMPTY`` will return
                ``0`` if the environment/dotenv value is not set.
                A value of ``REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set. Defaults to: ``EMPTY``.
        """
        method_name = 'get_int'
        arguments = {'key': key}
        keyword_arguments = {'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'int',
        )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        if not val:
            return 0
        if val == _DEFAULT:
            default = cast(int, default)
            return default
        try:
            return int(val)
        except ValueError as e:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'int',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=str(e)
            )
        except TypeError as e:
            raise ConfigurationTypeError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'int',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=str(e)
            )

    def get_list(
            self,
            key: str,
            default: Union[str, List[Any], Tuple[Any, ...]] = EMPTY
    ) -> List[Any]:
        """Return the environment/dotenv list value for the given key.

        Args:
            key (str): the environment or dotenv key.
            default (str or bool, optional): The default value to return if
                the environment/dotenv value is not set. The value can also be
                ``EMPTY`` or ``REQUIRED``.  A value of ``EMPTY`` will return
                ``[]`` if the environment/dotenv value is not set.
                A value of ``REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set. Defaults to: ``EMPTY``.
        """
        method_name = 'get_list'
        arguments = {'key': key}
        keyword_arguments = {'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'list',
        )
        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return []
        if val == _DEFAULT:
            default = cast(List[Any], default)
            return default

        try:
            ret = json.loads(val)
        except json.JSONDecodeError:
            pass
        else:
            if isinstance(ret, list):
                return ret

        arg = val
        if arg.startswith('[') is False:
            arg = '[{}'.format(arg)
        if arg.endswith(']') is False:
            arg = '{}]'.format(arg)

        quoted = quote_value(arg)
        try:
            out: list = json.loads(quoted)
        except json.JSONDecodeError:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'list',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=(
                    'This value is not in the proper format for a list.'
                )
            )
        return out

    def get_tuple(
            self,
            key: str,
            default: Union[str, List[str], Tuple[str, ...]] = EMPTY
    ) -> Tuple[Any, ...]:
        """Return the environment/dotenv tuple value for the given key.

        Args:
            key (str): the environment or dotenv key.
            default (str or bool, optional): The default value to return if
                the environment/dotenv value is not set. The value can also be
                ``EMPTY`` or ``REQUIRED``.  A value of ``EMPTY`` will return
                ``()`` if the environment/dotenv value is not set.
                A value of ``REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set. Defaults to: ``EMPTY``.
        """
        method_name = 'get_tuple'
        arguments = {'key': key}
        keyword_arguments = {'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'tuple',
        )
        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return ()
        if val == _DEFAULT:
            default = cast(Tuple[Any, ...], default)
            return default

        try:
            ret = json.loads(val)
        except json.JSONDecodeError:
            pass
        else:
            if isinstance(ret, list):
                return _deep_tuple(ret)

        arg = val
        if arg.startswith('[') is False:
            arg = '[{}'.format(arg)
        if arg.endswith(']') is False:
            arg = '{}]'.format(arg)

        quoted = quote_value(arg)
        try:
            out: list = json.loads(quoted)
        except json.JSONDecodeError:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'tuple',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=(
                    'This value is not in the proper format for a list.'
                )
            )
        return _deep_tuple(out)

    def get_url(
            self,
            key: str,
            default: str = EMPTY,
            only_public: bool = False,
    ) -> str:
        """Return the validated URL str value for the given
        environment/dotenv key.

        Args:
            key (str): the environment or dotenv key.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. The value can also be
                ``EMPTY`` or ``REQUIRED``.  A value of ``EMPTY`` will return
                an empty :obj:`~Url` object if the environment/dotenv value is
                not set. A value of ``REQUIRED`` will raise a :obj:`KeyError`
                if the environment/dotenv value is not set. Defaults to:
                ``EMPTY``.
            only_public (bool, optional): If set to :obj:`True` only public
                IP addresses will be allowed. If set to ``False`` then any
                IP address will be allowed. Defaults to: ``False``.
        """
        method_name = 'get_url'
        arguments = {'key': key}
        keyword_arguments = {'default': default, 'only_public': only_public}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        if not isinstance(only_public, bool):
            raise SettingsTypeError(
                method_name,
                arguments,
                keyword_arguments,
                'only_public',
                'bool',
                self._caller_name,
                self.settings_file,
                has_empty=False,
                has_required=False,
            )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'url',
        )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return ''
        if val == _DEFAULT:
            return default

        try:
            validate_url(val, public=only_public)
        except ValueError as e:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                additional_message=str(e)
            )
        return val

    @cached_property
    def default_database_url(self) -> str:
        path = os.path.join(
            self.package_data_home,
            f'{self.package_name}.sqlite3'
        )
        return f'sqlite://{path}'

    def get_database(
            self,
            key: str,
            default: str = EMPTY,
            time_zone: Optional[str] = None,
    ) -> Dict[str, Any]:
        """Return the environment/dotenv str value for the given key as a
        dictionary of values needed for the DATABASES setting.

        The value should be in a URL form e.g.
        ``sqlite://[package_data_home]/[package_name].sqlite3``


        Args:
            key (str): the environment or dotenv key.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. Defaults to:
                ``sqlite://[package_data_home]/[package_name].sqlite3``
            time_zone (str, optional): A string representing the timezone for
                datetimes stored in the database (assuming that the database
                doesn't support time zones) or :obj:`None`.  Defaults to
                :obj:`None`.
        """
        method_name = 'get_database'
        arguments = {'key': key}
        keyword_arguments = {'default': default, 'time_zone': time_zone}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'url',
        )
        self._validate_time_zone(
            time_zone,
            method_name,
            arguments,
            keyword_arguments,
        )

        if time_zone is None:
            time_zone = ''
        time_zone = cast(str, time_zone)

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        if val:
            if val == _DEFAULT:
                val = default
            else:
                try:
                    validate_url(val)
                except ValueError as e:
                    raise ConfigurationValueError(
                        method_name,
                        arguments,
                        keyword_arguments,
                        'None',
                        'str',
                        self._caller_name,
                        self.settings_file,
                        value=val,
                        additional_message=str(e)
                    )
        else:
            val = self.default_database_url

        out = {}
        url_obj: Url = self.parse_url(val)

        if url_obj.scheme == 'postgresql':
            out = {
                'ENGINE': 'django.db.backends.postgresql',
                'NAME': url_obj.database,
                'USER': url_obj.username,
                'PASSWORD': url_obj.password,
                'HOST': url_obj.host,
                'PORT': url_obj.port,
                'OPTIONS': url_obj.options,
                # The following are unused by django or psycopg2
                'SCHEMA': url_obj.schema,
                'URL': url_obj.url
            }
        elif url_obj.scheme == 'mysql':
            out = {
                'ENGINE': 'django.db.backends.mysql',
                'NAME': url_obj.database,
                'USER': url_obj.username,
                'PASSWORD': url_obj.password,
                'HOST': url_obj.host,
                'PORT': url_obj.port,
                'OPTIONS': url_obj.options,
                # The following are unused by django or psycopg2
                'SCHEMA': url_obj.schema,
                'URL': url_obj.url
            }

        elif url_obj.scheme == 'sqlite':
            if url_obj.path:
                out = {
                    'ENGINE': 'django.db.backends.sqlite3',
                    'PASSWORD': url_obj.password,
                    'TIME_ZONE': time_zone,
                    # The following is unused by django
                    'URL': url_obj.url
                }

                if os.path.isabs(url_obj.path):
                    path = url_obj.path
                else:
                    path = os.path.join(
                        self.package_data_home,
                        url_obj.path
                    )
                if os.path.isfile(path):
                    validate_file_is_read_write(path)
                else:
                    dirname = os.path.dirname(path)
                    if not os.path.exists(dirname):
                        if self.package_is_in_home is True:
                            os.makedirs(dirname, mode=0o700)
                        else:
                            os.makedirs(dirname, mode=0o770)
                    validate_directory_is_read_write(dirname)
                out['NAME'] = path

        else:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                value=val,
                scheme=url_obj.scheme,
                additional_message=(
                    'Unable to configure the database because the scheme '
                    '{scheme!r} is unknown.'
                )
            )

        if out and time_zone:
            out['TIME_ZONE'] = time_zone

        return out

    def set_email(
            self,
            key: str,
            default: str = EMPTY,
    ) -> None:
        """Sets the Django settings ``EMAIL_*`` variables.

        This is done by first extracting an email URL from the environment/
        dotenv then converting into an :obj:`Url` object; then using the
        values in the object to set the variables.

        Args:
            key (str): the environment or dotenv key.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. This value should be
                the email connection URL credentials.
        """
        method_name = 'set_email'
        arguments = {'key': key}
        keyword_arguments = {'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'url',
        )
        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return
        if val == _DEFAULT:
            val = default
        else:
            try:
                validate_url(val)
            except ValueError as e:
                raise ConfigurationValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'None',
                    'str',
                    self._caller_name,
                    self.settings_file,
                    additional_message=str(e)
                )

        obj = self.parse_url(val)

        if obj.scheme in ('smtp', 'smtps',):
            out = {
                'EMAIL_BACKEND': 'django.core.mail.backends.smtp.EmailBackend',
                'EMAIL_HOST': obj.host,
                'EMAIL_HOST_PASSWORD': obj.password,
                'EMAIL_HOST_USER': obj.username,
                'EMAIL_PORT': obj.port,
                'EMAIL_USE_TLS': obj.uses_ssl
            }

            for key in sorted(out.keys()):
                setattr(self.settings_module, key, out[key])
            return

        raise ConfigurationValueError(
            method_name,
            arguments,
            keyword_arguments,
            'None',
            'str',
            self._caller_name,
            self.settings_file,
            value=val,
            scheme=obj.scheme,
            additional_message=(
                'Unable to set the email server the database because the '
                'scheme {scheme!r} is invalid.'
            )
        )

    # noinspection PySameParameterValue
    def get_manifest(
            self,
            key: str = 'MANIFEST',
            default: Optional[str] = EMPTY,
            time_zone: Optional[str] = None,
    ) -> Manifest:
        """Returns a :obj:`~flanj.utils.manifest.Manifest` object that
        contains release information.

        Args:
            key (str, optional): the environment or dotenv key that holds
                the path to:

                1. a directory that may contain, the project's ``.git``
                   directory, or a manifest.toml file. If neither is found
                   then the path's parent directories will be searched.

                   If after searching the path's parent directory neither the
                   ``.git`` directory or a ``manifest.toml`` file are found,
                   a :obj:`~flanj.utils.manifest.Manifest` object will be
                   returned with default values.

                   If the directory contains both the ``.git`` sub-directory
                   and a ``manifest.toml`` file. The ``.git`` directory
                   will be used.

                2. the project's ``.git/`` directory.
                3. a ``manifest.toml`` file.

                Defaults to ``'MANIFEST``

            default (str, optional): The path to use if the given ``key``
                environment/dotenv variable does not exist or has no value.
                Defaults to ``EMPTY`` which will use the value of
                ``self.package_home``.
            time_zone (str, optional): A string of the time zone name which
                is used to adjust any timestamps in the manifest.
                Timestamps are stored in the manifest as UTC and setting
                this option will adjust those timestamps.  Defaults to:
                :obj:`None` which will keep all timestamps in the manifest
                at UTC.


        Any paths, given this method, can use:

        1. ``~/`` to represent HOME
        2. environment variables (e.g. ${{HOME}})

        Use the ``manage.py build-manifest`` command on build the
        ``manifest.toml`` file.
        """
        method_name = 'get_manifest'
        arguments = {}
        keyword_arguments = {
            'key': key,
            'default': default,
            'time_zone': time_zone,
        }

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'str',
        )
        self._validate_time_zone(
            time_zone,
            method_name,
            arguments,
            keyword_arguments,
        )

        if time_zone is None:
            time_zone = ''
        time_zone = cast(str, time_zone)

        if default == EMPTY:
            default = self.package_home

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        if val == _DEFAULT:
            val = default

        return build_manifest(
            self.package_name,
            path=val,
            time_zone=time_zone,
            called_from_settings=True,
        )

    def get_path(
            self,
            key: str,
            default: str = EMPTY,
            **kwargs: Any
    ) -> str:
        """Return a filesystem path as set in the environment/dotenv value for
        the given ``key``.

        The path will be normalized and user expanded.

        Args:
            key (str): the environment or dotenv key.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. This can also be set to
                ``env.EMPTY`` or ``env.REQUIRED``.  A value of ``env.EMPTY``
                will return ``''`` if the environment/dotenv value is not set.
                A value of ``env.REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set.  Defaults to: ``EMPTY``.
            **kwargs: Can be used to do variable substitution on the path.
        """
        method_name = 'get_path'
        arguments = {'key': key}
        keyword_arguments = {'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'str',
        )
        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        if val == _DEFAULT:
            val = default

        if not val:
            return val

        val = os.path.expanduser(val)
        val = os.path.expandvars(val)
        val = os.path.normpath(val)
        if kwargs:
            val = val.format(**kwargs)
        return val

    def get_admins(
            self,
            key: str = 'ADMINS',
            default: Union[str, List[Tuple[str, str]]] = EMPTY,
    ) -> List[Tuple[str, str]]:
        """For the given ``key``, return the admins as defined in
        https://docs.djangoproject.com/en/3.2/ref/settings/#admins

        Args:
            key (str, optional): the environment or dotenv key. Defaults to
                ``'ADMINS'``.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. This can also be set to
                ``env.EMPTY`` or ``env.REQUIRED``.  A value of ``env.EMPTY``
                will return ``''`` if the environment/dotenv value is not set.
                A value of ``env.REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set.  Defaults to: ``EMPTY``.
        """
        method_name = 'get_admins'
        arguments = {}
        keyword_arguments = {'key': key, 'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'admins',
        )
        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return ()
        if val == _DEFAULT:
            default = cast(Tuple[Tuple[str, str], ...], default)
            return _deep_tuple(default)

        try:
            ret = json.loads(val)
        except json.JSONDecodeError:
            arg = val
            if arg.startswith('[') is False:
                arg = '[{}'.format(arg)
            if arg.endswith(']') is False:
                arg = '{}]'.format(arg)

            quoted = quote_value(arg)
            try:
                ret = json.loads(quoted)
            except json.JSONDecodeError:
                raise ConfigurationValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'None',
                    'list or tuple',
                    self._caller_name,
                    self.settings_file,
                    value=val,
                    additional_message=(
                        'This value is not in the proper format for a list.'
                    )
                )
        try:
            validate_admins(ret)
        except TypeError as e:
            raise ConfigurationTypeError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'list or tuple',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=str(e)
            )
        except ValueError as e:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'list or tuple',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=str(e)
            )
        return list(_deep_tuple(ret))

    def get_time_zone(
            self,
            key: str = 'TIME_ZONE',
            default: str = EMPTY,
    ) -> str:
        """Return a validated time zone as set in the environment/dotenv
        value for the given ``key``.

        Args:
            key (str, optional): the environment or dotenv key. Defaults to
            ``'TIME_ZONE'``.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. This can also be set to
                ``env.EMPTY`` or ``env.REQUIRED``.  A value of ``env.EMPTY``
                will return ``''`` if the environment/dotenv value is not set.
                A value of ``env.REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set.  Defaults to: ``EMPTY``.
        """
        method_name = 'get_time_zone'
        arguments = {}
        keyword_arguments = {'key': key, 'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'time_zone',
        )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return ''
        if val == _DEFAULT:
            return default

        try:
            validate_time_zone(val)
        except ValueError as e:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=str(e)
            )
        return val

    def get_log_level(
            self,
            key: str = 'LOG_LEVEL',
            default: str = EMPTY,
    ) -> str:
        """Return a validated log level as set in the environment/dotenv
        value for the given ``key``.

        A valid value must be one of:
        ``CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET``

        Args:
            key (str, optional): the environment or dotenv key. Defaults to
            ``'LOG_LEVEL'``.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. This can also be set to
                ``env.EMPTY`` or ``env.REQUIRED``.  A value of ``env.EMPTY``
                will return ``''`` if the environment/dotenv value is not set.
                A value of ``env.REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set.  Defaults to: ``EMPTY``.

        """
        method_name = 'get_log_level'
        arguments = {}
        keyword_arguments = {'key': key, 'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        if is_str(default) and default not in (EMPTY, REQUIRED):
            default = default.upper()

        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'log_level',
        )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return ''
        if val == _DEFAULT:
            return default
        val = val.upper()
        if val not in LOG_LEVELS:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                value=val,
                levels=', '.join(map(lambda x: f"'{x}'", LOG_LEVELS)),
                additional_message=(
                    'The value must be one of: {levels}'
                )
            )
        return val

    def get_executable_path(
            self,
            key: str,
            default: str = EMPTY,
    ) -> str:
        """Return a validated path to an executable as set in the
        environment/dotenv value for the given ``key``.

        A valid value must be one of:
        ``CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET``

        Args:
            key (str): the environment or dotenv key.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. This can also be set to
                ``env.EMPTY`` or ``env.REQUIRED``.  A value of ``env.EMPTY``
                will return ``''`` if the environment/dotenv value is not set.
                A value of ``env.REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set.  Defaults to: ``EMPTY``.
                If this is set to only the executable name (without the
                full path to the executable), and the environment variable
                value returned nothing then the given 'default' value will
                be searched for.
        """
        method_name = 'get_executable_path'
        arguments = {}
        keyword_arguments = {'key': key, 'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'str',
        )

        if default.find(os.sep) > -1:
            if default[0] != os.sep:
                raise SettingsValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'default',
                    'str',
                    self._caller_name,
                    self.settings_file,
                    additional_message=(
                        'The value, {default!r}, must be a full path or only '
                        'the name of the executable.'
                    )
                )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return ''
        if val == _DEFAULT:
            if default.find(os.sep) == -1:
                out = shutil.which(default)
                if out:
                    return out
                raise ConfigurationValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'None',
                    'str',
                    self._caller_name,
                    self.settings_file,
                    value=val,
                    additional_message=(
                        'Unable to find the executable: {value!r}'
                    )
                )
            else:
                val = default

        if val[0] != os.sep:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message='The value must be a full path',
            )
        if not os.path.exists(val):
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message='The given path, {value!r} does not exist.',
            )
        if not os.path.isfile(val):
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message='The given path, {value!r} must be a file.',
            )
        if not os.access(val, os.X_OK):
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=(
                    'The given path, {value!r} is not an executable.'
                ),
            )
        return val

    def get_json(
            self,
            key: str,
            default: str = EMPTY,
            **kwargs: str,
    ) -> Union[Dict, List[Any]]:
        """Return a json decoded data.

        The given key is the name of the environment variable that can
        contain either, the JSON data itself or, the full path to a file
        that contains json data.

        Args:
            key (str): the environment or dotenv key.
            default (str, optional): The default value to use if the
                environment/dotenv value is not set. This can also be set to
                ``env.EMPTY`` or ``env.REQUIRED``.  A value of ``env.EMPTY``
                will return ``''`` if the environment/dotenv value is not set.
                A value of ``env.REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set.  Defaults to: ``EMPTY``.

                This can be set to a string of json data or a string
                containing the full path to a JSON file.
            **kwargs: Can be used to do variable substitution on the path.
        """
        method_name = 'get_json'
        arguments = {}
        keyword_arguments = {'key': key, 'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        if default.startswith(('~', '$', os.sep)):
            default = os.path.expanduser(default)
            default = os.path.expandvars(default)

        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'json',
        )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return {}
        if val == _DEFAULT:
            val = default
        if kwargs:
            val = val.format(**kwargs)
        pth = os.path.expanduser(val)
        pth = os.path.expandvars(pth)
        if pth.startswith(os.sep):
            if os.path.isfile(pth):
                try:
                    with open(pth) as f:
                        data = json.load(f)
                except json.JSONDecodeError as e:
                    raise ConfigurationValueError(
                        method_name,
                        arguments,
                        keyword_arguments,
                        'None',
                        'json',
                        self._caller_name,
                        self.settings_file,
                        value=val,
                        additional_message=str(e),
                    )
                else:
                    return data
            else:
                raise ConfigurationValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'None',
                    'json',
                    self._caller_name,
                    self.settings_file,
                    value=val,
                    additional_message=(
                        'Unable to load JSON file, {value!r} because it '
                        'does NOT exist.'
                    ),
                )

        else:
            try:
                data = json.loads(val)
            except json.JSONDecodeError as e:
                raise ConfigurationValueError(
                    method_name,
                    arguments,
                    keyword_arguments,
                    'None',
                    'json',
                    self._caller_name,
                    self.settings_file,
                    value=val,
                    additional_message=str(e),
                )
            else:
                return data

    def get_data_size(
            self,
            key: str,
            default: str = EMPTY
    ) -> int:
        """Return the environment/dotenv human readable data size value, for
        the given key, and return the number of bytes

        Args:
            key (str): the environment or dotenv key.
            default (str or int, optional): The default value to return if
                the environment/dotenv value is not set. The value can also be
                ``EMPTY`` or ``REQUIRED``.  A value of ``EMPTY`` will return
                ``0`` if the environment/dotenv value is not set.
                A value of ``REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set. Defaults to: ``EMPTY``.
        """
        method_name = 'get_data_size'
        arguments = {'key': key}
        keyword_arguments = {'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'data_size',
        )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        if not val:
            return 0
        if val == _DEFAULT:
            val = default
        try:
            return data_size(val)
        except ValueError as e:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'str',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=str(e)
            )

    def get_groups(
            self,
            key: str = 'GROUPS',
            default: Union[str, Iterable[dict[str, Any]]] = EMPTY,
            **kwargs: str,
    ) -> Iterable[dict[str, Any]]:
        """Return tuple of dicts representing the groups for the instance.

        The given key is the name of the environment variable that can
        contain either, the JSON data itself or, the full path to a file
        that contains json data.

        Args:
            key (str optional): the environment or dotenv key. Defaults to:
                ``'GROUPS'``
            default (str, Iterable[dict[str, Any] optional): The default
                value to use if the environment/dotenv value is not set. This
                can also be set to ``env.EMPTY`` or ``env.REQUIRED``.
                A value of ``env.EMPTY`` will return ``()`` if the
                environment/dotenv value is not set.
                A value of ``env.REQUIRED`` will raise a :obj:`KeyError` if the
                environment/dotenv value is not set.  Defaults to: ``EMPTY``.

                This can be set to a string of json data or a string
                containing the full path to a JSON file.
                This can also be an Iterable[dict[str, Any]
            **kwargs: Can be used to do variable substitution on the path.
        """
        method_name = 'get_groups'
        arguments = {}
        keyword_arguments = {'key': key, 'default': default}

        self._validate_key(
            key,
            method_name,
            arguments,
            keyword_arguments,
        )
        if hasattr(default, 'capitalize'):
            if default.startswith(('~', os.sep)):
                default = os.path.expanduser(default)
            default = os.path.expandvars(default)
            if kwargs:
                default = default.format(**kwargs)

        self._validate_default(
            default,
            method_name,
            arguments,
            keyword_arguments,
            'groups',
        )

        if default in (EMPTY, REQUIRED):
            _default = default
        else:
            _default = _DEFAULT

        val = self.get(
            key,
            _default,
            method_name,
            arguments,
            keyword_arguments,
        )
        val = val.strip()
        if not val:
            return ()
        if val == _DEFAULT:
            if hasattr(default, 'capitalize'):
                val = default
            else:
                return default
        if kwargs:
            val = val.format(**kwargs)
        val = os.path.expanduser(val)
        val = os.path.expandvars(val)
        try:
            return validate_groups(val, return_value=True)
        except (
                IsADirectoryError,
                PermissionError,
                FileNotFoundError
        ) as e:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'json',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=str(e),
            )
        except json.JSONDecodeError as e:
            raise ConfigurationValueError(
                method_name,
                arguments,
                keyword_arguments,
                'None',
                'json',
                self._caller_name,
                self.settings_file,
                value=val,
                additional_message=(
                    'Unable to decode JSON file, {value!r}: %s' % str(e)
                )
            )




def _get_settings_file_path(module: _UNION_MODULE) -> str:
    """Return the full path to the file that contains the given module."""
    out = 'settings.py'
    if module:
        if hasattr(module, '__file__'):
            hold = os.path.abspath(
                os.path.abspath(module.__file__)
            )
            if hold:
                out = hold
    return out


class Env(_Environment):

    """Provides methods that read values from the environment."""

    # noinspection PyMissingConstructor
    def __init__(
            self,
            package_name: str,
            settings_module: _UNION_MODULE,
            dotenv_varnames: Optional[Sequence] = None,
            dotenv_paths: Optional[Sequence] = None,
            dotenv_basenames: Optional[Sequence] = None,
            default_db_home: Optional[str] = None,
            package_home: Optional[str] = None,
            package_data_home: Optional[str] = None,
            package_config_home: Optional[str] = None,
            package_cert_home: Optional[str] = None,
            package_cert_private_home: Optional[str] = None,
    ) -> None:
        """
        Args:
            package_name (str): The name of the application this is the name
                of the package as set in the ``pyproject.toml`` file
            settings_module (:obj:`types.ModuleType`): The ``settings.py``
                module. This should always be set to ``sys.modules[__name__]``.
                this is needed so that the ``set_email`` method can properly
                configure the ``EMAIL_*`` values.
            dotenv_varnames (optional, :obj:`collections.abc.Sequence`): The
                environment variable name(s) which may have a value of each
                directory path (colon ``:`` separated). These paths will
                search for and load any dotenv files.
                Defaults to: None, which means no environment variables are
                used.
            dotenv_paths (optional, :obj:`collections.abc.Sequence`): The
                directory path(s) that may contain dotenv files. Any path can
                be user expandable (e.g. ``~/some_dir``) and environment
                variable expandable (e.g. ``$HOME/some_dir``). Defaults to:
                :obj:`None`.
            dotenv_basenames (optional, :obj:`collections.abc.Sequence`): The
                filename(s) (not the full path) of the dotenv files to search
                for. Defaults to: ``('.env', '.env.django', 'env',
                'env.django')``
            package_data_home (optional, str): The path of the directory that
                will hold the package data (e.g. default sqlite database).
                Defaults to: :obj:`None` which will use the value set in the
                environment variable ``[package_name.upper()]_DATA_HOME``; or,
                if the package is installed in a system directory, then
                `/var/opt/[package_name]/lib` will be used; or, if the
                package is installed in a user directory, then the user's
                data directory will be used.
            package_config_home (optional, str): The path of the directory that
                will hold any package config data. Defaults to: :obj:`None`
                which will use the value set in the environment variable
                ``[package_name.upper()]_CONFIG_HOME``; or, if the package is
                installed in a system directory, then
                ``/etc/opt/[package_name]/lib`` will be used; or, if the
                package is installed in a user directory, then the user's
                config directory will be used.
            package_cert_home (optional, str): The path of the directory that
                will hold any package certificates. Defaults to: :obj:`None`
                which will use the value set in the environment variable
                ``[package_name.upper()]_CERT_HOME``; or, if the package is
                installed in a system directory, then
                ``[package_config_home]/cert`` will be used.
            package_cert_private_home (optional, str): The path of the
                directory that will hold any package private keys. Defaults
                to: :obj:`None` which will use the value set in the environment
                variable ``[package_name.upper()]_CERT_PRIVATE_HOME``; or, if
                the package is installed in a system directory, then
                ``[package_cert_home]/private`` will be used.


        """
        self.package_name: str = package_name
        self.settings_module: _UNION_MODULE = settings_module
        self.settings_file = _get_settings_file_path(self.settings_module)
        self.dotenv_varnames: Optional[Sequence] = dotenv_varnames
        self.dotenv_paths: Optional[Sequence] = dotenv_paths
        self.dotenv_basenames: Optional[Sequence] = dotenv_basenames
        self._default_db_home: str = default_db_home or ''
        self._package_home: str = package_home or ''
        self._package_data_home: str = package_data_home or ''
        self._package_config_home: str = package_config_home or ''
        self._package_cert_home: str = package_cert_home or ''
        self._package_cert_private_home: str = package_cert_private_home or ''
        self._caller_name: str = self._get_caller_name()

    @cached_property
    def dotenv(self) -> Dict[str, str]:  # type: ignore
        return dot_env.values(
            env_varnames=self.dotenv_varnames,
            paths=self.dotenv_paths,
            basenames=self.dotenv_basenames,
        )
