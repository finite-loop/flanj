import os
from typing import Any
from urllib.parse import ParseResult
from .base import BaseBuildOptions

from typing import (
    Dict,
    cast,
)


class PostgresqlBuildOptions(BaseBuildOptions):

    SSL_MODES = (
        'disable',
        'allow',
        'prefer',
        'require',
        'verify-ca',
        'verify-full',
    )

    def __call__(self, url_obj: ParseResult, out: Dict[str, Any]) -> None:
        if out['scheme'] != 'postgresql':
            return
        out['options'] = {}
        for key, val in self.extract_query(url_obj).items():
            if key == 'sslmode':
                # https://tinyurl.com/ycvuq7d4
                #
                # This option determines whether or with what priority a
                # secure SSL TCP/IP connection will be negotiated with the
                # server. There are six modes:
                #
                # disable
                #   only try a non-SSL connection
                #
                # allow
                #   first try a non-SSL connection; if that fails, try an SSL
                #   connection
                #
                # prefer (default)
                #   first try an SSL connection; if that fails, try a non-SSL
                #   connection
                #
                # require
                #   only try an SSL connection. If a root CA file is present,
                #   verify the certificate in the same way as if verify-ca was
                #   specified
                #
                # verify-ca
                #   only try an SSL connection, and verify that the server
                #   certificate is issued by a trusted certificate
                #   authority (CA)
                #
                # verify-full
                #   only try an SSL connection, verify that the server
                #   certificate is issued by a trusted CA and that the
                #   requested server host name matches that in the certificate
                if val in self.SSL_MODES:
                    val = cast(str, val)
                    out['options']['sslmode'] = val
                else:
                    out['options']['sslmode'] = 'prefer'

            elif key == 'sslcompression':
                # https://tinyurl.com/ycvuq7d4
                #
                # If set to 1, data sent over SSL connections will be
                # compressed. If set to 0, compression will be disabled.
                # The default is 0. This parameter is ignored if a connection
                # without SSL is made.
                if val in (True, '1', 1):
                    out['options']['sslcompression'] = '1'
                else:
                    out['options']['sslcompression'] = '0'

            elif key == 'sslcert':
                # https://tinyurl.com/ycvuq7d4
                #
                # This parameter specifies the path and file name of the
                # client SSL certificate.
                try:
                    val = self.process_home(val, 'cert')
                except ValueError as e:
                    raise ValueError(
                        "Unable to set the 'sslcert' path. %s"
                        % str(e)
                    )
                out['options']['sslcert'] = val

            elif key == 'sslkey':
                # https://tinyurl.com/ycvuq7d4
                #
                # This parameter specifies the location for the secret key
                # used for the client certificate.
                try:
                    val = self.process_home(val, 'cert_private')
                except ValueError as e:
                    raise ValueError(
                        "Unable to set the 'sslkey' path. %s"
                        % str(e)
                    )
                out['options']['sslkey'] = val

            elif key == 'sslrootcert':
                # https://tinyurl.com/ycvuq7d4
                #
                # This parameter specifies the name of a file containing SSL
                # certificate authority (CA) certificate(s).
                try:
                    val = self.process_home(val, 'cert')
                except ValueError as e:
                    raise ValueError(
                        "Unable to set the 'sslrootcert' path. %s"
                        % str(e)
                    )
                out['options']['sslrootcert'] = val

            elif key == 'sslcrl':
                # https://tinyurl.com/ycvuq7d4
                #
                # This parameter specifies the file name of the client SSL
                # certificate.
                try:
                    val = self.process_home(val, 'cert')
                except ValueError as e:
                    raise ValueError(
                        "Unable to set the 'sslcrl' path. %s"
                        % str(e)
                    )
                out['options']['sslcrl'] = val

            elif key == 'passfile':
                # https://tinyurl.com/y3pdgbch
                #
                # Specifies the name of the file used to store passwords
                try:
                    val = self.process_home(val, 'config')
                except ValueError as e:
                    raise ValueError(
                        "Unable to set the 'passfile' path. %s"
                        % str(e)
                    )
                out['options']['passfile'] = val

            elif key == 'pgservicefile':
                # https://tinyurl.com/yd7vh9pl
                #
                # The connection service file allows libpq connection
                # parameters to be associated with a single service name.
                # That service name can then be specified by a libpq
                # connection, and the associated settings will be used.
                #
                # The connection service file can be a per-user service file
                # at ~/.pg_service.conf or the location specified by the
                # environment variable PGSERVICEFILE
                #
                # The file uses an “INI file” format where the section name is
                # the service name and the parameters are connection
                # parameters.
                try:
                    val = self.process_home(val, 'config')
                except ValueError as e:
                    raise ValueError(
                        "Unable to set the 'sysconfdir' path. %s"
                        % str(e)
                    )
                os.environ['PGSERVICEFILE'] = val

            elif key == 'service':
                # https://tinyurl.com/yd7vh9pl
                #
                # Service name to use for additional parameters. It specifies
                # a service name in pg_service.conf that holds additional
                # connection parameters. This allows applications to specify
                # only a service name so connection parameters can be
                # centrally maintained.
                if hasattr(val, 'encode') and val:
                    val = cast(str, val)
                    val = val.strip()
                    if val:
                        out['options']['service'] = val
