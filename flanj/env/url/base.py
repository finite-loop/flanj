import os
import sys
from collections import OrderedDict
from typing import (
    Any,
    Dict,
    Optional,
    Union,
    cast,
)

import urllib.parse


# noinspection Mypy
from flanj.utils.objs import is_str
from urllib.parse import ParseResult

python_version = sys.version_info[:3]


class BaseBuildOptions:

    # noinspection Mypy
    def __init__(
            self,
            parser: urllib.parse,               # type: ignore
            package_data_home: str,
            package_config_home: str,
            package_cert_home: str,
            package_cert_private_home: str,

    ) -> None:
        self._parser: urllib.parse = parser     # type: ignore
        self.package_data_home: str = package_data_home
        self.package_config_home: str = package_config_home
        self.package_cert_home: str = package_cert_home
        self.package_cert_private_home: str = package_cert_private_home

    def __call__(self, url_obj: ParseResult, out: Dict[str, Any]) -> None:
        out['options'] = {}

    def unquote(self, text: str) -> str:
        """Use to unquote url quoted strings

        Args:
             text (str): The string that will be unquoted.
        """
        if is_str(text):
            return self._parser.unquote_plus(text)  # type: ignore
        return ''

    # noinspection PyUnusedFunction
    def extract_query(
            self,
            url_obj: ParseResult
    ) -> Dict[str, Union[str, bool, None]]:
        """
        Extract the query section of the url into an OrderedDict

        Args:
            url_obj (:obj:`urllib.parse.ParseResult`): The url object.
        """
        if not url_obj.query:
            return {}
        if python_version < (3, 8, 0):
            out = OrderedDict(
                self._parser.parse_qsl(     # type: ignore
                    url_obj.query,
                    strict_parsing=True,
                )
            )
        else:
            out = OrderedDict(
                self._parser.parse_qsl(     # type: ignore
                    url_obj.query,
                    strict_parsing=True,
                    max_num_fields=20,
                )
            )
        for key, val in out.items():
            val = self.unquote(val.lower().strip())
            if val in ('true', 'yes'):
                out[key] = True
            elif val in ('false', 'no'):
                out[key] = False
            elif val in ('none', 'null'):
                out[key] = None
            else:
                out[key] = val
        return out

    def process_home(
            self,
            path: Union[str, bool, None],
            path_type: Optional[str] = None,
    ) -> str:
        if hasattr(path, 'encode'):
            path = cast(str, path)
            if path_type is None:
                path_type = ''
            out = os.path.expanduser(path)
            out = os.path.expandvars(out)
            out = os.path.normpath(out)
            if os.path.isabs(out):
                return out
            if path_type == 'cert':
                return os.path.join(self.package_cert_home, out)
            if path_type == 'cert_private':
                return os.path.join(self.package_cert_private_home, out)
            if path_type == 'data':
                return os.path.join(self.package_data_home, out)
            if path_type == 'config':
                return os.path.join(self.package_config_home, out)
        raise ValueError(
            "The given 'path' of %r must be an absolute path."
            % path
        )
