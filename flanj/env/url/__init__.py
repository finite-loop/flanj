from typing import (
    Any,
    Dict,
    NamedTuple,
    Optional,
    Tuple,
    Union,
)
import os
import ssl
from collections import OrderedDict
from collections.abc import Mapping

import urllib.parse as urlparser
from urllib import parse as Parse
from urllib.parse import ParseResult

from flanj.utils.objs import is_str
from .postgresql import PostgresqlBuildOptions


try:
    from functools import cached_property
except ImportError:
    from flutils.decorators import cached_property




class SslTrigger(NamedTuple):
    key: str
    vals: Tuple[str, ...]


class Url(NamedTuple):
    url: str
    scheme: str
    username: str
    password: str
    host: str
    port: int
    path: str
    query: Dict[str, Any]
    fragment: str
    database: str
    schema: str
    uses_ssl: bool
    options: Dict[str, Any]



class QueryDefaultPath(NamedTuple):
    scheme: str             # The name of the scheme
    root: str               # The full path to a directory
    keys: Tuple[str, ...]   # Each URL.query key from which the value may be
                            # prepended with the path in this object.


class ParseUrl:

    DEFAULT_PORTS: Dict[str, int] = {
        'postgresql': 5432,
        'postgis': 5432,
        'mysql': 3306,
        'redis': 6379,
        'smtp': 25,
        'memcached': 11211,
        'smtps': 587,
        'mongodb': 27017,
        'http': 80,
        'https': 443,
        'rabbitmq': 5672,
        'sqlite': 0,
    }

    QUERY_MAP = {
        'postgresql': {
            'cert_paths': {
                'root_path': '~/.postgresql',
                'keys': (
                    'sslcert',
                    'sslkey',
                    'sslrootcert',
                    'sslcrl'
                ),
            },
            'ssl_trigger': {
                'key': 'sslmode',
                'vals': (
                    'allow',
                    'prefer',
                    'require',
                    'verify-ca',
                    'verify-full'
                )
            }
        },
        'mongodb': {
            'cert_paths': {
                'root_path': '~/.mongodb',
                'keys': (
                    'ssl_ca_certs',
                    'ssl_crlfile',
                    'ssl_certfile',
                    'ssl_keyfile'
                )
            },
            'ssl_trigger': {
                'key': 'ssl',
                'vals': (
                    True,
                    'true',
                    'True',
                    'TRUE',
                    'yes',
                    'Yes',
                    'YES',
                ),
            }
        }
    }

    def __init__(
            self,
            package_data_home: str,
            package_config_home: str,
            package_cert_home: str,
            package_cert_private_home: str,
            package_name: Optional[str] = None,
            default_ports: Dict[str, int] = None,
    ) -> None:
        if isinstance(package_name, str) and package_name.strip():
            self.package_name: str = package_name
        else:
            self.package_name: str = ''
        if isinstance(default_ports, Mapping):
            self._default_ports: Dict[str, int] = default_ports
        else:
            self._default_ports: Dict[str, int] = self.DEFAULT_PORTS
        self.package_data_home: str = package_data_home
        self.package_config_home: str = package_config_home
        self.package_cert_home: str = package_cert_home
        self.package_cert_private_home: str = package_cert_private_home

    @cached_property
    def _schemes(self) -> Tuple[str, ...]:
        """All of the possible schemes."""
        schemes = set(list(self._default_ports.keys()))
        return tuple(sorted(list(schemes)))

    @cached_property
    def _option_extractors(self) -> Tuple[PostgresqlBuildOptions]:
        return (
            PostgresqlBuildOptions(
                self._parser,
                self.package_data_home,
                self.package_config_home,
                self.package_cert_home,
                self.package_cert_private_home,
            ),
        )

    @cached_property
    def _parser(self) -> Parse:
        """A `urllib.parse` object that has been primed with schemes."""
        for scheme in self._schemes:
            urlparser.uses_netloc.append(scheme)
        return urlparser

    def parse(self, url: str) -> ParseResult:
        """Parse a URL string into a ``urllib.parse.ParseResult`` object.

        Args:
             url (str): The URL string.

        Returns: ``urllib.parse.ParseResult``
        """
        return self._parser.urlparse(url)

    def unquote(self, text: str) -> str:
        """Use to unquote url quoted strings

        Args:
             text (str): The string that will be unquoted.
        """
        if is_str(text):
            return self._parser.unquote_plus(text)
        return ''

    def quote(self, text: str) -> str:
        """Use to URL quote strings

        Args:
             text (str): The string that will be unquoted.
        """
        if is_str(text):
            return self._parser.quote_plus(text)
        return ''

    @cached_property
    def _config_dirs(self) -> Tuple[str, ...]:
        """Each possible config directory.

        These dirs must exist on the system when running this.
        """
        out = []
        if self.package_name:
            paths = (
                os.path.expanduser('~/.config/%s' % self.package_name),
                os.path.join('etc', self.package_name),
            )
            for path in paths:
                if os.path.isdir(path):
                    out.append(path)
        if out:
            return tuple(out)
        return tuple()

    @cached_property
    def _possible_cert_dirs(self) -> Tuple[str, ...]:
        """Each possible TLS directory.

        These directories must exist on the system when running this.
        """
        out = []
        for path in self._config_dirs:
            path = os.path.join(path, 'tls')
            if os.path.isdir(path):
                out.append(path)
        if out:
            return tuple(out)
        return tuple()

    @cached_property
    def _scheme_default_cert_paths(self) -> Dict[str, QueryDefaultPath]:
        """A dict with the scheme as the key and a QueryDefaultPath object
        as the value.
        """
        out = {}
        for scheme, data in self.QUERY_MAP.items():
            path = os.path.expanduser(data['cert_paths']['root_path'])
            path = os.path.expandvars(path)
            args = [scheme, path, data['cert_paths']['keys']]
            out[scheme] = QueryDefaultPath(*args)
        return out

    def _get_default_cert_path(
            self,
            url_obj: ParseResult
    ) -> Union[QueryDefaultPath, None]:
        """Get the QueryDefaultPath object for the given URL object.

        Args:
             url_obj (ParseResult): The URL object.
        """
        scheme: str = self._extract_scheme(url_obj)
        return self._scheme_default_cert_paths.get(scheme, None)

    def _build_cert_path(self, path: str, root: str) -> str:
        """Ensure the given path is an absolute path to a cert file.

        Args:
             path (str): The path as extracted out of the URL.query.
             root (str): The default root path that may be prepended to
                the ``path`` if the ``path`` is not an absolute path.
        """
        path = os.path.expanduser(path)
        path = os.path.expandvars(path)
        if path.startswith(os.path.sep) is True:
            return path

        for root in self._possible_cert_dirs:
            new_path = os.path.join(root, path)
            if os.path.isfile(new_path):
                return new_path
        root = os.path.expanduser(root)
        root = os.path.expandvars(root)
        return os.path.join(root, path)

    @cached_property
    def _query_ssl_trigger(self) -> Dict[str, SslTrigger]:
        """Return a dictionary of nametuple trigger info for each scheme."""
        out = {}
        for scheme, data in self.QUERY_MAP.items():
            trigger = data.get('ssl_trigger')
            if trigger:
                key = trigger.get('key')
                if key:
                    vals = trigger.get('vals')
                    if vals:
                        out[scheme] = SslTrigger(
                            key,
                            vals
                        )
        return out

    def _extract_scheme(self, url_obj: ParseResult) -> str:
        """Extract and normalize the scheme from the given URL object.

        Args:
             url_obj (ParseResult): The URL object.
        """
        if url_obj.scheme.lower().startswith('sqlite'):
            return 'sqlite'
        if url_obj.scheme.lower().startswith('postgre'):
            return 'postgresql'
        if url_obj.scheme.lower().startswith('mongo'):
            return 'mongodb'
        if url_obj.scheme in self._schemes:
            return url_obj.scheme
        for scheme in self._schemes:
            if url_obj.scheme.lower().startswith(scheme):
                return scheme
        return url_obj.scheme

    def _extract_port(self, url_obj: ParseResult) -> int:
        """Extract the port number from the given URL object.

        If the given URL object does NOT have a port set then a default will
        be used.

        If the given URL object contains an invalid port number.  Port 65535
        will be used.

        Args:
             url_obj (ParseResult): The URL object.
        """
        scheme = self._extract_scheme(url_obj)
        default = self._default_ports.get(scheme, 65535)
        try:
            port = url_obj.port
        except ValueError:
            port = default

        try:
            port = int(port)
        except (TypeError, ValueError):
            port = default

        if port < 1:
            port = default
        if port > 65535:
            port = default
        return port

    def _extract_query(self, url_obj: ParseResult) -> Dict[str, Any]:
        """Return a namedtuple of the query section of the url_obj.

        Args:
             url_obj (ParseResult): The URL object.
        """
        out = {}
        default_cert_path = self._get_default_cert_path(url_obj)
        hold = OrderedDict(self._parser.parse_qsl(url_obj.query))
        for key, val in hold.items():
            if val.lower().strip() in ('true', 'false',):
                val = val.lower().strip() == 'true'
            elif val.lower().strip() in ('yes', 'no',):
                val = val.lower().strip() == 'yes'
            elif val.strip() == 'CERT_NONE':
                val = ssl.CERT_NONE
            else:
                if default_cert_path:
                    if key in default_cert_path.keys:
                        val = self._build_cert_path(val, default_cert_path.root)
            if is_str(val):
                val = self.unquote(val)
            out[key] = val
        return out

    def _extract_uses_ssl(
            self,
            url_obj: ParseResult,
            query_dict: Dict[str, Any]
    ) -> bool:
        """Return ``True`` if any of the query values indicates to use ssl.

        Args:
             url_obj (ParseResult): The URL object.
             query_dict (dict): The query section of the url.
        """
        scheme = self._extract_scheme(url_obj)
        ssl_trigger_obj = self._query_ssl_trigger.get(scheme, None)
        if ssl_trigger_obj:
            if ssl_trigger_obj.key in query_dict.keys():
                val = query_dict.get(ssl_trigger_obj.key)
                if val and val in ssl_trigger_obj.vals:
                    return True
        if url_obj.scheme == 'smtps':
            return True
        return False

    def _extract_database(
            self,
            url_obj: ParseResult,
    ) -> str:
        """Get the database name from the URL path

        Args:
             url_obj (ParseResult): The parse result object.
        """
        hold = url_obj.path.split('/')
        parts = []
        for part in hold:
            if part.strip():
                parts.append(part.strip())
        if len(parts) > 0:
            return self.unquote(parts[0])
        return ''

    def _extract_schema(
            self,
            url_obj: ParseResult,
            query_dict: Dict[str, Any]
    ) -> str:
        """Get the database schema name from the URL path.

        THIS IS DIFFERENT THAN SCHEME THIS IS SCHEMA!  Scheme is the first
        section of the URL; schema is for the database.

        Only URLs with a scheme of ``'postgresql'`` can have the schema set.
        The schema is set as the second option in the URL path::

            postgresql://[USER][:PASS]@HOSTNAME[:PORT]/DATABASE[?schema=SCHEMA]

        Args:
             url_obj (ParseResult): The parse result object.
             query_dict (dict): The query section of the url.
        """
        scheme = self._extract_scheme(url_obj)
        if scheme == 'postgresql':
            if 'schema' in query_dict.keys():
                out = query_dict['schema']
                if out:
                    return out
            return 'public'
        return ''

    def _build_url(self, data: Dict[str, Any]) -> str:
        """Take the given ``data`` dict and turn it into an URL."""
        parts = ["%s://" % data['scheme']]

        if data['username']:
            parts.append(self.quote(data['username']))

        if data['password']:
            parts.append(':%s' % self.quote(data['password']))

        if data['scheme'] != 'sqlite':
            parts.append('@%s' % self.quote(data['host']))
            parts.append(':%s' % data['port'])
        if data['path']:
            parts.append(data['path'])
        hold = []
        for key in sorted(data['query'].keys()):
            if key != 'schema':
                val = data['query'][key]
                hold.append('%s=%s' % (self.quote(key), self.quote(val)))
        if hold:
            parts.append('?%s' % '&'.join(hold))
        if data['fragment']:
            parts.append('#%s' % self.quote(data['fragment']))
        return ''.join(parts)

    def _set_options(self, url_obj: ParseResult, out: Dict[str, Any]) -> None:
        for func in self._option_extractors:
            func(url_obj, out)

    def as_dict(self, url: str) -> Dict[str, Any]:
        """Parse the given url string into it's various parts.

        scheme://username:password@host:port/(path/database)?query#fragment
        """
        out = {}
        if url:
            url_obj = self.parse(url)
            out['scheme'] = self._extract_scheme(url_obj)
            out['username'] = self.unquote(url_obj.username)
            out['password'] = self.unquote(url_obj.password)
            out['host'] = url_obj.hostname or ''
            out['port'] = self._extract_port(url_obj)
            out['path'] = self.unquote(url_obj.path)
            out['fragment'] = self.unquote(url_obj.fragment)
            query_dict = self._extract_query(url_obj)
            out['query'] = query_dict
            out['database'] = self.unquote(url_obj.path).lstrip('/')
            out['schema'] = self._extract_schema(url_obj, query_dict)
            out['uses_ssl'] = self._extract_uses_ssl(url_obj, query_dict)
            out['url'] = self._build_url(out)
            out['options'] = {}
            self._set_options(url_obj, out)
        else:
            out['scheme'] = ''
            out['username'] = ''
            out['password'] = ''
            out['host'] = ''
            out['port'] = 0
            out['path'] = ''
            out['fragment'] = ''
            out['database'] = ''
            out['schema'] = ''
            out['query'] = {}
            out['uses_ssl'] = False
            out['url'] = ''
            out['options'] = {}
        return out

    def __call__(self, url: str) -> Url:
        data = self.as_dict(url)
        return Url(**data)
