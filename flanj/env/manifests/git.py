from datetime import tzinfo
import getpass
from typing import (
    Optional,

)

from .base import (
    Commit,
    Manifest,
    Repo,
    Tag,
    build_migrations,
)

from flanj.utils.repo import (
    BranchInfo,
    CommitInfo,
    TagInfo,
    get_active_branch_info,
)

from flanj.utils.timestamps import NOW


def _build_commit(commit: CommitInfo, tz_info: tzinfo) -> Commit:
    return Commit(
        sha=commit.sha,
        author=commit.author,
        timestamp=commit.timestamp.astimezone(tz_info)
    )


def _build_tag(head: BranchInfo, tz_info: tzinfo) -> Optional[Tag]:
    if not head.tags:
        return None
    tag_info = head.tags[0]
    commit = _build_commit(tag_info.commits[0], tz_info)
    return Tag(
        name=tag_info.name,
        path=tag_info.path,
        sha=tag_info.sha,
        author=tag_info.author,
        timestamp=tag_info.timestamp.astimezone(tz_info),
        number_of_commits=len(tag_info.commits),
        is_lightweight=tag_info.is_lightweight,
        commit=commit,
    )


def _build_repo(head: BranchInfo, tz_info: tzinfo) -> Repo:
    commit = None
    if head.commit:
        commit = _build_commit(head.commit, tz_info)

    tag = None
    if head.tags:
        tag = _build_tag(head, tz_info)

    return Repo(
        branch=head.name,
        path=head.path,
        is_dirty=head.is_dirty,
        has_untracked_files=head.has_untracked_files,
        tracking_branch=head.tracking_branch,
        number_of_commits=len(head.commits_since_tag),
        tag=tag,
        commit=commit,
    )


def _get_head_commit_tag(head: BranchInfo) -> Optional[TagInfo]:
    if head.commit:
        for tag in head.tags:
            if head.commit.sha == tag.commits[0].sha:
                return tag
    return None


def _build_version(head: BranchInfo, repo: Repo) -> str:
    out = f'dev-{head.name}'
    tag = _get_head_commit_tag(head)
    if tag is None:
        return repo.commit.sha[:8]
    if head.name == 'master':
        tag = _get_head_commit_tag(head)
        if tag:
            out = tag.name
            if out.startswith('v'):
                out = out[1:]
            return out
        if head.commits_since_tag:
            if head.commit:
                commit = head.commit
                sha = commit.sha
                out = f'master-{sha}'
            return out
    if head.commit:
        author = head.commit.author.strip().lower().replace(' ', '-')
        if author:
            return f'{out}-{author}-{head.commit.sha}'
        return f'{out}-{head.commit.sha}'
    return out


def build_manifest_from_git(
        project_name: str,
        path: str,
        tz_info: tzinfo,
        called_from_settings: bool = False
) -> Manifest:
    head = get_active_branch_info(path)
    if called_from_settings is True:
        migrations = ()
    else:
        migrations = build_migrations()
    repo = _build_repo(head, tz_info)
    return Manifest(
        project_name=project_name,
        build_timestamp=NOW.astimezone(tz_info),
        version=_build_version(head, repo),
        origin='git',
        migrations=migrations,
        repo=repo
    )
