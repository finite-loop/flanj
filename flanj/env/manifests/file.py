from datetime import tzinfo
from typing import (
    Any,
    Dict,
)

from toml import load

from .base import (
    Manifest,
    manifest_from_dict
)


def build_manifest_from_file(
        project_name: str,
        path: str,
        tz_info: tzinfo,
) -> Manifest:
    with open(path, 'r', encoding='utf-8') as f:
        # noinspection Mypy
        data: Dict[str, Any] = load(f)  # type: ignore
    if not data.get('project_name', '').strip():
        data['project_name'] = project_name
    data['origin'] = 'file'
    return manifest_from_dict(data, tz_info)
