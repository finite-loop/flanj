from datetime import tzinfo

from .base import (
    Manifest,
    build_migrations,
)
from flanj.utils.timestamps import NOW


def build_manifest_default(
        project_name: str,
        tz_info: tzinfo,
) -> Manifest:
    return Manifest(
        project_name=project_name,
        build_timestamp=NOW.astimezone(tz_info),
        version='dev',
        origin='default',
        migrations=(),
        repo=None
    )
