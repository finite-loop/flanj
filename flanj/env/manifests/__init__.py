import os
import toml
from datetime import tzinfo
from typing import (
    Optional,
    Tuple,
    Union,
    cast,
)

from git import (
    Repo as _Repo,
    InvalidGitRepositoryError,
)
from flanj.utils.timestamps import as_tzinfo

from .default import build_manifest_default as _build_manifest_default
from .git import build_manifest_from_git as _build_manifest_from_git
from .file import build_manifest_from_file as _build_manifest_from_file


from .base import (
    Commit,
    Manifest,
    Migration,
    Repo,
    Tag,
    manifest_from_dict,
    manifest_to_dict,
)

__all__ = [
    'Commit',
    'Manifest',
    'Migration',
    'Repo',
    'Tag',
    'build_manifest',
    'manifest_to_dict',
    'manifest_from_dict',
]


def _is_toml_file(path: str) -> bool:
    if os.path.isfile(path):
        try:
            with open(path, 'r', encoding='utf-8') as f:
                data = toml.load(f)
        except (toml.TomlDecodeError, UnicodeDecodeError):
            pass
        else:
            if data:
                return True
    return False


def _is_git_repo(path: str) -> bool:
    if os.path.basename(path) == '.git':
        path = os.path.dirname(path)
    if os.path.isdir(path):
        try:
            _Repo(path)
        except InvalidGitRepositoryError:
            pass
        else:
            return True
    return False


_HOME = os.path.expandvars('~')


def _dir_usable(path: str) -> bool:
    if not os.path.isabs(path):
        return False
    if not os.path.isdir(path):
        return False
    if path == _HOME:
        return False
    if len(path.split(os.path.sep)) <= 3:
        return False
    return True


def _path_type(path: Optional[str]) -> Tuple[str, str]:
    if not hasattr(path, 'encode'):
        path = ''
    path = cast(str, path)
    if not path:
        return path, ''
    if not os.path.isabs(path):
        return '', ''
    if not os.path.isdir(path):
        if _is_toml_file(path):
            return path, 'toml'
        path = os.path.dirname(path)

    while _dir_usable(path):
        _path = os.path.join(path, '.git')
        if _is_git_repo(_path) is True:
            return _path, 'git'
        _path = os.path.join(path, 'manifest.toml')
        if os.path.isfile(_path):
            if _is_toml_file(_path) is True:
                return _path, 'toml'
        path = os.path.dirname(path)
    return '', ''


def build_manifest(
        project_name: str,
        path: Optional[str] = None,
        time_zone: Union[tzinfo, str, None] = None,
        called_from_settings: bool = False,
) -> Manifest:
    path, path_type = _path_type(path)
    tz_info = as_tzinfo(time_zone)

    if path and path_type == 'git':
        return _build_manifest_from_git(
            project_name,
            path,
            tz_info,
            called_from_settings=called_from_settings,
        )

    if path and path_type == 'toml':
        return _build_manifest_from_file(project_name, path, tz_info)

    return _build_manifest_default(project_name, tz_info)
