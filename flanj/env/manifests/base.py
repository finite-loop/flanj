from collections import OrderedDict
from collections.abc import Sequence
from datetime import (
    datetime,
    tzinfo,
)
from functools import singledispatch
from io import StringIO
from typing import (
    Any,
    Dict,
    List,
    NamedTuple,
    Optional,
    Tuple,
    Union,
    cast,
)


import dateutil.parser
from django.core.management import call_command
from flutils.codecs import get_encoding
from flanj.utils.timestamps import as_tzinfo


class Migration(NamedTuple):
    app: str
    name: str


class Commit(NamedTuple):
    sha: str
    author: str
    timestamp: datetime


class Tag(NamedTuple):
    name: str
    path: str
    sha: str
    author: str
    timestamp: datetime
    number_of_commits: int
    is_lightweight: int
    commit: Commit


class Repo(NamedTuple):
    branch: str
    path: str
    is_dirty: bool
    has_untracked_files: bool
    tracking_branch: str
    number_of_commits: int
    tag: Optional[Tag]
    commit: Optional[Commit]


class Manifest(NamedTuple):
    project_name: str
    build_timestamp: datetime
    version: str
    origin: str
    migrations: Tuple[Migration, ...]
    repo: Optional[Repo]


def build_migrations() -> Tuple[Migration, ...]:
    hold: Dict[str, str] = OrderedDict()
    with StringIO() as stdout:
        call_command(
            'showmigrations',
            '--plan',
            stdout=stdout
        )
        data: str = stdout.getvalue()
    for line in data.splitlines():
        if not line.startswith('['):
            continue
        full_migration = line.partition('  ')[2].strip()
        if not full_migration:
            continue
        app, _, migration = full_migration.partition('.')
        if not app or not migration:
            continue
        if app not in hold.keys():
            hold[app] = ''
        migration_number = migration.split('_')[0]
        hold[app] = migration_number
    out = []
    for key, val in hold.items():
        out.append(Migration(app=key, name=val))
    return tuple(out)


# noinspection PyUnusedFunction,PyUnusedLocal,PySameParameterValue
@singledispatch
def manifest_to_dict(
        obj: Any,
        key: str = '',
        is_initial: bool = True,
) -> Any:
    if is_initial is True:
        raise TypeError(
            'The given object must be of type '
            'flanj.utils.manifest.Manifest'
        )
    if obj is None:
        return 'None'
    return obj


# noinspection PyUnusedFunction,PyUnusedLocal
@manifest_to_dict.register(datetime)
def _manifest_to_dict_datetime(
        obj: datetime,
        key: str = '',
        is_initial: bool = True,
) -> str:
    if is_initial is True:
        raise TypeError(
            'The given object must be of type '
            'flanj.utils.manifest.Manifest'
        )
    return obj.isoformat()


# noinspection PyUnusedFunction,PyUnusedLocal
@manifest_to_dict.register(Migration)
def _manifest_to_dict_migration(
        obj: Migration,
        key: str = '',
        is_initial: bool = True,
) -> Tuple[str, str]:
    if is_initial is True:
        raise TypeError(
            'The given object must be of type '
            'flanj.utils.manifest.Manifest'
        )
    return obj.app, obj.name


# noinspection PyUnusedFunction
@manifest_to_dict.register(Manifest)
def _manifest_to_dict_manifest(
        obj: Manifest,
        key: str = '',
        is_initial: bool = True,
) -> Dict[str, Any]:
    if key or is_initial is False:
        raise TypeError(
            'flanj.utils.manifest.Manifest can only be the top object.'
        )

    out = {}
    # noinspection PyProtectedMember
    for _key in obj._fields:
        _val = getattr(obj, _key)
        out[_key] = manifest_to_dict(_val, _key, False)
    return out


_default_encoding = get_encoding()


# noinspection PyUnusedFunction
@manifest_to_dict.register(Sequence)
def _manifest_to_dict_sequence(
        obj: Sequence,
        key: str = '',
        is_initial: bool = True,
) -> Union[str, Tuple[Any, ...], Dict[str, Any]]:
    if is_initial is True:
        raise TypeError(
            'The given object must be of type '
            'flanj.utils.manifest.Manifest'
        )
    if hasattr(obj, 'encode'):
        obj = cast(str, obj)
        return obj
    if hasattr(obj, 'decode'):
        obj = cast(bytes, obj)
        obj = obj.decode(_default_encoding)
        obj = cast(str, obj)
        return obj
    if key == 'migrations':
        out = []
        for item in obj:
            out.append(manifest_to_dict(item, f'{key}._item_', False))
        return tuple(out)
    if hasattr(obj, '_fields'):
        out_dict = {}
        obj = cast(NamedTuple, obj)
        # noinspection PyProtectedMember
        for _key in obj._fields:
            _val = getattr(obj, _key)
            out_dict[_key] = manifest_to_dict(_val, f'{key}.{_key}', False)
        return out_dict
    out = []
    for item in obj:
        out.append(manifest_to_dict(item, f'{key}._item_', False))
    return tuple(out)


def _validate_obj_has_key(obj: Dict[str, Any], key: str) -> None:
    if key not in obj.keys():
        raise ValueError(
            f'The given dictionary cannot be converted into a '
            f'flanj.utils.manifest.Manifest object because the {key!r} key '
            f'is missing.'
        )


def _migrations_from_dict(obj: List[List[str]]) -> Tuple[Migration, ...]:
    if not hasattr(obj, 'index'):
        raise TypeError(
            "The given dictionary cannot be converted into a "
            "flanj.utils.manifest.Manifest object because the "
            "value for 'migrations' is not a list. \n\n %s" % repr(obj)
        )
    out = []
    for item in obj:
        new_obj = Migration(*item)
        out.append(new_obj)
    return tuple(out)


def _convert_timestamps(obj: Dict[str, Any], tz_info: tzinfo) -> None:
    for key, val in obj.items():
        if hasattr(val, 'items'):
            _convert_timestamps(val, tz_info)
        convert = False
        if key.startswith('timestamp_'):
            convert = True
        elif key.endswith('_timestamp'):
            convert = True
        elif key == 'timestamp':
            convert = True

        if convert is False:
            continue

        val = dateutil.parser.parse(val)
        obj[key] = val.astimezone(tz_info)


def _commit_from_dict(obj: Dict[str, Any], name: str) -> Commit:
    if not hasattr(obj, 'items'):
        raise TypeError(
            f"The given obj cannot be converted into a "
            f"flanj.utils.manifest.Manifest object because the "
            f"value for 'repo.{name}' is not a dict."
        )
    for field in Commit._fields:
        _validate_obj_has_key(obj, field)
    return Commit(**obj)


def _tag_from_dict(obj: Dict[str, Any]) -> Optional[Tag]:
    if not hasattr(obj, 'items'):
        return None
    for field in Tag._fields:
        _validate_obj_has_key(obj, field)
    kwargs = {
        'name': obj['name'],
        'path': obj['path'],
        'sha': obj['sha'],
        'author': obj['author'],
        'timestamp': obj['timestamp'],
        'number_of_commits': obj['number_of_commits'],
        'is_lightweight': obj['is_lightweight'],
        'commit': _commit_from_dict(obj['commit'], 'tag.commit')
    }
    return Tag(**kwargs)


def _repo_from_dict(obj: Dict[str, Any]) -> Optional[Repo]:
    if not hasattr(obj, 'items'):
        return None
    # noinspection PyProtectedMember
    for field in Repo._fields:
        _validate_obj_has_key(obj, field)
    kwargs = {
        'branch': obj['branch'],
        'path': obj['path'],
        'is_dirty': obj['is_dirty'],
        'has_untracked_files': obj['has_untracked_files'],
        'tracking_branch': obj['tracking_branch'],
        'number_of_commits': obj['number_of_commits'],
        'tag': _tag_from_dict(obj['tag']),
        'commit': _commit_from_dict(obj['commit'], 'commit')
    }
    return Repo(**kwargs)


def manifest_from_dict(
        obj: Dict[str, Any],
        time_zone: Union[tzinfo, str, None] = None,
) -> Manifest:
    tz_info = as_tzinfo(time_zone)

    # noinspection PyProtectedMember
    for field in Manifest._fields:
        _validate_obj_has_key(obj, field)

    _convert_timestamps(obj, tz_info)

    kwargs = {
        'project_name': obj['project_name'],
        'build_timestamp': obj['build_timestamp'],
        'version': obj['version'],
        'origin': obj['origin'],
        'migrations': _migrations_from_dict(obj['migrations']),
        'repo': _repo_from_dict(obj['repo']),
    }
    return Manifest(**kwargs)
