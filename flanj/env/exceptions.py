from functools import cached_property
from typing import (
    Any,
    Dict,
    Optional,
    cast,
)


NOT_SET: str = '__NOT_SET__'
EMPTY: str = '__empty__'
REQUIRED: str = '__required__'


def _build_function_signature(
        format_values: Dict,
) -> None:
    ret: str = '{func_name}('.format(**format_values)
    pos: int = -1
    for key, val in format_values['arguments'].items():
        pos += 1
        if pos == 0:
            ret = '%s%r' % (ret, val)
        else:
            ret = '%s, %r' % (ret, val)
    for key, val in format_values['keyword_arguments'].items():
        pos += 1
        if pos == 0:
            ret = '%s%s=%r' % (ret, key, val)
        else:
            ret = '%s, %s=%r' % (ret, key, val)
    ret = '%s)' % ret
    format_values['signature'] = ret


class _SettingsErrorMixin:

    def __init__(
            self,
            func_name: str,
            arguments: Dict,
            keyword_arguments: Dict,
            problem_arg_name: str,
            expected_type_name: str,
            caller_name: str,
            settings_file: str,
            additional_message: Optional[str] = None,
            has_empty: bool = True,
            has_required: bool = True,
            empty_value: str = EMPTY,
            required_value: str = REQUIRED,
            value: Any = NOT_SET,
            template_override: str = None,
            **format_values: Any
    ) -> None:
        format_values.update(arguments)
        format_values.update(keyword_arguments)
        format_values['func_name'] = func_name
        format_values['arguments'] = arguments
        format_values['keyword_arguments'] = keyword_arguments
        format_values['problem_arg_name'] = problem_arg_name
        format_values['expected_type_name'] = expected_type_name
        format_values['caller_name'] = caller_name
        format_values['settings_file'] = settings_file
        format_values['has_empty'] = has_empty
        format_values['has_required'] = has_required
        format_values['empty_value'] = empty_value
        format_values['required_value'] = required_value
        format_values['value'] = value
        if not hasattr(template_override, 'capitalize'):
            template_override = ''
            template_override = cast(str, template_override)

        _build_function_signature(format_values)
        if not hasattr(additional_message, 'capitalize'):
            additional_message = ''
            additional_message = cast(str, additional_message)
        self.message = self._build_message(
            format_values,
            additional_message,
            template_override,
        )
        super().__init__(self.message)

    @staticmethod
    def _build_message(
            format_values: Dict,
            additional_message: str,
            template_override: str,
    ) -> str:
        raise NotImplementedError


class SettingsTypeError(_SettingsErrorMixin, TypeError):

    @staticmethod
    def _build_message(
            format_values: Dict,
            additional_message: str,
            template_override: str,
    ) -> str:
        if template_override:
            ret: str = template_override
        else:
            ret = (
                'When calling {signature}, the value of '
                '{problem_arg_name!r} must be of type '
                '{expected_type_name!r}'
            )

            if (
                format_values['has_empty'] is True and
                format_values['has_required'] is True
            ):
                ret = (
                    '%s, '
                    'have the value of {caller_name}.EMPTY '
                    'or have the value of {caller_name}.REQUIRED.'
                ) % ret
            elif format_values['has_empty'] is True:
                ret = (
                    '%s or '
                    'have the value of {caller_name}.EMPTY.'
                ) % ret
            elif format_values['has_required'] is True:
                ret = (
                    '%s or '
                    'have the value of {caller_name}.REQUIRED.'
                ) % ret
            else:
                ret = '%s.' % ret

            if additional_message:
                ret = '%s %s' % (ret, additional_message)

            ret = '%s Please fix in: {settings_file!r}.' % ret
        return ret.format(**format_values)


class SettingsValueError(_SettingsErrorMixin, ValueError):

    @staticmethod
    def _build_message(
            format_values: Dict,
            additional_message: str,
            template_override: str,
    ) -> str:
        if template_override:
            ret: str = template_override
        else:
            ret = (
                'When calling {signature}, '
                'the value of {problem_arg_name!r} is invalid'
            )
            if additional_message:
                ret = '%s: %s' % (ret, additional_message)

            ret = '%s Please fix in: {settings_file!r}' % ret
        return ret.format(**format_values)


class _BaseConfigurationError(_SettingsErrorMixin, SystemExit):

    @staticmethod
    def _build_message(
            format_values: Dict,
            additional_message: str,
            template_override: str,
    ) -> str:
        if template_override:
            ret = template_override
        else:
            ret = 'The environment/dotenv variable, {key!r},'
            if format_values['value'] == NOT_SET:
                ret = '%s has a value which is invalid.' % ret
            else:
                ret = '%s has the value {value!r} which is invalid.' % ret

            if additional_message:
                ret = '%s %s' % (ret, additional_message)

        return ret.format(**format_values)


class ConfigurationTypeError(_BaseConfigurationError):
    pass


class ConfigurationValueError(_BaseConfigurationError):
    pass


class ConfigurationKeyError(_BaseConfigurationError):

    @staticmethod
    def _build_message(
            format_values: Dict,
            additional_message: str,
            template_override: str,
    ) -> str:
        if template_override:
            ret = template_override
        else:
            ret = (
                'The environment/dotenv variable, {key!r}, is not '
                'set or has an empty value.'
            )
            if (
                format_values.get('default', '') ==
                format_values['required_value']
            ):
                ret = '%s This is REQUIRED to be set.' % ret

            if additional_message:
                ret = '%s %s' % (ret, additional_message)
        return ret.format(**format_values)
