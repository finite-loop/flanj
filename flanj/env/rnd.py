import string
from random import SystemRandom

random: SystemRandom = SystemRandom()
try:
    from secrets import choice
except ImportError:
    choice = random.choice

SYMBOLS: str = "!@^&-_=+[]?~."
DIGITS: str = string.digits
LOWERCASE: str = string.ascii_lowercase
UPPERCASE: str = string.ascii_uppercase
CHARACTERS: str = string.ascii_letters + string.digits + SYMBOLS


def chars(length: int = 0) -> str:
    """Generate a randomized string of ascii characters.

    The generated string will contain at least one upper-case character.

    The generated string will contain at least one lower-case character.

    The generated string will contain at least one numeric character.

    The generated string will contain at least one non-alphanumeric character.

    Raises a ValueError if `length` is not an integer or `length` is an integer
    greater than 0 and less than 4.

    The characters choices are::
        abcdefghijklmnopqrstuvwxyz
        ABCDEFGHIJKLMNOPQRSTUVWXYZ
        0123456789
        !@#$%^&*(-_=+)[]<>?.{}

    Args:
        length (integer, optional): The desired length of the returned
            character string.  A value of 0 (zero) will be converted to a
            randomized integer between 28 and 50.  Any non zero value must
            be greater than or equal to 4

    Raises
        :obj:`ValueError` if the given `length` is less than `3`.
    """
    if not isinstance(length, int):
        raise ValueError(
            "The length argument in generate_random_characters must be "\
            "an integer."
        )
    if length == 0:
        length = random.randrange(28, 50)

    if length < 4:
        raise ValueError(
            "The length argument in generate_random_characters must be "\
            "an integer with a value greater than or equal to 4."
        )

    for _ in range(length - 1):
        out = [
            choice(UPPERCASE),
            choice(LOWERCASE),
            choice(DIGITS),
            choice(SYMBOLS)
        ]
        for i in range(length - 5):
            out.append(choice(CHARACTERS))

        random.shuffle(out)
        out = ''.join(out)
    out = '%s%s' % (choice(string.ascii_letters), out)
    return out

