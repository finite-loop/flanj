# The validators in this file are NOT Django validators. Also,
# no validator on this page can use a Django validator or a
# validator from flanj.validators because this causes a circular
# import problem.

import json
import os

from collections.abc import (
    Sequence,
    Iterable,
)
from functools import singledispatch
from typing import (
    Any,
    Union,
    cast,
)
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
from flanj.utils.objs import (
    is_list_like,
    is_str,
)

from pytz import all_timezones as timezones
from .url import validate_url


def validate_time_zone(value: Any) -> None:
    if hasattr(value, 'encode'):
        value = cast(str, value)
        if value and value in timezones:
            return
    raise ValueError(
        "This value must must be a valid timezone name. A valid timezone "
        "name can be found at https://tinyurl.com/h6edjuq in the 'TZ "
        "database name' column."
    )


@singledispatch
def validate_default_sequence(
        value: Any,
        root_type: str,
        is_root: bool = True
) -> None:
    value_type = type(value).__name__
    if is_root is True:
        raise TypeError(
            'The value must be of type %r, got %r' % (root_type, value_type)
        )
    if value is None:
        return
    raise TypeError(
        'This sequence contains an item with the invalid type of %r. '
        'Each sequence item can only be of type: '
        'bool, float, int, None, list, tuple or str.' % value_type
    )


# noinspection PyUnusedFunction,PyUnusedLocal
@validate_default_sequence.register(float)
def _validate_default_sequence_float(
        value: float,
        root_type: str,
        is_root: bool = True,
) -> None:
    if is_root is True:
        raise TypeError(
            "The value must be of type %r, got 'float'" % root_type
        )
    return


# noinspection PyUnusedFunction,PyUnusedLocal
@validate_default_sequence.register(int)
def _validate_default_sequence_int(
        value: int,
        root_type: str,
        is_root: bool = True,
) -> None:
    if is_root is True:
        raise TypeError(
            "The value must be of type %r, got 'int'" % root_type
        )
    return


# noinspection PyUnusedFunction,PyUnusedLocal
@validate_default_sequence.register(bool)
def _validate_default_sequence_bool(
        value: bool,
        root_type: str,
        is_root: bool = True
) -> None:
    if is_root is True:
        raise TypeError(
            "The value must be of type %r, got 'bool'" % root_type
        )
    return


# noinspection PyUnusedFunction
@validate_default_sequence.register(Sequence)
def _validate_default_sequence_sequence(
        value: Sequence,
        root_type: str,
        is_root: bool = True
) -> None:

    if hasattr(value, 'capitalize'):
        value_type = 'str'
    elif isinstance(value, list):
        value_type = 'list'
    else:
        value_type = 'tuple'

    if is_root is True and value_type != root_type:
        raise TypeError(
            'The value must be of type %r, got %r' % (root_type, value_type)
        )

    if value_type == 'str':
        return

    for val in value:
        validate_default_sequence(val, root_type, is_root=False)


def validate_admins(value: Sequence) -> None:
    validate_email = EmailValidator()
    if not is_list_like(value):
        raise TypeError("The value must be of type 'list' or 'tuple'")
    for r, row in enumerate(value):
        if not is_list_like(row):
            raise TypeError(
                f"The value at [{r}], must be of type "
                f"'list' or 'tuple'."
            )
        if len(row) != 2:
            raise ValueError(
                f"The value at [{r}], must be of type 'list' or 'tuple' and "
                f"contain only two values of type 'str'."
            )
        for c, val in enumerate(row):
            if not is_str(val):
                raise TypeError(
                    f"The value at [{r}][{c}], must be of type 'str'."
                )
            if c == 1:
                try:
                    validate_email(val)
                except ValidationError:
                    raise ValueError(
                        f'The value at [{r}][{c}], must be a valid email '
                        f'address.'
                    )


def validate_directory_is_read_write(path: str) -> None:
    if not os.path.exists(path):
        raise FileNotFoundError(
            'The directory, %r, does not exist.' % path
        )
    if not os.path.isdir(path):
        raise NotADirectoryError(path)
    if not os.access(path, os.R_OK):
        raise PermissionError(
            'The directory, %r, is not readable.' % path
        )
    if not os.access(path, os.W_OK):
        raise PermissionError(
            'The directory, %r, is not writable.' % path
        )


def validate_file_is_read_write(path: str) -> None:
    if not os.path.exists(path):
        raise FileNotFoundError(
            'The file, %r, does not exist.' % path
        )
    if os.path.isdir(path):
        raise IsADirectoryError(path)
    if not os.access(path, os.R_OK):
        raise PermissionError(
            'The file, %r, is not readable.' % path
        )
    if not os.access(path, os.W_OK):
        raise PermissionError(
            'The file, %r, is not writable.' % path
        )


def validate_groups(
        value: Union[str, Iterable[dict[str, Any]]],
        return_value: bool = False
) -> Iterable[dict[str, Any]]:
    if hasattr(value, 'capitalize'):
        value = cast(str, value)
        if value.startswith(os.sep):
            if os.path.exists(value):
                if not os.path.isfile(value):
                    raise IsADirectoryError(value)
                if not os.access(value, os.R_OK):
                    raise PermissionError(
                        f'The file, {value!r}, is not readable.'
                    )
                with open(value) as f:
                    out = json.load(f)
                if return_value is True:
                    return out
            else:
                raise FileNotFoundError(
                    f'The file, {value!r}, does not exist.'
                )
        else:
            out = json.loads(value)
            if return_value is True:
                return out
    return []
