import os


def get_home(
        key: str,
        val: str,
        env_var: str,
        settings_file: str,
        default: str
) -> str:
    out = val or ''
    if not hasattr(out, 'encode'):
        out = ''
    out = out.strip()

    # Load the path from environment variable
    env_val = ''
    from_env = False
    if not out:
        env_val = os.getenv(env_var, '').strip()
        if env_val:
            from_env = True
            out = env_val

    if out:
        out = os.path.expanduser(out)
        out = os.path.expandvars(out)
        out = os.path.normpath(out)
        if not os.path.isabs(out):
            if from_env is True:
                raise ValueError(
                    "The value of %r from the environment variable, "
                    "%r, is NOT an absolute path. " % (
                        env_val,
                        env_var
                    )
                )
            else:
                raise ValueError(
                    "The value for %r of, %r, is NOT an "
                    "absolute path. Please correct the "
                    "'package_data_home' argument when calling 'Env' in "
                    "%r." % (
                        key,
                        val,
                        settings_file
                    )
                )
    else:
        out = default
    return out
