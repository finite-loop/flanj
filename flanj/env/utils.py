
from humanfriendly import (
    InvalidSize,
    parse_size
)


def data_size(text) -> int:
    """Parse a human readable data size and return the number of bytes."""
    try:
        return parse_size(text)
    except InvalidSize as e:
        raise ValueError(str(e))
