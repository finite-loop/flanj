from typing import (
    Union,
    Optional,
)
from django.core.management.base import CommandError
from .base import (
    BaseSetup,
    Database,
    List,
)


DROP = """
  DROP DATABASE IF EXISTS {}
"""


CREATE = """
  CREATE DATABASE {}
    WITH OWNER = {}
      ENCODING = 'UTF8'
      TABLESPACE = pg_default
      LC_COLLATE = 'en_US.UTF-8'
      LC_CTYPE = 'en_US.UTF-8'
      CONNECTION LIMIT = -1;    
"""

DISCONNECT = """
  UPDATE
    pg_database
  SET
    datallowconn = 'false'
  WHERE datname = %s;
"""

TERMINATE = """
  SELECT
    pg_terminate_backend(pid) 
  FROM
    pg_stat_activity
  WHERE
    datname = %s
"""

ALLOWCONNECT = """
  UPDATE
    pg_database
  SET
    datallowconn = 'true'
  WHERE datname = %s;
"""


# noinspection SqlDialectInspection,SqlNoDataSourceInspection
class Databases(BaseSetup):

    def fetch(
            self,
            dbname: str,
            hide_stdout: bool = False
    ) -> Union[Database, tuple]:
        return self._fetch_database(dbname, hide_stdout=hide_stdout)

    def disconnect_all(
            self,
            dbname: str
    ) -> None:
        if self.fetch(dbname, hide_stdout=True):
            self._echo_begin(
                f"Disconnecting all connections to database: '{dbname}'."
            )

            self._execute(DISCONNECT, params=[dbname])
            self._execute(TERMINATE, params=[dbname])

            self._echo_success(
                f"All connections to database: '{dbname}' have been "
                f"disconnected."
            )

    def allow_connections(
            self,
            dbname: str
    ) -> None:
        if self.fetch(dbname, hide_stdout=True):
            self._echo_begin(
                f"Allowing connections to database: '{dbname}'."
            )

            self._execute(ALLOWCONNECT, params=[dbname])

            self._echo_success(
                f"All connections to database: '{dbname}' are now allowed. "
            )



    def drop(
            self,
            dbname: str
    ) -> None:
        if self.fetch(dbname, hide_stdout=True):
            self._echo_begin(f"Dropping database '{dbname}'.")

            msg: str = self._execute(DROP, ident_args=[dbname])

            if self.fetch(dbname, hide_stdout=True):
                raise CommandError(
                    f"Unable to drop the database '{dbname}'.{msg}"
                )

            self._echo_success(f"Database: '{dbname}' has been dropped.")

    def create(
            self,
            name: str,
            owner: str,
            force: Optional[bool] = None
    ) -> None:
        self._echo_begin(f"Creating database '{name}'.")
        text = (
            "Unable to create the database: '%s' with the owner of: '%s'."
            % (name, owner)
        )
        self._validate_user_exists(owner, text=text)

        if force is None:
            force = self.force
        if force is True:
            self.drop(name)

        if self.fetch(name, hide_stdout=True):
            self._echo_success("Database: '{}' already exists.", name)
            return None

        msg: str = self._execute(CREATE, ident_args=[name, owner])

        if not self.fetch(name, hide_stdout=True):
            raise CommandError(
                "Unable to create the database: '%s'\n%s"
                % (name, msg)
            )

        self._echo_success("Database '{}' has been created.", name)

    def grant(
            self,
            dbname: str,
            role: str,
            create: bool = False,
            temporary: bool = False,
            connect: bool = False,
            grant_option: bool = False
    ) -> None:
        if any((create, temporary, connect)):
            self._echo_begin(
                f"Granting privileges to role: '{role}' on database: "
                f"'{dbname}'."
            )
            text = (
                f"Unable to grant privileges to role: '{role}' on database: "
                f"'{dbname}'."
            )
            self._validate_database_exists(dbname, text=text, hide_stdout=True)
            self._validate_role_exists(role, text=text, hide_stdout=True)

            parts: List[str] = ['  GRANT\n']
            if all((create, temporary, connect)):
                parts.append('    ALL\n')
            else:
                sub_parts: List[str] = list()
                if create is True:
                    sub_parts.append('    CREATE')
                if temporary is True:
                    sub_parts.append('    TEMPORARY')
                if connect is True:
                    sub_parts.append('    CONNECT')
                parts.append('%s\n' % ',\n'.join(sub_parts))
            parts.append('  ON DATABASE {} TO {}\n')
            if grant_option is True:
                parts.append('  WITH GRANT OPTION\n')
            query: str = '%s;' % ''.join(parts)

            self._execute(query, ident_args=[dbname, role])

            # Need to verify that the privilege has actually been set.

            self._echo_success(
                f"Granted privileges to role: '{role}' on database: "
                f"'{dbname}'."
            )

    def revoke_all(
            self,
            role: str,
            dbname: str
    ) -> None:
        self._echo_begin(
            f"Revoking all privileges from role: '{role}' on database: "
            f"'{dbname}'."
        )
        text = (
            f"Unable to revoke all privileges from role: '{role}' on database: "
            f"'{dbname}'."
        )
        self._validate_database_exists(dbname, text=text, hide_stdout=True)
        self._validate_role_exists(role, text=text, hide_stdout=True)

        query = "REVOKE ALL ON DATABASE {} FROM {};"
        self._execute(query, ident_args=[dbname, role])

        # Need to verify that the privilege has actually been set.

        self._echo_success(
            f"Revoked all privileges from role: '{role}' on database: {dbname}."
        )

    def _validate_role(
            self,
            role: str,
            privilege_type: str,
            is_grant: bool = True,
            hide_stdout: bool = True
    ) -> None:
        action: str = ''
        dest: str = ''
        dbname = self.conn_info.database
        if is_grant is True:
            action = 'grant'
            dest = 'to'
        else:
            action = 'revoke'
            dest = 'from'

        text = (
            f"Unable to {action} all default-{privilege_type}-privileges "
            f"{dest} role: '{role}' on database: '{dbname}'."
        )
        self._validate_role_exists(role, text=text, hide_stdout=hide_stdout)

    def _echo_start(
            self,
            role: str,
            privilege_type: str,
            is_grant: bool = True,
            hide_stdout: bool = False
    ) -> None:
        dbname = self.conn_info.database
        if hide_stdout is False and self.verbosity > 1:
            if self.verbosity > 2:
                self._print('')
                self._print('-' * 80)
                self._print('')
                self._print('')
            action: str = ''
            dest: str = ''
            if is_grant is True:
                action = 'Granting'
                dest = 'to'
            else:
                action = 'Revoking all'
                dest = 'from'

            self._print(
                self._header(
                    f"{action} all default-{privilege_type}-privileges {dest} "
                    f"role: '{role}' in database: '{dbname}':"
                )
            )
            self._print('')

            self._validate_role(
                role,
                privilege_type,
                is_grant=is_grant,
                hide_stdout=hide_stdout
            )

    def _echo_complete(
            self,
            role: str,
            privilege_type: str,
            is_grant: bool = True,
            hide_stdout: bool = False
    ) -> None:
        dbname = self.conn_info.database
        if hide_stdout is False and self.verbosity > 0:
            action: str = ''
            dest: str = ''
            if is_grant is True:
                action = 'Granted'
                dest = 'to'
            else:
                action = 'Revoked all'
                dest = 'from'

            self._print(
                self._success(
                    f"{action} default-{privilege_type}-privileges {dest} "
                    f"role: '{role}' in database: '{dbname}'."
                )
            )
            if self.verbosity > 1:
                self._print('')
                self._print('-' * 80)
                self._print('')

    def revoke_all_default_table_privileges(
            self,
            role: str,
            hide_stdout: bool = False
    ) -> None:
        args = [role, 'table']
        kwargs = dict(is_grant=False, hide_stdout=hide_stdout)
        self._echo_start(*args, **kwargs)

        if role.lower() == 'public':
            role = 'PUBLIC'

        first = (
            "ALTER DEFAULT PRIVILEGES \n"
            "  REVOKE GRANT OPTION FOR ALL\n"
            "  ON TABLES\n"
            "  FROM {};"
        )
        self._execute(first, ident_args=[role])

        second = (
            "ALTER DEFAULT PRIVILEGES\n"
            "  REVOKE ALL\n"
            "  ON TABLES\n"
            "  FROM {};"
        )
        self._execute(second, ident_args=[role])
        self._echo_complete(*args, **kwargs)

    def set_default_table_privileges(
            self,
            role: str,
            select: bool = False,
            insert: bool = False,
            update: bool = False,
            delete: bool = False,
            truncate: bool = False,
            references: bool = False,
            trigger: bool = False,
            grant_option: bool = False,
    ) -> None:
        args = [role, 'table']
        self._echo_start(*args)
        if role.lower() == 'public':
            role = 'PUBLIC'
        parts: List[str] = [
            'ALTER DEFAULT PRIVILEGES\n'
        ]
        if any((select, insert, update, delete, truncate, references, trigger)):
            parts.append('  GRANT\n')
        else:
            return None
        if all((select, insert, update, delete, truncate, references, trigger)):
            parts.append('    ALL\n')
        else:
            if select is True:
                parts.append('    SELECT\n')
            if insert is True:
                parts.append('    INSERT\n')
            if update is True:
                parts.append('    UPDATE\n')
            if delete is True:
                parts.append('    DELETE\n')
            if truncate is True:
                parts.append('    TRUNCATE\n')
            if references is True:
                parts.append('    REFERENCES\n')
            if trigger is True:
                parts.append('    TRIGGER\n')

        parts.append('  ON TABLES TO {}')
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self.revoke_all_default_table_privileges(role, hide_stdout=True)

        self._execute(query, ident_args=[role])
        self._echo_complete(*args)

    def revoke_all_default_sequence_privileges(
            self,
            role: str,
            hide_stdout: bool = False
    ) -> None:
        args = [role, 'sequence']
        kwargs = dict(is_grant=False, hide_stdout=hide_stdout)
        self._echo_start(*args, **kwargs)
        if role.lower() == 'public':
            role = 'PUBLIC'

        first = (
            "ALTER DEFAULT PRIVILEGES\n"
            "  REVOKE GRANT OPTION FOR ALL\n"
            "  ON SEQUENCES\n"
            "  FROM {};"
        )
        self._execute(first, ident_args=[role])

        second = (
            "ALTER DEFAULT PRIVILEGES\n"
            "  REVOKE ALL\n"
            "  ON SEQUENCES\n"
            "  FROM {};"
        )
        self._execute(second, ident_args=[role])
        self._echo_complete(*args, **kwargs)

    def set_default_sequence_privileges(
            self,
            role: str,
            usage: bool = False,
            select: bool = False,
            update: bool = False,
            grant_option: bool = False
    ) -> None:
        args = [role, 'sequence']
        self._echo_start(*args)
        if role.lower() == 'public':
            role = 'PUBLIC'

        parts: List[str] = [
            'ALTER DEFAULT PRIVILEGES\n'
        ]
        if any((usage, select, update)):
            parts.append('  GRANT\n')
        else:
            return None
        if all((usage, select, update)):
            parts.append('    ALL\n')
        else:
            if usage is True:
                parts.append('    USAGE\n')
            if select is True:
                parts.append('    SELECT\n')
            if update is True:
                parts.append('    UPDATE\n')

        parts.append('  ON SEQUENCES TO {}')
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self.revoke_all_default_sequence_privileges(role, hide_stdout=True)

        self._execute(query, ident_args=[role])
        self._echo_complete(*args)

    def revoke_all_default_function_privileges(
            self,
            role: str,
            hide_stdout: bool = False
    ) -> None:
        args = [role, 'function']
        kwargs = dict(is_grant=False, hide_stdout=hide_stdout)
        self._echo_start(*args, **kwargs)
        if role.lower() == 'public':
            role = 'PUBLIC'

        first = (
            "ALTER DEFAULT PRIVILEGES\n"
            "  REVOKE GRANT OPTION FOR ALL\n"
            "  ON FUNCTIONS\n"
            "  FROM {};"
        )
        self._execute(first, ident_args=[role])

        second = (
            "ALTER DEFAULT PRIVILEGES\n"
            "  REVOKE ALL\n"
            "  ON FUNCTIONS\n"
            "  FROM {};"
        )
        self._execute(second, ident_args=[role])
        self._echo_complete(*args, **kwargs)

    def set_default_function_privileges(
            self,
            role: str,
            execute: bool = False,
            grant_option: bool = False
    ) -> None:
        args = [role, 'function']
        self._echo_start(*args)
        if role.lower() == 'public':
            role = 'PUBLIC'

        parts: List[str] = [
            'ALTER DEFAULT PRIVILEGES\n',
            '    GRANT\n'
        ]
        if execute is True:
            parts.append('    EXECUTE\n')
        else:
            parts.append('    ALL\n')

        parts.append('  ON FUNCTIONS TO {}')
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self.revoke_all_default_function_privileges(role, hide_stdout=True)

        self._execute(query, ident_args=[role])
        self._echo_complete(*args)

    def revoke_all_default_type_privileges(
            self,
            role: str,
            hide_stdout: bool = False
    ) -> None:
        args = [role, 'type']
        kwargs = dict(is_grant=False, hide_stdout=hide_stdout)
        self._echo_start(*args, **kwargs)
        if role.lower() == 'public':
            role = 'PUBLIC'

        first = (
            "ALTER DEFAULT PRIVILEGES\n"
            "  REVOKE GRANT OPTION FOR ALL\n"
            "  ON TYPES\n"
            "  FROM {};"
        )
        self._execute(first, ident_args=[role])

        second = (
            "ALTER DEFAULT PRIVILEGES\n"
            "  REVOKE ALL\n"
            "  ON TYPES\n"
            "  FROM {};"
        )
        self._execute(second, ident_args=[role])
        self._echo_complete(*args, **kwargs)

    def set_default_type_privileges(
            self,
            role: str,
            usage: bool = False,
            grant_option: bool = False
    ) -> None:
        args = [role, 'type']
        self._echo_start(*args)
        if role.lower() == 'public':
            role = 'PUBLIC'

        parts: List[str] = [
            'ALTER DEFAULT PRIVILEGES\n',
            '  GRANT\n'
        ]
        if usage is True:
            parts.append('    USAGE\n')
        else:
            parts.append('    ALL\n')

        parts.append('  ON TYPES TO {}')
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self.revoke_all_default_type_privileges(role, hide_stdout=True)

        self._execute(query, ident_args=[role])
        self._echo_complete(*args)

    def set_admin_privileges(
            self,
            dbname: str,
            role: str = 'global_admin'
    ) -> None:
        self.grant(
            dbname,
            role,
            create=True,
            temporary=True,
            connect=True,
            grant_option=True
        )

    def set_default_admin_privileges(
            self,
            role: str = 'global_admin'
    ) -> None:
        self.set_default_table_privileges(
            role,
            select=True,
            insert=True,
            update=True,
            delete=True,
            truncate=True,
            references=True,
            trigger=True,
            grant_option=True
        )
        self.set_default_sequence_privileges(
            role,
            usage=True,
            select=True,
            update=True,
            grant_option=True
        )
        self.set_default_function_privileges(role, grant_option=True)
        self.set_default_type_privileges(role, grant_option=True)

    def set_usage_privileges(
            self,
            dbname: str,
            role: str = 'global_usage'
    ) -> None:
        self.grant(
            dbname,
            role,
            create=True,
            temporary=True,
            connect=True
        )

    def set_default_usage_privileges(
            self,
            role: str = 'global_usage'
    ) -> None:
        self.set_default_table_privileges(
            role,
            select=True,
            insert=True,
            update=True,
            delete=True,
            truncate=True,
            references=True,
            trigger=True
        )
        self.set_default_sequence_privileges(
            role,
            usage=True,
            select=True,
            update=True,
        )
        self.set_default_function_privileges(role)
        self.set_default_type_privileges(role)

    def set_readonly_privileges(
            self,
            dbname: str,
            role: str = 'global_readonly'
    ) -> None:
        self.grant(
            dbname,
            role,
            connect=True
        )

    def set_default_readonly_privileges(
            self,
            role: str = 'global_readonly'
    ) -> None:
        self.set_default_table_privileges(
            role,
            select=True
        )
        self.set_default_sequence_privileges(
            role,
            select=True
        )

