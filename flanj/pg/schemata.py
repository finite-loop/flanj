from .base import (
    BaseSetup,
    Schema,
)
from typing import (
    NamedTuple,
    Union,
    Optional,
    List,
)
from django.core.management.base import CommandError


ALTER_OWNER = "ALTER SCHEMA {} OWNER TO {};"

CREATE = "CREATE SCHEMA {} AUTHORIZATION {};"

DROP = "DROP SCHEMA {} CASCADE;"


class Schemata(BaseSetup):

    def fetch(
            self,
            name: str,
            hide_stdout: bool = False
    ) -> Union[Schema, tuple]:
        return self._fetch_schema(name, hide_stdout=hide_stdout)

    def set_owner(
            self,
            name: str,
            owner: str,
    ) -> None:
        self._echo_begin(f"Schema: '{name}' setting owner to '{owner}'.")
        text = (
            "Unable to set the owner of schema: '%s' to role: '%s' in database "
            "'%s'."
            % (name, owner, self.conn_info.database)
        )
        self._validate_schema_exists(name, text, hide_stdout=True)
        self._validate_user_exists(owner, text, hide_stdout=True)

        schema: Schema = self.fetch(name, hide_stdout=True)
        if schema.owner != owner:

            msg: str = self._execute(
                ALTER_OWNER,
                ident_args=[name, owner]
            )

            schema: Schema = self.fetch(name, hide_stdout=True)
            if schema.owner != owner:
                raise CommandError(
                    "Unable to set owner to '%s' in schema '%'.\n%s"
                    % (owner, schema, msg)
                )
            self._echo_success(
                "Schema: '{}' owner has been set to '{}'.",
                name,
                owner
            )

    def drop(
            self,
            name: str
    ) -> None:
        self._echo_begin(f"Dropping schema '{name}'.")
        if self.fetch(name, hide_stdout=True):
            msg: str = self._execute(DROP, ident_args=[name])
            if self.fetch(name, hide_stdout=True):
                raise CommandError(
                    "Unable to drop the schema '%s'\n%s"
                    % (name, msg)
                )

            self._echo_success(f"Schema: '{name}' dropped.")
        else:
            self._echo_warning(f"Schema: '{name}' does not exist.")

    def create(
            self,
            name: str,
            owner: str,
            force: Optional[bool] = None
    ) -> None:
        self._echo_begin(
            "Creating schema '{}' in database '{}'.",
            name,
            self.conn_info.database
        )
        text = (
            "Unable to create the schema: '%s' owned by: '%s' in database: "
            "'%s'."
            % (name, owner, self.conn_info.database)
        )
        self._validate_role_exists(owner, text, hide_stdout=True)
        if force is None:
            force = self.force
        if force is True:
            self.drop(name)
        if self.fetch(name, hide_stdout=True):
            self._echo_success(
                "Schema: '{}' already exists in database: '{}'.",
                name,
                self.conn_info.database,
            )
            return None

        msg: str = self._execute(CREATE, ident_args=[name, owner])

        if not self.fetch(name, hide_stdout=True):
            raise CommandError(
                "Unable to create the schema: '%s' in database '%s'.\n%s"
                % (name, self.conn_info.database, msg)
            )

        self._echo_success(
            "Schema: '{}' has been created in database '{}'",
            name,
            self.conn_info.database
        )

    def grant(
            self,
            name: str,
            role: str,
            privilege: str,
            grant_option: bool = False
    ):
        privileges = (
            'all',
            'create',
            'usage',
        )
        privilege = privilege.strip().lower()
        if privilege not in privileges:
            raise ValueError(
                "Unknown privilege %r. Must be one of %r"
                % (privilege, privileges)
            )
        if privileges in ('usage', 'create',):
            with_grant_option = False

        priv: str = privilege
        if grant_option is True:
            priv = '%s with grant option' % privilege

        self._echo_begin(
            f"Granting privilege: '{priv}' to role: '{role}' on schema: "
            f"'{name}'."
        )

        text = (
            "Unable to grant privileges on schema: '%s' to role: '%s'."
            % (name, role)
        )
        self._validate_role_exists(role, text=text)
        self._validate_schema_exists(name, text=text)

        parts: List[str] = [
            '  GRANT\n',
            '    %s\n' % privilege.upper(),
            '  ON SCHEMA {}\n',
            '  TO {}'
        ]
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self._execute(query, ident_args=[name, role])

        # Need to verify that the privilege has actually been set.

        self._echo_success(
            f"Granted privilege: '{priv}' to role: '{role}' on schema: "
            f"'{name}'."
        )

    def _validate_schema_role(
            self,
            name: str,
            role: str,
            privilege_type: str,
            is_grant: bool = True,
            hide_stdout: bool = True
    ) -> None:
        action: str = ''
        dest: str = ''
        if is_grant is True:
            action = 'grant'
            dest = 'to'
        else:
            action = 'revoke'
            dest = 'from'

        text = (
                f"Unable to {action} all default-{privilege_type}-privileges "
                f"{dest} role: '{role}' in schema: '{name}'."
        )
        self._validate_role_exists(role, text=text, hide_stdout=hide_stdout)
        self._validate_schema_exists(name, text=text, hide_stdout=hide_stdout)

    def _echo_start(
            self,
            name: str,
            role: str,
            privilege_type: str,
            is_grant: bool = True,
            hide_stdout: bool = False
    ) -> None:
        if hide_stdout is False and self.verbosity > 1:
            if self.verbosity > 2:
                self._print('')
                self._print('-' * 80)
                self._print('')
                self._print('')
            action: str = ''
            dest: str = ''
            if is_grant is True:
                action = 'Granting'
                dest = 'to'
            else:
                action = 'Revoking all'
                dest = 'from'

            self._print(
                self._header(
                    f"{action} all default-{privilege_type}-privileges {dest} "
                    f"role: '{role}' in schema: '{name}':"
                )
            )
            self._print('')

    def _echo_complete(
            self,
            name: str,
            role: str,
            privilege_type: str,
            is_grant: bool = True,
            hide_stdout: bool = False
    ) -> None:
        if hide_stdout is False and self.verbosity > 0:
            action: str = ''
            dest: str = ''
            if is_grant is True:
                action = 'Granted'
                dest = 'to'
            else:
                action = 'Revoked all'
                dest = 'from'

            self._print(
                self._success(
                    f"{action} default-{privilege_type}-privileges {dest} "
                    f"role: '{role}' in schema: '{name}'."
                )
            )
            if self.verbosity > 1:
                self._print('')
                self._print('-' * 80)
                self._print('')

    def revoke_all_default_table_privileges(
            self,
            name: str,
            role: str,
            hide_stdout: bool = False
    ) -> None:
        args = [name, role, 'table']
        kwargs = dict(is_grant=False, hide_stdout=hide_stdout)
        self._echo_start(*args, **kwargs)
        self._validate_schema_role(*args, **kwargs)

        if role.lower() == 'public':
            role = 'PUBLIC'

        first = (
            "ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n"
            "  REVOKE GRANT OPTION FOR ALL\n"
            "  ON TABLES\n"
            "  FROM {role};"
        )
        self._execute(
            first,
            ident_kwargs=dict(name=name, role=role)
        )
        second = (
            "ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n"
            "  REVOKE ALL\n"
            "  ON TABLES\n"
            "  FROM {role};"
        )
        self._execute(
            second,
            ident_kwargs=dict(name=name, role=role)
        )
        self._echo_complete(*args, **kwargs)

    def set_default_table_privileges(
            self,
            name: str,
            role: str,
            select: bool = False,
            insert: bool = False,
            update: bool = False,
            delete: bool = False,
            truncate: bool = False,
            references: bool = False,
            trigger: bool = False,
            grant_option: bool = False,
    ) -> None:
        args = [name, role, 'table']
        self._echo_start(*args)
        self._validate_schema_role(*args)
        if role.lower() == 'public':
            role = 'PUBLIC'
        if role.lower() == 'public':
            role = 'PUBLIC'

        parts: List[str] = [
            'ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n'
        ]
        if any((select, insert, update, delete, truncate, references, trigger)):
            parts.append('  GRANT\n')
        else:
            return None
        if all((select, insert, update, delete, truncate, references, trigger)):
            parts.append('    ALL\n')
        else:
            if select is True:
                parts.append('    SELECT\n')
            if insert is True:
                parts.append('    INSERT\n')
            if update is True:
                parts.append('    UPDATE\n')
            if delete is True:
                parts.append('    DELETE\n')
            if truncate is True:
                parts.append('    TRUNCATE\n')
            if references is True:
                parts.append('    REFERENCES\n')
            if trigger is True:
                parts.append('    TRIGGER\n')

        parts.append('  ON TABLES TO {role}')
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self.revoke_all_default_table_privileges(
            name,
            role,
            hide_stdout=True
        )

        self._execute(
            query,
            ident_kwargs=dict(name=name, role=role)
        )
        self._echo_complete(*args)

    def revoke_all_default_sequence_privileges(
            self,
            name: str,
            role: str,
            hide_stdout: bool = False
    ) -> None:
        args = [name, role, 'sequence']
        kwargs = dict(is_grant=False, hide_stdout=hide_stdout)
        self._echo_start(*args, **kwargs)
        self._validate_schema_role(*args, **kwargs)
        if role.lower() == 'public':
            role = 'PUBLIC'

        first = (
            "ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n"
            "  REVOKE GRANT OPTION FOR ALL\n"
            "  ON SEQUENCES\n"
            "  FROM {role};"
        )
        self._execute(
            first,
            ident_kwargs=dict(name=name, role=role)
        )
        second = (
            "ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n"
            "  REVOKE ALL\n"
            "  ON SEQUENCES\n"
            "  FROM {role};"
        )
        self._execute(
            second,
            ident_kwargs=dict(name=name, role=role)
        )
        self._echo_complete(*args, **kwargs)

    def set_default_sequence_privileges(
            self,
            name: str,
            role: str,
            usage: bool = False,
            select: bool = False,
            update: bool = False,
            grant_option: bool = False
    ) -> None:
        args = [name, role, 'sequence']
        self._echo_start(*args)
        self._validate_schema_role(*args)
        if role.lower() == 'public':
            role = 'PUBLIC'

        parts: List[str] = [
            'ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n'
        ]
        if any((usage, select, update)):
            parts.append('  GRANT\n')
        else:
            return None
        if all((usage, select, update)):
            parts.append('    ALL\n')
        else:
            if usage is True:
                parts.append('    USAGE\n')
            if select is True:
                parts.append('    SELECT\n')
            if update is True:
                parts.append('    UPDATE\n')

        parts.append('  ON SEQUENCES TO {role}')
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self.revoke_all_default_sequence_privileges(
            name,
            role,
            hide_stdout=True
        )

        self._execute(
            query,
            ident_kwargs=dict(name=name, role=role)
        )
        self._echo_complete(*args)

    def revoke_all_default_function_privileges(
            self,
            name: str,
            role: str,
            hide_stdout: bool = False
    ) -> None:
        args = [name, role, 'function']
        kwargs = dict(is_grant=False, hide_stdout=hide_stdout)
        self._echo_start(*args, **kwargs)
        self._validate_schema_role(*args, **kwargs)
        if role.lower() == 'public':
            role = 'PUBLIC'

        first = (
            "ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n"
            "  REVOKE GRANT OPTION FOR ALL\n"
            "  ON FUNCTIONS\n"
            "  FROM {role};"
        )
        self._execute(
            first,
            ident_kwargs=dict(name=name, role=role)
        )
        second = (
            "ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n"
            "  REVOKE ALL\n"
            "  ON FUNCTIONS\n"
            "  FROM {role};"
        )
        self._execute(
            second,
            ident_kwargs=dict(name=name, role=role)
        )
        self._echo_complete(*args, **kwargs)

    def set_default_function_privileges(
            self,
            name: str,
            role: str,
            execute: bool = False,
            grant_option: bool = False
    ) -> None:
        args = [name, role, 'function']
        self._echo_start(*args)
        self._validate_schema_role(*args)
        if role.lower() == 'public':
            role = 'PUBLIC'

        parts: List[str] = [
            'ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n',
            '    GRANT\n'
        ]
        if execute is True:
            parts.append('    EXECUTE\n')
        else:
            parts.append('    ALL\n')

        parts.append('  ON FUNCTIONS TO {role}')
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self.revoke_all_default_function_privileges(
            name,
            role,
            hide_stdout=True
        )

        self._execute(
            query,
            ident_kwargs=dict(name=name, role=role)
        )

        self._echo_complete(*args)

    def revoke_all_default_type_privileges(
            self,
            name: str,
            role: str,
            hide_stdout: bool = False
    ) -> None:
        args = [name, role, 'type']
        kwargs = dict(is_grant=False, hide_stdout=hide_stdout)
        self._echo_start(*args, **kwargs)
        self._validate_schema_role(*args, **kwargs)
        if role.lower() == 'public':
            role = 'PUBLIC'

        first = (
            "ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n"
            "  REVOKE GRANT OPTION FOR ALL\n"
            "  ON TYPES\n"
            "  FROM {role};"
        )
        self._execute(
            first,
            ident_kwargs=dict(name=name, role=role)
        )

        second = (
            "ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n"
            "  REVOKE ALL\n"
            "  ON TYPES\n"
            "  FROM {role};"
        )
        self._execute(
            second,
            ident_kwargs=dict(name=name, role=role)
        )
        self._echo_complete(*args, **kwargs)

    def set_default_type_privileges(
            self,
            name: str,
            role: str,
            usage: bool = False,
            grant_option: bool = False
    ) -> None:
        args = [name, role, 'type']
        self._echo_start(*args)
        self._validate_schema_role(*args)
        if role.lower() == 'public':
            role = 'PUBLIC'

        parts: List[str] = [
            'ALTER DEFAULT PRIVILEGES IN SCHEMA {name}\n',
            '  GRANT\n'
        ]
        if usage is True:
            parts.append('    USAGE\n')
        else:
            parts.append('    ALL\n')

        parts.append('  ON TYPES TO {role}')
        if grant_option is True:
            parts.append(' WITH GRANT OPTION')

        query: str = '%s;' % ''.join(parts)

        self.revoke_all_default_type_privileges(
            name,
            role,
            hide_stdout=True
        )

        self._execute(
            query,
            ident_kwargs=dict(name=name, role=role)
        )
        self._echo_complete(*args)

    def set_admin_privileges(
            self,
            schema: str,
            role: str = 'global_admin'
    ) -> None:
        self.grant(
            schema,
            role,
            'all',
            grant_option=True
        )
        self.set_default_table_privileges(
            schema,
            role,
            select=True,
            insert=True,
            update=True,
            delete=True,
            truncate=True,
            references=True,
            trigger=True,
            grant_option=True
        )
        self.set_default_sequence_privileges(
            schema,
            role,
            usage=True,
            select=True,
            update=True,
            grant_option=True
        )
        self.set_default_function_privileges(
            schema,
            role,
            grant_option=True
        )
        self.set_default_type_privileges(
            schema,
            role,
            grant_option=True
        )

    def set_usage_privileges(
            self,
            schema: str,
            role: str = 'global_usage'
    ) -> None:
        self.grant(
            schema,
            role,
            'all'
        )
        self.set_default_table_privileges(
            schema,
            role,
            select=True,
            insert=True,
            update=True,
            delete=True,
            truncate=True,
            references=True,
            trigger=True,
        )
        self.set_default_sequence_privileges(
            schema,
            role,
            usage=True,
            select=True,
            update=True,
        )
        self.set_default_function_privileges(
            schema,
            role
        )
        self.set_default_type_privileges(
            schema,
            role
        )

    def set_readonly_privileges(
            self,
            schema: str,
            role: str = 'global_readonly'
    ) -> None:
        self.grant(
            schema,
            role,
            'usage'
        )
        self.set_default_table_privileges(
            schema,
            role,
            select=True
        )
        self.set_default_sequence_privileges(
            schema,
            role,
            select=True,
        )
        self.set_default_function_privileges(
            schema,
            role,
            execute=True
        )
        self.set_default_type_privileges(
            schema,
            role,
            usage=True,
        )


