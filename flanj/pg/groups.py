from .base import (
    BaseSetup,
    Result,
    Group,
)
from typing import (
    NamedTuple,
    Union,
    Optional,
    List,
)
from django.core.management.base import CommandError


# noinspection SqlDialectInspection,SqlNoDataSourceInspection
class Groups(BaseSetup):

    def fetch(
            self,
            name: str,
            hide_stdout: bool = False
    ) -> Union[Group, tuple]:
        return self._fetch_group(name, hide_stdout=hide_stdout)

    def drop(
            self,
            name: str
    ) -> None:
        self._echo_begin(f"Dropping group: '{name}'...")
        if self.fetch(name, hide_stdout=True):
            msg: str = self._execute("DROP ROLE {};", ident_args=[name])

            if self.fetch(name, hide_stdout=True):
                raise CommandError(f"Unable to remove the group '{name}'{msg}")

            self._echo_success(f"Group: '{name}' was dropped.")
        else:
            self._echo_warning(f"Group: {name} does not exist.")

    def create(
            self,
            name: str,
            is_superuser: bool = False,
            can_create_database: bool = False,
            can_create_role: bool = False,
            inherit: bool = False,
            can_replicate: bool = False,
            connection_limit: int = -1,
            force: Optional[bool] = None
    ) -> None:
        self._echo_begin(f"Creating group '{name}'...")
        if force is None:
            force = self.force
        if force is True:
            self.drop(name)

        if self.fetch(name, hide_stdout=True):
            self._echo_success(f"Group: '{name}' already exists.")
            return None

        parts: List[str] = ['CREATE ROLE {}']

        if is_superuser is True:
            parts.append('SUPERUSER')
        else:
            parts.append('NOSUPERUSER')

        if can_create_database is True:
            parts.append('CREATEDB')
        else:
            parts.append('NOCREATEDB')

        if can_create_role is True:
            parts.append('CREATEROLE')
        else:
            parts.append('NOCREATEROLE')

        if inherit is True:
            parts.append('INHERIT')
        else:
            parts.append('NOINHERIT')

        parts.append('NOLOGIN')

        if can_replicate is True:
            parts.append('REPLICATION')
        else:
            parts.append('NOREPLICATION')

        if connection_limit > -1:
            parts.append('CONNECTION LIMIT %s' % connection_limit)

        query: str = ' '.join(parts)

        msg: str = self._execute(query, ident_args=[name])

        if not self.fetch(name, hide_stdout=True):
            raise CommandError(f"Unable to create the group: '{name}'.{msg}")

        self._echo_success(f"Group: '{name}' has been created.")

    def create_admin(
            self,
            name: str = 'global_admin',
            connection_limit: int = -1,
            force: bool = False,
    ) -> None:
        self.create(
            name,
            can_create_database=True,
            can_create_role=True,
            connection_limit=connection_limit,
            force=force
        )

    def create_usage(
            self,
            name: str = 'global_usage',
            connection_limit: int = -1,
            force: bool = False
    ) -> None:
        self.create(
            name,
            can_create_database=True,
            connection_limit=connection_limit,
            force=force
        )

    def create_readonly(
            self,
            name: str = 'global_readonly',
            connection_limit: int = -1,
            force: bool = False
    ) -> None:
        self.create(
            name,
            connection_limit=connection_limit,
            force=force
        )
