import sys
import ipaddress
import socket
import getpass
from typing import (
    Optional,
    NamedTuple,
    Union,
    Tuple,
)
from pathlib import (
    Path,
    PosixPath,
)

from flanj.network import (
    system_addresses,
    IpV4,
    IpV6,
)


class PgpassRow(NamedTuple):
    host: str
    port: int
    dbname: str
    user: str
    password: str


class Pgpass:

    def __init__(self, path: Union[str, PosixPath, None] = None) -> None:
        if not isinstance(path, PosixPath):
            if not isinstance(path, str):
                path = ''
            path = path.strip()
            if not path:
                path = '~/.pgpass'
            path = Path(path)
        self.path: PosixPath = path.expanduser()
        self._addrs_: Union[Tuple[Union[IpV4, IpV6]], None] = None
        self._af_: int = 0
        self._has_gateway_: Union[bool, None] = None
        self._rows_: Union[Tuple[PgpassRow], None] = None

    @property
    def _addrs(self) -> Tuple[Union[IpV4, IpV6]]:
        """The system ip-addresses associated with a gateway."""
        if self._addrs_ is None:
            self._addrs_ = system_addresses()
        return self._addrs_

    @property
    def _af(self) -> int:
        if self._af_ <= 0:
            if self._addrs:
                if self._addrs[0].address.version == 6:
                    self._af_ = socket.AF_INET6
            if self._af_ <= 0:
                self._af_ = socket.AF_INET
        return self._af_

    @property
    def _has_gateway(self) -> bool:
        """True if this system's network is using a gateway."""
        if self._has_gateway_ is None:
            if self._addrs:
                self._has_gateway_ = True
            else:
                self._has_gateway_ = False
        return self._has_gateway_

    def fetch_ipaddress(self, host: str, port: int = 5432) -> str:
        try:
            return str(ipaddress.ip_address(host))
        except (TypeError, ValueError):
            pass
        if host == 'localhost':
            if self._af == socket.AF_INET:
                return '127.0.0.1'
            else:
                return '::1'

        if self._has_gateway:
            data = socket.getaddrinfo(host, port, family=self._af)
            data = list(map(lambda x: x[4][0], data))
            if data:
                return data[0]
        return host

    @property
    def _rows(self) -> Tuple[PgpassRow]:
        if self._rows_ is None:
            out = list()
            if self.path.is_file():
                text = self.path.read_text(encoding=sys.getdefaultencoding())
                for line in text.splitlines():
                    line = line.strip()
                    if not line.startswith('#'):
                        line = line.split('#')[0]
                        line = line.strip()
                        args = line.split(':')
                        try:
                            args[1] = int(args[1])
                        except (TypeError, ValueError):
                            continue
                        args[0] = self.fetch_ipaddress(args[0], args[1])
                        args[4] = args[4].strip()
                        out.append(PgpassRow(*args))
            self._rows_ = tuple(out)
        return self._rows_

    def _find_password(
            self,
            user: str,
            host: str,
            dbname: str,
            port: int = 5432
    ) -> str:
        host = self.fetch_ipaddress(host, port)
        for row in self._rows:
            if row.host == host:
                if row.port == port:
                    if row.dbname == dbname or row.dbname == '*':
                        return row.password
        return ''

    def get_password(
        self,
        user: str,
        host: str,
        dbname: str,
        port: int = 5432
    ) -> str:
        out = self._find_password(user, host, dbname, port)
        while not out:
            out = getpass.getpass(
                prompt='Password for %s@%s: ' % (user, host)
            ).strip()
        return out
