import sys
from psycopg2.extras import NamedTupleConnection
from psycopg2.extras import NamedTupleCursor
from psycopg2.extensions import quote_ident
from psycopg2.extensions import connection as Connection
from psycopg2 import (
    ProgrammingError,
    InternalError,
)

from typing import (
    Any,
    Generator,
    List,
    NamedTuple,
    Optional,
    Tuple,
    Union,
    cast,
)
from collections.abc import (
    Sequence,
    Mapping,
)
from django.core.management.color import Style
from django.core.management.base import CommandError


class ConnInfo(NamedTuple):
    user: str
    password: str
    database: str
    host: str
    port: int


class Result(NamedTuple):
    data: Tuple[Any]
    index: Union[int, None]
    last_index: Union[int, None]
    msg: str


class Results(NamedTuple):
    data: Tuple[Result]
    count: int
    msg: str


class Group(NamedTuple):
    id: int
    name: str
    is_superuser: bool
    inherit: bool
    can_create_role: bool
    can_create_database: bool
    can_login: bool
    can_replicate: bool
    connection_limit: int
    members: Tuple[str, ...]
    member_of: Tuple[str, ...]


SELECT_GROUP = """
    SELECT
        g.grosysid AS "id",
        g.groname AS "name",
        r.rolsuper AS "is_superuser",
        r.rolinherit AS "inherit",
        r.rolcreaterole AS "can_create_role",
        r.rolcreatedb AS "can_create_database",
        r.rolcanlogin AS "can_login",
        r.rolreplication AS "can_replicate",
        r.rolconnlimit AS "connection_limit"
    FROM pg_group g
        INNER JOIN pg_catalog.pg_roles r ON g.groname = r.rolname
    WHERE g.groname = %s;
"""


class Database(NamedTuple):
    name: str
    connection_limit: int
    owner: str
    acl: str


SELECT_DATABASE = """
    SELECT
        d.datname as "name",
        d.datconnlimit as "connection_limit",
        r.rolname as "owner",
        d.datacl as "acl"
    FROM
        pg_database d
        INNER JOIN pg_roles r ON d.datdba = r.oid
    WHERE d.datname = %s;
"""


class User(NamedTuple):
    id: int
    name: str
    password: str
    is_superuser: bool
    inherit: bool
    can_create_role: bool
    can_create_database: bool
    can_login: bool
    can_replicate: bool
    connection_limit: int
    members: Tuple[str, ...]
    member_of: Tuple[str, ...]


SELECT_ROLE = """
  SELECT
    r.oid AS "id",
    r.rolname AS "name",
    CASE WHEN u.usesysid IS NULL THEN false ELSE true END AS "is_user",
    r.rolsuper AS "is_superuser",
    r.rolinherit AS "inherit",
    r.rolcreaterole AS "can_create_role",
    r.rolcreatedb AS "can_create_database",
    r.rolcanlogin AS "can_login",
    r.rolreplication AS "can_replicate",
    r.rolconnlimit AS "connection_limit"
    FROM pg_catalog.pg_roles as r
      LEFT OUTER JOIN pg_catalog.pg_user u ON r.rolname = u.usename
    WHERE r.rolname = %s;
"""


class Role(NamedTuple):
    id: int
    name: str
    is_user: bool
    is_superuser: bool
    inherit: bool
    can_create_role: bool
    can_create_database: bool
    can_login: bool
    can_replicate: bool
    connection_limit: int
    members: Tuple[str, ...]
    member_of: Tuple[str, ...]

SELECT_USER = """
    SELECT
        u.usesysid AS "id",
        u.usename AS "name",
        s.passwd AS "password",
        r.rolsuper AS "is_superuser",
        r.rolinherit AS "inherit",
        r.rolcreaterole AS "can_create_role",
        r.rolcreatedb AS "can_create_database",
        r.rolcanlogin AS "can_login",
        r.rolreplication AS "can_replicate",
        r.rolconnlimit AS "connection_limit"
    FROM
        pg_catalog.pg_user u
        INNER JOIN pg_shadow s on u.usename = s.usename
        INNER JOIN pg_catalog.pg_roles r on u.usename = r.rolname
    WHERE u.usename = %s;
"""

class RoleMember(NamedTuple):
    role_id: int
    role_name: str
    member_id: int
    member_name: str

SELECT_MEMBERS = """
  SELECT
    a.roleid AS "role_id",
    r.rolname AS "role_name",
    m.oid AS "member_id",
    m.rolname AS "member_name"
  FROM
    pg_auth_members a
    LEFT JOIN pg_roles r ON a.roleid = r.oid
    LEFT JOIN pg_roles m ON a.member = m.oid
"""


SELECT_USER_ROLE = """

  WITH RECURSIVE cte AS (
    SELECT oid FROM pg_roles WHERE rolname = %s

    UNION ALL
    SELECT m.roleid
    FROM cte
    JOIN pg_auth_members m ON m.member = cte.oid 
  )
  SELECT pg_get_userbyid(oid) FROM cte;
"""



class Schema(NamedTuple):
    name: str
    owner: str
    acl: str


SELECT_SCHEMA = """
    SELECT
        s.schema_name AS "name",
        s.schema_owner AS "owner",
        n.nspacl AS "acl"
    FROM
        information_schema.schemata s
        INNER JOIN pg_catalog.pg_namespace n ON s.schema_name = n.nspname

    WHERE s.schema_name = %s;
"""


# noinspection PyUnusedFunction
class BaseSetup:

    def __init__(
            self,
            style: Optional[Style] = None,
            conn: Optional[Connection] = None,
            force: bool = False,
            verbosity: int = 1
    ) -> None:
        self._style: Union[style, None] = style
        self.__conn: Union[Connection, None] = None
        self.__conn_info: Union[ConnInfo, None] = None
        self.force: bool = force
        self.verbosity: int = verbosity
        if conn is not None:
            self.conn = conn

    @property
    def conn(self) -> NamedTupleConnection:
        if not self.__conn:
            raise RuntimeError("The attribute 'conn' has not been set.")
        return self.__conn

    @conn.setter
    def conn(self, conn: Connection) -> None:
        if isinstance(conn, Connection):
            if conn.cursor_factory.__name__ == 'NamedTupleCursor':
                self.__conn = conn
                self.__set_conn_info(self.__conn)
                if self.verbosity > 0:
                    self._print(
                        '%s {}@{}:{}/{}' % self._success('Server:'),
                        self.conn_info.user,
                        self.conn_info.host,
                        self.conn_info.port,
                        self.conn_info.database
                    )
                return None
            else:
                raise TypeError(
                    "Attribute 'conn' can only be set to an instance of "
                    "'psycopg2.extensions.connection' and has "
                    "'cursor_factory' set to the 'NamedTupleCursor' class."
                )
        raise TypeError(
            "Attribute 'conn' can only be set to an instance of "
            "'psycopg2.extensions.connection'."
        )

    @property
    def conn_info(self) -> Union[ConnInfo, None]:
        return self.__conn_info

    def __set_conn_info(self, conn: Connection) -> None:
        if isinstance(conn, Connection):
            if conn.cursor_factory.__name__ == 'NamedTupleCursor':
                hold = dict()
                for part in conn.dsn.split(' '):
                    part = cast(str, part)
                    part = part.strip()
                    key, _, val = part.partition('=')
                    key = key.strip()
                    val = val.strip()
                    if key == 'port':
                        val = int(val)
                    if key == 'dbname':
                        key = 'database'
                    if key in ('user', 'password', 'database', 'host', 'port'):
                        hold[key] = val

                if hold:
                    self.__conn_info = ConnInfo(**hold)
                else:
                    self.__conn_info = None
                return None
            else:
                raise TypeError(
                    "Attribute 'conn_info' can only be set with an instance of "
                    "'psycopg2.extensions.connection' and has "
                    "'cursor_factory' set to the 'NamedTupleCursor' class."
                )
        raise TypeError(
            "Attribute 'conn_info' can only be set with an instance of "
            "'psycopg2.extensions.connection'."
        )

    @property
    def current_database(self) -> str:
        if not self.__current_database:
            with self.conn.cursor() as cur:
                cur = cast(NamedTupleCursor, cur)
                query = 'SELECT current_database() AS "database";'
                cur.execute(query)
                res: Tuple[str] = cur.fetchone()
                if res:
                    self.__current_database = res[0]
        return self.__current_database

    def _quote_ident(self, data):
        """Return a quoted identifier accourding to PostgreSQL quoting rules."""
        if data.startswith('"') and data.endswith('"'):
            return data
        return quote_ident(data, self.conn)

    def _format_query(
            self,
            cur: NamedTupleCursor,
            query: str,
            params: Union[Sequence, Mapping, None] = None,
            ident_args: Optional[Sequence] = None,
            ident_kwargs: Optional[Mapping] = None,
            hide_stdout: bool = False
    ) -> str:
        if not isinstance(ident_args, Sequence):
            ident_args = list()
        ident_args = list(ident_args)
        for x, arg in enumerate(ident_args):
            if arg.upper() == 'PUBLIC':
                ident_args[x] = arg
            else:
                ident_args[x] = quote_ident(arg, cur)
        if not isinstance(ident_kwargs, Mapping):
            ident_kwargs = dict()
        for key, val in ident_kwargs.items():
            if val.upper() == 'PUBLIC':
                ident_kwargs[key] = val
            else:
                ident_kwargs[key] = quote_ident(val, cur)

        query = query.format(*ident_args, **ident_kwargs)
        if params:
            query = cur.mogrify(query, params)
            query = query.decode(sys.getdefaultencoding())

        if self.verbosity > 2:
            self._print("---\n%s\n" % query)
        return query

    def _fetch_one(
            self,
            query: str,
            params: Union[Sequence, Mapping, None] = None,
            ident_args: Optional[Sequence] = None,
            ident_kwargs: Optional[Mapping] = None,
            hide_stdout: bool = False
    ) -> Result:
        with self.conn.cursor() as cur:
            cur = cast(NamedTupleCursor, cur)
            query = self._format_query(
                cur,
                query,
                params,
                ident_args,
                ident_kwargs,
                hide_stdout=hide_stdout
            )
            cur.execute(query)
            status_msg: str = cur.statusmessage

            out = cur.fetchone()
            # qry: str = cur.query.decode(self.conn.encoding)
            cnt: Union[int, None] = cur.rowcount
            if cnt is None:
                cnt = -1
            if hide_stdout is False and self.verbosity > 2:
                self._print('Count: %s' % cnt)

        self._echo_query_finished()
        if status_msg:
            status_msg = status_msg.replace('\n', '\n    ')
            status_msg = '\n  %s' % status_msg
        if self.verbosity > 2:
            if status_msg:
                self._echo_info("StatusMessage:{}\n", status_msg)

        if not out:
            return Result(tuple(), None, None, status_msg)
        return Result(out, 0, 0, status_msg)

    def _fetch_many(
            self,
            query: str,
            params: Union[Sequence, Mapping, None] = None,
            ident_args: Optional[Sequence] = None,
            ident_kwargs: Optional[Mapping] = None,
            hide_stdout: bool = False
    ) -> Results:
        with self.conn.cursor() as cur:
            cur = cast(NamedTupleCursor, cur)
            query = self._format_query(
                cur,
                query,
                params,
                ident_args,
                ident_kwargs,
                hide_stdout=hide_stdout
            )
            cur.execute(query)
            status_msg: str = cur.statusmessage

            # qry: str = cur.query.decode(self.conn.encoding)
            cnt: Union[int, None] = cur.rowcount
            if cnt is None:
                cnt = -1
            if hide_stdout and self.verbosity > 2:
                self._print('Count: %s' % cnt)

            data = list()
            for x, record in enumerate(cur):
                data.append(Result(record, x, cnt - 1, msg))

        self._echo_query_finished()
        if status_msg:
            status_msg = status_msg.replace('\n', '\n    ')
            status_msg = '\n  %s' % status_msg
        if self.verbosity > 2:
            if status_msg:
                self._echo_info("StatusMessage:{}\n", status_msg)

        return Results(tuple(data), cnt, status_msg)

    def _fetch_each(
            self,
            query: str,
            params: Union[Sequence, Mapping, None] = None,
            ident_args: Optional[Sequence] = None,
            ident_kwargs: Optional[Mapping] = None,
            hide_stdout: bool = False
    ) -> Generator[Result, None, None]:
        with self.conn.cursor() as cur:
            cur = cast(NamedTupleCursor, cur)
            query = self._format_query(
                cur,
                query,
                params,
                ident_args,
                ident_kwargs,
                hide_stdout=hide_stdout
            )
            cur.execute(query)
            # qry: str = cur.query.decode(self.conn.encoding)
            self._echo_query_finished()
            status_msg: str = cur.statusmessage
            if status_msg:
                status_msg = status_msg.replace('\n', '\n    ')
                status_msg = '\n  %s' % status_msg
            if self.verbosity > 2:
                if status_msg:
                    self._echo_info("StatusMessage:{}\n", status_msg)

            cnt: Union[int, None] = cur.rowcount
            if cnt is None:
                cnt = -1
            if hide_stdout is False and self.verbosity > 2:
                self._print('Count: %s' % cnt)

            if cnt > 0:
                for data in cur:
                    if data:
                        row = cur.rownumber - 1
                        yield Result(data, row, cnt - 1, status_msg)

    def _execute(
            self,
            query: str,
            params: Union[Sequence, Mapping, None] = None,
            ident_args: Optional[Sequence] = None,
            ident_kwargs: Optional[Mapping] = None,
            hide_stdout: bool = False
    ) -> str:
        error_msg = ''
        status_msg = ''
        with self.conn.cursor() as cur:
            cur = cast(NamedTupleCursor, cur)
            query = self._format_query(
                cur,
                query,
                params,
                ident_args,
                ident_kwargs,
                hide_stdout=hide_stdout
            )
            try:
                cur.execute(query)
            except (ProgrammingError, InternalError) as err:
                error_msg = '%s' % str(err)
            else:
                status_msg = '%s' % cur.statusmessage
        self._echo_query_finished()
        if error_msg:
            error_msg = error_msg.replace('\n', '\n    ')
            error_msg = '\n  %s' % error_msg
        if status_msg:
            status_msg = status_msg.replace('\n', '\n    ')
            status_msg = '\n  %s' % status_msg
        if self.verbosity > 2:
            if status_msg:
                self._echo_info("StatusMessage:{}\n", status_msg)
            if error_msg:
                self._echo_error("Error:{}\n", error_msg)
        if error_msg:
            return error_msg
        return status_msg

    @staticmethod
    def _print(
            text: str,
            *args: Any,
            sep: str = '',
            end: str = '\n',
            flush: bool = True,
            **kwargs: Any
    ) -> None:
        text = text.format(*args, **kwargs)
        print(text, sep=sep, end=end, file=sys.stdout, flush=flush)

    def _error(self, text: str, *args: Any, **kwargs: Any) -> str:
        text = text.format(*args, **kwargs)
        if self._style:
            return self._style.ERROR(text)
        return text

    def _notice(self, text: str, *args: Any, **kwargs: Any) -> str:
        text = text.format(*args, **kwargs)
        if self._style:
            text = self._style.NOTICE(text)
        return text

    def _success(self, text: str, *args: Any, **kwargs: Any) -> str:
        text = text.format(*args, **kwargs)
        if self._style:
            text = self._style.SUCCESS(text)
        return text

    def _warning(self, text: str, *args: Any, **kwargs: Any) -> str:
        text = text.format(*args, **kwargs)
        if self._style:
            text = self._style.WARNING(text)
        return text

    def _header(self, text: str, *args: Any, **kwargs: Any) -> str:
        text = text.format(*args, **kwargs)
        if self._style:
            text = self._style.MIGRATE_HEADING(text)
        return text

    def _echo_begin(
            self,
            text,
            *args,
            hide_stdout: bool = False,
            **kwargs
    ) -> None:
        if hide_stdout is False and self.verbosity > 1:
            self._print(
                self._header(text, *args, **kwargs)
            )

    def _echo_warning(
            self,
            text,
            *args,
            hide_stdout: bool = False,
            **kwargs
    ) -> None:
        if hide_stdout is False and self.verbosity > 1:
            self._print(
                self._warning(text, *args, **kwargs)
            )

    def _echo_info(
            self,
            text,
            *args,
            hide_stdout: bool = False,
            **kwargs
    ) -> None:
        if hide_stdout is False and self.verbosity > 1:
            self._print(text, *args, **kwargs)

    def _echo_error(
            self,
            text,
            *args,
            hide_stdout: bool = False,
            **kwargs
    ) -> None:
        if hide_stdout is False and self.verbosity > 0:
            self._print(
                self._error(text, *args, **kwargs)
            )

    def _echo_success(
            self,
            text,
            *args,
            hide_stdout: bool = False,
            **kwargs
    ) -> None:
        if hide_stdout is False and self.verbosity > 0:
            self._print(
                self._success(text, *args, **kwargs)
            )
            if self.verbosity > 1:
                self._print('')
                self._print('-' * 80)
                self._print('')

    def _echo_query_finished(
            self,
            hide_stdout: bool = False,
    ) -> None:
        if hide_stdout is False and self.verbosity > 2:
            self._print('Query Finished.\n')

    # noinspection PyProtectedMember
    def _fetch_group(
            self,
            name: str,
            hide_stdout: bool = False
    ) -> Union[Group, tuple]:
        self._echo_begin(
            "Fetching group '{}'",
            name,
            hide_stdout=hide_stdout
        )
        res: Result = self._fetch_one(
            SELECT_GROUP,
            params=[name],
            hide_stdout=hide_stdout
        )
        if not res.data:
            self._echo_warning(
                "Group: '{}' not found.{}",
                name,
                res.msg,
                hide_stdout=hide_stdout
            )
            return tuple()

        data = res.data._asdict()
        data['members'] = self._fetch_members(
            res.data.name,
            hide_stdout=hide_stdout
        )
        data['member_of'] = self._fetch_member_of(
            res.data.name,
            hide_stdout=hide_stdout
        )
        return Group(**data)

    def _validate_group_exists(
            self,
            name: str,
            text: str,
            hide_stdout: bool = True
    ) -> None:
        self._echo_info(
            f"Validating group: '{name}' exists...",
            hide_stdout=hide_stdout
        )
        out = self._fetch_group(name, hide_stdout=hide_stdout)
        if not out:
            msg = (
                "The group: '%s' does not exist on the server: '%s@%s'."
                % (name, self.conn_info.user, self.conn_info.host)
            )
            if text:
                msg = '%s\n    %s' % (text, msg)
            raise CommandError(msg)

        self._echo_info(
            f"Done validating group: '{name}' exists.",
            hide_stdout=hide_stdout
        )
        return out

    # noinspection PyProtectedMember
    def _fetch_user(
            self,
            name: str,
            hide_stdout: bool = False
    ) -> Union[User, tuple]:
        self._echo_begin(f"Fetching user: '{name}'...", hide_stdout=hide_stdout)
        res: Result = self._fetch_one(SELECT_USER, params=[name])
        if not res.data:
            self._echo_warning(
                f"User: '{name}' not found.{res.data}",
                name,
                res.msg,
                hide_stdout=hide_stdout
            )
            return tuple()

        data = res.data._asdict()
        data['members'] = self._fetch_members(
            res.data.name,
            hide_stdout=hide_stdout
        )
        data['member_of'] = self._fetch_member_of(
            res.data.name,
            hide_stdout=hide_stdout
        )
        return User(**data)

    def _validate_user_exists(
            self,
            name: str,
            text: str,
            hide_stdout: bool = True
    ) -> User:
        self._echo_info(
            f"Validating user: '{name}' exists...",
            hide_stdout=hide_stdout
        )

        out = self._fetch_user(name, hide_stdout=hide_stdout)
        if not out:
            msg = (
                "The user: '%s' does not exist on the server: '%s@%s'."
                % (name, self.conn_info.user, self.conn_info.host)
            )
            if text:
                msg = '%s\n    %s' % (text, msg)
            raise CommandError(msg)
        self._echo_info(
            f"Done validating user: '{name}' exists.",
            hide_stdout=hide_stdout
        )
        return out

    def _fetch_database(
            self,
            name: str,
            hide_stdout: bool = False
    ) -> Union[Database, tuple]:
        self._echo_begin(
            r"Fetching database: '{name}'...",
            hide_stdout=hide_stdout
        )
        res: Result = self._fetch_one(SELECT_DATABASE, params=[name])
        if not res.data:
            self._echo_warning(
                f"Database: '{name}' not found.",
                hide_stdout=hide_stdout
            )
        return res.data

    def _validate_database_exists(
            self,
            name: str,
            text: str = '',
            hide_stdout: bool = True
    ) -> Database:
        self._echo_info(
            f"Validating database: '{name}' exists...",
            hide_stdout=hide_stdout
        )

        out = self._fetch_database(name, hide_stdout=hide_stdout)
        if not out:
            msg = (
                "The database: '%s' does not exist on the server: '%s@%s'."
                % (name, self.conn_info.user, self.conn_info.host)
            )
            if text:
                msg = '%s\n    %s' % (text, msg)
            raise CommandError(msg)
        self._echo_info(
            f"Done validating database: '{name}' exists.",
            hide_stdout=hide_stdout
        )
        return out

    def _fetch_schema(
            self,
            name: str,
            hide_stdout: bool = False
    ) -> Union[Schema, tuple]:
        self._echo_begin(
            f"Fetching schema: '{name}'...",
            hide_stdout=hide_stdout
        )
        res: Result = self._fetch_one(
            SELECT_SCHEMA,
            params=[name],
            hide_stdout=hide_stdout
        )
        if not res.data:
            self._echo_warning(
                f"Schema: '{name}' not found.",
                hide_stdout=hide_stdout
            )
        return res.data

    def _validate_schema_exists(
            self,
            name: str,
            text: str = '',
            hide_stdout: bool = True
    ) -> Schema:
        self._echo_info(
            f"Validating schema: '{name}' exists...",
            hide_stdout=hide_stdout
        )

        out = self._fetch_schema(name, hide_stdout=hide_stdout)
        if not out:
            msg = (
                "The schema: '%s' does not exist in the database: '%s' on "
                "the server: '%s@%s'." % (
                    name,
                    self.conn_info.database,
                    self.conn_info.user,
                    self.conn_info.host
                )
            )
            if text:
                msg = '%s\n    %s' % (text, msg)
            raise CommandError(msg)
        self._echo_info(
            f"Done validating schema: '{name}' exists.",
            hide_stdout=hide_stdout
        )
        return out

    # noinspection PyProtectedMember
    def _fetch_role(
            self,
            name: str,
            hide_stdout: bool = False
    ) -> Union[User, tuple]:
        self._echo_begin(
            f"Fetching role '{name}'...",
            hide_stdout=hide_stdout
        )
        res: Result = self._fetch_one(SELECT_ROLE, params=[name])
        if not res.data:
            self._echo_warning(
                f"Role: '{name}' not found.{res.msg}",
                hide_stdout=hide_stdout
            )
            return tuple()

        data = res.data._asdict()
        data['members'] = self._fetch_members(
            res.data.name,
            hide_stdout=hide_stdout
        )
        data['member_of'] = self._fetch_member_of(
            res.data.name,
            hide_stdout=hide_stdout
        )
        return Role(**data)

    def _validate_role_exists(
            self,
            name: str,
            text: str = '',
            hide_stdout: bool = True
    ) -> Role:
        if name.lower() == 'public':
            return None
        self._echo_info(
            f"Validating role: '{name}' exists...",
            hide_stdout=hide_stdout
        )
        out = self._fetch_role(name, hide_stdout=hide_stdout)

        if not out:
            msg = (
                "The role: '%s' does not exit on the server: '%s@%s'." % (
                    name,
                    self.conn_info.user,
                    self.conn_info.host
                )
            )
            if text:
                msg = '%s\n    %s' % (text, msg)
            raise CommandError(msg)
        self._echo_info(
            f"Done validating role: '{name}' exists.",
            hide_stdout=hide_stdout
        )
        return out

    def _fetch_members(
            self,
            role_name: str,
            hide_stdout: bool = True
    ) -> Tuple[str, ...]:
        out = set()
        query = "{}WHERE r.rolname = %s\n".format(SELECT_MEMBERS)
        rows = self._fetch_each(
            query,
            params=[role_name],
            hide_stdout=hide_stdout
        )
        for row in rows:
            row = cast(Result, row)
            data: RoleMember = row.data
            out.add(data.member_name)
        return tuple(sorted(out))

    def _fetch_member_of(
            self,
            role_name: str,
            hide_stdout: bool = True
    ) -> Tuple[str, ...]:
        self._echo_info(
            f"Fetching 'member_of' for role: {role_name}...",
            hide_stdout=hide_stdout
        )
        out = set()
        query = "{}WHERE m.rolname = %s\n".format(SELECT_MEMBERS)
        rows = self._fetch_each(
            query,
            params=[role_name],
            hide_stdout=hide_stdout
        )
        for row in rows:
            row = cast(Result, row)
            data: RoleMember = row.data
            out.add(data.role_name)
        return tuple(sorted(out))
