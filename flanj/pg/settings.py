from collections.abc import Mapping
from typing import (
    NamedTuple,
    Dict,
    Union,
    Tuple,
)

from django.conf import settings
from django.core.management.base import CommandError


class DjangoSettingsPostgres(NamedTuple):
    key: str
    ENGINE: str
    NAME: str
    USER: str
    PASSWORD: str
    HOST: str
    PORT: int
    OPTIONS: Dict[str, str]
    SCHEMA: str


def fetch_django_settings_database(
        databases_key: str = 'default'
) -> DjangoSettingsPostgres:
    databases = getattr(settings, 'DATABASES', dict())
    if not isinstance(databases, Mapping):
        raise CommandError(
            "Django settings DATABASES must be a 'dict'.  Got %r."
            % type(databases).__name__
        )
    data = databases.get(databases_key, dict())
    if not isinstance(data, Mapping):
        raise CommandError(
            "Django settings DATABASES['%s'] must be a 'dict'.  Got %r."
            % (databases_key, type(databases).__name__)
        )
    if not data:
        raise CommandError(
            "Django settings DATABASES['%s'] is empty please set."
            % databases_key
        )

    engine = data.get('ENGINE', '')
    if engine != 'django.db.backends.postgresql':
        raise CommandError(
            "Django settings DATABASES['%s']['ENGINE'] must be set to:\n"
            "    'django.db.backends.postgresql\n"
            "This command only works with Postgres database servers. Got: '%s'."
            % (databases_key, engine)
        )
    name = data.get('NAME', '')
    if not isinstance(name, str) or not name:
        raise CommandError(
            "Django settings DATABASES['%s']['NAME'] must be set to the "
            "database name." % databases_key
        )

    user = data.get('USER', '')
    if not isinstance(user, str) or not user:
        raise CommandError(
            "Django settings DATABASES['%s']['USER'] must be set to the "
            "database username." % databases_key
        )

    password = data.get('PASSWORD', '')
    if not isinstance(password, str):
        raise CommandError(
            "Django settings DATABASES['%s']['PASSWORD'] must be set to the "
            "password for USER." % databases_key
        )

    host = data.get('HOST', '')
    if not isinstance(host, str) or not host:
        raise CommandError(
            "Django settings DATABASES['%s']['HOST'] must be set to the "
            "database server hostname or ip-address." % databases_key
        )
    if host == 'localhost':
        host = '127.0.0.1'

    port = data.get('PORT', 5432)
    if not isinstance(port, int) or (1 <= port <= 65535) is False:
        raise CommandError(
            "Django settings DATABASES['%s']['PORT'] must be set to the\n"
            "database server's listening port number.  This must be an 'int'\n"
            "between 1 and 65535.  If unsure, use: 5432\n"
            % databases_key
        )

    options = data.get('OPTIONS', dict())

    schema = data.get('SCHEMA', 'public')
    if not isinstance(user, str) or not user:
        raise CommandError(
            "Django settings DATABASES['%s']['SCHEMA'] must be set to the "
            "database schema." % databases_key
        )

    return DjangoSettingsPostgres(
        databases_key,
        engine,
        name,
        user,
        password,
        host,
        port,
        options,
        schema
    )


def _should_process(data: Dict[str, Union[str, int, bool]]) -> bool:
    if isinstance(data, Mapping):
        engine = data.get('ENGINE', '')
        if engine == 'django.db.backends.postgresql':
            return True
    return False


def fetch_django_settings_databases() -> Tuple[DjangoSettingsPostgres]:
    out = list()
    databases = getattr(settings, 'DATABASES', dict())
    if not isinstance(databases, Mapping):
        raise CommandError(
            "Django settings DATABASES must be a 'dict'.  Got %r."
            % type(databases).__name__
        )
    data = databases.get('default', dict())
    if _should_process(data):
        out.append(fetch_django_settings_database('default'))
    for key in sorted(databases.keys()):
        if key != 'default':
            data = databases[key]
            if _should_process(data):
                out.append(fetch_django_settings_database(key))
    return tuple(out)


