from .base import (
    BaseSetup,
    Role,
    Result,
)
from typing import (
    Union,
    Tuple,
    List,
    Dict,
    Optional,
)
from django.core.management.base import CommandError
from psycopg2 import InternalError


SELECT_SETTINGS = """
  SELECT 
    r.rolname AS "username", 
    d.datname AS "database", 
    rs.setconfig AS "settings"
  FROM
    pg_db_role_setting rs
    LEFT JOIN pg_roles r ON r.oid = rs.setrole
    LEFT JOIN pg_database d ON d.oid = rs.setdatabase
  WHERE r.rolname = %s AND d.datname = %s
"""

DROP = """
  DROP ROLE IF EXISTS {};
"""


# noinspection SqlDialectInspection,SqlNoDataSourceInspection
class Roles(BaseSetup):

    def fetch(
            self,
            role: str,
            hide_stdout: bool = False
    ) -> Union[Role, tuple]:
        return self._fetch_role(role, hide_stdout=hide_stdout)

    def drop(
            self,
            role: str,
            warn: bool = False
    ) -> None:
        if self.fetch(role, hide_stdout=True):
            self._echo_begin(f"Dropping role '{role}'.")

            msg: str = self._execute(DROP, ident_args=[role])

            if self.fetch(role, hide_stdout=True):
                if warn is True:
                    self._echo_warning(msg)
                else:
                    raise CommandError(
                        f"Unable to drop role: '{role}'.{msg}"
                    )
            self._echo_success(
                f"Role: '{role}' was dropped."
            )

    def add_member(
            self,
            member: str,
            role: str,
    ) -> None:
        self._echo_begin(f"Adding member: '{member}' to role: '{role}'...")
        text = f"Unable to add member: '{member}' to role: '{role}'."
        self._validate_role_exists(member, text=text, hide_stdout=True)
        role_obj = self._validate_role_exists(
            role,
            text=text,
            hide_stdout=True
        )

        if member in role_obj.members:
            self._echo_success(
                f"Member: '{member}' is already a member of role: {role}."
            )
            return None

        query = "GRANT {} TO {};"

        msg: str = self._execute(query, ident_args=[role, member])

        member_obj: Role = self._fetch_role(member, hide_stdout=True)
        if role not in member_obj.member_of:
            raise CommandError(
                f"Unable to add member: '{member}' to role: '{role}'.{msg}"
            )
        self._echo_success(
            f"Member: '{member}' has been added to role: '{role}'."
        )

    def remove_member(
            self,
            member: str,
            role: str,
    ) -> None:
        self._echo_begin(f"Removing member: '{member}' from role: '{role}'...")
        text = f"Unable to remove member: '{member}' from role: '{role}'."
        self._validate_role_exists(member, text=text, hide_stdout=True)
        role_obj = self._validate_role_exists(
            role,
            text=text,
            hide_stdout=True
        )

        if member not in role_obj.members:
            self._echo_success(
                f"Member: '{member}' is already NOT a member of role: {role}."
            )
            return None

        query = "REVOKE {} FROM {};"

        msg: str = self._execute(query, ident_args=[role, member])

        member_obj: Role = self._fetch_role(member, hide_stdout=True)
        if role in member_obj.member_of:
            raise CommandError(
                f"Unable to remove member: '{member}' from role: '{role}'.{msg}"
            )
        self._echo_success(
            f"Member: '{member}' has been removed from role: '{role}'."
        )

    # noinspection PyAttributeOutsideInit
    @property
    def default_search_path(self) -> Tuple[str, ...]:
        if not hasattr(self, '_default_search_path'):
            self._default_search_path = tuple()
        if not self._default_search_path:
            query = "SELECT boot_val FROM pg_settings WHERE name='search_path';"
            res: Result = self._fetch_one(query)
            if res.data:
                boot_val: str = res.data.boot_val
                if boot_val:
                    out = list()
                    for part in boot_val.split(','):
                        part = part.strip()
                        if part:
                            out.append(part)
                    self._default_search_path = tuple(out)
        return self._default_search_path

    @staticmethod
    def _process_settings(settings: List[str]) -> Dict[str, str]:
        out = dict()
        for row in settings:
            key, val = row.split('=')
            if key in ('search_path',):
                val = list(map(lambda x: x.strip(), val.split(',')))
            out[key] = val
        return out

    def fetch_settings(
            self,
            role: str,
            dbname: str,
            hide_stdout: bool = False
    ) -> Dict[str, str]:
        self._echo_begin(
            f"Fetching settings for role: '{role}' on database: '{dbname}'.",
            hide_stdout=hide_stdout
        )
        text = (
            f"Unable to fetch 'settings' for role: '{role}' on database: "
            f"'{dbname}'."
        )
        self._validate_role_exists(role, text=text, hide_stdout=hide_stdout)
        self._validate_database_exists(
            dbname,
            text=text,
            hide_stdout=hide_stdout
        )
        res: Result = self._fetch_one(
            SELECT_SETTINGS,
            params=[role, dbname]
        )

        if not res.data:
            self._echo_warning(
                f"Settings for role: '{role}' on database: '{dbname}' not "
                f"found.",
                hide_stdout=hide_stdout
            )
        if res.data:
            return self._process_settings(res.data.settings)
        return dict()

    def reset_search_path(
            self,
            role: str,
            dbname: str,
    ) -> None:
        self._echo_begin(
            f"Resetting the 'search_path' for role: '{role}' on database: "
            f"'{dbname}'."
        )
        text = (
            f"Unable to reset 'search_path' for role '{role}' on database "
            f"'{dbname}'."
        )
        self._validate_role_exists(role, text=text)
        self._validate_database_exists(dbname, text=text)

        search_path: List[str] = self.fetch_settings(
            role,
            dbname,
            hide_stdout=True
        ).get(
            'search_path',
            list()
        )
        if search_path:
            msg = self._execute(
                "ALTER ROLE {} IN DATABASE {} RESET search_path",
                ident_args=[role, dbname]
            )

            self.echo_done()

            search_path: List[str] = self.fetch_settings(
                role,
                dbname,
                hide_stdout=True
            ).get(
                'search_path',
                list()
            )

            if search_path:
                raise CommandError(
                    f"Unable to reset the 'search_path' for role: '{role}' on "
                    f"database: '{dbname}'.\n{msg}"
                )

            self._echo_success(
                f"The 'search_path' for role: '{role}' on database: '{dbname}' "
                f"has been reset."
            )

    def set_search_path(
            self,
            role: str,
            dbname: str,
            schema: str,
            force: Optional[bool] = None
    ) -> None:
        # It appears that search path only works with users not groups.
        self._echo_begin(
            f"Setting 'search_path' for role: '{role}' in database '{dbname}'."
        )
        text = (
            f"Unable to set the 'search_path' for role '{role}' on database "
            f"'{dbname}'.\nOnly works with user-roles."
        )
        self._validate_user_exists(role, text=text)
        self._validate_database_exists(dbname, text=text)

        if force is None:
            force = self.force

        search_path: List[str] = self.fetch_settings(
            role,
            dbname,
            hide_stdout=True
        ).get(
            'search_path',
            list()
        )
        if schema in search_path:
            if force is True:
                self.reset_search_path(role, dbname)
            else:
                self._echo_success(
                    f"Schema: '{schema}' is already in the 'search_path' for "
                    f"role: '{role}' on database: '{dbname}'."
                )
                return None
        if not search_path:
            search_path = [schema] + list(self.default_search_path)

        if schema not in search_path:
            search_path.insert(0, schema)

        search_path = map(self._quote_ident, search_path)

        new_path: str = ', '.join(search_path)

        self._echo_info(
            f"Setting search_path={new_path} for role: '{role}' in database: "
            f"'{dbname}'."
        )

        msg = self._execute(
            "ALTER ROLE {} IN DATABASE {} SET search_path=%s" % new_path,
            ident_args=[role, dbname]
        )

        search_path: List[str] = self.fetch_settings(
            role,
            dbname,
            hide_stdout=True
        ).get(
            'search_path',
            list()
        )

        if schema not in search_path:
            raise CommandError(
                f"Unable to set search_path={new_path} for role: '{role}' on "
                f"database: '{dbname}'.{msg}"
            )

        self._echo_success(
            f"Schema: '{schema}' has been added to the 'search_path' for role: "
            f"'{role}' on database: '{dbname}'."
        )

