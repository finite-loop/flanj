import re
from .base import (
    BaseSetup,
    Result,
    User,
)
from typing import (
    NamedTuple,
    Union,
    Tuple,
    Dict,
    Optional,
    List
)
from django.core.management.base import CommandError


DROP = """
    DROP ROLE IF EXISTS {};
"""

# noinspection SqlDialectInspection,SqlNoDataSourceInspection
class Users(BaseSetup):

    def fetch(
            self,
            name: str,
            hide_stdout: bool = False
    ) -> Union[User, tuple]:
        return self._fetch_user(name, hide_stdout=hide_stdout)

    def drop(
            self,
            name: str
    ) -> None:
        self._echo_begin("Dropping user '{}'.", name)

        if self.fetch(name, hide_stdout=True):

            msg: str = self._execute(DROP, ident_args=[name])

            if self.fetch(name, hide_stdout=True):
                raise CommandError(
                    "Unable to remove the user '%s'\n%s" % (name, msg)
                )

            self._echo_success("User: '{}' was dropped.", name)
        else:
            self._echo_warning("User: '{name}' does not exist.")

    def create(
            self,
            name: str,
            password: str,
            is_superuser: bool = False,
            can_create_database: bool = False,
            can_create_role: bool = False,
            inherit: bool = False,
            can_replicate: bool = False,
            connection_limit: int = -1,
            force: Optional[bool] = None
    ) -> None:
        self._echo_begin("Creating user '{}'.", name)
        if force is None:
            force = self.force
        if force is True:
            self.drop(name)

        if self.fetch(name, hide_stdout=True):
            self._echo_success("User: '{}' already exists.", name)
            return None

        parts: List[str] = ['CREATE ROLE {}']

        if is_superuser is True:
            parts.append('SUPERUSER')
        else:
            parts.append('NOSUPERUSER')

        if can_create_database is True:
            parts.append('CREATEDB')
        else:
            parts.append('NOCREATEDB')

        if can_create_role is True:
            parts.append('CREATEROLE')
        else:
            parts.append('NOCREATEROLE')

        if inherit is True:
            parts.append('INHERIT')
        else:
            parts.append('NOINHERIT')

        parts.append('LOGIN')

        if can_replicate is True:
            parts.append('REPLICATION')
        else:
            parts.append('NOREPLICATION')

        if connection_limit > -1:
            parts.append('CONNECTION LIMIT %s' % connection_limit)

        parts.append('PASSWORD %s')

        query: str = '%s;' % ' '.join(parts)

        msg: str = self._execute(
            query,
            params=[password],
            ident_args=[name]
        )

        if not self.fetch(name, hide_stdout=True):
            raise CommandError(
                "Unable to create the user: '%s'.\n%s" % (name, msg)
            )

        self._echo_success("User: '{}' has been created.", name)

