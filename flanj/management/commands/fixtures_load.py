import json
import os
from argparse import ArgumentParser
from functools import cached_property
from typing import (
    Any,
    Dict,
    Generator,
    Optional,
    Tuple,
    Type,
)

from django.apps import apps as django_apps
from django.db.models import Model
from django.core.management import call_command
from django.core.management.commands import loaddata
from flanj.command import BaseCommand
from flanj.command.exceptions import CommandError
from flanj.command.prnt import Spin

from .fixtures_build import (
    AppMeta,
    get_app_model_titles,
    get_apps_meta,
    get_fixture_home,
    is_fixture_filename,
)


class Command(BaseCommand):

    ADD_DRY_RUN_ARGUMENT = True
    requires_migrations_checks = True
    help = "load fixtures into database"

    def add_arguments(self, parser: ArgumentParser) -> None:
        parser.add_argument(
            '--no-spinner',
            action='store_true',
            default=False,
            help="A spinner is NOT used in the screen output."
        )

    @cached_property
    def fixture_home(self) -> str:
        path = get_fixture_home()

        if not os.path.exists(path):
            raise CommandError(
                'The FIXTURE_HOME directory, {!r}, does not exist.', path
            )
        if not os.path.isdir(path):
            raise CommandError(
                'The FIXTURE_HOME, {!r}, is not a directory.', path
            )
        self.prnt.info('Fixtures from: {}', path)
        return path

    @cached_property
    def apps_meta(self) -> Tuple[AppMeta, ...]:
        return get_apps_meta()

    @cached_property
    def app_model_titles(self) -> Tuple[str, ...]:
        return get_app_model_titles(self.apps_meta)

    @cached_property
    def all_models(self) -> Dict[str, Dict[str, Type[Model]]]:
        return django_apps.all_models

    def get_model(
            self,
            app_name: str,
            model_name: str
    ) -> Optional[Type[Model]]:
        app = self.all_models.get(app_name.lower(), None)
        if app is not None:
            model = app.get(model_name.lower(), None)
            # The following is an additional protection
            # against loading fixtures tied to django models.
            if model is not None:
                module: str = getattr(model, '__module__')
                if not module.startswith('django'):
                    return model
        return None

    @staticmethod
    def validate_fixture_file(path: str) -> None:
        basename = os.path.basename(path)
        name = basename.split('.')[0]
        parts = name.split('__')
        app_name = parts[1].lower()
        model_name = parts[2].lower()
        app_model_name = f'{app_name}.{model_name}'
        size = os.path.getsize(path)
        if size > 200:
            size = 200
        with open(path, 'r', encoding='utf-8') as f:
            text = f.read(size)
        text = text.split(',')[0]
        text = '%s}]' % text
        try:
            data = json.loads(text)
        except json.JSONDecodeError:
            data = []

        if not data or not hasattr(data, 'index'):
            raise CommandError('The file: {!r} is an invalid fixture.', path)

        value: Dict[str, str] = data[0]
        if not isinstance(value, dict):
            raise CommandError('The file: %{!r} is an invalid fixture.', path)

        app_model = value.get('model', '')
        if not hasattr(app_model, 'encode'):
            raise CommandError('The file: {!r} is an invalid fixture.', path)
        if app_model != app_model_name:
            raise CommandError(
                'The file: {!r} is an invalid fixture. (possibly a hacking '
                'attempt)', path
            )

    def is_fixture_loaded(self, path: str) -> bool:
        basename = os.path.basename(path)
        name = basename.split('.')[0]
        parts = name.split('__')
        app_name = parts[1]
        model_name = parts[2]
        model = self.get_model(app_name, model_name)
        # The following check is an additional protection
        # against loading fixtures tied to django models.
        # In theory, model, should never be None.
        if model is None:
            raise CommandError(
                f'The fixture {basename!r} is invalid.'
            )

        if model.objects.count() > 0:
            return True
        return False

    def each_fixture_file(self) -> Generator[str, None, None]:
        hold = []
        for name in os.listdir(self.fixture_home):
            path = os.path.join(self.fixture_home, name)
            if os.path.isfile(path):
                is_filename = is_fixture_filename(
                    path,
                    self.app_model_titles,
                    self.apps_meta
                )
                if is_filename is True:
                    self.validate_fixture_file(path)
                    hold.append(path)
        hold.sort()

        if not hold:
            raise CommandError('There are no fixtures to load.')

        for path in hold:
            yield path

    def handle(self, *args: Any, **options: Any) -> None:
        for path in self.each_fixture_file():
            name = os.path.basename(path)
            txt = 'Loading fixture: {}'
            suc = 'Loaded fixture: {}'
            msg = 'Fixture: {} already loaded'
            if options['dry_run'] is True:
                txt = '<warn>DRY RUN</warn> %s' % txt
                suc = '<warn>DRY RUN</warn> %s' % suc
                msg = 'DRY RUN %s' % msg
            if self.is_fixture_loaded(path) is True:
                self.prnt.warning(msg, name)
            else:
                if options['no_spinner'] is True:
                    call_command(
                        loaddata.Command(),
                        path,
                        verbosity=0,
                        stdout=self.stdout,
                        stderr=self.stderr,
                    )
                    self.prnt.success(suc, name)
                else:
                    if options['dry_run'] is True:
                        self.prnt.success(suc, name)
                    else:
                        with Spin(txt, name, suc=suc):
                            call_command(
                                loaddata.Command(),
                                path,
                                verbosity=0,
                                stdout=self.stdout,
                                stderr=self.stderr,
                            )
        self.prnt.finished()
