
import os
import shutil
import toml
import pprint
from argparse import (
    Action,
    Namespace,
)
from functools import cached_property
from typing import (
    Any,
    Dict,
    Optional,
)
from importlib import import_module

import pytz
from django.conf import settings
from django.core.management.base import CommandParser
from flanj.command import BaseCommand
from flanj.command.exceptions import CommandError
from flanj.env.manifests import (
    Manifest,
    build_manifest,
    manifest_to_dict,
)
from flanj.utils.repo import (
    find_git_home,
)


def _fetch_git_home() -> str:
    """The path to the project's .git directory."""
    path = ''
    try:
        path = settings.PROJECT_HOME
    except AttributeError:
        pass
    else:
        path = find_git_home(path)

    if not path:
        try:
            urlconf = settings.ROOT_URLCONF
        except AttributeError:
            pass
        else:
            module = import_module(urlconf)
            path = os.path.realpath(module.__file__)
            path = find_git_home(path)

    if not path:
        path = find_git_home(os.getcwd())

    return path


class DirectoryAction(Action):

    def __call__(
            self,
            parser: CommandParser,
            namespace: Namespace,
            path: str,
            option_string: Optional[str] = None
    ) -> None:
        if path == '':
            setattr(
                namespace,
                self.dest,
                os.getcwd()
            )
        else:
            path = os.path.expanduser(path)
            path = os.path.expandvars(path)
            path = os.path.abspath(path)
            if os.path.exists(path):
                if os.path.isdir(path):
                    setattr(
                        namespace,
                        self.dest,
                        path
                    )
                else:
                    raise CommandError(
                        f'The given path of {path!r} is not a directory.'
                    )
            else:
                raise CommandError(
                    f'The given directory path of {path!r} does not exist. '
                    f'Please create.'
                )


# noinspection PyUnusedClass
class Command(BaseCommand):

    requires_migrations_checks = True
    # noinspection PyUnusedName
    help = 'Build a manifest toml file'

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            '--force',
            action='store_true',
            help=(
                'overwrite the manifest file if it already exists.'
            )

        )
        parser.add_argument(
            'dir_path',
            nargs='?',
            action=DirectoryAction,
            default='',
            help=(
                'The directory path where to create manifest file. '
                'Defaults to the current working directory.'
            )
        )

    @cached_property
    def git_home(self) -> str:
        """The path to the project's .git directory."""
        return _fetch_git_home()

    @cached_property
    def project_name(self) -> str:
        try:
            out = settings.PROJECT_NAME
        except AttributeError:
            pass
        else:
            out = out.strip()
            if out:
                return out
        path = self.git_home
        if path.endswith(f'{os.path.sep}.git'):
            path = os.path.dirname(path)
        return os.path.basename(path).strip()

    @cached_property
    def manifest(self) -> Manifest:
        return build_manifest(
            self.project_name,
            path=self.git_home,
            time_zone=pytz.UTC,
        )

    @cached_property
    def data(self) -> Dict[str, Any]:
        return manifest_to_dict(self.manifest)

    def build_path(
            self,
            dir_path: str,
            force: bool,
    ) -> str:
        path = os.path.join(dir_path, 'manifest.toml')
        if os.path.exists(path):
            if force is True:
                if os.path.isdir(path) and not os.path.islink(path):
                    shutil.rmtree(path)
                else:
                    os.unlink(path)
                msg = f'Existing {path!r} was deleted.'
                self.prnt.warning(msg)
            else:
                raise CommandError(
                    f"Unable to create the manifest file, {path!r}, "
                    f"because it already exists. Use the '--force' option "
                    f"to force the removal of this file."
                )
        return path

    def handle(self, *args: Any, **options: Any) -> None:
        verbosity = options.get('verbosity', 1)
        if verbosity > 1:
            txt = pprint.pformat(args, indent=2).strip()
            print(
                f'args=\n{txt}',
                file=self.stdout
            )
            txt = pprint.pformat(options, indent=2).strip()
            print(
                f'options=\n{txt}',
                file=self.stdout
            )

        force = options.get('force', False)
        dir_path = options.get('dir_path', '')
        path = self.build_path(dir_path, force)
        if verbosity > 1:
            print(
                '\n'.join((
                    f'force={force!r}',
                    f'dir_path={dir_path!r}',
                    f'path={path!r}',
                    f'git_home={self.git_home!r}',
                )),
                file=self.stdout
            )
        if verbosity > 1:
            print(
                f"data=\n{self.data!r}",
                file=self.stdout
            )

        with open(path, 'w', encoding='utf-8') as f:
            toml.dump(self.data, f)

        if os.path.isfile(path):
            self.stdout.write(f'created: {path}\n')
            self.stdout.flush()

        self.prnt.finished()
