import os
import getpass
import urllib.parse
from dataclasses import dataclass

from typing import (
    Any,
    List,
    Tuple,
    Optional,
    Union,
    Dict,
    Generator,
)

from psycopg2 import (
    connect,
    OperationalError
)

# noinspection PyPep8Naming
from psycopg2.extensions import connection as Connection
from psycopg2.extras import NamedTupleCursor
from django.core.management.base import (
    BaseCommand,
    CommandError,
)

from django.core.management.color import Style
from django.conf import settings

from flanj.pg import (
    Groups,
    Users,
    Databases,
    Schemata,
    Roles,
    fetch_django_settings_databases,
    DjangoSettingsPostgres,
    Pgpass,
)


@dataclass
class CmdArgs:
    admin_user: str
    admin_database: str
    admin_dsn: str
    host: str
    port: int
    project_group: str
    project_user: str
    project_user_password: str
    project_database: str
    project_schema: str
    project_dsn: str
    force: bool
    verbosity: int


class BuildCmdArgs:

    def __init__(self) -> None:
        self._pgpass: Pgpass = Pgpass()
        self._django_databases_: Dict[str, DjangoSettingsPostgres] = dict()

    @property
    def django_databases(self) -> Tuple[DjangoSettingsPostgres]:
        if not self._django_databases_:
            self._django_databases_ = fetch_django_settings_databases()
        return self._django_databases_

    @staticmethod
    def _format_admin_user(admin_user: Union[str, None]) -> str:
        if not isinstance(admin_user, str):
            admin_user = ''
        admin_user = admin_user.strip()
        if not admin_user:
            admin_user = getpass.getuser()
        return admin_user

    @staticmethod
    def _format_admin_dbname(dbname: Union[str, None]) -> str:
        if not isinstance(dbname, str):
            dbname = ''
        dbname = dbname.strip()
        if not dbname:
            dbname = 'postgres'
        return dbname

    def _format_admin_password(
            self,
            admin_user: str,
            host: str,
            dbname: str,
            port: int = 5432
    ) -> str:
        return self._pgpass.get_password(
            admin_user,
            host,
            dbname,
            port=port
        )

    def _format_host(self, host: Union[str, None]) -> str:
        if not isinstance(host, str):
            host = ''
        host = host.strip()
        if host == 'localhost' or not host:
            host = '127.0.0.1'
        host = self._pgpass.fetch_ipaddress(host)
        return host

    @staticmethod
    def _format_port(port: Union[str, int, None]) -> int:
        try:
            port = int(port)
        except (TypeError, ValueError):
            raise CommandError(
                "The port must be set to an 'int' between 1 and 65535"
            )
        if (1 <= port <= 65535) is False:
            raise CommandError(
                "The port must be set to an 'int' between 1 and 65535"
            )
        return port

    @staticmethod
    def _format_admin_parameters(data: Union[str, None]) -> str:
        if not isinstance(data, str):
            data = ''
        data = data.strip()
        if data and not data.startswith('?'):
            data = '?%s' % data
        return data

    @staticmethod
    def _format_project_group(project_group: Union[str, None]) -> str:
        if not isinstance(project_group, str):
            project_group = ''
        project_group = project_group.strip()
        if not project_group:
            project_group = '%s_dev' % settings.PROJECT_NAME
        return project_group

    def __call__(
            self,
            admin_user: Optional[str] = None,
            admin_dbname: Optional[str] = None,
            admin_password: Optional[str] = None,
            admin_parameters: Optional[str] = None,
            host: Optional[str] = None,
            port: Union[str, int, None] = None,
            project_group: Optional[str] = None,
            force: bool = False,
            verbosity: Optional[int] = None,
            no_color: bool = False,
            **kwargs: Any
    ) -> Generator[CmdArgs, None, None]:
        admin_user: str = self._format_admin_user(admin_user)
        admin_dbname: str = self._format_admin_dbname(admin_dbname)
        host: str = self._format_host(host)
        port: int = self._format_port(port)
        params: str = self._format_admin_parameters(admin_parameters)
        admin_password: str = self._format_admin_password(
            admin_user,
            host,
            admin_dbname,
            port=port
        )
        project_group: str = self._format_project_group(
            project_group
        )

        for db_settings in self.django_databases:
            if db_settings.HOST == host and db_settings.PORT == port:
                admin_user = urllib.parse.quote_plus(admin_user)
                admin_pw = urllib.parse.quote_plus(admin_password)
                dsn = f"postgresql://{admin_user}:{admin_pw}@{host}:{port}"

                db = ''
                if admin_dbname:
                    db = '/%s' % urllib.parse.quote_plus(admin_dbname)
                admin_dsn = f"{dsn}{db}{params}"

                db = '/%s' % urllib.parse.quote_plus(db_settings.NAME)
                project_dsn = f"{dsn}{db}{params}"

                yield CmdArgs(
                    admin_user,
                    admin_dbname,
                    admin_dsn,
                    host,
                    port,
                    project_group,
                    db_settings.USER,
                    db_settings.PASSWORD,
                    db_settings.NAME,
                    db_settings.SCHEMA,
                    project_dsn,
                    force,
                    verbosity
                )


each_cmd_args = BuildCmdArgs()


class Base:

    def __init__(
            self,
            cmd_args: CmdArgs,
            style: Optional[Style] = None
    ) -> None:
        self.cmd_args: CmdArgs = cmd_args
        self.style: Union[Style, None] = style
        self.admin_dsn: str = cmd_args.admin_dsn
        self.project_dsn: str = cmd_args.project_dsn
        self.force: bool = cmd_args.force
        self.verbosity: int = cmd_args.verbosity
        self._conn_admin_: Union[Connection, None] = None
        self._conn_project_: Union[Connection, None] = None
        self._groups_: Union[Groups, None] = None
        self._users_: Union[Users, None] = None
        self._databases_: Union[Databases, None] = None
        self._project_database_: Union[Databases, None] = None
        self._schemata_: Union[Schemata, None] = None
        self._roles_: Union[Roles, None] = None

    def __del__(self):
        if self._conn_admin_ is not None:
            self._conn_admin_.close()
        if self._conn_project_ is not None:
            self._conn_project_.close()

    @property
    def _conn_admin(self) -> Connection:
        if self._conn_admin_ is None:
            self._conn_admin_ = connect(
                dsn=self.admin_dsn,
                cursor_factory=NamedTupleCursor
            )
            self._conn_admin_.set_session(autocommit=True)
        return self._conn_admin_

    @property
    def _conn_project(self) -> Connection:
        if self._conn_project_ is None:
            print(self.project_dsn)
            self._conn_project_ = connect(
                dsn=self.project_dsn,
                cursor_factory=NamedTupleCursor
            )
            self._conn_project_.set_session(autocommit=True)
        return self._conn_project_

    @property
    def _groups(self) -> Groups:
        if self._groups_ is None:
            self._groups_ = Groups(
                style=self.style,
                conn=self._conn_admin,
                force=self.force,
                verbosity=self.verbosity
            )
        return self._groups_

    @property
    def _users(self) -> Users:
        if self._users_ is None:
            self._users_ = Users(
                style=self.style,
                conn=self._conn_admin,
                force=self.force,
                verbosity=self.verbosity
            )
        return self._users_

    @property
    def _roles(self) -> Roles:
        if self._roles_ is None:
            self._roles_ = Roles(
                style=self.style,
                conn=self._conn_admin,
                force=self.force,
                verbosity=self.verbosity
            )
        return self._roles_

    @property
    def _databases(self) -> Databases:
        if self._databases_ is None:
            self._databases_ = Databases(
                style=self.style,
                conn=self._conn_admin,
                force=self.force,
                verbosity=self.verbosity
            )
        return self._databases_

    @property
    def _project_database(self) -> Databases:
        if self._project_database_ is None:
            self._project_database_ = Databases(
                style=self.style,
                conn=self._conn_project,
                force=self.force,
                verbosity=self.verbosity
            )
        return self._project_database_

    @property
    def _schemata(self) -> Schemata:
        if self._schemata_ is None:
            self._schemata_ = Schemata(
                style=self.style,
                conn=self._conn_project,
                force=self.force,
                verbosity=self.verbosity
            )
        return self._schemata_


class Reset(Base):

    def drop_roles(self) -> None:
        roles: List[str] = [
            self.cmd_args.project_user,
            self.cmd_args.project_group,
            'global_readonly',
            'global_developers',
            'global_usage',
            'global_admin'
        ]
        for role in roles:
            self._roles.drop(role, warn=True)

    def revoke_privileges(self) -> None:
        if self._databases.fetch(
                self.cmd_args.project_database,
                hide_stdout=True
        ):
            roles: List[str] = [
                self.cmd_args.project_group,
                'global_readonly',
                'global_developers',
                'global_usage',
                'global_admin',
                self.cmd_args.admin_user,
            ]
            try:
                self._project_database
            except OperationalError:
                return None
            funcs: List[Any] = [
                self._project_database.revoke_all_default_type_privileges,
                self._project_database.revoke_all_default_function_privileges,
                self._project_database.revoke_all_default_sequence_privileges,
                self._project_database.revoke_all_default_table_privileges,

            ]
            for role in roles:
                for func in funcs:
                    func(role)

    def __call__(self) -> None:

        self.revoke_privileges()

        # Disconnect all connections to the project database.
        self._databases.disconnect_all(
            self.cmd_args.project_database
        )

        # Drop the database
        self._databases.drop(
            self.cmd_args.project_database
        )

        # Drop all of the roles:
        self.drop_roles()


class Prep(Base):

    def groups(self) -> None:
        self._groups.create_admin(
            force=False
        )
        self._groups.create_usage(
            force=False
        )
        self._groups.create_readonly(
            force=False
        )
        self._groups.create_usage(
            name=self.cmd_args.project_group,
            force=False
        )

    def project_user(self) -> None:
        self._users.create(
            self.cmd_args.project_user,
            self.cmd_args.project_user_password,
            can_create_database=True,
            inherit=True,
            force=False
        )

        # Add project user to project group
        self._roles.add_member(
            self.cmd_args.project_user,
            self.cmd_args.project_group
        )

    def database(self) -> None:
        self._databases.create(
            self.cmd_args.project_database,
            self.cmd_args.admin_user
        )
        self._databases.revoke_all(
            'PUBLIC',
            self.cmd_args.project_database
        )

        self._databases.set_admin_privileges(
            self.cmd_args.project_database
        )
        self._databases.set_usage_privileges(
            self.cmd_args.project_database
        )
        self._databases.set_readonly_privileges(
            self.cmd_args.project_database
        )
        self._databases.set_usage_privileges(
            self.cmd_args.project_database,
            role=self.cmd_args.project_group
        )
        self._databases.allow_connections(
            self.cmd_args.project_database
        )

        if (self.cmd_args.project_schema != 'public' and
                self.cmd_args.project_schema != self.cmd_args.project_user):
            self._roles.set_search_path(
                self.cmd_args.project_user,
                self.cmd_args.project_database,
                self.cmd_args.project_schema,
                force=False
            )

        self._project_database.set_default_admin_privileges()
        self._project_database.set_default_usage_privileges()
        self._project_database.set_default_readonly_privileges()
        self._project_database.set_default_usage_privileges(
            self.cmd_args.project_group
        )
        self._project_database.revoke_all_default_table_privileges(
            'PUBLIC'
        )
        self._project_database.revoke_all_default_sequence_privileges(
            'PUBLIC'
        )
        self._project_database.revoke_all_default_function_privileges(
            'PUBLIC'
        )
        self._project_database.revoke_all_default_type_privileges(
            'PUBLIC'
        )

    def schema(self) -> None:

        # Recreate the public schema with the admin_user as the owner
        # and to clear-out the default privileges.
        self._schemata.drop('public')
        self._schemata.create(
            'public',
            self.cmd_args.admin_user,
            force=False
        )
        self._schemata.set_admin_privileges('public')
        self._schemata.set_usage_privileges('public')
        self._schemata.set_readonly_privileges('public')
        self._schemata.set_usage_privileges(
            'public',
            role=self.cmd_args.project_group
        )

        if self.cmd_args.project_schema != 'public':
            self._schemata.create(
                self.cmd_args.project_schema,
                self.cmd_args.admin_user,
                force=False
            )
            self._schemata.set_admin_privileges(
                self.cmd_args.project_schema,
            )
            self._schemata.set_usage_privileges(
                self.cmd_args.project_schema,
            )
            self._schemata.set_readonly_privileges(
                self.cmd_args.project_schema
            )
            self._schemata.set_usage_privileges(
                self.cmd_args.project_schema,
                role=self.cmd_args.project_group
            )

    def __call__(self) -> None:

        # Disconnect all connections to the project database.
        self._databases.disconnect_all(
            self.cmd_args.project_database
        )
        self.groups()
        self.project_user()
        self.database()
        self.schema()


def run(style: Optional[Style] = None, **options) -> None:
    processed = list()
    for cmd_args in each_cmd_args(**options):
        if options.get('reset', False) is True:
            process = Reset(cmd_args, style=style)
        else:
            process = Prep(cmd_args, style=style)

        process()
        msg = 'Finished with %s@%s' % (
            process.cmd_args.project_user,
            process.cmd_args.project_database
        )
        if style:
            processed.append(style.SUCCESS(msg))
        else:
            processed.append(msg)

    if processed:
        for msg in processed:
            print(msg)
        if style:
            print(style.SUCCESS('Done'))
        else:
            print('Done')
    else:
        if style:
            print(style.WARNING('Nothing processed.'))
        else:
            print('Nothing processed.')


def _get_project_name() -> str:
    out = '__PROJECT__'
    if hasattr(settings, 'PROJECT_NAME'):
        out = getattr(settings, 'PROJECT_NAME', '__PROJECT__')

    if out == '__PROJECT__':
        if hasattr(settings, 'PROJECT'):
            obj = getattr(settings, 'PROJECT')
            try:
                out = obj.get('name', '__PROJECT__')
            except AttributeError:
                pass
    return out


PROJECT_NAME = _get_project_name()


class Command(BaseCommand):

    help = "Setup a Postgres database for use with %s." % PROJECT_NAME
    requires_system_checks = False

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.cmd_args: Union[CmdArgs, None] = None
        self._prep_: Union[Prep, None] = None
        self._reset_: Union[Reset, None] = None

    def add_arguments(self, parser):
        parser.add_argument(
            '--admin-user',
            default=getpass.getuser(),
            help=(
                "The Postgres username that has the ability to create "
                "databases.  Defaults to: '%(default)s'."
            )
        )
        parser.add_argument(
            '--admin-database',
            default='postgres',
            help=(
                "The initial database that the 'admin-user' will connect to "
                "when logging in.  Defaults to: '%(default)s'."
            )
        )
        parser.add_argument(
            '--host',
            default='localhost',
            help=(
                "The hostname or ip-address of the Postgres database server.  "
                "Defaults to: '%(default)s'."
            )
        )
        parser.add_argument(
            '--port',
            type=int,
            default=5432,
            help=(
                "The port used to connect to the 'host'.  Defaults to: "
                "%(default)s."
            )
        )
        parser.add_argument(
            '--admin_parameters',
            default='',
            help=(
                "Any additional parameters needed for the admin-user to "
                "connect to the database.  This should be a string of "
                "parameters separated with an ampersand '&'.  (e.g. "
                "--parameters 'connect_timeout=10&sslmode=disable')  See: "
                "https://bit.ly/2sW74D9 for information on parameter "
                "keywords.  Defaults to an empty string."
            )
        )
        parser.add_argument(
            '--project-group',
            default='%s_dev' % PROJECT_NAME,
            help=(
                "The name of the project's database group.  This is the "
                "a group that contains application users (Django users)."
                "Defaults to: '%(default)s'."
            )
        )
        parser.add_argument(
            '--force',
            action='store_true',
            default=False,
            help=(
                "This will drop and recreate the database."
            )
        )

        parser.add_argument(
            '--reset',
            action='store_true',
            default=False,
            help=(
                "This will remove everything created by this command.  "
                "This will take precedence if the '--force' option is given."
            )
        )

    def handle(self, *args, **options) -> None:
        run(style=self.style, **options)
