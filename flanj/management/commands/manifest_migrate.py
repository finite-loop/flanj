
import os
import shutil
import toml
import pprint
from argparse import (
    Action,
    Namespace,
)
from functools import cached_property
from io import StringIO
from typing import (
    Any,
    Dict,
    Optional,
)
from importlib import import_module

import pytz
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import CommandParser
from flanj.command import BaseCommand
from flanj.command.exceptions import CommandError
from flanj.env.manifests import (
    Manifest,
    build_manifest,
    manifest_to_dict,
)
from flanj.utils.repo import (
    find_git_home,
)
from flanj.env import Env


def _fetch_git_home() -> str:
    """The path to the project's .git directory."""
    path = ''
    try:
        path = settings.PROJECT_HOME
    except AttributeError:
        pass
    else:
        path = find_git_home(path)

    if not path:
        try:
            urlconf = settings.ROOT_URLCONF
        except AttributeError:
            pass
        else:
            module = import_module(urlconf)
            path = os.path.realpath(module.__file__)
            path = find_git_home(path)

    if not path:
        path = find_git_home(os.getcwd())

    return path


class DirectoryAction(Action):

    def __call__(
            self,
            parser: CommandParser,
            namespace: Namespace,
            path: str,
            option_string: Optional[str] = None
    ) -> None:
        if path == '':
            setattr(
                namespace,
                self.dest,
                os.getcwd()
            )
        else:
            path = os.path.expanduser(path)
            path = os.path.expandvars(path)
            path = os.path.abspath(path)
            if os.path.exists(path):
                if os.path.isdir(path):
                    setattr(
                        namespace,
                        self.dest,
                        path
                    )
                else:
                    raise CommandError(
                        f'The given path of {path!r} is not a directory.'
                    )
            else:
                raise CommandError(
                    f'The given directory path of {path!r} does not exist. '
                    f'Please create.'
                )


# noinspection PyUnusedClass
class Command(BaseCommand):

    requires_migrations_checks = True
    # noinspection PyUnusedName
    help = 'Build a manifest toml file'

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            '--force',
            action='store_true',
            help=(
                'overwrite the manifest file if it already exists.'
            )

        )
        parser.add_argument(
            'dir_path',
            nargs='?',
            action=DirectoryAction,
            default='',
            help=(
                'The directory path where to create manifest file. '
                'Defaults to the current working directory.'
            )
        )

    @cached_property
    def git_home(self) -> str:
        """The path to the project's .git directory."""
        return _fetch_git_home()

    @cached_property
    def project_name(self) -> str:
        try:
            out = settings.PROJECT_NAME
        except AttributeError:
            pass
        else:
            out = out.strip()
            if out:
                return out
        path = self.git_home
        if path.endswith(f'{os.path.sep}.git'):
            path = os.path.dirname(path)
        return os.path.basename(path).strip()

    @cached_property
    def manifest(self) -> Manifest:
        return build_manifest(
            self.project_name,
            path=self.git_home,
            time_zone=settings.TIME_ZONE,
        )

    def handle(self, *args: Any, **options: Any) -> None:
        verbosity = options.get('verbosity', 1)
        if verbosity > 1:
            txt = pprint.pformat(args, indent=2).strip()
            print(
                f'args=\n{txt}',
                file=self.stdout
            )
            txt = pprint.pformat(options, indent=2).strip()
            print(
                f'options=\n{txt}',
                file=self.stdout
            )
        for migration in self.manifest.migrations:
            print(f'{migration.app}.{migration.name}')
            with StringIO() as stdout:
                call_command(
                    'migrate',
                    '--no-input',
                    migration.app,
                    migration.name,
                    stdout=stdout,
                )
                print(stdout.getvalue())

        self.prnt.finished()
