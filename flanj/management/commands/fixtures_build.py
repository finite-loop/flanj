import os
import shutil
from functools import cached_property
from typing import (
    Any,
    Dict,
    Generator,
    NamedTuple,
    Optional,
    Tuple,
    cast,
)
from django.apps import apps as django_apps
from django.conf import settings
from django.core.management.base import CommandParser
from django.core.management import call_command
from flanj.command import BaseCommand
from flanj.command.exceptions import CommandError
from flanj.command.prnt import Spin
from flutils.objutils import is_list_like


def get_fixture_home() -> str:
    """Return the full path to the directory that holds the fixtures.

    The following settings.py values are checked using the first one
    that exists.

    1. settings.FIXTURE_HOME
    2. settings.SERVER_HOME/fixtures
    3. settings.BASE_HOME/fixtures
    4. settings.BASE_DIR/fixtures
    5. settings.PROJECT_HOME/fixtures
    """
    try:
        return settings.FIXTURE_HOME
    except AttributeError:
        pass

    try:
        out = settings.SERVER_HOME
    except AttributeError:
        pass
    else:
        return os.path.join(out, 'fixtures')

    try:
        out = settings.BASE_HOME
    except AttributeError:
        pass
    else:
        return os.path.join(out, 'fixtures')

    try:
        out = settings.BASE_DIR
    except AttributeError:
        pass
    else:
        return os.path.join(out, 'fixtures')

    try:
        out = settings.PROJECT_HOME
    except AttributeError:
        pass
    else:
        return os.path.join(out, 'fixtures')

    raise CommandError(
        "Unable to determine the FIXTURE_HOME. Please make sure that at least "
        "one of the following is set in 'settings.py': FIXTURE_HOME, "
        "SERVER_HOME, BASE_DIR, BASE_HOME, PROJECT_HOME"
    )


class AppMeta(NamedTuple):
    """Holds information regarding a Django app and all of it's
    model names.
    """
    name: str
    models: Tuple[str, ...]


def get_apps_meta() -> Tuple[AppMeta, ...]:
    """Return a tuple of :obj:`~AppMeta` objects for the Django project.
    The applications listed in the ``settings.py`` file in ``INSTALLED_APPS``
    are used, with exceptions given to any ``django.`` application.

    The objects returned in this function are used to determine the possible
    models (tables) can be used as a fixture.
    """
    out = []
    for app, models_ in django_apps.all_models.items():
        hold = []
        for model, klass in models_.items():
            module: str = getattr(klass, '__module__')
            if not module.startswith('django'):
                hold.append(model)
        if hold:
            out.append(
                AppMeta(
                    name=app,
                    models=tuple(sorted(hold))
                )
            )
    return tuple(sorted(out, key=lambda x: x.name))


def get_app_model_titles(
        apps: Optional[Tuple[AppMeta, ...]] = None
) -> Tuple[str, ...]:
    """Return a tuple of all app-model-titles.  An app-model-title is
    a portion of a fixture filename.
    """
    if apps is None:
        apps = get_apps_meta()
    out = set()
    for app in apps:
        for model in app.models:
            name = '{}__{}'.format(
                app.name.lower().replace('.', '_'),
                model.lower().replace('.', '_'),
            )
            out.add(name)
    return tuple(sorted(out))


def is_fixture_filename(
        path: str,
        app_model_titles: Optional[Tuple[str, ...]] = None,
        apps: Optional[Tuple[AppMeta, ...]] = None,
) -> bool:
    """Return ``True`` if the given ``path``'s basename is correct for a
    fixture. Return ``False`` otherwise.
    """
    basename = os.path.basename(path)
    if not basename.endswith('.json'):
        return False
    basename = basename[:-5]

    num, sep, name = basename.partition('__')
    if sep != '__':
        return False
    if not num.isdigit():
        return False
    if not name:
        return False

    if app_model_titles is None:
        app_model_titles = get_app_model_titles(apps=apps)

    if name.lower() in app_model_titles:
        return True
    return False


def get_settings_fixtures() -> Tuple[Tuple[str, str], ...]:
    """Return a tuple with each row being a tuple holding the application
    name and a model name (within that app), of the desired fixture.

    All desired fixtures are set in the ``settings.py`` file in the
    ``FIXTURES`` variable.
    """
    out = []
    try:
        fixtures = settings.FIXTURES
    except AttributeError:
        raise CommandError(
            "In 'settings.py' the value for FIXTURES is not set. This must "
            "be a tuple or list. Each row must be a list or tuple with the "
            "first item having a valid name of an app and the second item "
            "having a valid name of a model within the app."

        )
    if not is_list_like(fixtures):
        raise CommandError(
            "In 'settings.py' the value in FIXTURES must be a tuple or "
            "list. Each row must be a list or tuple with the first item "
            "having a valid name of an app and the second item "
            "having a valid name of a model within the app."
        )
    for index, row in enumerate(fixtures):
        if not is_list_like(row):
            raise CommandError(
                f"In 'settings.py' the value in FIXTURES at index {index} "
                f"must be a tuple or list having two items. The first item "
                f"must be a valid name of an app and the second item must be "
                f"a valid name of a model within the app."
            )
        if len(row) != 2:
            raise CommandError(
                f"In 'settings.py' the value in FIXTURES at index {index} "
                f"must be a tuple or list having two items. The first item "
                f"must be a valid name of an app and the second item must be "
                f"a valid name of a model within the app."
            )
        item = (str(row[0]), str(row[1]))
        if item in out:
            raise CommandError(
                f"In 'settings.py' the value in FIXTURES at index {index} "
                f"is a duplicate."
            )
        else:
            out.append(item)
    return tuple(out)


def fetch_app_model(
        app_name: str,
        model_name: str,
        index: int,
        apps: Optional[Tuple[AppMeta, ...]] = None
) -> Tuple[AppMeta, str]:
    """Return the ``AppMeta`` object for the given ``app_name`` and
    ``model_name``

    Args:
        app_name (str): The name of the application as in ``settings.py``
            ``INSTALLED_APPS``.
        model_name (str): The name of the desired application's model.
        index (int): The index of the ``settings.py`` ``FIXTURES`` value.
            Used for more verbose error messages.
        apps (optional): A tuple of :obj:`~AppMeta` objects. If this is not
            set the values will be obtained by calling ``get_apps_meta``.
    """
    if apps is None:
        apps = get_apps_meta()
    for app in apps:
        if app.name.lower() == app_name.lower():
            for model in app.models:
                if model.lower() == model_name.lower():
                    return app, model

    raise CommandError(
        f"In 'settings.py' the value in FIXTURES at index {index} "
        f"must be a tuple or list. Each row must be a list or tuple with "
        f"the first item having a valid name of an app and the second item "
        f"having a valid name of a model within the app. Unable to find app: "
        f"{app_name!r} and model: {model_name!r}"
    )


class Options(NamedTuple):
    """The options used in the command to build the fixture."""
    exclude: Tuple[str,  ...]
    natural_foreign: bool = True
    natural_primary: bool = True
    format: str = 'json'


def build_options(app: AppMeta, model: str) -> Options:
    """Return an ``Options`` object with the given ``app`` object and
    ``model`` name."""
    hold = []
    for name in app.models:
        if name.lower() != model.lower():
            hold.append('%s.%s' % (app.name, name))
    return Options(exclude=tuple(sorted(hold)))


class FixtureMeta(NamedTuple):
    """Holds all the information for calling the ``manage.py dumpdata``
    command.
    """
    num: int
    app: str
    model: str
    filename: str
    path: str
    args: Tuple[str, ...]
    options: Options


def each_fixture_meta(
        apps: Optional[Tuple[AppMeta, ...]] = None,
        fixture_home: Optional[str] = None,
) -> Generator[FixtureMeta, None, None]:
    """Yield a ``FixtureMeta`` object for each row listed in the
    ``settings.py`` ``FIXTURES`` setting.

    Args:
        apps (optional): A tuple of :obj:`~AppMeta` objects. If this is not
            set the values will be obtained by calling ``get_apps_meta``.
        fixture_home (str, optional): The directory where the fixture
            files will be built. If this is not set, the returned value
            from calling ``get_fixture_home`` will be used.
    """
    if apps is None:
        apps = get_apps_meta()
    if not hasattr(fixture_home, 'encode') or not fixture_home:
        fixture_home = get_fixture_home()
    fixture_home = cast(str, fixture_home)
    for index, row in enumerate(get_settings_fixtures()):
        app, model = fetch_app_model(row[0], row[1], index, apps=apps)
        options = build_options(app, model)
        filename = '{num:04d}__{app}__{model}.json'.format(
            num=index,
            app=app.name.replace('.', '_'),
            model=model.replace('.', '_'),
        )
        app_name = app.name.split('.')[-1]
        path = os.path.join(fixture_home, filename)
        args = ('dumpdata', app_name)
        yield FixtureMeta(
            num=index,
            app=app.name,
            model=model,
            filename=filename,
            path=path,
            args=args,
            options=options,
        )


class Command(BaseCommand):

    help = 'Build the fixture files'
    requires_migrations_checks = True
    ADD_DRY_RUN_ARGUMENT = True

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            '--wipe',
            action='store_true',
            default=False,
            help="Remove all fixtures before building."
        )

        parser.add_argument(
            '--force',
            action='store_true',
            default=False,
            help="Force overwrite of existing fixtures."
        )

    @cached_property
    def fixtures_home(self) -> str:
        path = get_fixture_home()
        if os.path.exists(path):
            if not os.path.isdir(path):
                raise CommandError("The path %r must be a directory." % path)
            self.prnt.success('Using existing dir: {}', path)
        else:
            os.makedirs(path, mode=0o700)
            self.prnt.success('Created dir: {}', path)
        return path

    def wipe(self, options: Dict[str, Any]) -> None:
        if options['wipe'] is not True:
            return
        wiped = False
        for name in os.listdir(self.fixtures_home):
            path = os.path.join(self.fixtures_home, name)
            if os.path.isdir(path):
                shutil.rmtree(path)
                if os.path.exists(path):
                    os.rmdir(path)
            else:
                os.unlink(path)
            if options['verbose'] >= 2:
                if options['dry_run'] is True:
                    self.prnt.success(
                        '<warn>DRY RUN</warn> removed: {}',
                        path
                    )
                else:
                    self.prnt.success(
                        'Removed: {}',
                        path
                    )
            else:
                wiped = True
        if wiped is True:
            if options['dry_run'] is True:
                self.prnt.success(
                    '<warn>DRY RUN</warn> wiped dir: {}',
                    self.fixtures_home
                )
            else:
                self.prnt.success(
                    'Wiped dir: {}',
                    self.fixtures_home
                )

    def handle(self, *args: Any, **options: Any) -> None:
        self.wipe(options)
        for obj in each_fixture_meta(fixture_home=self.fixtures_home):
            if os.path.isfile(obj.path):
                if options['force'] is True:
                    if options['dry_run'] is True:
                        self.prnt.success(
                            '<warn>DRY RUN</warn> removed: {}',
                            obj.filename
                        )
                    else:
                        os.unlink(obj.path)
                        self.prnt.warning('Removed: {}', obj.filename)
                else:
                    self.prnt.warning(
                        'Skipped existing fixture: {}',
                        obj.filename
                    )
                    continue

            txt = 'Building fixture {}'
            suc = 'Built fixture {}'
            if options['dry_run'] is True:
                txt = '<warn>DRY RUN</warn> %s' % txt
                suc = '<warn>DRY RUN</warn> %s' % suc

            with Spin(txt, obj.filename, suc=suc):
                if options['dry_run'] is False:
                    with open(obj.path, 'w') as f:
                        # noinspection PyProtectedMember
                        kwargs = obj.options._asdict()
                        if not kwargs['exclude']:
                            del kwargs['exclude']
                        call_command(
                            *obj.args,
                            stdout=f,
                            **kwargs
                        )
        self.prnt.finished()
