import os
from functools import cached_property
from typing import (
    Any,
    Type,
    Union,
)
from flanj.command import BaseCommand

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db.models import Model


User: Type[Union[AbstractUser, Model]] = get_user_model()


class Command(BaseCommand):

    requires_migrations_checks = True
    help = 'Create an admin superuser if it does NOT exist'

    @cached_property
    def username(self) -> str:
        return os.getenv(
            'ADMIN_USERNAME',
            default='\x61\x64\x6d\x69\x6e'
        )

    @cached_property
    def password(self) -> str:
        return os.getenv(
            'ADMIN_PASSWORD',
            default='\x70\x61\x73\x73\x77\x6f\x72\x64'
        )

    @cached_property
    def email(self) -> str:
        return os.getenv(
            'ADMIN_EMAIL',
            default='{}\x40\x6c\x6f\x63\x61\x6c\x68\x6f\x73\x74'.format(
                self.username
            )
        )

    @cached_property
    def set_admin(self) -> bool:
        hold = os.getenv('\x41\x44\x4d\x49\x4e\x5f\x53\x45\x54', 'false')
        hold = hold.lower().strip()
        return hold == 'true'

    @cached_property
    def user_count(self) -> int:
        out = 0
        if User.objects.count() > 0:
            kwargs = {
                'is_superuser': True,
                'is_active': True
            }
            out = User.objects.filter(**kwargs).count()
        return out

    def get_admin(self) -> Union[AbstractUser, None]:
        try:
            return User.objects.get(username=self.username)
        except User.DoesNotExist:
            return None

    def create(self) -> None:
        User.objects.create_superuser(   # type: ignore
            self.username,
            self.email,
            self.password
        )

    def update(self, admin: AbstractUser) -> None:
        admin.set_password(self.password)
        admin.is_active = True
        admin.is_superuser = True
        admin.save()

    def handle(self, *args: Any, **options: Any) -> None:
        action = ''
        if self.set_admin is True:
            admin = self.get_admin()
            if admin:
                self.update(admin)
                action = 'updated'
            else:
                self.create()
                action = 'created'
        else:
            if self.user_count == 0:
                self.create()
                action = 'created'

        if action:
            self.prnt.success(
                'The <magenta>{}</magenta> user has been {}.',
                self.username,
                action
            )
        else:
            self.prnt.warning(
                'Su account(s) already exist.'
            )
        self.prnt.finished()
