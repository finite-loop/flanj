from typing import (
    Any,
    Iterable,
    Union,
    cast,
)
from functools import cached_property
from django.conf import settings
from django.contrib.auth.models import (
    Permission,
    Group,

)
from django.core.management.base import CommandParser

from flanj.command import BaseCommand
from flanj.command.exceptions import CommandError
from flanj.conf.permissions import (
    BUILTIN_SENTINELS,
    each_permission_setting,
    validate_permissions_settings,
)
from flanj.conf.groups import (
    each_group_setting,
    validate_groups_settings,
)
from flanj.conf.types import GroupSetting
from flanj.models import GroupIdentifier


def _get_group(setting: GroupSetting) -> tuple[Group, bool]:
    try:
        identifier = GroupIdentifier.objects.get(identifier=setting.identifier)
    except GroupIdentifier.DoesNotExist:
        obj = Group(name=setting.name)
        obj.save()
        identifier = GroupIdentifier(
            identifier=setting.identifier,
            group=obj,
        )
        identifier.save()
        return obj, True
    return identifier.group, False


class Command(BaseCommand):
    requires_migrations_checks = True
    help = (
        'Create/update permissions and groups as set in settings'
    )

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._delete_orphans = False

    @cached_property
    def _settings_delete_orphans(self) -> bool:
        out: Union[str, bool, int] = getattr(
            settings,
            'DELETE_GROUP_ORPHAN_PERMISSIONS',
            False,
        )
        if isinstance(out, bool):
            out = cast(bool, out)
            return out
        out = str(out)
        out = cast(str, out)
        if out.lower() in ('t', 'true', '0', 'yes'):
            return True
        return False

    @property
    def delete_orphans(self) -> bool:
        return self._delete_orphans

    @delete_orphans.setter
    def delete_orphans(self, val: bool) -> None:
        if not isinstance(val, bool):
            val = False
        if val is not True and self._settings_delete_orphans is True:
            val = True
        self._delete_orphans = val

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            '--permissions-settings-variable',
            default='PERMISSIONS',
            help=(
                "Set the name of the variable, in settings.py, "
                "holding the permission settings. Defaults to: 'PERMISSIONS'"
            )
        )
        parser.add_argument(
            '--groups-settings-variable',
            default='GROUPS',
            help=(
                "Set the name of the variable, in settings.py, "
                "holding the group settings. Defaults to: 'GROUPS'"
            )
        )
        parser.add_argument(
            '--delete-group-orphan-permissions',
            default=False,
            action='store_true',
            help=(
                "Delete any permissions that are assigned to the group "
                "but not listed in the group settings. "
                "Defaults to: False"
            )
        )

    def process_permissions(self, options: Any) -> None:
        settings_key = options['permissions_settings_variable']
        settings_val = getattr(settings, settings_key, ())
        if settings_val:
            count: int = 0
            kwargs = {
                'settings_key': settings_key,
                'settings_val': settings_val,
            }
            try:
                validate_permissions_settings(**kwargs)
            except (ValueError, TypeError) as e:
                raise CommandError(f'{e!s}\n')

            for setting in each_permission_setting(**kwargs):
                count += 1
                created = False
                if setting.permission_name in BUILTIN_SENTINELS:
                    self.prnt.success(
                        f'Permission: '
                        f'<magenta>{setting.content_type.app_label}.'
                        f'{setting.permission_codename} </magenta>'
                        f'<yellow>built-in</yellow> already exists'
                    )
                    continue

                exists = Permission.objects.filter(
                        codename=setting.permission_codename,
                        content_type=setting.content_type,
                ).exists()
                if exists is not True:
                    obj = Permission(
                        codename=setting.permission_codename,
                        name=setting.permission_name,
                        content_type=setting.content_type
                    )
                    obj.save()
                    created = True

                app_label = setting.content_type_app_label
                codename = setting.permission_codename
                if created:
                    self.prnt.success(
                        f'Permission: '
                        f'<magenta>{app_label}.{codename}</magenta> '
                        f'was created'
                    )
                else:
                    self.prnt.success(
                        f'Permission: '
                        f'<magenta>{app_label}.{codename}</magenta> '
                        f'already exists'
                    )
            name = 'permissions'
            verb = 'were'
            if count == 1:
                name = 'permission'
                verb = 'was'
            self.prnt.info(f'There {verb} {count} {name} processed')
        else:
            self.prnt.warning(
                f'settings.{settings_key} was not set or empty.'
            )
        self.prnt('')

    def _process_group_permissions(
            self,
            group: Group,
            setting: GroupSetting,
    ) -> None:
        for permission in setting.permissions:
            app_label: str = permission.content_type.app_label
            codename: str = permission.codename
            if permission in group.permissions.all():
                self.prnt.success(
                    f'<cyan>*</cyan> Permission <magenta>{app_label}.'
                    f'{codename}</magenta> already exists in the group'
                )
                continue
            group.permissions.add(permission)
            self.prnt.success(
                f'<cyan>*</cyan> Permission <magenta>{app_label}.{codename}'
                f'</magenta> was added to the group'
            )
        if self.delete_orphans is True:
            for permission in group.permissions.all():
                app_label: str = permission.content_type.app_label
                codename: str = permission.codename
                if permission not in setting.permissions:
                    group.permissions.remove(permission)
                    self.prnt.success(
                        f'<cyan>*</cyan> Permission <magenta>'
                        f'{app_label}.{codename}</magenta> was '
                        f'<yellow>removed</yellow> from '
                        f'the group'
                    )

    def process_groups(self, options: Any) -> None:
        settings_key = options['groups_settings_variable']
        settings_val = getattr(settings, settings_key, ())
        if settings_val:
            count: int = 0
            kwargs = {
                'settings_key': settings_key,
                'settings_val': settings_val,
            }
            try:
                validate_groups_settings(**kwargs)
            except (ValueError, TypeError) as e:
                raise CommandError(f'{e!s}\n')

            for setting in each_group_setting(**kwargs):
                count += 1
                obj, created = _get_group(setting)
                if created:
                    self.prnt.success(
                        f'Group: <magenta>{setting.name}</magenta> '
                        f'Identifier: <magenta>{setting.identifier}</magenta> '
                        f'was created'
                    )
                else:
                    self.prnt.success(
                        f'Group: <magenta>{setting.name}</magenta> '
                        f'Identifier: <magenta>{setting.identifier}</magenta> '
                        f'already exists'
                    )
                self._process_group_permissions(
                    obj,
                    setting,
                )

            name = 'groups'
            verb = 'were'
            if count == 1:
                name = 'group'
                verb = 'was'
            self.prnt.info(f'There {verb} {count} {name} processed')
        else:
            self.prnt.warning(
                f'settings.{settings_key} was not set or empty.'
            )
        self.prnt('')

    def handle(self, *args: Any, **options: Any) -> None:
        self.delete_orphans = options.get(
            'delete_group_orphan_permissions',
            False
        )
        self.prnt('')
        self.process_permissions(options)
        self.process_groups(options)
        self.prnt.finished()
