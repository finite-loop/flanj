from typing import NamedTuple

from functools import cached_property
from typing import Any

from django.contrib.auth.models import Permission
from flanj.command import BaseCommand

_LABELS: dict[str, str] = {
    'pk': 'PK',
    'app': 'APP_LABEL',
    'model': 'MODEL',
    'codename': 'CODENAME',
    'permission': 'PERMISSION',
}


class Length(NamedTuple):
    app: int
    model: int
    codename: int
    permission: int
    pk: int


class Row(NamedTuple):
    app: str
    model: str
    codename: str
    permission: str
    pk: str


def _build_row(perm: Permission) -> Row:
    kwargs = {
        'app': perm.content_type.app_label,
        'model': perm.content_type.model,
        'codename': perm.codename,
        'permission': f'{perm.content_type.app_label}.{perm.codename}',
        'pk': str(perm.pk),
    }
    return Row(**kwargs)


def _largest(permissions: tuple[Row, ...], attr: str) -> int:
    out = len(_LABELS[attr])
    for perm in permissions:
        length = len(getattr(perm, attr).strip())
        if length > out:
            out = length
    return out


def _build_length(permissions: tuple[Row, ...]) -> Length:
    kwargs = {}
    for attr in Length._fields:
        kwargs[attr] = _largest(permissions, attr)
    return Length(**kwargs)


class Command(BaseCommand):
    requires_migrations_checks = True
    help = 'Show all of the permissions'

    @cached_property
    def permissions(self) -> tuple[Row, ...]:
        out = list(map(_build_row, Permission.objects.all()))
        return tuple(sorted(out))

    @cached_property
    def length(self) -> Length:
        return _build_length(self.permissions)

    @cached_property
    def header(self) -> str:
        top = []
        bottom = []
        for attr, label in _LABELS.items():
            length = getattr(self.length, attr)
            top.append(f'{label:<{length}}')
            bottom.append('-' * length)
        top_str = '  '.join(top)
        bottom_str = '  '.join(bottom)
        return f'{top_str}\n{bottom_str}'

    def _build_line(self, row: Row) -> str:
        out = []
        for attr in _LABELS.keys():
            val = getattr(row, attr)
            length = getattr(self.length, attr)
            out.append(f'{val:<{length}}')
        return '  '.join(out)

    def handle(self, *args: Any, **options: Any) -> None:
        print('')
        print(f'{self.header}')
        for row in self.permissions:
            line = self._build_line(row)
            print(line)
