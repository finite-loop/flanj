from typing import Any

from django.contrib.contenttypes.models import ContentType
from flanj.command import BaseCommand


class Command(BaseCommand):
    requires_migrations_checks = True
    help = 'Show all of the content types'

    def handle(self, *args: Any, **options: Any) -> None:
        print('{:<30} {:<30} CLASS'.format('APP_LABEL', 'MODEL'))
        print('{:<30} {:<30} -----'.format('---------', '-----'))
        for i, obj in enumerate(ContentType.objects.all()):
            model_class = obj.model_class().__module__
            print(f'{obj.app_label:<30} {obj.model:<30} {model_class}')
