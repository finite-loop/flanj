import os
from functools import cached_property
from importlib import import_module
from types import ModuleType

from django.core.management.base import CommandError
from flanj.command import BaseCommand
from flanj.env import Env


class Command(BaseCommand):

    help = "Show the flanj.Env values."
    requires_system_checks = False

    @cached_property
    def settings_module(self) -> str:
        out: str = os.environ.get('DJANGO_SETTINGS_MODULE', '')
        if not out:
            raise CommandError(
                'The environment variable DJANGO_SETTINGS_MODULE is not set.'
            )
        return out

    @cached_property
    def settings(self) -> ModuleType:
        return import_module(self.settings_module)

    @cached_property
    def env(self) -> Env:
        return self.settings.env

    def handle(self, *args, **options) -> None:
        self.prnt.heading(f'{self.settings_module}.env values')
        self.prnt(
            f'env.package_name={self.env.package_name!r}'
        )
        self.prnt(
            f'env.package_home={self.env.package_home!r}'
        )
        self.prnt(
            f'env.package_is_installed={self.env.package_is_installed!r}'
        )
        self.prnt(
            f'env.package_is_in_home={self.env.package_is_in_home!r}'
        )
        self.prnt(
            (
                f'env.package_data_home_env_var='
                f'{self.env.package_data_home_env_var!r}'
            )
        )
        self.prnt(
            f'env.package_data_home={self.env.package_data_home!r}'
        )
        self.prnt(
            (
                f'env.package_config_home_env_var='
                f'{self.env.package_config_home_env_var!r}'
            )
        )
        self.prnt(
            f'env.package_config_home={self.env.package_config_home!r}'
        )
        self.prnt(
            (
                f'env.package_cert_home_env_var='
                f'{self.env.package_cert_home_env_var!r}'
            )
        )
        self.prnt(
            f'env.package_cert_home={self.env.package_cert_home!r}'
        )
        self.prnt(
            (
                f'env.package_cert_private_home_env_var='
                f'{self.env.package_cert_private_home_env_var!r}'
            )
        )
        self.prnt(
            (
                f'env.package_cert_private_home='
                f'{self.env.package_cert_private_home!r}'
            )
        )
        self.prnt(
            f'env.default_database_url={self.env.default_database_url!r}'
        )
        if self.env.dotenv:
            self.prnt.heading('DOTENV VALS')
            for key, val in self.env.dotenv.items():
                if hasattr(val, 'replace'):
                    val = val.replace('{', '{{')
                    val = val.replace('}', '}}')
                out = f'{key}={val!r}'
                self.prnt(out)
