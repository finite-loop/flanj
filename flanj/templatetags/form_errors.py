import json
from django.template import (
    Node,
    Library,
    TemplateSyntaxError
)

register = Library()


class GetFormErrorsDataNode(Node):
    def __init__(self, variable):
        self.variable = variable

    def render(self, context):
        if context.get('form'):
            if hasattr(context['form'], 'errors'):
                if hasattr(context['form'].errors, 'as_json'):
                    data = context['form'].errors.as_json()
                    data = json.loads(data)
                    context[self.variable] = data
                    context['CHECKING'] = 'data'
                else:
                    context[self.variable] = ''
                    context['CHECKING'] = "no context['form'].errors.as_json()"
            else:
                context[self.variable] = ''
                context['CHECKING'] = "no context['form'].errors"
        else:
            context[self.variable] = ''
            context['CHECKING'] = "no context['form']"
        return ''


@register.tag('get_form_errors_data')
def do_get_form_errors_data(parser, token):

    args = token.contents.split()
    if len(args) != 3:
        raise TemplateSyntaxError(
            "'get_form_errors_data' as VARIABLE_NAME (got %r)" % args
        )

    variable = args[2]
    return GetFormErrorsDataNode(variable)


class GetFormErrorsJsonNode(Node):
    def __init__(self, variable):
        self.variable = variable

    def render(self, context):
        if context.get('form'):
            if hasattr(context['form'], 'errors'):
                if hasattr(context['form'].errors, 'as_json'):
                    data = context['form'].errors.as_json()
                    context[self.variable] = data
                    context['CHECKING'] = 'json'
                else:
                    context[self.variable] = ''
                    context['CHECKING'] = "no context['form'].errors.as_json()"
            else:
                context[self.variable] = ''
                context['CHECKING'] = "no context['form'].errors"
        else:
            context[self.variable] = ''
            context['CHECKING'] = "no context['form']"
        return ''


@register.tag('get_form_errors_json')
def do_get_form_errors_json(parser, token):

    args = token.contents.split()
    if len(args) != 3:
        raise TemplateSyntaxError(
            "'get_form_errors_json' as VARIABLE_NAME (got %r)" % args
        )

    variable = args[2]
    return GetFormErrorsJsonNode(variable)


class GetPasswordChangeErrors(Node):

    def __init__(self, variable):
        self.variable = variable

    def extract_errors(self, context):
        if context.get('form'):
            if hasattr(context['form'], 'errors'):
                if hasattr(context['form'].errors, 'as_json'):
                    data = context['form'].errors.as_json()
                    data = json.loads(data)
                    fields = list()
                    if 'new_password1' in data.keys():
                        fields.append('new_password1')
                    if 'new_password2' in data.keys():
                        fields.append('new_password2')
                    if fields:
                        return fields, data
        return list(), dict()

    def process_error(self, error):
        code = error.get('code', '')
        if code == 'password_too_similar':
            return (
                "The password can't be too similar to any personal "
                "information."
            )
        message = error.get('message', '')
        return message.replace('This password', 'The password')

    def process_errors(self, context):
        out = list()
        fields, data = self.extract_errors(context)
        for field in fields:
            errors = data[field]
            for error in errors:
                msg = self.process_error(error)
                if msg and msg not in out:
                    out.append(msg)
        return out

    def render(self, context):
        context[self.variable] = self.process_errors(context)
        return ''


@register.tag('get_password_change_errors')
def do_get_password_change_errors(parser, token):

    args = token.contents.split()
    if len(args) != 3:
        raise TemplateSyntaxError(
            "'get_password_change_errors' as VARIABLE_NAME (got %r)" % args
        )

    variable = args[2]
    return GetPasswordChangeErrors(variable)
