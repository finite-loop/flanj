from django.conf import settings
from django.template import (
    Node,
    Library,
    TemplateSyntaxError
)

register = Library()


class GetSettingNode(Node):
    def __init__(self, variable, name=None):
        self.variable = variable
        if name is None:
            self.name = variable
        else:
            self.name = name

    def render(self, context):
        context[self.name] = getattr(settings, self.variable)
        return ''


@register.tag('get_setting')
def do_get_setting(parser, token):

    # token.split_contents() isn't useful here because this ver doesn't accept
    # variable as arguments
    args = token.contents.split()
    if len(args) != 2 and len(args) != 4:
        raise TemplateSyntaxError(
            "'get_setting' VARIABLE_NAME [as NAME] (got %r)" % args
        )
    if len(args) == 4 and args[2] != 'as':
        raise TemplateSyntaxError(
            "'get_setting' VARIABLE_NAME [as NAME] (got %r)" % args
        )

    variable = args[1]
    name = None
    if len(args) == 4:
        name = args[3]

    return GetSettingNode(variable, name=name)


class GetAuthUserModelNode(Node):
    def __init__(self, variable, name=None):
        self.variable = variable
        if name is None:
            self.name = variable
        else:
            self.name = name

    def render(self, context):
        model_name = getattr(settings, 'AUTH_USER_MODEL', '')
        model_name_parts = model_name.split('.')
        hold = {
            'name': model_name_parts[0].lower(),
            'cls_name': model_name_parts[1].lower(),
        }
        context[self.name] = hold.get(self.variable, '')
        return ''


@register.tag('get_auth_user_model')
def do_get_auth_user_model(parser, token):

    # token.split_contents() isn't useful here because this ver doesn't accept
    # variable as arguments
    args = token.contents.split()
    if len(args) != 2 and len(args) != 4:
        raise TemplateSyntaxError(
            "'get_auth_user_model' VARIABLE_NAME [as NAME] (got %r)" % args
        )
    if len(args) == 4 and args[2] != 'as':
        raise TemplateSyntaxError(
            "'get_auth_user_model' VARIABLE_NAME [as NAME] (got %r)" % args
        )

    variable = args[1]
    name = None
    if len(args) == 4:
        name = args[3]

    return GetAuthUserModelNode(variable, name=name)

