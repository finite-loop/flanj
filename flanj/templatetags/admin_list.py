from django.contrib.admin.templatetags.admin_list import (
    register,
    get_template,
    paginator_number,
    pagination,
    pagination_tag,
    result_headers,
    items_for_result,
    ResultList,
    results,
    result_hidden_fields,
    result_list,
    result_list_tag,
    date_hierarchy,
    date_hierarchy_tag,
    search_form,
    search_form_tag,
    admin_actions,
    admin_actions_tag,
    change_list_object_tools_tag,
)
# from django.template import Library
# from django.template.loader import get_template

# register = Library()


@register.simple_tag
def admin_list_filter(cl, spec):
    tpl = get_template(spec.template)
    return tpl.render({
        'filter_index': getattr(spec, 'filter_index', -1),
        'filter_is_last': getattr(spec, 'filter_is_last', False),
        'parameter_name': getattr(spec, 'parameter_name', ''),
        'title': spec.title,
        'choices': list(spec.choices(cl)),
        'spec': spec,
    })
