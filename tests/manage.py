#!/usr/bin/env python
import os
import sys

_PROJECT_HOME = os.path.dirname(os.path.abspath(__file__))
_ENV_PATH = '%s:%s' % (
    os.path.join(_PROJECT_HOME, 'envs'),
    os.path.join(_PROJECT_HOME, 'envs_blank'),
)

if __name__ == '__main__':
    os.environ.setdefault('ENV_PATH', _ENV_PATH)
    python_path = os.environ.get('PYTHONPATH', '')
    if python_path:
        os.environ['PYTHONPATH'] = f'{_PROJECT_HOME}:{python_path}'
    else:
        os.environ['PYTHONPATH'] = f'{_PROJECT_HOME}'
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'flanjer.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
