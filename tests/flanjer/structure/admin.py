from django.contrib import admin
from . import models


@admin.register(models.Building)
class BuildingAdmin(admin.ModelAdmin):
    _app_position = 3
    list_display = (
        'id',
        'type',
        'name'
    )
    ordering = (
        'name',
    )
    search_fields = (
        'id',
        'name',
    )


@admin.register(models.Floor)
class FloorAdmin(admin.ModelAdmin):
    _app_position = 2

    list_display = (
        'id',
        'building',
        'number',
        'name',
    )
    ordering = (
        'building__name',
        'number',
        'name',
    )


@admin.register(models.Room)
class RoomAdmin(admin.ModelAdmin):
    _app_position = 1
    list_display = (
        'id',
        'floor',
        'name',
    )
    ordering = (
        'floor__building__name',
        'floor__name',
        'name',
    )






