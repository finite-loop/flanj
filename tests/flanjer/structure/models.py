from django.db import models



BUILDING_TYPES = (
    (0, 'residential', ),
    (1, 'business', ),
    (2, 'residential/business', )
)


class Building(models.Model):

    type = models.IntegerField(
        choices=BUILDING_TYPES,
        null=False
    )
    name = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        unique=True,
    )

    def __str__(self) -> str:
        return self.name


class Floor(models.Model):

    building = models.ForeignKey(
        Building,
        related_name='floors',
        on_delete=models.CASCADE,
    )
    number = models.SmallIntegerField(
        null=False
    )
    name = models.CharField(
        max_length=255,
        null=False,
        blank=False,
    )

    class Meta:
        unique_together = (
            ('building', 'name', ),
            ('building', 'number', ),
        )

    def __str__(self) -> str:
        return '{} -> {}'.format(self.building.name, self.name)


class Room(models.Model):

    floor = models.ForeignKey(
        Floor,
        related_name='rooms',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        max_length=255,
        null=False,
        blank=False,
    )

    class Meta:
        unique_together = (
            ('floor', 'name', ),
        )

    def __str__(self) -> str:
        return '{} -> {}'.format(self.floor.name, self.name)





