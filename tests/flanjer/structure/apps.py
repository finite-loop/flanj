from django.apps import AppConfig


class StructureConfig(AppConfig):
    name = 'flanjer.structure'
