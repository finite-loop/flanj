# type: ignore

from unittest.mock import (
    NonCallableMock,
    patch,
)

from django.test import TestCase

from flanj.utils import repo

MOD = 'flanj.utils.repo'


class TestFindGitHome(TestCase):

    MSG = (
        '\n\n'
        'val = %s.find_git_home(path={path!r})\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.os.path.expanduser', return_value='/foo/bar')
    @patch(f'{MOD}.os.getcwd', return_value='/foo/bar/foo')
    @patch(f'{MOD}.os.path.abspath', return_value='/foo/bar/foo')
    @patch(f'{MOD}.os.path.isdir', side_effect=[True, False])
    def test_home_stop(self, *_: NonCallableMock) -> None:
        kwargs = {
            'path': None,
            'exp': ''
        }
        kwargs['val'] = repo.find_git_home(path=kwargs['path'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.os.path.expanduser', return_value='/root')
    @patch(f'{MOD}.os.getcwd', return_value='/foo/bar/foo')
    @patch(f'{MOD}.os.path.abspath', return_value='/foo/bar/foo')
    @patch(f'{MOD}.os.path.isdir', side_effect=[True, False, False])
    def test_two_parents_stop(self, *_: NonCallableMock) -> None:
        kwargs = {
            'path': None,
            'exp': ''
        }
        kwargs['val'] = repo.find_git_home(path=kwargs['path'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestEachHeadCommit(TestCase):

    MSG = (
        '\n\n'
        'val = list(%s._each_head_commit({arg!r}))\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    def test_none(self) -> None:
        kwargs = {
            'arg': None,
            'exp': []
        }
        # noinspection PyTypeChecker
        kwargs['val'] = list(repo._each_head_commit(kwargs['arg']))
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestRepo(TestCase):

    def test_init_raises_type_error(self) -> None:
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            repo.Repo(None)

    @patch(f'{MOD}.os.path.isdir', return_value=False)
    def test_init_raises_value_error(self, *_: NonCallableMock) -> None:
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            repo.Repo('/foo/bar')

    @patch(f'{MOD}.os.path.isdir', return_value=True)
    @patch(f'{MOD}.Repo.tags', [])
    def test_find_tag_return_none(self, *_: NonCallableMock) -> None:
        r = repo.Repo('/foo/bar')
        val = r._find_tag('abcdef0123456789')
        self.assertIsNone(val)



