# type: ignore

from pathlib import Path
from unittest.mock import (
    NonCallableMock,
    patch,
)

from django.test import TestCase

from flanj.utils import paths

MOD = 'flanj.utils.paths'


class TestContractPath(TestCase):

    MSG = (
        '\n\n'
        'val = %s.contract_path({arg!r})\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.isinstance', return_value=False)
    @patch(f'{MOD}.hasattr', return_value=False)
    def test_raises(self, *_: NonCallableMock) -> None:
        with self.assertRaises(TypeError):
            paths.contract_path('foo/bar')

    @patch(f'{MOD}.os.path.expanduser', return_value='/foo/bar')
    def test_value(self, *_: NonCallableMock) -> None:
        kwargs = {
            'arg': Path('/foo/bar/blah'),
            'exp': '~/blah',
        }
        kwargs['val'] = paths.contract_path(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestContractPathsInArgs(TestCase):

    MSG = (
        '\n\n'
        'val = %s.contract_path_in_args({args!r}, '
        'escape_html={escape_html!r})\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.os.path.sep', '/')
    @patch(f'{MOD}.contract_path', return_value='~/blah')
    @patch(f'{MOD}.escape', side_effect=('~/blah', 'foo-bar',))
    def test_str(self, *_: NonCallableMock) -> None:
        kwargs = {
            'args': ('/foo/bar/blah', '<a>foo-bar</a>'),
            'escape_html': True,
            'exp': ('~/blah', 'foo-bar')
        }
        kwargs['val'] = paths.contract_paths_in_args(
            kwargs['args'],
            escape_html=kwargs['escape_html'],
        )
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.os.path.sep', '/')
    @patch(f'{MOD}.escape', return_value='foo-bar')
    @patch(f'{MOD}.contract_path', return_value='~/blah')
    def test_path(self, *_: NonCallableMock) -> None:
        kwargs = {
            'args': (Path('/foo/bar/blah'), '<a>foo-bar</a>'),
            'escape_html': True,
            'exp': ('~/blah', 'foo-bar')
        }
        kwargs['val'] = paths.contract_paths_in_args(
            kwargs['args'],
            escape_html=kwargs['escape_html'],
        )
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestContractPathsInKwargs(TestCase):

    MSG = (
        '\n\n'
        '%s.contract_path_in_args({kwargs!r}, '
        'escape_html={escape_html!r})\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.os.path.sep', '/')
    @patch(f'{MOD}.contract_path', return_value='~/blah')
    @patch(f'{MOD}.escape', side_effect=('~/blah', 'foo-bar',))
    def test_str(self, *_: NonCallableMock) -> None:

        kwargs = {
            'kwargs': {'a': '/foo/bar/blah', 'b': '<a>foo-bar</a>'},
            'escape_html': True,
            'exp': {'a': '~/blah', 'b': 'foo-bar'}
        }
        kwargs['val'] = kwargs['kwargs'].copy()
        paths.contract_paths_in_kwargs(
            kwargs['val'],
            escape_html=kwargs['escape_html'],
        )
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.os.path.sep', '/')
    @patch(f'{MOD}.escape', return_value='foo-bar')
    @patch(f'{MOD}.contract_path', return_value='~/blah')
    def test_path(self, *_: NonCallableMock) -> None:
        kwargs = {
            'kwargs': {'a': Path('/foo/bar/blah'), 'b': '<a>foo-bar</a>'},
            'escape_html': True,
            'exp': {'a': '~/blah', 'b': 'foo-bar'}
        }
        kwargs['val'] = kwargs['kwargs'].copy()
        paths.contract_paths_in_kwargs(
            kwargs['val'],
            escape_html=kwargs['escape_html'],
        )
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )
