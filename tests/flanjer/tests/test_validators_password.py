# type: ignore
from typing import (
    Any,
    NamedTuple,
)

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from flanj.validators import (
    PasswordCommonValidator,
    PasswordDifferentValidator,
    PasswordLowercaseCharacterValidator,
    PasswordMaximumLengthValidator,
    PasswordMinimumLengthValidator,
    PasswordNumericCharacterValidator,
    PasswordNumericValidator,
    PasswordSpecialCharacterValidator,
    PasswordUppercaseCharacterValidator,
    PasswordUserAttributeSimilarityValidator,
)
from flanj.validators import password

# noinspection PyTypeChecker
User: AbstractUser = get_user_model()


MOD = 'flanj.validators.password'


class TestPasswordValidators(TestCase):

    # noinspection PyUnresolvedReferences
    @staticmethod
    def _get_user() -> AbstractUser:
        try:
            return User.objects.get(username='admin')
        except User.DoesNotExist:
            return User.objects.create_superuser(
                'admin',
                'admin@localhost',
                'password',
            )

    def test_min_length(self) -> None:
        validator = PasswordMinimumLengthValidator()
        self.assertIsNone(
            validator.validate('password')
        )

    def test_min_length_help_text(self) -> None:
        validator = PasswordMinimumLengthValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_min_length_error_message(self) -> None:
        validator = PasswordMinimumLengthValidator()
        with self.assertRaises(ValidationError) as cm:
            validator.validate('pass')
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_max_length(self) -> None:
        validator = PasswordMaximumLengthValidator(max_length=5)
        self.assertIsNone(
            validator.validate('12345')
        )

    def test_max_length_help_text(self) -> None:
        validator = PasswordMaximumLengthValidator(max_length=5)
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_max_length_error_message(self) -> None:
        validator = PasswordMaximumLengthValidator(max_length=5)
        with self.assertRaises(ValidationError) as cm:
            validator.validate('123456')
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_user_attribute_similar(self) -> None:
        user = self._get_user()
        validator = PasswordUserAttributeSimilarityValidator()
        self.assertIsNone(
            validator.validate('password', user=user)
        )

    def test_user_attribute_similar_help_text(self) -> None:
        validator = PasswordUserAttributeSimilarityValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_user_attribute_similar_error_message(self) -> None:
        user = self._get_user()
        validator = PasswordUserAttributeSimilarityValidator()
        with self.assertRaises(ValidationError) as cm:
            validator.validate('admin@localhost', user=user)
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_common(self) -> None:
        validator = PasswordCommonValidator()
        self.assertIsNone(
            validator.validate('paSs_w0rd*&^')
        )

    def test_common_help_text(self) -> None:
        validator = PasswordCommonValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_common_error_message(self) -> None:
        validator = PasswordCommonValidator()
        with self.assertRaises(ValidationError) as cm:
            validator.validate('password')
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_different(self) -> None:
        user = self._get_user()
        validator = PasswordDifferentValidator()
        self.assertIsNone(
            validator.validate('passwords', user=user)
        )

    def test_different_help_text(self) -> None:
        validator = PasswordDifferentValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_different_error_message(self) -> None:
        user = self._get_user()
        validator = PasswordDifferentValidator()
        with self.assertRaises(ValidationError) as cm:
            validator.validate('password', user=user)
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_numeric(self) -> None:
        validator = PasswordNumericValidator()
        self.assertIsNone(
            validator.validate('paSs_w0rd*&^')
        )

    def test_numeric_help_text(self) -> None:
        validator = PasswordNumericValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_numeric_error_message(self) -> None:
        validator = PasswordNumericValidator()
        with self.assertRaises(ValidationError) as cm:
            validator.validate('123456')
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_uppercase_character(self) -> None:
        validator = PasswordUppercaseCharacterValidator()
        self.assertIsNone(
            validator.validate('paSs_w0rd*&^')
        )

    def test_uppercase_character_help_text(self) -> None:
        validator = PasswordUppercaseCharacterValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_uppercase_character_error_message(self) -> None:
        validator = PasswordUppercaseCharacterValidator()
        with self.assertRaises(ValidationError) as cm:
            validator.validate('abc1234_')
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_lowercase_character(self) -> None:
        validator = PasswordLowercaseCharacterValidator()
        self.assertIsNone(
            validator.validate('paSs_w0rd*&^')
        )

    def test_lowercase_character_help_text(self) -> None:
        validator = PasswordLowercaseCharacterValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_lowercase_character_error_message(self) -> None:
        validator = PasswordLowercaseCharacterValidator()
        with self.assertRaises(ValidationError) as cm:
            validator.validate('ABC1234_')
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_numeric_character(self) -> None:
        validator = PasswordNumericCharacterValidator()
        self.assertIsNone(
            validator.validate('paSs_w0rd*&^')
        )

    def test_numeric_character_help_text(self) -> None:
        validator = PasswordNumericCharacterValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_numeric_character_error_message(self) -> None:
        validator = PasswordNumericCharacterValidator()
        with self.assertRaises(ValidationError) as cm:
            # noinspection SpellCheckingInspection
            validator.validate('ABCabc_')
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_special_character(self) -> None:
        validator = PasswordSpecialCharacterValidator()
        self.assertIsNone(
            validator.validate('paSs_w0rd*&^')
        )

    def test_special_character_help_text(self) -> None:
        validator = PasswordSpecialCharacterValidator()
        help_text = validator.get_help_text()
        self.assertGreater(len(help_text), 0)

    def test_special_character_error_message(self) -> None:
        validator = PasswordSpecialCharacterValidator()
        with self.assertRaises(ValidationError) as cm:
            # noinspection SpellCheckingInspection
            validator.validate('ABCabc ')
        e = cm.exception
        self.assertGreater(len(e.messages), 0)
        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)


class UserObj(NamedTuple):
    username: Any
    first_name: Any
    last_name: Any
    email: Any


class TestPasswordUserAttributeSimilarityValidator(TestCase):

    def setUp(self) -> None:
        self.obj = password.PasswordUserAttributeSimilarityValidator()

    def test_none(self) -> None:
        # noinspection PyNoneFunctionAssignment
        val = self.obj.validate('foo')
        self.assertIsNone(val)
