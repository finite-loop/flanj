from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.template import (
    Context,
    Template,
)
from django.test import TestCase
from django.utils import timezone
from django.template.base import TemplateSyntaxError


User = get_user_model()

# noinspection PyProtectedMember
_AUTH_GROUP_APP_VERBOSE_NAME = Group._meta.app_config.verbose_name

# noinspection PyProtectedMember
_AUTH_GROUP_APP_LABEL = Group._meta.app_label

# noinspection PyProtectedMember
_AUTH_USER_APP_LABEL = User._meta.app_label

# noinspection PyProtectedMember
_AUTH_USER_VERBOSE_NAME_PLURAL = User._meta.verbose_name_plural


class TestTemplateTags(TestCase):

    def test_get_settings(self) -> None:

        context = Context({})
        template = Template(
            '{% load settings %}'
            '{% get_settings %}'
        )
        template.render(context)
        exp = timezone.now().strftime('%Y')
        self.assertEqual(context['settings'].COPYRIGHT_YEAR, exp)

    def test_get_settings_developer(self) -> None:
        context = Context({})
        template = Template(
            '{% load settings %}'
            '{% get_settings %}'
            '<h1>{{ settings.DEVELOPER }}</h1>'
        )
        rendered_template = template.render(context)
        exp = '<h1>{}</h1>'.format(settings.DEVELOPER)
        self.assertInHTML(exp, rendered_template)

    def test_get_settings_auth_user_and_group_names(self) -> None:
        vals = {
            'AUTH_GROUP_APP_VERBOSE_NAME': _AUTH_GROUP_APP_VERBOSE_NAME,
            'AUTH_GROUP_APP_LABEL': _AUTH_GROUP_APP_LABEL,
            'AUTH_USER_APP_LABEL': _AUTH_USER_APP_LABEL,
            'AUTH_USER_VERBOSE_NAME_PLURAL': _AUTH_USER_VERBOSE_NAME_PLURAL
        }
        for key, val in vals.items():
            context = Context({})
            parts = [
                '{% load settings %}',
                '{% get_settings %}',
                '<h1>{{ settings.%s }}</h1>' % key
            ]
            template = Template(''.join(parts))
            rendered_template = template.render(context)
            exp = '<h1>{}</h1>'.format(val)
            self.assertInHTML(exp, rendered_template)

    def test_get_settings_as(self) -> None:
        context = Context({})
        template = Template(
            '{% load settings %}'
            '{% get_settings as foo %}'
            '<h1>{{ foo.PROJECT_TITLE }}</h1>'
        )
        rendered_template = template.render(context)
        exp = '<h1>{}</h1>'.format(settings.PROJECT_TITLE)
        self.assertInHTML(exp, rendered_template)

    def test_get_settings_as_error(self) -> None:
        with self.assertRaises(TemplateSyntaxError):
            Template(
                '{% load settings %}'
                '{% get_settings as %}'
                '<h1>{{ foo.PROJECT_TITLE }}</h1>'
            )

    def test_get_settings_bad_identifier(self) -> None:
        with self.assertRaises(TemplateSyntaxError):
            Template(
                '{% load settings %}'
                '{% get_settings as 33foo %}'
                '<h1>{{ foo.PROJECT_TITLE }}</h1>'
            )

