# type: ignore
from typing import (
    Type,
    Union,
    cast,
)
from django.test import TestCase
from django.db.models import Model
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from flanj.validators import (
    UserEmailUniqueValidator,
    UsernameMaxLengthValidator,
    UsernameMinLengthValidator,
    UsernameUnicodeValidator,
    UsernameUniqueValidator,
)


# noinspection Mypy
User: Union[Type[Model], Type[AbstractUser]] = get_user_model()
# noinspection PyTypeChecker
User = cast(Type[AbstractUser], User)


class TestUsernameValidators(TestCase):

    # noinspection PyUnresolvedReferences
    @staticmethod
    def _get_admin_user() -> AbstractUser:
        try:
            return User.objects.get(username='admin')
        except User.DoesNotExist:
            return User.objects.create_superuser(
                'admin',
                'admin@localhost',
                'password',
            )

    # noinspection PyUnresolvedReferences
    @staticmethod
    def _get_foo_user() -> AbstractUser:
        try:
            return User.objects.get(username='foo')
        except User.DoesNotExist:
            return User.objects.create_user(
                'foo',
                'foo@localhost',
                'password',
            )

    def test_username_min_length_validator(self) -> None:
        validate = UsernameMinLengthValidator(8)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate('12345678'))

        with self.assertRaises(ValidationError):
            validate('1234567')

    def test_username_max_length_validator(self) -> None:
        validate = UsernameMaxLengthValidator(9)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate('123456789'))

        with self.assertRaises(ValidationError):
            validate('1234567890')

    def test_username_unicode_validator(self) -> None:
        validate = UsernameUnicodeValidator()
        self.assertTrue(callable(validate))
        self.assertIsNone(validate('a@good.+-_username'))

        with self.assertRaises(ValidationError):
            validate('a bad username')

    def test_username_unique_validator(self) -> None:
        self._get_admin_user()
        foo_user = self._get_foo_user()
        validate = UsernameUniqueValidator(
            message='already exists',
            code='invalid_user',
        )
        self.assertTrue(callable(validate))
        self.assertIsNone(validate('bar'))

        with self.assertRaises(ValidationError):
            validate('foo')

        validate.set_user(foo_user)
        self.assertEqual(validate._user, foo_user)
        self.assertEqual(validate._user_id, foo_user.id)
        self.assertIsNone(validate('foo'))
        with self.assertRaises(ValidationError):
            validate('admin')

    def test_user_email_unique_validator(self) -> None:
        self._get_admin_user()
        foo_user = self._get_foo_user()
        validate = UserEmailUniqueValidator()
        self.assertTrue(callable(validate))
        self.assertIsNone(validate('bar@localhost'))

        with self.assertRaises(ValidationError):
            validate('foo@localhost')

        validate.set_user(foo_user)
        self.assertEqual(validate._user, foo_user)
        self.assertEqual(validate._user_id, foo_user.id)
        self.assertIsNone(validate('foo@localhost'))
        with self.assertRaises(ValidationError):
            validate('admin@localhost')

