import os
import json
import shutil
import tempfile
from datetime import datetime
from unittest import skipIf

from django.test import TestCase
from django.conf import settings
from flanj.env import (
    EmailValueError,
    _Environment,
)
from flanj.env.validators import validate_default_sequence
from flanj.env.url import Url
from flanj.env.manifests import (
    Manifest,
    Repo,
    Tag,
    Commit,
)
from flanj.env import dot_env


_DIR = os.path.dirname(os.path.abspath(__file__))
_PROJECT_HOME = os.path.normpath(os.path.join(_DIR, '..', '..'))
_ENVS =os.path.join(_PROJECT_HOME, 'envs')
os.environ['ENV_PATH'] = _ENVS
_BASH = shutil.which('bash')
_FLANJER_HOME = os.path.dirname(_DIR)
_TESTS_HOME = os.path.dirname(_FLANJER_HOME)
_MANAGE_PY = os.path.join(_TESTS_HOME, 'manage.py')


class TestValidateDefaultSequence(TestCase):

    def test_list(self) -> None:
        arg = [True, 1.789, 10, None, [], (), 'a']
        val = validate_default_sequence(arg, 'list')
        self.assertEqual(val, None)

    def test_list_none_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence(None, 'list')

    def test_list_float_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence(1.54, 'list')

    def test_list_int_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence(54, 'list')

    def test_list_bool_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence(True, 'list')

    def test_list_str_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence('foo', 'list')

    def test_list_invalid_item_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence([1, {}], 'list')

    def test_tuple(self) -> None:
        arg = (True, 1.789, 10, None, [], (), 'a')
        val = validate_default_sequence(arg, 'tuple')
        self.assertEqual(val, None)

    def test_tuple_none_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence(None, 'tuple')

    def test_tuple_float_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence(1.54, 'tuple')

    def test_tuple_int_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence(54, 'tuple')

    def test_tuple_bool_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence(True, 'tuple')

    def test_tuple_str_root_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence('foo', 'tuple')

    def test_tuple_invalid_item_raises(self) -> None:
        with self.assertRaises(TypeError):
            validate_default_sequence((1, {}), 'tuple')


class TestEnv(TestCase):

    def setUp(self) -> None:
       self.env: _Environment = _Environment(
            'flanj',
            dotenv_values=dot_env.values(env_varnames=('ENV_PATH',)),
            package_data_home=_PROJECT_HOME,
        )

    def test_dotenv(self) -> None:
        values = self.env.dotenv
        self.assertTrue('DOTENV_TEST_VALUE' in values.keys())
        self.assertEqual(values['DOTENV_TEST_VALUE'], 'FooBar')

    def test_get_value(self) -> None:
        val = self.env.get('DOTENV_TEST_VALUE', default=self.env.REQUIRED)
        self.assertEqual(val, 'FooBar')

    def test_get_default_required(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get('TEST___VALUE', default=self.env.REQUIRED)

    def test_get_default_empty(self) -> None:
        val = self.env.get('TEST___VALUE', default=self.env.EMPTY)
        self.assertEqual(val, '')

    def test_get_default_value(self):
        val = self.env.get('TEST___VALUE', default='11')
        self.assertEqual(val, '11')

        val = self.env.get('TEST___VALUE', default='None')
        self.assertEqual(val, 'None')

        val = self.env.get('TEST___VALUE', default='hello')
        self.assertEqual(val, 'hello')

    def test_rnd_chars(self) -> None:
        val = self.env.rnd_chars(length=10)
        l = len(val)
        self.assertEqual(l, 10)

    def test_get_bool_true(self) -> None:
        val = self.env.get_bool('BOOL_TRUE', default=self.env.REQUIRED)
        self.assertEqual(val, True)

    def test_get_bool_false(self) -> None:
        val = self.env.get_bool('BOOL_FALSE', default=self.env.REQUIRED)
        self.assertEqual(val, False)

    def test_get_bool_required(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_bool('BOOL___DATA', default=self.env.REQUIRED)

    def test_get_bool_bad_default(self) -> None:
        with self.assertRaises(TypeError):
            self.env.get_bool('BOOL___DATA', default=None)

    def test_get_bool_bad_value(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_bool('BOOL_BAD', default=self.env.REQUIRED)

    def test_get_bool_empty(self) -> None:
        val = self.env.get_bool('BOOL___EMPTY', default=self.env.EMPTY)
        self.assertEqual(val, False)

    def test_get_list_bad_default(self) -> None:
        with self.assertRaises(TypeError):
            self.env.get_list('LIST___DATA', default=None)

        with self.assertRaises(TypeError):
            self.env.get_list('LIST___DATA', default=True)

    def test_get_list_required_error(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_list('LIST___DATA', default=self.env.REQUIRED)

    def test_get_list_default_empty(self) -> None:
        val = self.env.get_list('LIST___DATA', default=self.env.EMPTY)
        self.assertEqual(val, [])

    def test_get_list_values(self) -> None:
        val = self.env.get_list('LIST_VALS')
        self.assertEqual(val, [1, 2, 'test'])

    def test_get_list_values2(self) -> None:
        val = self.env.get_list('LIST_VALS2')
        self.assertEqual(val, ['localhost'])

    def test_get_list_default(self) -> None:
        exp = ['localhost']
        val = self.env.get_list('NOTHING__', default=exp)
        self.assertEqual(val, exp)

    def test_get_list_default_tuple_raises(self) -> None:
        with self.assertRaises(TypeError):
            self.env.get_list('NOTHING__', default=('localhost'))

    def test_get_list_default(self) -> None:
        exp = ['localhost', ('foo', 1, ['bar', 1.3, None, True])]
        val = self.env.get_list('NOTHING__', default=exp)
        self.assertEqual(val, exp)

    def test_get_tuple_bad_default(self) -> None:
        with self.assertRaises(TypeError):
            self.env.get_tuple('TUPLE___DATA', default=None)

        with self.assertRaises(TypeError):
            self.env.get_tuple('TUPLE___DATA', default=['a', 2])

    def test_get_tuple_required_error(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_tuple('TUPLE___DATA', default=self.env.REQUIRED)

    def test_get_tuple_default_empty(self) -> None:
        val = self.env.get_tuple('TUPLE___DATA', default=self.env.EMPTY)
        self.assertEqual(val, ())

    def test_get_tuple_values(self) -> None:
        val = self.env.get_tuple('TUPLE_VALS')
        self.assertEqual(val, (1, 2, 3))

    def test_get_tuple_deep_values1(self) -> None:
        env = self.env.get('TUPLE_DEEP_VALS1')
        val = self.env.get_tuple('TUPLE_DEEP_VALS1')
        exp = (
            ('one', 1),
            ('two', 2),
        )
        self.assertEqual(
            val,
            exp,
            msg=(
                f'\n\n'
                f'  Env val: {env}\n'
                f'      Got: {val!r}\n'
                f'Expecting: {exp!r}\n'
            )
        )

    def test_get_tuple_deep_values2(self) -> None:
        env = self.env.get('TUPLE_DEEP_VALS2')
        val = self.env.get_tuple('TUPLE_DEEP_VALS2')
        exp = (
            ('one', None),
            ('two', True),
        )
        self.assertEqual(
            val,
            exp,
            msg=(
                f'\n\n'
                f'  Env val: {env}\n'
                f'      Got: {val!r}\n'
                f'Expecting: {exp!r}\n'
            )
        )

    def test_get_url_bad_default(self) -> None:
        with self.assertRaises(TypeError):
            self.env.get_url('URL___DATA', default=None)

    def test_get_url_default_required(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_url('URL___DATA', default=self.env.REQUIRED)

    # def test_get_url_empty(self) -> None:
    #     obj = self.env.get_url('URL___DATA')
    #     self.assertIsInstance(obj, str)
    #     self.assertEqual(obj.url, '')

    # def test_get_url_value_url(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertTrue(isinstance(obj, str))
    #
    # def test_get_url_value_url_url(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     exp = 'postgresql://user:pass@localhost:5432/userdb'
    #     self.assertEqual(obj.url, exp)

    # def test_get_url_value_url_scheme(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.scheme, 'postgresql')
    #
    # def test_get_url_value_url_username(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.username, 'user')
    #
    # def test_get_url_value_url_password(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.password, 'pass')
    #
    # def test_get_url_value_url_host(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.host, 'localhost')
    #
    # def test_get_url_value_url_port(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.port, 5432)
    #
    # def test_get_url_value_url_path(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.path, '/userdb')
    #
    # def test_get_url_value_url_query(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.query, {'schema': 'foo'})
    #
    # def test_get_url_value_url_fragment(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.fragment, '')
    #
    # def test_get_url_value_url_database(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.database, 'userdb')
    #
    # def test_get_url_value_url_schema(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.schema, 'foo')
    #
    # def test_get_url_value_url_uses_ssl(self) -> None:
    #     obj = self.env.get_url('DB_URL', default=self.env.REQUIRED)
    #     self.assertEqual(obj.uses_ssl, False)

    def test_get_database_bad_default(self) -> None:
        with self.assertRaises(TypeError):
            self.env.get_database('DB___URL', default=None)

    def test_get_database_missing_required(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_database('DB___URL', default=self.env.REQUIRED)

    def test_get_database_required_not_postgres(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_database('EMAIL_URL', default=self.env.REQUIRED)

    def test_get_database_empty(self) -> None:
        obj = self.env.get_database('DB___URL')
        exp = {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': '/Users/len/Sandbox/flanj/tests/flanj.sqlite3',
            'PASSWORD': '',
            'TIME_ZONE': '',
            'URL': f'sqlite://{_PROJECT_HOME}/flanj.sqlite3'
        }
        self.assertEqual(obj, exp)

    def test_get_database_sqlite(self) -> None:
        name = 'flanj.sqlite3'
        obj = self.env.get_database('DB__URL', time_zone='America/New_York')
        self.assertTrue('ENGINE' in obj.keys())
        self.assertTrue('NAME' in obj.keys())
        self.assertEqual(obj['ENGINE'], 'django.db.backends.sqlite3')
        self.assertEqual(obj['NAME'], os.path.join(_PROJECT_HOME, name))
        self.assertEqual(obj['TIME_ZONE'], 'America/New_York')

    def test_get_database(self) -> None:
        obj = self.env.get_database('DB_URL', default=self.env.REQUIRED)
        self.assertTrue('ENGINE' in obj.keys())
        self.assertTrue('NAME' in obj.keys())
        self.assertTrue('USER' in obj.keys())
        self.assertTrue('PASSWORD' in obj.keys())
        self.assertTrue('HOST' in obj.keys())
        self.assertTrue('PORT' in obj.keys())
        self.assertTrue('SCHEMA' in obj.keys())
        self.assertTrue('URL' in obj.keys())
        self.assertEqual(obj['ENGINE'], 'django.db.backends.postgresql')
        self.assertEqual(obj['NAME'], 'userdb')
        self.assertEqual(obj['USER'], 'user')
        self.assertEqual(obj['PASSWORD'], 'pass')
        self.assertEqual(obj['HOST'], 'localhost')
        self.assertEqual(obj['PORT'], 5432)
        self.assertEqual(obj['SCHEMA'], 'foo')
        exp = 'postgresql://user:pass@localhost:5432/userdb'
        self.assertEqual(obj['URL'], exp)

    def test_set_email_default_type_error(self) -> None:
        with self.assertRaises(TypeError):
            self.env.set_email('EMAIL___URL', default=None)

    def test_set_email_required_error(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.set_email('EMAIL___URL', default=self.env.REQUIRED)

    def test_set_email_scheme_error(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.set_email('DB_URL', default=self.env.REQUIRED)

    def test_set_email_values(self) -> None:
        self.env.set_email('EMAIL_URL', default=self.env.REQUIRED)
        self.assertEqual(
            self.env.settings_module.EMAIL_BACKEND,
            'django.core.mail.backends.smtp.EmailBackend'
        )
        self.assertEqual(
            self.env.settings_module.EMAIL_HOST,
            '127.0.0.1'
        )
        self.assertEqual(
            self.env.settings_module.EMAIL_HOST_PASSWORD,
            'pass'
        )
        self.assertEqual(
            self.env.settings_module.EMAIL_HOST_USER,
            'user'
        )
        self.assertEqual(
            self.env.settings_module.EMAIL_PORT,
            587
        )
        self.assertEqual(
            self.env.settings_module.EMAIL_USE_TLS,
            True
        )

    def test_get_manifest(self) -> None:
        """This only tests if the get_manifest method returns a Manifest
        object. And checks that some basic values exist. Testing of the
        actual flanj.env.manifest module can be found in the
        ``test_env_manifest*.py`` files.
        """
        os.environ['MANIFEST'] = _PROJECT_HOME
        obj = self.env.get_manifest('MANIFEST')
        self.assertIsInstance(obj, Manifest)
        self.assertEqual(obj.project_name, 'flanjer')
        self.assertIsInstance(obj.build_timestamp, datetime)
        self.assertTrue(len(obj.version) > 0)
        self.assertTrue(len(obj.origin) > 0)
        self.assertTrue(len(obj.migrations) > 0)

    def test_get_path_bad_default(self) -> None:
        with self.assertRaises(TypeError):
            self.env.get_path('A___PATH', default=None)

    def test_get_path_required(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_path('A___PATH', default=self.env.REQUIRED)

    def test_get_path_empty(self) -> None:
        obj = self.env.get_path('A___PATH')
        self.assertEqual(obj, '')

    def test_get_path_one(self) -> None:
        obj = self.env.get_path('PATH_ONE', default=self.env.REQUIRED)
        exp = os.path.expanduser('~/tmp/a_file.txt')
        self.assertEqual(obj, exp)

    def test_get_path_two(self) -> None:
        obj = self.env.get_path(
            'PATH_TWO',
            default=self.env.REQUIRED,
            filename='a_file.txt'
        )
        exp = os.path.expanduser('~/tmp/a_file.txt')
        self.assertEqual(obj, exp)

    def test_get_admins1(self) -> None:
        env = self.env.get('ADMINS1')
        val = self.env.get_admins('ADMINS1')
        exp = [
            ('John', 'john@example.com'),
            ('Mary Doe', 'mary@example.com'),
        ]
        self.assertEqual(
            val,
            exp,
            msg=(
                f'\n\n'
                f'  Env val: {env}\n'
                f'      Got: {val!r}\n'
                f'Expecting: {exp!r}\n'
            )
        )

    def test_get_database_postgres_options(self) -> None:
        env = self.env.get('DB_URL2')
        val = self.env.get_database('DB_URL2')
        val = val['OPTIONS']
        exp = {
            'sslmode': 'verify-ca',
            'sslcompression': '1',
            'sslcert': os.path.join(
                self.env.package_cert_home,
                'flanj.crt',
            ),
            'sslkey': os.path.join(
                os.path.expanduser('~'),
                'tmp',
                'flanj.key',
            ),
            'sslrootcert': os.path.join(
                self.env.package_cert_home,
                'flanj.ca',
            ),
            'sslcrl': '/etc/flanj/flanj.crl',
            'passfile': os.path.join(
                self.env.package_config_home,
                '.pgpass'
            ),
            'service': 'foo'
        }
        self.assertEqual(
            val,
            exp,
            msg=(
                f'\n\n'
                f'  Env val: {env}\n'
                f'      Got: {val!r}\n'
                f'Expecting: {exp!r}\n'
            )
        )
        self.assertEqual(
            os.environ.get('PGSERVICEFILE'),
            os.path.join(
                self.env.package_config_home,
                'flanj.as_cfg'
            )
        )

    def test_get_admins2(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_admins('ADMINS2')

    def test_get_admins3(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_admins('ADMINS3')

    def test_get_admins4(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_admins('ADMINS4')

    def test_get_admins5(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_admins('ADMINS5')

    def test_get_time_zone_basic(self) -> None:
        exp = 'America/Nome'
        val = self.env.get_time_zone()
        msg = (
            f'\n\n'
            f'env.get_time_zone()\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_time_zone_invalid(self) -> None:
        with self.assertRaises(SystemExit):
            key = 'TIME_ZONE_BAD'
            self.env.get_time_zone(key)

    def test_get_time_zone_empty(self) -> None:
        key = 'EMPTY_TIME_ZONE'
        exp = ''
        val = self.env.get_time_zone(key)
        msg = (
            f'\n\n'
            f'env.get_time_zone({key!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_time_zone_with_default(self) -> None:
        key = 'A_TIME_ZONE'
        default = 'Asia/Tokyo'
        exp = 'Asia/Tokyo'
        val = self.env.get_time_zone(key, default=default)
        msg = (
            f'\n\n'
            f'env.get_time_zone({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_time_zone_with_invalid_default(self) -> None:
        with self.assertRaises(ValueError):
            key = 'ANOTHER_BAD_TIME_ZONE'
            default = 'America/England'
            self.env.get_time_zone(key, default=default)

    def test_get_time_zone_required_with_empty(self) -> None:
        with self.assertRaises(SystemExit):
            key = 'ANOTHER_BAD_TIME_ZONE'
            self.env.get_time_zone(key, default=self.env.REQUIRED)

    def test_get_log_level_with_no_args(self) -> None:
        exp = 'INFO'
        val = self.env.get_log_level()
        msg = (
            f'\n\n'
            f'env.get_log_level()\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_log_level_with_default(self) -> None:
        key = 'A_LOG_LEVEL'
        default = 'critical'
        exp = 'CRITICAL'
        val = self.env.get_log_level(key, default=default)
        msg = (
            f'\n\n'
            f'env.get_log_level({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_log_level_with_invalid_value(self) -> None:
        with self.assertRaises(SystemExit):
            key = 'A_BAD_LOG_LEVEL'
            self.env.get_log_level(key, default=self.env.REQUIRED)

    def test_get_log_level_with_invalid_default(self) -> None:
        with self.assertRaises(ValueError):
            key = 'A_LOG_LEVEL'
            default = 'bad_log_level'
            self.env.get_log_level(key, default=default)

    def test_get_log_level_required_with_empty(self) -> None:
        with self.assertRaises(SystemExit):
            key = 'A_LOG_LEVEL'
            self.env.get_log_level(key, default=self.env.REQUIRED)

    def test_get_int(self) -> None:
        key = 'INT_VALUE'
        exp = 376
        val = self.env.get_int(key)
        msg = (
            f'\n\n'
            f'env.get_int({key!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_int_with_default(self) -> None:
        key = 'AN_INT_VALUE'
        default = 5
        exp = 5
        val = self.env.get_int(key, default=default)
        msg = (
            f'\n\n'
            f'env.get_int({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_int_invalid_value(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_int('ANOTHER_INT_VALUE')

    def test_get_int_invalid_default_value(self) -> None:
        with self.assertRaises(TypeError):
            self.env.get_int('AN_INT_VALUE', default='invalid')

    @skipIf(len(_BASH) < 1, 'unable to find bash, skipping test')
    def test_get_executable(self) -> None:
        key = 'AN_EXECUTABLE_PATH'
        default = 'bash'
        exp = _BASH
        val = self.env.get_executable_path(key, default=default)
        msg = (
            f'\n\n'
            f'env.get_executable_path({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_executable(self) -> None:
        key = 'AN_EXECUTABLE_PATH'
        default = 'bash'
        exp = _BASH
        val = self.env.get_executable_path(key, default=default)
        msg = (
            f'\n\n'
            f'env.get_executable_path({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_executable_path_required(self) -> None:
        with self.assertRaises(SystemExit):
            self.env.get_executable_path(
                'AN_EXECUTABLE_PATH',
                default=self.env.REQUIRED,
            )


_JSON_DATA = [
    {'a': 1, 'b': False, 'c': None},
    {'a1': 1, 'b1': True, 'c1': 55},
]


_JSON_TMP_HOME = os.path.expandvars('~/tmp')
_JSON_TMP_FILE = os.path.join(_JSON_TMP_HOME, 'test.json')


def _build_json_file() -> str:
    os.makedirs(_JSON_TMP_HOME, mode=0o770, exist_ok=True)
    with tempfile.NamedTemporaryFile('w', delete=False) as f:
        out = f.name
        json.dump(_JSON_DATA, f)
    return out


def _json_file_cleanup(path) -> None:
    if os.path.exists(path):
        os.unlink(path)


class TestGetGroups(TestCase):

    def test_settings_value(self) -> None:
        key = 'GROUPS_TEST'
        exp = [
            {
                'identifier': 'adjuster',
                'name': 'Adjuster',
                'permissions': [
                    'core.role_adjuster',
                ]
            },
            {
                'identifier': 'specialist',
                'name': 'Specialist',
                'permissions': [
                    'core.role_adjuster',
                    'core.role_specialist',
                ]
            },
        ]
        val = getattr(settings, key)
        msg = (
            f'\n\n'
            f' settings.{key!r}\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_settings_default(self) -> None:
        key = 'GROUPS'
        exp = settings.GROUPS_DEFAULT
        val = getattr(settings, key)
        msg = (
            f'\n\n'
            f' settings.{key!r}\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)


class TestEnvJson(TestCase):

    def setUp(self) -> None:
        self.env: _Environment = _Environment(
            'flanj',
            dotenv_values=dot_env.values(env_varnames=('ENV_PATH',)),
            package_data_home=_PROJECT_HOME,
        )
        json_file = _build_json_file()
        self.json_file = json_file
        self.addCleanup(_json_file_cleanup, json_file)

    def test_get_json_default_json(self) -> None:
        key = 'A_JSON_DEFAULT'
        default = '{"a": 1}'
        exp = {'a': 1}
        val = self.env.get_json(key, default=default)
        msg = (
            f'\n\n'
            f'env.get_json({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_json_default_file(self) -> None:
        key = 'A_JSON_DEFAULT'
        default = self.json_file
        exp = _JSON_DATA
        val = self.env.get_json(key, default=default)
        msg = (
            f'\n\n'
            f'env.get_json({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_json_value(self) -> None:
        key = 'JSON_VALUE'
        default = self.env.EMPTY
        exp = _JSON_DATA
        val = self.env.get_json(key, default=default)
        msg = (
            f'\n\n'
            f'env.get_json({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_json_file_value(self) -> None:
        key = 'JSON_FILE'
        default = self.env.EMPTY
        exp = _JSON_DATA
        val = self.env.get_json(
            key,
            default=default,
            json_file=self.json_file,
        )
        msg = (
            f'\n\n'
            f'env.get_json({key!r}, default={default!r}, '
            f'json_file={self.json_file!r}'
            f')\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_data_size_1(self):
        key = 'DATA_SIZE_1'
        default = self.env.EMPTY
        exp = 10000000
        val = self.env.get_data_size(
            key,
            default=default,
        )
        msg = (
            f'\n\n'
            f'env.get_data_size({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)

    def test_get_data_size_2(self):
        key = 'DATA_SIZE_2'
        default = self.env.EMPTY
        exp = 10485760
        val = self.env.get_data_size(
            key,
            default=default,
        )
        msg = (
            f'\n\n'
            f'env.get_data_size({key!r}, default={default!r})\n'
            f'      Got: {val!r}\n'
            f'Expecting: {exp!r}\n'
        )
        self.assertEqual(val, exp, msg=msg)
