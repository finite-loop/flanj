import os
from datetime import datetime
from django.test import TestCase

from flanj.env import manifests

DIR = os.path.dirname(os.path.abspath(__file__))
TEST_HOME = os.path.abspath(os.path.join(DIR, '..', '..'))
PROJECT_HOME = os.path.dirname(TEST_HOME)
PROJECT_NAME = 'flanjer'
TIME_ZONE = 'America/New_York'


# Doing this outside of the class to speed up the tests in this file.
MANIFEST_OBJ = manifests.build_manifest(
    PROJECT_NAME,
    path=PROJECT_HOME,
    time_zone=TIME_ZONE
)


class TestManifestFromGit(TestCase):

    path = PROJECT_HOME

    project_name = PROJECT_NAME

    time_zone = TIME_ZONE

    msg = (
        '\n\n'
        'obj = manifests.build_manifest(%r, path=%r time_zone=%r)\n\n'
        'value = {key!r}\n\n'
        'got:  {got!r}\n\n'
        'expected: {exp!r}\n\n'
    ) % (PROJECT_NAME, PROJECT_HOME, TIME_ZONE)

    obj = MANIFEST_OBJ

    def test_manifests_project_name(self) -> None:
        kwargs = {
            'key': 'self.obj.project_name',
            'got': self.obj.project_name,
            'exp': self.project_name,
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_build_timestamp(self) -> None:
        kwargs = {
            'key': 'self.obj.build_timestamp',
            'got': self.obj.build_timestamp,
            'exp': 'datetime',
        }
        self.assertIsInstance(
            kwargs['got'],
            datetime,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'str(self.obj.build_timestamp.tzinfo)',
            'got': str(self.obj.build_timestamp.tzinfo),
            'exp': self.time_zone,
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_version(self) -> None:
        kwargs = {
            'key': 'type(self.obj.version).__name__',
            'got': type(self.obj.version).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.version,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.version)',
            'got': str(len(self.obj.version)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.version) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_origin(self) -> None:
        kwargs = {
            'key': 'self.obj.origin',
            'got': self.obj.origin,
            'exp': 'git',
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_migrations(self) -> None:
        kwargs = {
            'key': 'self.obj.migrations',
            'got': self.obj.migrations,
            'exp': 'tuple',
        }
        self.assertIsInstance(
            kwargs['got'],
            tuple,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo)',
            'got': type(self.obj.repo).__name__,
            'exp': 'flanj.env.manifests.Repo',
        }
        self.assertIsInstance(
            self.obj.repo,
            manifests.Repo,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_branch(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.branch).__name__',
            'got': type(self.obj.repo.branch).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.branch,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.branch)',
            'got': str(len(self.obj.repo.branch)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.repo.branch) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_path(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.path).__name__',
            'got': type(self.obj.repo.path).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.path,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.path)',
            'got': str(len(self.obj.repo.path)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.repo.path) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_is_dirty(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.is_dirty)',
            'got': type(self.obj.repo.is_dirty).__name__,
            'exp': 'bool',
        }

        self.assertIsInstance(
            self.obj.repo.is_dirty,
            bool,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_has_untracked_files(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.has_untracked_files)',
            'got': type(self.obj.repo.has_untracked_files).__name__,
            'exp': 'bool',
        }

        self.assertIsInstance(
            self.obj.repo.has_untracked_files,
            bool,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tracking_branch(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tracking_branch).__name__',
            'got': type(self.obj.repo.tracking_branch).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.tracking_branch,
            str,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_number_of_commits(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.number_of_commits).__name__',
            'got': type(self.obj.repo.number_of_commits).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.number_of_commits,
            int,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.number_of_commits)',
            'got': str(self.obj.repo.number_of_commits),
            'exp': 'value > -1',
        }
        self.assertTrue(
            self.obj.repo.number_of_commits > -1,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag).__name__',
            'got': type(self.obj.repo.tag).__name__,
            'exp': 'flanj.env.manifests.Tag',
        }
        self.assertIsInstance(
            self.obj.repo.tag,
            manifests.Tag,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_name(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.name).__name__',
            'got': type(self.obj.repo.tag.name).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.tag.name,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.tag.name)',
            'got': str(len(self.obj.repo.tag.name)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.repo.tag.name) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_path(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.path).__name__',
            'got': type(self.obj.repo.tag.path).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.tag.path,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.tag.path)',
            'got': str(len(self.obj.repo.tag.path)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.repo.tag.path) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_sha(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.sha).__name__',
            'got': type(self.obj.repo.tag.sha).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.tag.sha,
            str,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_author(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.author).__name__',
            'got': type(self.obj.repo.tag.author).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.tag.author,
            str,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_timestamp(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.timestamp).__name__',
            'got': type(self.obj.repo.tag.timestamp).__name__,
            'exp': 'datetime.datetime',
        }
        self.assertIsInstance(
            self.obj.repo.tag.timestamp,
            datetime,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'str(self.obj.repo.tag.timestamp.tzinfo)',
            'got': str(self.obj.repo.tag.timestamp.tzinfo),
            'exp': self.time_zone,
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_number_of_commits(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.number_of_commits).__name__',
            'got': type(self.obj.repo.tag.number_of_commits).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.tag.number_of_commits,
            int,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.tag.number_of_commits)',
            'got': str(self.obj.repo.tag.number_of_commits),
            'exp': 'value > -1',
        }
        self.assertTrue(
            self.obj.repo.tag.number_of_commits > -1,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_is_lightweight(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.is_lightweight)',
            'got': type(self.obj.repo.tag.is_lightweight).__name__,
            'exp': 'bool',
        }
        self.assertIsInstance(
            self.obj.repo.tag.is_lightweight,
            bool,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_commit(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.commit).__name__',
            'got': type(self.obj.repo.tag.commit).__name__,
            'exp': 'flanj.env.manifests.Commit',
        }
        self.assertIsInstance(
            self.obj.repo.tag.commit,
            manifests.Commit,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_commit_sha(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.commit.sha).__name__',
            'got': type(self.obj.repo.tag.commit.sha).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.tag.commit.sha,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.tag.commit.sha)',
            'got': str(len(self.obj.repo.tag.commit.sha)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.repo.tag.commit.sha) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_commit_author(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.commit.author).__name__',
            'got': type(self.obj.repo.tag.commit.author).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.tag.commit.author,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.tag.commit.author)',
            'got': str(len(self.obj.repo.tag.commit.author)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.repo.tag.commit.author) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_tag_commit_timestamp(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.tag.commit.timestamp).__name__',
            'got': type(self.obj.repo.tag.commit.timestamp).__name__,
            'exp': 'datetime.datetime',
        }
        self.assertIsInstance(
            self.obj.repo.tag.commit.timestamp,
            datetime,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'str(self.obj.repo.tag.commit.timestamp.tzinfo)',
            'got': str(self.obj.repo.tag.commit.timestamp.tzinfo),
            'exp': self.time_zone,
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_commit(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.commit).__name__',
            'got': type(self.obj.repo.commit).__name__,
            'exp': 'flanj.env.manifests.Commit',
        }
        self.assertIsInstance(
            self.obj.repo.commit,
            manifests.Commit,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_commit_sha(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.commit.sha).__name__',
            'got': type(self.obj.repo.commit.sha).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.commit.sha,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.commit.sha)',
            'got': str(len(self.obj.repo.commit.sha)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.repo.commit.sha) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_commit_author(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.commit.author).__name__',
            'got': type(self.obj.repo.commit.author).__name__,
            'exp': 'str',
        }
        self.assertIsInstance(
            self.obj.repo.commit.author,
            str,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'len(self.obj.repo.commit.author)',
            'got': str(len(self.obj.repo.commit.author)),
            'exp': 'value > 0',
        }
        self.assertTrue(
            len(self.obj.repo.commit.author) > 0,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo_commit_timestamp(self) -> None:
        kwargs = {
            'key': 'type(self.obj.repo.commit.timestamp).__name__',
            'got': type(self.obj.repo.commit.timestamp).__name__,
            'exp': 'datetime.datetime',
        }
        self.assertIsInstance(
            self.obj.repo.commit.timestamp,
            datetime,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'str(self.obj.repo.commit.timestamp.tzinfo)',
            'got': str(self.obj.repo.commit.timestamp.tzinfo),
            'exp': self.time_zone,
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )
