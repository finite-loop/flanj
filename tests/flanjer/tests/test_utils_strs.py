# type: ignore

from datetime import datetime
from unittest.mock import (
    NonCallableMock,
    patch,
)

from django.test import TestCase

from flanj.utils import strs

MOD = 'flanj.utils.strs'


class TestStripHtml(TestCase):

    MSG = (
        '\n\n'
        'val = %s.strip_html({arg!r})\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    def test_true(self, *_: NonCallableMock) -> None:
        kwargs = {
            'arg': '<a>test</a>',
            'exp': 'test',
        }
        kwargs['val'] = strs.strip_html(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestContainsHtml(TestCase):

    MSG = (
        '\n\n'
        'val = %s.contains_html({arg!r})\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.strip_html', return_value='test')
    def test_false(self, *_: NonCallableMock) -> None:
        kwargs = {
            'arg': 'test',
            'exp': False,
        }
        kwargs['val'] = strs.contains_html(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.strip_html', return_value='test')
    def test_true(self, *_: NonCallableMock) -> None:
        kwargs = {
            'arg': '<a>test</a>',
            'exp': True,
        }
        kwargs['val'] = strs.contains_html(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestSequenceToStr(TestCase):

    MSG = (
        '\n\n'
        'val = %s.sequence_to_str({arg!r})\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    def test_raises(self) -> None:
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            strs.sequence_to_str(datetime.now())

    def test_bytes(self) -> None:
        kwargs = {
            'arg': b'test',
            'exp': 'test',
        }
        kwargs['val'] = strs.sequence_to_str(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    def test_str(self) -> None:
        kwargs = {
            'arg': 'test',
            'exp': 'test',
        }
        kwargs['val'] = strs.sequence_to_str(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    def test_list(self) -> None:
        kwargs = {
            'arg': ['a', 'b', 'c', 'd'],
            'exp': "'a', 'b', 'c' or 'd'",
        }
        kwargs['val'] = strs.sequence_to_str(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )
