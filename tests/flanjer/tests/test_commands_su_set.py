from io import StringIO
from typing import (
    Type,
    Union,
)
from unittest.mock import (
    NonCallableMock,
    patch,
)


from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.management import call_command
from django.core.management.base import CommandError
from django.db.models import Model
from django.test import TestCase
from flanj.management.commands import su_set


User: Type[Union[AbstractUser, Model]] = get_user_model()
MOD = 'flanj.management.commands.su_set'


class TestCommandsSuSet(TestCase):

    def setUp(self) -> None:
        self.stdout = StringIO()
        self.command: su_set.Command = su_set.Command(
            stdout=self.stdout
        )

    def test_functionality(self) -> None:
        # noinspection Mypy
        User.objects.create_user('foo', 'foo@localhost', 'foobar')
        out = StringIO()
        call_command('su_set', stdout=out)
        user = User.objects.get(username='admin')
        self.assertIsInstance(user, User)
        self.assertIn('user has been created.', out.getvalue())

    @patch(f'{MOD}.User.objects.get', side_effect=User.DoesNotExist)
    def test_get_admin(self, *_: NonCallableMock) -> None:
        val = self.command.get_admin()
        self.assertEqual(val, None)

    def test_update(self) -> None:
        admin = self.command.get_admin()
        if admin is None:
            admin = User.objects.create_superuser(
                self.command.username,
                self.command.email,
                'foobar'
            )
        self.command.update(admin)
        admin = self.command.get_admin()
        pw = admin.check_password(self.command.password)
        admin.delete()
        self.assertTrue(pw)

    @patch(f'{MOD}.Command.set_admin', True)
    @patch(f'{MOD}.Command.get_admin', return_value=None)
    @patch(f'{MOD}.Command.create', return_value=None)
    def test_handle_create(
            self,
            create: NonCallableMock,
            get_admin: NonCallableMock,
    ) -> None:
        self.command.handle()
        get_admin.assert_called_once()
        create.assert_called_once()
        self.assertIn('user has been created.', self.stdout.getvalue())

    @patch(f'{MOD}.Command.set_admin', True)
    @patch(f'{MOD}.Command.get_admin', return_value='admin')
    @patch(f'{MOD}.Command.update', return_value=None)
    def test_handle_update(
            self,
            update: NonCallableMock,
            get_admin: NonCallableMock,
    ) -> None:
        self.command.handle()
        get_admin.assert_called_once()
        update.assert_called_once_with('admin')
        self.assertIn('user has been updated.', self.stdout.getvalue())

    @patch(f'{MOD}.Command.set_admin', False)
    @patch(f'{MOD}.Command.user_count', 2)
    def test_handle_exists(self, *_: NonCallableMock) -> None:
        self.command.handle()
        self.assertIn('already exist.', self.stdout.getvalue())
