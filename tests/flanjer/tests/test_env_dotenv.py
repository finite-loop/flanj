import tempfile
import os
from django.test import TestCase
from flanj.env import dot_env


class TestEnvDotenv(TestCase):

    def test_process_name_general(self) -> None:
        hold = {'a', 'b'}
        exp = {'a', 'b', 'c'}
        dot_env._process_name(hold, 'c')
        self.assertSetEqual(hold, exp)

    def test_process_name_dirname(self):
        hold = set()
        with tempfile.TemporaryDirectory() as dirname:
            exp = {dirname}
            dot_env._process_name(hold, dirname, is_dirname=True)
        self.assertSetEqual(hold, exp)

    def test_process_names_str_value(self) -> None:
        hold = {'a', 'b', 'c'}
        exp = {'a', 'b', 'c', 'd'}
        dot_env._process_names(hold, 'd')
        self.assertSetEqual(hold, exp)

    def test_process_names_list_value(self) -> None:
        hold = {'a', 'b', 'c'}
        exp = {'a', 'b', 'c', 'd', 'e'}
        dot_env._process_names(hold, ['d', 'e'])
        self.assertSetEqual(hold, exp)

    def test_process_names_tuple_value(self) -> None:
        hold = {'a', 'b', 'c'}
        exp = {'a', 'b', 'c', 'd', 'e'}
        dot_env._process_names(hold, ('d', 'e'))
        self.assertSetEqual(hold, exp)

    def test_each_name_none_value(self) -> None:
        exp = []
        val = list(dot_env._each_name())
        self.assertEqual(val, exp)

    def test_each_name_string_value(self) -> None:
        arg = 'A_TEST_NAME'
        exp = ['A_TEST_NAME']
        val = list(dot_env._each_name(arg))
        self.assertEqual(val, exp)

    def test_each_name_list_value(self) -> None:
        arg = ['B_TEST_NAME', 1, None, 'B_TEST_NAME', 'A_TEST_NAME']
        exp = ['A_TEST_NAME', 'B_TEST_NAME']
        val = list(dot_env._each_name(arg))
        self.assertEqual(val, exp)

    def test_each_name_tuple_value(self) -> None:
        arg = ('B_TEST_NAME', 1, None, 'B_TEST_NAME', 'A_TEST_NAME')
        exp = ['A_TEST_NAME', 'B_TEST_NAME']
        val = list(dot_env._each_name(arg))
        self.assertEqual(val, exp)

    def test_basenames_default(self) -> None:
        val = dot_env._basenames()
        self.assertEqual(val, dot_env.DOTENV_DEFAULT_BASENAMES)

    def test_basenames_str_value(self) -> None:
        exp = ('FLANJ_TEST_DOTENV',)
        val = dot_env._basenames(exp[0])
        self.assertEqual(val, exp)

    def test_basenames_list_value(self) -> None:
        exp = ('FLANJ_TEST_DOTENV',)
        arg = ['FLANJ_TEST_DOTENV', ' ']
        val = dot_env._basenames(arg)
        self.assertEqual(val, exp)

    def test_basenames_tuple_value(self) -> None:
        exp = ('FLANJ_TEST_DOTENV',)
        arg = ('FLANJ_TEST_DOTENV', ' ', 'FLANJ_TEST_DOTENV', 5)
        val = dot_env._basenames(arg)
        self.assertEqual(val, exp)

    def test_process_path_str_value(self) -> None:
        hold = set()
        with tempfile.TemporaryDirectory() as dirname1:
            with tempfile.TemporaryDirectory() as dirname2:
                exp = {dirname1, dirname2}
                dot_env._process_path(hold, f'{dirname1}:{dirname2}')
        self.assertSetEqual(hold, exp)

    def test_process_path_list_value(self) -> None:
        hold = set()
        with tempfile.TemporaryDirectory() as dirname:
            exp = {dirname}
            arg = [dirname, 1, 'test/jump', f'{dirname}/tmp/../']
            dot_env._process_path(hold, arg)
        self.assertSetEqual(hold, exp)

    def test_process_path_tuple_value(self) -> None:
        hold = set()
        with tempfile.TemporaryDirectory() as dirname:
            exp = {dirname}
            arg = (dirname, 1, 'test/jump', f'{dirname}/tmp/../', None)
            dot_env._process_path(hold, arg)
        self.assertSetEqual(hold, exp)

    def test_each_path_dirname_none_values(self) -> None:
        val = list(dot_env._each_path_dirname())
        self.assertEqual(val, [])

    def test_each_path_dirname_paths_value(self) -> None:
        with tempfile.TemporaryDirectory() as dirname1:
            with tempfile.TemporaryDirectory() as dirname2:
                with tempfile.TemporaryDirectory() as dirname3:
                    arg = (f'{dirname1}:{dirname2}', dirname3)
                    exp =[dirname1, dirname2, dirname3]
                    exp.sort()
                    val = list(dot_env._each_path_dirname(arg))
        self.assertEqual(val, exp)

    def test_each_path_dirname_env_varnames_value(self) -> None:
        with tempfile.TemporaryDirectory() as dirname1:
            with tempfile.TemporaryDirectory() as dirname2:
                with tempfile.TemporaryDirectory() as dirname3:
                    os.environ['A_TEST_VAR'] = f'{dirname1}:{dirname2}'
                    os.environ['B_TEST_VAR'] = dirname3
                    arg = ('A_TEST_VAR', 'B_TEST_VAR')
                    exp =[dirname1, dirname2, dirname3]
                    exp.sort()
                    val = list(dot_env._each_path_dirname(env_varnames=arg))
        self.assertEqual(val, exp)

    def test_each_path_dirname_both_arguments(self) -> None:
        with tempfile.TemporaryDirectory() as dirname1:
            with tempfile.TemporaryDirectory() as dirname2:
                with tempfile.TemporaryDirectory() as dirname3:
                    with tempfile.TemporaryDirectory() as dirname4:
                        with tempfile.TemporaryDirectory() as dirname5:
                            os.environ['A_TEST_VAR'] = f'{dirname1}:{dirname2}'
                            os.environ['B_TEST_VAR'] = dirname3
                            kwargs = {
                                'paths': (dirname4, dirname5),
                                'env_varnames': ('A_TEST_VAR', 'B_TEST_VAR')
                            }
                            exp =[
                                dirname1,
                                dirname2,
                                dirname3,
                                dirname4,
                                dirname5,
                            ]
                            exp.sort()
                            val = list(dot_env._each_path_dirname(**kwargs))
        self.assertEqual(val, exp)


class TestEnvDotenvIntegration(TestCase):

    def setUp(self) -> None:
        self.data = {
            'a': '1',
            'b': '2',
            'c': '3',
            'd': '4',
            'e': '5',
        }
        self.paths = set()
        self.basenames = []
        self.filenames = []
        self.file1 = tempfile.NamedTemporaryFile()
        self.filenames.append(self.file1.name)
        self.paths.add(os.path.dirname(self.file1.name))
        self.basenames.append(os.path.basename(self.file1.name))
        self.file1.file.write('a=1\n'.encode('utf-8'))
        self.file1.file.flush()
        self.addCleanup(self.file1.close)

        self.file2 = tempfile.NamedTemporaryFile()
        self.filenames.append(self.file2.name)
        self.paths.add(os.path.dirname(self.file2.name))
        self.basenames.append(os.path.basename(self.file2.name))
        self.file2.file.write('b=2\n'.encode('utf-8'))
        self.file2.file.flush()
        self.addCleanup(self.file2.close)

        self.file3 = tempfile.NamedTemporaryFile()
        self.filenames.append(self.file3.name)
        self.env_val = os.path.dirname(self.file3.name)
        self.basenames.append(os.path.basename(self.file3.name))
        self.file3.file.write('c=3\nd=4\n'.encode('utf-8'))
        self.file3.file.flush()
        self.addCleanup(self.file3.close)

        self.file4 = tempfile.NamedTemporaryFile()
        self.filenames.append(self.file4.name)
        self.env_val = '{}:{}'.format(
            self.env_val,
            os.path.dirname(self.file4.name)
        )
        self.basenames.append(os.path.basename(self.file4.name))
        self.file4.file.write('e=5\n'.encode('utf-8'))
        self.file4.file.flush()
        self.addCleanup(self.file4.close)
        self.env_var = 'A_TEST_PATH'
        os.environ[self.env_var] = self.env_val

    def tearDown(self) -> None:
        del os.environ[self.env_var]

    def test_each_dotenv_filepath(self) -> None:
        exp = list(sorted(self.filenames))
        kwargs = {
            'env_varnames': self.env_var,
            'paths': list(self.paths),
            'basenames': self.basenames
        }
        val = list(dot_env._each_dotenv_filepath(**kwargs))
        msg = (
            "\n\n"
            "kwargs = {\n"
            "  'env_varnames': %r\n"
            "  'paths': %r\n"
            "  'basenames': %r\n"
            "}\n\n"
            "val = %r\n\n"
            "exp = %r\n\n" % (
                self.env_var,
                list(self.paths),
                self.basenames,
                val,
                exp,
            )
        )
        self.assertEqual(val, exp, msg=msg)

    def test_values(self) -> None:
        kwargs = {
            'env_varnames': self.env_var,
            'paths': list(self.paths),
            'basenames': self.basenames
        }
        val = dot_env.values(**kwargs)
        msg = (
                "\n\n"
                "kwargs = {\n"
                "  'env_varnames': %r\n"
                "  'paths': %r\n"
                "  'basenames': %r\n"
                "}\n\n"
                "val = %r\n\n"
                "exp = %r\n\n" % (
                    self.env_var,
                    list(self.paths),
                    self.basenames,
                    val,
                    self.data,
                )
        )
        self.assertDictEqual(val, self.data)
