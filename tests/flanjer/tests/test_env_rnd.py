from string import ascii_letters
from django.test import TestCase
from flanj.env import rnd


class TestEnvRnd(TestCase):

    def test_chars(self) -> None:
        val = rnd.chars()
        l = len(val)
        self.assertTrue(l >= 28)
        self.assertTrue(l <= 50)

    def test_chars_length(self) -> None:
        val = rnd.chars(length=60)
        l = len(val)
        self.assertEqual(l,  60)

    def test_chars_length_error(self) -> None:
        with self.assertRaises(ValueError):
            rnd.chars(length=3)

    def test_return_length(self) -> None:
        for l in range(20, 60):
            val = rnd.chars(length=l)
            self.assertEqual(len(val), l)

    def test_first_char_letter(self) -> None:
        for _ in range(5):
            val = rnd.chars()
            self.assertIn(val[0], ascii_letters)

