# type: ignore
from collections import (
    UserString,
    namedtuple,
)
from socket import gaierror
from unittest.mock import (
    NonCallableMock,
    patch,
)

from django.test import TestCase

from flanj.utils import objs
from pytz.exceptions import UnknownTimeZoneError

MOD = 'flanj.utils.objs'


class TestHasAttrs(TestCase):

    def test_true(self) -> None:
        self.assertTrue(objs.has_attrs('hello', 'capitalize', 'encode'))

    def test_false(self) -> None:
        self.assertFalse(objs.has_attrs(55, 'capitalize', 'encode'))


class TestIsDict(TestCase):

    def test_true(self) -> None:
        self.assertTrue(objs.is_dict({}))

    def test_false(self) -> None:
        self.assertFalse(objs.is_dict(55))


class TestIsList(TestCase):

    def test_true(self) -> None:
        self.assertTrue(objs.is_list([]))

    def test_false(self) -> None:
        self.assertFalse(objs.is_list(()))


class TestIsTuple(TestCase):

    def test_true(self) -> None:
        self.assertTrue(objs.is_tuple(()))

    def test_false_list(self) -> None:
        self.assertFalse(objs.is_tuple([]))

    def test_false_str(self) -> None:
        self.assertFalse(objs.is_tuple(''))

    def test_false_bytes(self) -> None:
        self.assertFalse(objs.is_tuple(b''))

    def test_false_user_string(self) -> None:
        self.assertFalse(objs.is_tuple(UserString('')))

    def test_false_int(self) -> None:
        self.assertFalse(objs.is_tuple(55))


class TestIsListLike(TestCase):

    def test_true_list(self) -> None:
        self.assertTrue(objs.is_list_like([]))

    def test_true_tuple(self) -> None:
        self.assertTrue(objs.is_list_like(()))

    def test_true_namedtuple(self) -> None:
        build = namedtuple('foo', 'foo, bar')
        val = build(1, 2)
        self.assertTrue(objs.is_list_like(val))

    def test_false_namedtuple(self) -> None:
        build = namedtuple('foo', 'foo, bar')
        val = build(1, 2)
        self.assertFalse(objs.is_list_like(val, namedtuple_ok=False))

    def test_false_int(self) -> None:
        self.assertFalse(objs.is_list_like(55))

    def test_false_none(self) -> None:
        self.assertFalse(objs.is_list_like(None))

    def test_false_str(self) -> None:
        self.assertFalse(objs.is_list_like(''))

    def test_false_bytes(self) -> None:
        self.assertFalse(objs.is_list_like(b''))


class TestIsStr(TestCase):

    def test_true_str(self) -> None:
        self.assertTrue(objs.is_str(''))

    def test_false_bytes(self) -> None:
        self.assertFalse(objs.is_str(b''))


class TestIsIpAddress(TestCase):

    def test_true(self) -> None:
        self.assertTrue(objs.is_ip_address('192.168.1.1'))

    def test_false(self) -> None:
        self.assertFalse(objs.is_ip_address('localhost'))


class TestIsHost(TestCase):

    @patch(f'{MOD}.is_ip_address', return_value=True)
    def test_true_ip_address(self, *_: NonCallableMock) -> None:
        self.assertTrue(objs.is_host('192.168.1.1'))

    @patch(f'{MOD}.is_ip_address', return_value=False)
    def test_false_non_str(self, *_: NonCallableMock) -> None:
        self.assertFalse(objs.is_host(55))

    @patch(f'{MOD}.is_ip_address', return_value=False)
    @patch(f'{MOD}.socket.gethostbyname', return_value='192.168.1.1')
    def test_true_localhost(self, *_: NonCallableMock) -> None:
        self.assertTrue(objs.is_host('localhost'))

    @patch(f'{MOD}.is_ip_address', return_value=False)
    @patch(f'{MOD}.socket.gethostbyname', return_value='')
    @patch(f'{MOD}.socket.getaddrinfo', side_effect=gaierror())
    def test_false_unknown(self, *_: NonCallableMock) -> None:
        self.assertFalse(objs.is_host('unknown'))

    @patch(f'{MOD}.is_ip_address', return_value=False)
    @patch(f'{MOD}.socket.gethostbyname', return_value='')
    @patch(f'{MOD}.socket.getaddrinfo', return_value=True)
    def test_true_known(self, *_: NonCallableMock) -> None:
        self.assertTrue(objs.is_host('known'))


class TestIsTzName(TestCase):

    @patch(f'{MOD}.is_str', return_value=False)
    def test_false_non_str(self, *_: NonCallableMock) -> None:
        self.assertFalse(objs.is_tz_name(None))

    @patch(f'{MOD}.is_str', return_value=True)
    @patch(f'{MOD}.available_timezones', return_value=('America/New_York',))
    def test_false_str(self, *_: NonCallableMock) -> None:
        self.assertFalse(objs.is_tz_name('foo'))

    @patch(f'{MOD}.is_str', return_value=True)
    @patch(f'{MOD}.available_timezones', return_value=('America/New_York',))
    def test_true(self, *_: NonCallableMock) -> None:
        self.assertTrue(objs.is_tz_name('America/New_York'))


class TestIsHash(TestCase):

    def test_true(self) -> None:
        self.assertTrue(objs.is_hash('abcdef0123456789'))

    def test_false(self) -> None:
        self.assertFalse(objs.is_hash('foobar'))


class TestisPort(TestCase):

    def test_true(self) -> None:
        self.assertTrue(objs.is_port(65535))

    def test_false_65536(self) -> None:
        self.assertFalse(objs.is_port('65536'))

    def test_false_0(self) -> None:
        self.assertFalse(objs.is_port(0))

    def test_false_None(self) -> None:
        self.assertFalse(objs.is_port(None))
