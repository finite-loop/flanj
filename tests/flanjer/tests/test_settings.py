import os
from unittest import skip
from django.test import TestCase
from django.conf import settings


class TestEnv(TestCase):

    def test_secret_val(self) -> None:
        val = settings.SECRET_VAL
        exp = '2-b89tx)rax5&g&j+ib8pd+u#q3$35mp_wx_g3yz2ss#g1@#1m%-'
        self.assertEqual(val, exp)

    def test_secret_key(self) -> None:
        val = settings.SECRET_KEY
        l = len(val)
        self.assertEqual(l, 60)

    def test_secret_dot(self) -> None:
        val = settings.SECRET_DOT
        exp = 'NhJdMqz1FH34m3ywQDo[izhWsGUne'
        self.assertEqual(val, exp)

    def test_settings_django_debug(self) -> None:
        val = settings.DJANGO_DEBUG
        self.assertEqual(val, True)

    def test_set_email_values(self) -> None:
        self.assertEqual(
            settings.EMAIL_BACKEND,
            'django.core.mail.backends.locmem.EmailBackend'
        )
        self.assertEqual(
            settings.EMAIL_HOST,
            '127.0.0.1'
        )
        self.assertEqual(
            settings.EMAIL_HOST_PASSWORD,
            'pass'
        )
        self.assertEqual(
            settings.EMAIL_HOST_USER,
            'user'
        )
        self.assertEqual(
            settings.EMAIL_PORT,
            587
        )
        self.assertEqual(
            settings.EMAIL_USE_TLS,
            True
        )
