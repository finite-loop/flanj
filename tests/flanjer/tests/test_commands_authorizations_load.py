from io import StringIO

from django.conf import settings
from django.contrib.auth.models import Group
from django.core.management import call_command
from django.test import TestCase


class TestAuthorizationsLoad(TestCase):

    def test_functionality(self):
        out = StringIO()
        call_command('authorizations_load', stdout=out)
        screen = out.getvalue()
        for row in settings.GROUPS:
            name = row['name']
            try:
                group = Group.objects.get(name=name)
            except Group.DoesNotExist:
                group = None
            self.assertIsInstance(group, Group)
            self.assertIn(name, screen)
