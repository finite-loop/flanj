from appdirs import AppDirs
from django.test import TestCase
from flanj.env.url import (
    ParseUrl,
    Url,
)


package_name = 'flanj-test'
app_dirs = AppDirs(package_name)
package_data_home = app_dirs.user_data_dir
package_config_home = app_dirs.user_config_dir
package_cert_home = app_dirs.user_config_dir
package_cert_private_home = app_dirs.user_config_dir


class TestEnvUrlPostgres(TestCase):

    def setUp(self) -> None:
        self.url = 'postgresql://user:pass@localhost/userdb?schema=foo'
        self.exp = 'postgresql://user:pass@localhost:5432/userdb'
        self.parse_url = ParseUrl(
            package_data_home,
            package_config_home,
            package_cert_home,
            package_cert_private_home,
            package_name=package_name,
        )

    def test_env_url(self) -> None:
        obj = self.parse_url(self.url)
        self.assertTrue(isinstance(obj, Url))

    def test_env_url_url(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.url, self.exp)

    def test_env_url_scheme(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.scheme, 'postgresql')

    def test_env_url_username(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.username, 'user')

    def test_env_url_password(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.password, 'pass')

    def test_env_url_host(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.host, 'localhost')

    def test_env_url_port(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.port, 5432)

    def test_env_url_path(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.path, '/userdb')

    def test_env_url_query(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.query, {'schema': 'foo'})

    def test_env_url_fragment(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.fragment, '')

    def test_env_url_database(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.database, 'userdb')

    def test_env_url_schema(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.schema, 'foo')

    def test_env_url_uses_ssl(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.uses_ssl, False)

    def test_package_data_home(self) -> None:
        self.assertEqual(
            self.parse_url.package_data_home,
            package_data_home
        )

    def test_package_config_home(self) -> None:
        self.assertEqual(
            self.parse_url.package_config_home,
            package_config_home
        )

    def test_package_cert_home(self) -> None:
        self.assertEqual(
            self.parse_url.package_cert_home,
            package_cert_home
        )

    def test_package_cert_private_home(self) -> None:
        self.assertEqual(
            self.parse_url.package_cert_private_home,
            package_cert_private_home
        )

    def test_package_name(self) -> None:
        self.assertEqual(
            self.parse_url.package_name,
            package_name
        )


class TestEnvUrlSqlite(TestCase):

    def setUp(self) -> None:
        self.url = 'sqlite:///tmp/db.sqlite'
        self.parse_url = ParseUrl(
            package_data_home,
            package_config_home,
            package_cert_home,
            package_cert_private_home,
            package_name=package_name,
        )

    def test_scheme(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.scheme, 'sqlite')

    def test_username(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.username, '')

    def test_host(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.host, '')

    def test_port(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.port, 0)

    def test_path(self) -> None:
        obj = self.parse_url(self.url)
        self.assertEqual(obj.path, '/tmp/db.sqlite')


