import os
from datetime import datetime
from django.test import TestCase

import pytz
from flanj.env import manifests

TIME_ZONE = 'America/New_York'
TIME_ZONE_OBJ = pytz.timezone(TIME_ZONE)

DIR = os.path.dirname(os.path.abspath(__file__))
TEST_HOME = os.path.abspath(os.path.join(DIR, '..', '..'))
PROJECT_NAME = 'flanjer'
PROJECT_HOME = os.path.dirname(TEST_HOME)
TOML_FILE = os.path.join(TEST_HOME, 'manifest.toml')


MOD = 'flanj.env.manifests'


class TestBuildManifestDefault(TestCase):

    def setUp(self) -> None:
        self.path = None
        self.project_name = PROJECT_NAME
        self.time_zone: str = 'America/New_York'
        self.msg = (
            '\n\n'
            'obj = manifests.build_manifest(%r, path=%r time_zone=%r)\n\n'
            'value for: {key!r}\n\n'
            'got:  {got!r}\n\n'
            'expected: {exp!r}\n\n'
        ) % (self.project_name, self.path, self.time_zone)

    def test_manifests_project_name(self) -> None:
        obj = manifests.build_manifest(
            self.project_name,
            time_zone=self.time_zone,
        )
        kwargs = {
            'key': 'obj.project_name',
            'got': obj.project_name,
            'exp': self.project_name,
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_build_timestamp(self) -> None:
        obj = manifests.build_manifest(
            self.project_name,
            time_zone=self.time_zone,
        )
        kwargs = {
            'key': 'obj.build_timestamp',
            'got': obj.build_timestamp,
            'exp': 'datetime',
        }
        self.assertIsInstance(
            kwargs['got'],
            datetime,
            msg=self.msg.format(**kwargs)
        )
        kwargs = {
            'key': 'str(obj.build_timestamp.tzinfo)',
            'got': str(obj.build_timestamp.tzinfo),
            'exp': self.time_zone,
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_version(self) -> None:
        obj = manifests.build_manifest(
            self.project_name,
            time_zone=self.time_zone,
        )
        kwargs = {
            'key': 'obj.version',
            'got': obj.version,
            'exp': 'dev',
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_origin(self) -> None:
        obj = manifests.build_manifest(
            self.project_name,
            time_zone=self.time_zone,
        )
        kwargs = {
            'key': 'obj.origin',
            'got': obj.origin,
            'exp': 'default',
        }
        self.assertEqual(
            kwargs['got'],
            kwargs['exp'],
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_migrations(self) -> None:
        obj = manifests.build_manifest(
            self.project_name,
            time_zone=self.time_zone,
        )
        kwargs = {
            'key': 'obj.migrations',
            'got': obj.migrations,
            'exp': 'tuple',
        }
        self.assertIsInstance(
            kwargs['got'],
            tuple,
            msg=self.msg.format(**kwargs)
        )

    def test_manifests_repo(self) -> None:
        obj = manifests.build_manifest(
            self.project_name,
            time_zone=self.time_zone,
        )
        kwargs = {
            'key': 'obj.repo',
            'got': obj.repo,
            'exp': None,
        }
        self.assertEqual(
            kwargs['got'],
            None,
            msg=self.msg.format(**kwargs)
        )
