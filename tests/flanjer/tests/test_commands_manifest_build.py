import os
import shutil
from argparse import Namespace
from io import StringIO
from pathlib import Path
from tempfile import mkdtemp
from types import SimpleNamespace
from typing import Any
from unittest.mock import (
    NonCallableMock,
    patch,
)

from django.core.management import call_command
from django.core.management.base import CommandParser
from django.test import TestCase
from flanj.management.commands import manifest_build
from flanj.command.exceptions import CommandError


MOD = 'flanj.management.commands.manifest_build'
DIRNAME = os.path.dirname(os.path.realpath(__file__))
TEST_HOME = os.path.realpath(os.path.join(DIRNAME, '..', '..'))
PROJECT_HOME = os.path.realpath(os.path.join(TEST_HOME, '..'))
GIT_HOME = os.path.join(PROJECT_HOME, '.git')
PWD = os.getcwd()


class _CleanUp:

    def __init__(self, dirname: str) -> None:
        self.dirname: str = dirname

    def __call__(self, *args: Any, **kwargs: Any) -> None:
        os.chdir(PWD)
        if os.path.exists(self.dirname):
            shutil.rmtree(self.dirname)


class TestCommandsBuildManifestFunctional(TestCase):

    def setUp(self) -> None:
        self.dirname: str = mkdtemp()
        _clean_up = _CleanUp(self.dirname)
        self.addCleanup(_clean_up)

    def test_with_path(self) -> None:
        msg = (
            "\n\n"
            "command: %s/manage.py manifest_build --force {dirname}\n\n"
            "val = os.path.isfile('{path}')\n\n"
            "exp: True\n\n"
            "got: False\n\n"
            "note: The directory '{dirname}' has already been removed.\n"
        ) % TEST_HOME
        Path(os.path.join(self.dirname, 'manifest.toml')).touch()
        with StringIO() as out:
            args = [self.dirname]
            options = {'force': True, 'stdout': out}
            call_command('manifest_build', *args, **options)
            path = os.path.join(self.dirname, 'manifest.toml')
            self.assertTrue(
                os.path.isfile(path),
                msg=msg.format(path=path, dirname=self.dirname)
            )

    def test_without_path(self) -> None:
        msg = (
                  "\n\n"
                  "command: %s/manage.py manifest_build --force\n\n"
                  "val = os.path.isfile('{path}')\n\n"
                  "exp: True\n\n"
                  "got: False\n\n"
                  "note: The directory '{dirname}' has already been removed.\n"
              ) % TEST_HOME
        Path(os.path.join(self.dirname, 'manifest.toml')).mkdir()
        with StringIO() as out:
            os.chdir(self.dirname)
            options = {'force': True, 'stdout': out}
            call_command('manifest_build', **options)
            path = os.path.join(self.dirname, 'manifest.toml')
            self.assertTrue(
                os.path.isfile(path),
                msg=msg.format(path=path, dirname=self.dirname)
            )

    def test_verbosity(self) -> None:
        with StringIO() as out:
            os.chdir(self.dirname)
            options = {'force': True, 'stdout': out, 'verbosity': 2}
            call_command('manifest_build', **options)
            self.assertIn("args=", out.getvalue())


class TestDirectoryAction(TestCase):

    def setUp(self) -> None:
        self.parser: CommandParser = CommandParser()
        self.namespace: Namespace = Namespace()
        # noinspection Mypy,PyUnresolvedReferences
        self.directory_action: manifest_build.DirectoryAction = \
            manifest_build.DirectoryAction('', 'foo')

    @patch(f'{MOD}.os.path.expanduser', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.expandvars', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.abspath', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.exists', return_value=True)
    @patch(f'{MOD}.os.path.isdir', return_value=False)
    def test_not_a_directory(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):  # type: ignore
            self.directory_action(
                self.parser,
                self.namespace,
                '/foo/bar',
            )

    @patch(f'{MOD}.os.path.expanduser', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.expandvars', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.abspath', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.exists', return_value=False)
    def test_does_not_exist(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):  # type: ignore
            self.directory_action(
                self.parser,
                self.namespace,
                '/foo/bar',
            )


MODULE = SimpleNamespace(__file__=TEST_HOME)

class TestFetchGitHome(TestCase):

    MSG = (
        '\n\n'
        'val = %s._fetch_git_home()\n\n'
        'exp: {exp}\n\n'
        'val: {val}\n\n'
    ) % MOD

    @patch(f'{MOD}.settings', SimpleNamespace(PROJECT_HOME='/foo/bar'))
    @patch(f'{MOD}.find_git_home', return_value='/foo/bar/.git')
    def test_project_home(self, *_: NonCallableMock) -> None:
        kwargs = {'exp': '/foo/bar/.git'}
        kwargs['val'] = manifest_build._fetch_git_home()
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.settings', SimpleNamespace(ROOT_URLCONF='foo.bar'))
    @patch(f'{MOD}.import_module', return_value=MODULE)
    @patch(f'{MOD}.os.path.realpath', return_value='/foo/bar')
    @patch(f'{MOD}.find_git_home', return_value='/foo/bar/.git')
    def test_root_urlconf(self, *_: NonCallableMock) -> None:
        kwargs = {'exp': '/foo/bar/.git'}
        kwargs['val'] = manifest_build._fetch_git_home()
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.settings', SimpleNamespace())
    @patch(f'{MOD}.find_git_home', return_value='/foo/bar/.git')
    def test_no_path(self, *_: NonCallableMock) -> None:
        kwargs = {'exp': '/foo/bar/.git'}
        kwargs['val'] = manifest_build._fetch_git_home()
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestCommand(TestCase):

    def setUp(self) -> None:
        self.command: manifest_build.Command = manifest_build.Command()

    @patch(f'{MOD}.settings', SimpleNamespace())
    @patch(f'{MOD}._fetch_git_home', return_value='/for/bar/.git')
    def test_project_name_no_settings_project_name(
            self, *_: NonCallableMock
    ) -> None:
        self.assertEqual(
            self.command.project_name,
            'bar'
        )

    @patch(f'{MOD}.os.path.exists', return_value=True)
    def test_build_path_raises(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            self.command.build_path('/foo/bar', False)
