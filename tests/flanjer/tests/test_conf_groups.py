from unittest.mock import (
    NonCallableMock,
    patch,
)

from django.test import TestCase
# noinspection PyProtectedMember
from flanj.conf.groups import (
    validate_groups_settings,
    _validate_str as validate_str,
)
from django.contrib.auth.models import Permission

MOD = 'flanj.management.commands.authorizations_load'
DoesNotExist = Permission.DoesNotExist


class TestValidateGroupsSettings(TestCase):

    def test_settings_val_type_error(self) -> None:
        kwargs = {
            'settings_key': 'GROUPS',
            'settings_val': 0
        }
        with self.assertRaises(TypeError):
            validate_groups_settings(**kwargs)

    def test_settings_val_type_error_string(self) -> None:
        kwargs = {
            'settings_key': 'GROUPS',
            'settings_val': 'test'
        }
        with self.assertRaises(TypeError):
            validate_groups_settings(**kwargs)

    def test_settings_val_row_type_error(self) -> None:
        kwargs = {
            'settings_key': 'GROUPS',
            'settings_val': [5]
        }
        with self.assertRaises(TypeError):
            validate_groups_settings(**kwargs)

    def test_validate_str_value_error_1(self) -> None:
        args = [
            'GROUPS',
            0,
            {
                'foo': 'bar'
            },
            'name',
        ]
        kwargs = {
            'current_values': []
        }
        with self.assertRaises(ValueError):
            validate_str(*args, **kwargs)

    def test_validate_str_value_error_2(self) -> None:
        args = [
            'GROUPS',
            0,
            {
                'name': ''
            },
            'name',
        ]
        kwargs = {
            'current_values': []
        }
        with self.assertRaises(ValueError):
            validate_str(*args, **kwargs)

    def test_validate_str_value_error_3(self) -> None:
        args = [
            'GROUPS',
            0,
            {
                'name': 'foo'
            },
            'name',
        ]
        kwargs = {
            'current_values': ['foo']
        }
        with self.assertRaises(ValueError):
            validate_str(*args, **kwargs)

    def test_settings_val_row_permissions_value_error(self) -> None:
        kwargs = {
            'settings_key': 'GROUPS',
            'settings_val': [
                {
                    'identifier': 'foo',
                    'name': 'Foo',
                }
            ]
        }
        with self.assertRaises(ValueError):
            validate_groups_settings(**kwargs)

    def test_settings_val_row_permissions_type_error(self) -> None:
        kwargs = {
            'settings_key': 'GROUPS',
            'settings_val': [
                {
                    'identifier': 'foo',
                    'name': 'Foo',
                    'permissions': 'foobar'
                }
            ]
        }
        with self.assertRaises(TypeError):
            validate_groups_settings(**kwargs)

    def test_settings_val_row_permission_row_type_error(self) -> None:
        kwargs = {
            'settings_key': 'GROUPS',
            'settings_val': [
                {
                    'identifier': 'foo',
                    'name': 'Foo',
                    'permissions': [1]
                }
            ]
        }
        with self.assertRaises(TypeError):
            validate_groups_settings(**kwargs)

    def test_settings_val_row_permission_row_value_error_1(self) -> None:
        kwargs = {
            'settings_key': 'GROUPS',
            'settings_val': [
                {
                    'identifier': 'foo',
                    'name': 'Foo',
                    'permissions': ['']
                }
            ]
        }
        with self.assertRaises(ValueError):
            validate_groups_settings(**kwargs)

    @patch(f'{MOD}.Permission.objects.get', side_effect=DoesNotExist)
    def test_settings_val_row_permission_row_value_error_2(
            self,
            *_: NonCallableMock,
    ) -> None:
        kwargs = {
            'settings_key': 'GROUPS',
            'settings_val': [
                {
                    'identifier': 'foo',
                    'name': 'Foo',
                    'permissions': ['blah.blah']
                }
            ]
        }
        with self.assertRaises(ValueError):
            validate_groups_settings(**kwargs)
