from datetime import datetime
from types import SimpleNamespace
from unittest.mock import (
    patch,
    NonCallableMock,
)

import pytz
from dateutil.tz import gettz
from django.test import TestCase
from flanj.utils import timestamps
from tzlocal import get_localzone


MOD = 'flanj.utils.timestamps'
LOCAL_TZINFO = get_localzone()
DATEUTIL_NEW_YORK = gettz('America/New_York')
PYTZ_NEW_YORK = pytz.timezone('America/New_York')
PYTZ_TOKYO = pytz.timezone('Asia/Tokyo')
DATEUTIL_TOKYO = gettz('Asia/Tokyo')
PENDULUM_TOKYO = SimpleNamespace(name='Asia/Tokyo')


class TestAsTzInfo(TestCase):

    MSG = (
        '\n\n'
        'value = %s.as_tzinfo(tzinfo={tz_info!r}, default={default!r})\n\n'
        'value: {val!r}\n\n'
        'expected: {exp!r}\n\n'
    ) % MOD

    def test_tz_info_default_both_none(self) -> None:
        with self.assertRaises(ValueError):
            timestamps.as_tzinfo(None, None)

    def test_default(self) -> None:
        kwargs = {
            'tz_info': None,
            'default': 'LOCAL_TZINFO',
        }
        kwargs['val'] = timestamps.as_tzinfo()
        kwargs['exp'] = LOCAL_TZINFO
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.pytz.timezone', return_value=PYTZ_NEW_YORK)
    def test_default_str(self, pytz_timezone: NonCallableMock) -> None:
        kwargs = {
            'tz_info': None,
            'default': 'America/New_York',
        }
        kwargs['val'] = timestamps.as_tzinfo(
            kwargs['tz_info'],
            default=kwargs['default'],
        )
        kwargs['exp'] = PYTZ_NEW_YORK  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )
        pytz_timezone.assert_called_once()

    @patch(f'{MOD}.pytz.timezone', side_effect=pytz.UnknownTimeZoneError)
    def test_default_invalid_str(
            self,
            pytz_timezone: NonCallableMock,
    ) -> None:
        with self.assertRaises(ValueError):
            timestamps.as_tzinfo(None, 'foobar')
        pytz_timezone.assert_called_once_with('foobar')

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.get_tz_database_name', return_value='America/New_York')
    @patch(f'{MOD}.pytz.timezone', return_value=PYTZ_NEW_YORK)
    def test_default_dateutil(
            self,
            pytz_timezone: NonCallableMock,
            get_tz_database_name: NonCallableMock,
            is_pytz_tzinfo: NonCallableMock,
    ) -> None:
        kwargs = {
            'tz_info': None,
            'default': DATEUTIL_NEW_YORK,
        }
        kwargs['val'] = timestamps.as_tzinfo(
            kwargs['tz_info'],
            default=kwargs['default'],
        )
        kwargs['exp'] = PYTZ_NEW_YORK
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

        is_pytz_tzinfo.assert_called_once_with(DATEUTIL_NEW_YORK)
        get_tz_database_name.assert_called_once_with(DATEUTIL_NEW_YORK)
        pytz_timezone.assert_called_once()

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.get_tz_database_name', side_effect=TypeError)
    def test_default_bad_tzinfo(
            self,
            get_tz_database_name: NonCallableMock,
            is_pytz_tzinfo: NonCallableMock,
    ) -> None:
        with self.assertRaises(TypeError):
            timestamps.as_tzinfo(None, DATEUTIL_NEW_YORK)

        is_pytz_tzinfo.assert_called_once_with(DATEUTIL_NEW_YORK)
        get_tz_database_name.assert_called_once_with(DATEUTIL_NEW_YORK)

    def test_default_bad_type(self) -> None:
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            timestamps.as_tzinfo(None, 55)  # type: ignore

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)  # type: ignore
    @patch(f'{MOD}.get_tz_database_name', return_value='America/New_York')
    @patch(f'{MOD}.pytz.timezone', return_value=PYTZ_NEW_YORK)
    def test_default_dateutil(
            self,
            pytz_timezone: NonCallableMock,
            get_tz_database_name: NonCallableMock,
            is_pytz_tzinfo: NonCallableMock,
    ) -> None:
        kwargs = {
            'tz_info': None,
            'default': DATEUTIL_NEW_YORK,
        }
        kwargs['val'] = timestamps.as_tzinfo(
            kwargs['tz_info'],
            default=kwargs['default'],
        )
        kwargs['exp'] = PYTZ_NEW_YORK
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

        is_pytz_tzinfo.assert_called_once_with(DATEUTIL_NEW_YORK)
        get_tz_database_name.assert_called_once_with(DATEUTIL_NEW_YORK)
        pytz_timezone.assert_called_once()

    @patch(f'{MOD}.pytz.timezone', return_value=PYTZ_TOKYO)
    def test_tz_info_str(self, pytz_timezone: NonCallableMock) -> None:
        kwargs = {
            'tz_info': 'Asia/Tokyo',
            'default': PYTZ_NEW_YORK,
        }
        kwargs['val'] = timestamps.as_tzinfo(
            kwargs['tz_info'],
            default=kwargs['default'],
        )
        kwargs['exp'] = PYTZ_TOKYO  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )
        pytz_timezone.assert_called_once()

    @patch(f'{MOD}.pytz.timezone', side_effect=pytz.UnknownTimeZoneError)
    def test_tz_info_invalid_str_error(
            self,
            pytz_timezone: NonCallableMock,
    ) -> None:
        with self.assertRaises(ValueError):
            timestamps.as_tzinfo('foobar', None)
        pytz_timezone.assert_called_once_with('foobar')

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=True)
    @patch(f'{MOD}.pytz.timezone', side_effect=pytz.UnknownTimeZoneError)
    def test_tz_info_invalid_str_default_returned(
            self,
            pytz_timezone: NonCallableMock,
            is_pytz_tzinfo: NonCallableMock,
    ) -> None:
        kwargs = {
            'tz_info': 'foobar',
            'default': PYTZ_TOKYO,
        }
        kwargs['val'] = timestamps.as_tzinfo(
            kwargs['tz_info'],
            default=kwargs['default'],
        )
        kwargs['exp'] = PYTZ_TOKYO  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )
        is_pytz_tzinfo.assert_called_once_with(PYTZ_TOKYO)
        pytz_timezone.assert_called_once_with('foobar')

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=True)
    def test_tz_info_pytz(
            self,
            is_pytz_tzinfo: NonCallableMock,
    ) -> None:
        kwargs = {
            'tz_info': PYTZ_TOKYO,
            'default': None,
        }
        kwargs['val'] = timestamps.as_tzinfo(kwargs['tz_info'], None)
        kwargs['exp'] = PYTZ_TOKYO  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )
        is_pytz_tzinfo.assert_called_once_with(PYTZ_TOKYO)

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.get_tz_database_name', return_value='Asia/Tokyo')
    @patch(f'{MOD}.pytz.timezone', return_value=PYTZ_TOKYO)
    def test_tz_info_dateutil(
            self,
            pytz_timezone: NonCallableMock,
            get_tz_database_name: NonCallableMock,
            is_pytz_tzinfo: NonCallableMock,
    ) -> None:
        kwargs = {
            'tz_info': DATEUTIL_TOKYO,
            'default': None,
        }
        kwargs['val'] = timestamps.as_tzinfo(
            kwargs['tz_info'],
            default=kwargs['default'],
        )
        kwargs['exp'] = PYTZ_TOKYO
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

        is_pytz_tzinfo.assert_called_once_with(DATEUTIL_TOKYO)
        get_tz_database_name.assert_called_once_with(DATEUTIL_TOKYO)
        pytz_timezone.assert_called_once()

    @patch(f'{MOD}.is_pytz_tzinfo', side_effect=[True, False])
    @patch(f'{MOD}.get_tz_database_name', side_effect=TypeError)
    def test_tz_info_bad_dateutil_return_default(
            self,
            get_tz_database_name: NonCallableMock,
            is_pytz_tzinfo: NonCallableMock,
    ) -> None:
        kwargs = {
            'tz_info': DATEUTIL_TOKYO,
            'default': PYTZ_NEW_YORK,
        }
        kwargs['val'] = timestamps.as_tzinfo(
            kwargs['tz_info'],
            default=kwargs['default'],
        )
        kwargs['exp'] = PYTZ_NEW_YORK
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

        is_pytz_tzinfo.assert_called()
        get_tz_database_name.assert_called_once_with(DATEUTIL_TOKYO)

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.get_tz_database_name', side_effect=TypeError)
    def test_tz_info_bad_dateutil_default_none(
            self,
            get_tz_database_name: NonCallableMock,
            is_pytz_tzinfo: NonCallableMock,
    ) -> None:
        with self.assertRaises(TypeError):
            timestamps.as_tzinfo(DATEUTIL_TOKYO, None)
        is_pytz_tzinfo.assert_called_once_with(DATEUTIL_TOKYO)
        get_tz_database_name.assert_called_once_with(DATEUTIL_TOKYO)

    def test_bad_tzinfo(self) -> None:
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            timestamps.as_tzinfo(57, None)  # type: ignore


class TestIsDateutilTzinfo(TestCase):

    MSG = (
        '\n\n'
        'value = %s.is_dateutil_tzinfo({tz_info!r})\n\n'
        'value: {val!r}\n\n'
        'expected: {exp!r}\n\n'
    ) % MOD

    def test_true(self) -> None:
        kwargs = {
            'tz_info': DATEUTIL_TOKYO,
        }
        kwargs['val'] = timestamps.is_dateutil_tzinfo(
            kwargs['tz_info']
        )
        kwargs['exp'] = True  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    def test_false(self) -> None:
        kwargs = {
            'tz_info': PYTZ_TOKYO,
        }
        kwargs['val'] = timestamps.is_dateutil_tzinfo(
            kwargs['tz_info']
        )
        kwargs['exp'] = False  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )


class TestIsPytzTzinfo(TestCase):

    MSG = (
        '\n\n'
        'value = %s.is_pytz_tzinfo({tz_info!r})\n\n'
        'value: {val!r}\n\n'
        'expected: {exp!r}\n\n'
    ) % MOD

    def test_true(self) -> None:
        kwargs = {
            'tz_info': PYTZ_TOKYO,
        }
        kwargs['val'] = timestamps.is_pytz_tzinfo(
            kwargs['tz_info']
        )
        kwargs['exp'] = True  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    def test_false(self) -> None:
        kwargs = {
            'tz_info': DATEUTIL_TOKYO,
        }
        kwargs['val'] = timestamps.is_pytz_tzinfo(
            kwargs['tz_info']
        )
        kwargs['exp'] = False  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )


class TestIsPendulumTzinfo(TestCase):

    MSG = (
        '\n\n'
        'value = %s.is_pendulum_tzinfo({tz_info!r})\n\n'
        'value: {val!r}\n\n'
        'expected: {exp!r}\n\n'
    ) % MOD

    def test_true(self) -> None:
        kwargs = {
            'tz_info': PENDULUM_TOKYO,
        }
        # noinspection PyTypeChecker
        kwargs['val'] = timestamps.is_pendulum_tzinfo(
            kwargs['tz_info']
        )
        kwargs['exp'] = True  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    def test_false(self) -> None:
        kwargs = {
            'tz_info': DATEUTIL_TOKYO,
        }
        kwargs['val'] = timestamps.is_pendulum_tzinfo(
            kwargs['tz_info']
        )
        kwargs['exp'] = False  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )


class TestIsValidTzDatabaseName(TestCase):

    MSG = (
        '\n\n'
        'value = %s.is_valid_tz_database_name({name!r})\n\n'
        'value: {val!r}\n\n'
        'expected: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.pytz.timezone', return_value=PYTZ_NEW_YORK)
    def test_true(self, pytz_timezone: NonCallableMock) -> None:
        kwargs = {
            'name': 'America/New_York',
        }
        kwargs['val'] = timestamps.is_valid_tz_database_name(
            kwargs['name']
        )
        kwargs['exp'] = True  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )
        pytz_timezone.assert_called_once_with(kwargs['name'])

    @patch(f'{MOD}.pytz.timezone', side_effect=pytz.UnknownTimeZoneError)
    def test_false(self, pytz_timezone: NonCallableMock) -> None:
        kwargs = {
            'name': 'foobar',
        }
        kwargs['val'] = timestamps.is_valid_tz_database_name(
            kwargs['name']
        )
        kwargs['exp'] = False  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )
        pytz_timezone.assert_called_once_with(kwargs['name'])


class TestGetTzDatabaseName(TestCase):

    MSG = (
        '\n\n'
        'value = %s.get_tz_database_name({tz_info!r})\n\n'
        'value: {val!r}\n\n'
        'expected: {exp!r}\n\n'
    ) % MOD

    def test_none(self) -> None:
        kwargs = {
            'tz_info': None  # type: ignore
        }
        kwargs['val'] = timestamps.get_tz_database_name(
            kwargs['tz_info']
        )
        kwargs['exp'] = ''  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.is_dateutil_tzinfo', return_value=True)
    def test_dateutil_tzinfo(self, *_: NonCallableMock) -> None:
        kwargs = {
            'tz_info': DATEUTIL_TOKYO  # type: ignore
        }
        kwargs['val'] = timestamps.get_tz_database_name(
            kwargs['tz_info']
        )
        kwargs['exp'] = 'Asia/Tokyo'  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.is_dateutil_tzinfo', return_value=False)
    @patch(f'{MOD}.is_pytz_tzinfo', return_value=True)
    def test_pytz_tzinfo(self, *_: NonCallableMock) -> None:
        kwargs = {
            'tz_info': PYTZ_TOKYO  # type: ignore
        }
        kwargs['val'] = timestamps.get_tz_database_name(
            kwargs['tz_info']
        )
        kwargs['exp'] = 'Asia/Tokyo'  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.is_dateutil_tzinfo', return_value=False)
    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.is_pendulum_tzinfo', return_value=True)
    def test_pendulum_tzinfo(self, *_: NonCallableMock) -> None:
        kwargs = {
            'tz_info': PENDULUM_TOKYO  # type: ignore
        }
        # noinspection PyTypeChecker
        kwargs['val'] = timestamps.get_tz_database_name(
            kwargs['tz_info']
        )
        kwargs['exp'] = 'Asia/Tokyo'  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.is_dateutil_tzinfo', return_value=False)
    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.is_pendulum_tzinfo', return_value=False)
    def test_zoneinfo_tzinfo(self, *_: NonCallableMock) -> None:
        # Using a pytz object because it's similar to Python's ZoneInfo object
        # This was written using Python 3.8 which doesn't have the zoneinfo
        # module. The zoneinfo module became available in Python 3.9
        kwargs = {
            'tz_info': PYTZ_TOKYO  # type: ignore
        }
        kwargs['val'] = timestamps.get_tz_database_name(
            kwargs['tz_info']
        )
        kwargs['exp'] = 'Asia/Tokyo'  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    def test_tz_info_not_tzinfo(self) -> None:
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            timestamps.get_tz_database_name(55)  # type: ignore


TIMESTAMP_BASIC = datetime(1970, 1, 1, 0, 0, 0)
TIMESTAMP_UTC = TIMESTAMP_BASIC.replace(tzinfo=pytz.UTC)
TIMESTAMP_LOCAL = TIMESTAMP_UTC.astimezone(LOCAL_TZINFO)
TIMESTAMP_DATEUTIL = TIMESTAMP_UTC.astimezone(DATEUTIL_TOKYO)
TIMESTAMP_PYTZ = TIMESTAMP_UTC.astimezone(PYTZ_TOKYO)


class TestNormalizeDatetimeTzinfo(TestCase):

    MSG = (
        '\n\n'
        'value = %s.normalize_datetime_tzinfo({timestamp!r}, '
        'default_tz_info={default_tz_info!r})\n\n'
        'value: {val!r}\n\n'
        'expected: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=True)
    def test_timestamp_tzinfo_pytz(self, *_: NonCallableMock) -> None:
        kwargs = {
            'timestamp': TIMESTAMP_UTC,
            'default_tz_info': None
        }
        kwargs['val'] = timestamps.normalize_datetime_tzinfo(
            kwargs['timestamp'],
            default_tz_info=kwargs['default_tz_info'],
        )
        kwargs['exp'] = TIMESTAMP_UTC
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.get_tz_database_name', return_value='Asia/Tokyo')
    @patch(f'{MOD}.pytz.timezone', return_value=PYTZ_TOKYO)
    def test_timestamp_tzinfo_dateutil(self, *_: NonCallableMock) -> None:
        kwargs = {
            'timestamp': TIMESTAMP_DATEUTIL,
            'default_tz_info': None
        }
        kwargs['val'] = timestamps.normalize_datetime_tzinfo(
            kwargs['timestamp'],
            default_tz_info=kwargs['default_tz_info'],
        )
        kwargs['exp'] = TIMESTAMP_PYTZ
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.get_tz_database_name', return_value='')
    @patch(f'{MOD}.as_tzinfo', return_value=PYTZ_TOKYO)
    def test_timestamp_default(self, *_: NonCallableMock) -> None:
        kwargs = {
            'timestamp': TIMESTAMP_BASIC,
            'default_tz_info': PYTZ_TOKYO
        }
        kwargs['val'] = timestamps.normalize_datetime_tzinfo(
            kwargs['timestamp'],
            default_tz_info=kwargs['default_tz_info'],
        )
        kwargs['exp'] = TIMESTAMP_BASIC.replace(tzinfo=PYTZ_TOKYO)
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    @patch(f'{MOD}.get_tz_database_name', return_value='')
    def test_timestamp_unable_to_convert(self, *_: NonCallableMock) -> None:
        with self.assertRaises(AttributeError):
            timestamps.normalize_datetime_tzinfo(TIMESTAMP_BASIC, None)


class TestAsIso8601(TestCase):

    MSG = (
          '\n\n'
          'value = %s.as_iso8601(\n'
          '    {timestamp!r},\n'
          '    force_tz_info={force_tz_info!r}\n'
          ')\n\n'
          'value: {val!r}\n\n'
          'expected: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=False)
    def test_timestamp_tzinfo_not_pytz(self, *_: NonCallableMock) -> None:
        with self.assertRaises(AttributeError):
            timestamps.as_iso8601(TIMESTAMP_BASIC, None)

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=True)
    @patch(f'{MOD}.as_tzinfo', return_value=pytz.UTC)
    def test_force_tz_info_utc(self, *_: NonCallableMock) -> None:
        kwargs = {
            'timestamp': TIMESTAMP_LOCAL,
            'force_tz_info': pytz.UTC
        }
        kwargs['val'] = timestamps.as_iso8601(
            kwargs['timestamp'],
            force_tz_info=kwargs['force_tz_info'],
        )
        kwargs['exp'] = '1970-01-01T00:00:00Z'
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.is_pytz_tzinfo', return_value=True)
    def test_timestamp(self, *_: NonCallableMock) -> None:
        kwargs = {
            'timestamp': TIMESTAMP_PYTZ,
            'force_tz_info': None
        }
        kwargs['val'] = timestamps.as_iso8601(
            kwargs['timestamp'],
            force_tz_info=kwargs['force_tz_info'],
        )
        kwargs['exp'] = '1970-01-01T09:00:00+09:00'  # type: ignore
        self.assertEqual(
            kwargs['val'],
            kwargs['exp'],
            msg=self.MSG.format(**kwargs)
        )
