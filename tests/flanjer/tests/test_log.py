import logging
from datetime import datetime
from io import StringIO
from typing import (
    Tuple,
)

import pytz
from django.test import TestCase


stream = StringIO()
date_format = '%Y-%m-%d %H:%M:%S.%f %z'
TIME_ZONE = 'America/New_York'
TIME_ZONE_OBJ = pytz.timezone(TIME_ZONE)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'test.verbose': {
            '()': 'flanj.log.Formatter',
            'datefmt': date_format,
            'format': (
                '{asctime} | '
                '{created} | ' 
                '{levelname: <7} | '
                '{name} | '
                '{message}'
            ),
            'style': '{',
            'timezone': TIME_ZONE
        },
        'test.server.verbose': {
            '()': 'flanj.log.ServerFormatter',
            'datefmt': date_format,
            'format': (
                '{server_time} | '
                '{created} | '
                '{levelname: <7} | '
                '{name} | '
                '{message}'
            ),
            'style': '{',
            'timezone': TIME_ZONE,
        },
    },
    'handlers': {
        'flanj.test': {
            'class': 'logging.StreamHandler',
            'formatter': 'test.verbose',
            'level': 'INFO',
            'stream': stream,
        },
        'flanj.test.server': {
            'class': 'logging.StreamHandler',
            'formatter': 'test.server.verbose',
            'level': 'INFO',
            'stream': stream,
        },
    },
    'loggers': {
        'flanj.test': {
            'handlers': ['flanj.test'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'flanj.test.server': {
            'handlers': ['flanj.test.server'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}

# noinspection Mypy,PyUnresolvedReferences
logging.config.dictConfig(LOGGING)
logger_test = logging.getLogger('flanj.test')
logger_server_test = logging.getLogger('flanj.test.server')


def log_test_error() -> None:
    logger_test.error('test message')


def log_test_server_error() -> None:
    logger_server_test.error('test server message')


def extract_times() -> Tuple[str, datetime, str]:
    """Extracts the local timestamp and the utc timestamp from the
    log message and returns them both as datetime objects.
    """
    val: str = stream.getvalue().strip()
    row: str = val.splitlines()[-1]
    parts = list(map(lambda x: x.strip(), row.split('|')))
    local_str = parts[0]
    utc_str = parts[1]
    try:
        local_obj = datetime.strptime(local_str, date_format)
        local = local_obj.isoformat()
    except (ValueError, OverflowError) as e:
        raise ValueError(
            "flanj.log.Formatter did not write a parsable timestamp:\n\n"
            "local_str=%r\n\n"
            "utc_str=%r\n\n"
            "error_message=%r\n\n"
            % (
                local_str,
                utc_str,
                str(e),
            )
        )

    try:
        utc_float = float(utc_str)
    except (ValueError, TypeError) as e:
        raise ValueError(
            "flanj.log.Formatter did not write a proper timestamp float:\n\n"
            "local_str=%r\n\n"
            "utc_str=%r\n\n"
            "error_message=%r\n\n"
            % (
                local_str,
                utc_str,
                str(e),
            )
        )

    # noinspection PyTypeChecker
    utc = datetime.fromtimestamp(utc_float, pytz.UTC)
    return local, utc, row


class TestFlanjLogging(TestCase):

    def test_log_message(self) -> None:
        # noinspection PyTypeChecker
        with self.assertLogs(logger='flanj.test', level='ERROR') as context:
            log_test_error()

            self.assertIn(
                'test message',
                context.output[-1]
            )

    def test_log_asctime(self) -> None:
        log_test_error()
        try:
            local, utc, row = extract_times()
        except ValueError as e:
            self.fail(str(e))
        local_dt = utc.astimezone(TIME_ZONE_OBJ)
        exp = local_dt.isoformat()
        msg = (
            f'\n\n'
            f'row: {row}\n\n'
            f'exp: {exp}\n\n'
            f'val: {local}\n\n'
        )
        self.assertEqual(local, exp, msg=msg)

    def test_log_server_time(self) -> None:
        log_test_server_error()
        try:
            local, utc, row = extract_times()
        except ValueError as e:
            self.fail(str(e))
        local_dt = utc.astimezone(TIME_ZONE_OBJ)
        exp = local_dt.isoformat()
        msg = (
            f'\n\n'
            f'row: {row}\n\n'
            f'exp: {exp}\n\n'
            f'val: {local}\n\n'
        )
        self.assertEqual(local, exp, msg=msg)
