import json
from django.test import TestCase
from flanj.env import quote_value



DATA_BASIC = (
    ('[hello]', '["hello"]'),
    ('[hel\\[lo]', '["hel[lo"]'),
    ('(hello)', '["hello"]'),
    ('(hel\\[lo\\])', '["hel[lo]"]'),
    ('(hel\\(lo\\))', '["hel(lo)"]'),
    ('(hel\\{lo\\})', '["hel{lo}"]'),
    ('(hel\\{lo\\:\\}\\,)', '["hel{lo:},"]'),
    ('[he\\"llo]', '["he\\\"llo"]'),
    ('{hello}', '{"hello"}'),
    ('[foo, bar]', '["foo", "bar"]'),
    ('[1, one]', '[1, "one"]'),
    ('[[1, one], [2, two]]', '[[1, "one"], [2, "two"]]'),
    ('1, 2, 3', '1, 2, 3'),
)

DATA_VALUES = (
    ('[hello]', ['hello']),
    ('(hel\\(lo\\))', ["hel(lo)"]),
    ('[(1, one), (2, two)]', [[1, 'one'], [2, 'two']]),
    ('{a: 5}', {'a': 5}),
    ('{b: 5.4}', {'b': 5.4}),
    ('{c: false}', {'c': False}),
    ('{d: True}', {'d': True}),
    ('{e: none, f: null}', {'e': None, 'f': None}),
)


class TestEnvQuoteValue(TestCase):

    def test_env_quote_value(self) -> None:
        for arg, exp in DATA_BASIC:
            val = quote_value(arg)
            self.assertEqual(
                val,
                exp,
                msg=(
                    f'\n\n'
                    f' Input of: {arg}\n'
                    f'      Got: {val}\n'
                    f'Expecting: {exp}\n'
                )
            )

    def test_env_quote_value_python_value(self) -> None:
        for arg, exp in DATA_VALUES:
            hold = quote_value(arg)
            val = json.loads(hold)
            self.assertEqual(
                val,
                exp,
                msg=(
                    f'\n\n'
                    f'                Input of: {arg}\n'
                    f'quote_value return value: {hold}\n'
                    f' json.loads return value: {val!r}\n'
                    f'               Expecting: {exp!r}\n'
                )
            )





