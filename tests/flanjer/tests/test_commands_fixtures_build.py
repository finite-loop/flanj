# type: ignore

# import shutil
from io import StringIO
from json import JSONDecodeError
from types import SimpleNamespace
from unittest.mock import (
    MagicMock,
    NonCallableMock,
    mock_open,
    patch,
)

# from django.conf import settings
# from django.core.management import call_command
from django.core.management.base import CommandParser
from django.test import TestCase
from flanj.command.exceptions import CommandError
from flanj.management.commands import fixtures_build

from ..structure import models

MOD = 'flanj.management.commands.fixtures_build'


class TestGetFixtureHome(TestCase):

    MSG = (
        '\n\n'
        'val = %s.get_fixture_home()\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    FIXTURE_HOME = SimpleNamespace(FIXTURE_HOME='/foo/bar')

    @patch(f'{MOD}.settings', FIXTURE_HOME)
    def test_fixture_home(self, *_: NonCallableMock) -> None:
        kwargs = {
            'exp': '/foo/bar',
            'val': fixtures_build.get_fixture_home()
        }
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    SERVER_HOME = SimpleNamespace(SERVER_HOME='/foo/bar')

    @patch(f'{MOD}.settings', SERVER_HOME)
    def test_server_home(self, *_: NonCallableMock) -> None:
        kwargs = {
            'exp': '/foo/bar/fixtures',
            'val': fixtures_build.get_fixture_home()
        }
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    BASE_DIR = SimpleNamespace(BASE_DIR='/ichi/ni')

    @patch(f'{MOD}.settings', BASE_DIR)
    def test_base_dir(self, *_: NonCallableMock) -> None:
        kwargs = {
            'exp': '/ichi/ni/fixtures',
            'val': fixtures_build.get_fixture_home()
        }
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    BASE_HOME = SimpleNamespace(BASE_HOME='/san/shi')

    @patch(f'{MOD}.settings', BASE_HOME)
    def test_base_home(self, *_: NonCallableMock) -> None:
        kwargs = {
            'exp': '/san/shi/fixtures',
            'val': fixtures_build.get_fixture_home()
        }
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    PROJECT_HOME = SimpleNamespace(PROJECT_HOME='/go/roku')

    @patch(f'{MOD}.settings', PROJECT_HOME)
    def test_project_home(self, *_: NonCallableMock) -> None:
        kwargs = {
            'exp': '/go/roku/fixtures',
            'val': fixtures_build.get_fixture_home()
        }
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    NOTHING = SimpleNamespace()

    @patch(f'{MOD}.settings', NOTHING)
    def test_raises(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            fixtures_build.get_fixture_home()


class TestGetAppsMeta(TestCase):

    MSG = (
        '\n\n'
        'called: %s.get_apps_meta()\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    User = SimpleNamespace(__module__='django.contrib.auth.models.User')
    Foo = SimpleNamespace(__module__='foo.bar.models.Foo')
    Bar = SimpleNamespace(__module__='foo.bar.models.Bar')
    APPS = SimpleNamespace(
        all_models={
            'bar': {
                'foo': Foo,
                'bar': Bar,
            },
            'auth': {
                'user': User,
            }
        }
    )

    @patch(f'{MOD}.django_apps', APPS)
    def test_bar(self, *_: NonCallableMock) -> None:
        res = fixtures_build.get_apps_meta()
        kwargs = {
            'val': res[0].name,
            'exp': 'bar',
        }
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.django_apps', APPS)
    def test_foo(self, *_: NonCallableMock) -> None:
        res = fixtures_build.get_apps_meta()
        kwargs = {
            'val': res[0].models,
            'exp': ('bar', 'foo',),
        }
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestGetAppModelTitles(TestCase):
    MSG = (
        '\n\n'
        'val = %s.get_app_model_titles(apps=None)\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    APP = fixtures_build.AppMeta(name='foo.bar', models=('Bar', 'Foo'))

    @patch(f'{MOD}.get_apps_meta', return_value=(APP,))
    def test_result(self, *_: NonCallableMock) -> None:
        kwargs = {
            'exp': ('foo_bar__bar', 'foo_bar__foo'),
        }
        kwargs['val'] = fixtures_build.get_app_model_titles()
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestIsFixtureFilename(TestCase):
    MSG = (
        '\n\n'
        'val = %s.is_fixture_filename({arg!r})\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    def test_no_extension(self) -> None:
        kwargs = {
            'arg': 'foo',
            'exp': False
        }
        kwargs['val'] = fixtures_build.is_fixture_filename(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    def test_wrong_extension(self) -> None:
        kwargs = {
            'arg': 'foo.xml',
            'exp': False
        }
        kwargs['val'] = fixtures_build.is_fixture_filename(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    def test_no_sep(self) -> None:
        kwargs = {
            'arg': 'foo.json',
            'exp': False
        }
        kwargs['val'] = fixtures_build.is_fixture_filename(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    def test_no_number(self) -> None:
        kwargs = {
            'arg': '0B__foo.json',
            'exp': False
        }
        kwargs['val'] = fixtures_build.is_fixture_filename(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    def test_no_name(self) -> None:
        kwargs = {
            'arg': '004__.json',
            'exp': False
        }
        kwargs['val'] = fixtures_build.is_fixture_filename(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.get_app_model_titles', return_value=('foo_bar__table',))
    def test_name_in_app_model_titles(
            self,
            *_: NonCallableMock,
    ) -> None:
        kwargs = {
            'arg': '004__foo_bar__Table.json',
            'exp': True
        }
        kwargs['val'] = fixtures_build.is_fixture_filename(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.get_app_model_titles', return_value=())
    def test_name_not_in_app_model_titles(
            self,
            *_: NonCallableMock,
    ) -> None:
        kwargs = {
            'arg': '004__foo_bar_Test.json',
            'exp': False
        }
        kwargs['val'] = fixtures_build.is_fixture_filename(kwargs['arg'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )




class TestFetchAppModel(TestCase):

    MSG = (
        '\n\n'
        'app, model = %s.fetch_app_model({app_name!r}, {model_name!r}, '
        '{index!r})\n\n'
        'val = {value}\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    APP = fixtures_build.AppMeta(name='foo.bar', models=('Bar', 'Foo'))

    @patch(f'{MOD}.get_apps_meta', return_value=(APP,))
    def test_app_name(self, *_: NonCallableMock) -> None:
        kwargs = {
            'app_name': 'foo.bar',
            'model_name': 'Foo',
            'index': 0,
            'value': 'app.name',
            'exp': 'foo.bar',
        }
        app, _ = fixtures_build.fetch_app_model(
            kwargs['app_name'],
            kwargs['model_name'],
            kwargs['index'],
        )
        kwargs['val'] = app.name
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.get_apps_meta', return_value=(APP,))
    def test_app_models(self, *_: NonCallableMock) -> None:
        kwargs = {
            'app_name': 'foo.bar',
            'model_name': 'Foo',
            'index': 0,
            'value': 'app.models',
            'exp': ('Bar', 'Foo'),
        }
        app, _ = fixtures_build.fetch_app_model(
            kwargs['app_name'],
            kwargs['model_name'],
            kwargs['index'],
        )
        kwargs['val'] = app.models
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.get_apps_meta', return_value=(APP,))
    def test_model(self, *_: NonCallableMock) -> None:
        kwargs = {
            'app_name': 'foo.bar',
            'model_name': 'Foo',
            'index': 0,
            'value': 'model',
            'exp': 'Foo',
        }
        _, model = fixtures_build.fetch_app_model(
            kwargs['app_name'],
            kwargs['model_name'],
            kwargs['index'],
        )
        kwargs['val'] = model
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.get_apps_meta', return_value=(APP,))
    def test_not_found(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            fixtures_build.fetch_app_model('foo.bar', 'FooBar', 5)


class TestGetSettingsFixtures(TestCase):

    MSG = (
        '\n\n'
        'val = %s.get_settings_fixtures()\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    @patch(f'{MOD}.settings', SimpleNamespace())
    def test_no_fixtures(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            fixtures_build.get_settings_fixtures()

    @patch(f'{MOD}.settings', SimpleNamespace(FIXTURES='foobar'))
    def test_not_list_like(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            fixtures_build.get_settings_fixtures()

    @patch(f'{MOD}.settings', SimpleNamespace(FIXTURES=('foobar',)))
    def test_row_not_list_like(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            fixtures_build.get_settings_fixtures()

    @patch(f'{MOD}.settings', SimpleNamespace(FIXTURES=(('foobar',),)))
    def test_bad_row_length(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            fixtures_build.get_settings_fixtures()

    @patch(f'{MOD}.settings', SimpleNamespace(FIXTURES=(('foo.bar', 'Foo'),)))
    def test_results(self, *_: NonCallableMock) -> None:
        kwargs = {
            'val': fixtures_build.get_settings_fixtures(),
            'exp': (('foo.bar', 'Foo'),),
        }
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    FIXTURES = (
        ('foo.bar', 'Foo'),
        ('foo.bar', 'Bar'),
        ('foo.bar', 'Foo'),
    )

    @patch(f'{MOD}.settings', SimpleNamespace(FIXTURES=FIXTURES))
    def test_duplicate(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            fixtures_build.get_settings_fixtures()


class TestEachFixtureMeta(TestCase):

    MSG = (
        '\n\n'
        'res = list(%s.each_fixture_meta(apps=None, fixture_home=None))\n\n'
        'val = res[{pos}].{var}\n\n'
        'val: {val!r}\n\n'
        'exp: {exp!r}\n\n'
    ) % MOD

    APP = fixtures_build.AppMeta(name='foo.bar', models=('Bar', 'Foo'))
    FIXTURES = (
        ('foo.bar', 'Foo'),
        ('foo.bar', 'Bar'),
    )

    @patch(f'{MOD}.get_apps_meta', return_value=(APP,))
    @patch(f'{MOD}.get_fixture_home', return_value='/foo/bar')
    @patch(f'{MOD}.get_settings_fixtures', return_value=FIXTURES)
    def test_result(self, *_: NonCallableMock) -> None:
        kwargs = {
            'pos': 0,
            'var': 'path',
            'exp': '/foo/bar/0001__foo_bar__Foo.json'
        }
        res = list(fixtures_build.each_fixture_meta())
        kwargs['val'] = getattr(res[kwargs['pos']], kwargs['var'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )

    @patch(f'{MOD}.get_apps_meta', return_value=(APP,))
    @patch(f'{MOD}.get_fixture_home', return_value='/foo/bar')
    @patch(f'{MOD}.get_settings_fixtures', return_value=FIXTURES)
    def test_result(self, *_: NonCallableMock) -> None:
        kwargs = {
            'pos': 1,
            'var': 'path',
            'exp': '/foo/bar/0001__foo_bar__Bar.json'
        }
        res = list(fixtures_build.each_fixture_meta())
        kwargs['val'] = getattr(res[kwargs['pos']], kwargs['var'])
        self.assertEqual(
            kwargs['exp'],
            kwargs['val'],
            msg=self.MSG.format(**kwargs)
        )


class TestCommand(TestCase):

    def setUp(self) -> None:
        building = models.Building(
            type=1,
            name='main'
        )
        building.save()
        basement = models.Floor(
            building=building,
            number=0,
            name='Basement'
        )
        basement.save()
        laundry_room = models.Room(
            floor=basement,
            name='Laundry Room',
        )
        laundry_room.save()
        first_floor = models.Floor(
            building=building,
            number=1,
            name='First Floor'
        )
        first_floor.save()
        living_room = models.Room(
            floor=first_floor,
            name='Living Room',
        )
        living_room.save()
        kitchen = models.Room(
            floor=first_floor,
            name='Kitchen',
        )
        kitchen.save()
        dining_room = models.Room(
            floor=first_floor,
            name='Dining Room',
        )
        dining_room.save()
        half_bathroom = models.Room(
            floor=first_floor,
            name='Half Bathroom',
        )
        half_bathroom.save()
        second_floor = models.Floor(
            building=building,
            number=2,
            name='Second Floor'
        )
        second_floor.save()
        master_bedroom = models.Room.objects.create(
            floor=second_floor,
            name='Master Bedroom',
        )
        master_bedroom.save()
        master_bathroom = models.Room(
            floor=second_floor,
            name='Master Bathroom',
        )
        master_bathroom.save()
        west_bedroom = models.Room(
            floor=second_floor,
            name='West Bedroom',
        )
        west_bedroom.save()
        east_bedroom = models.Room(
            floor=second_floor,
            name='East Bedroom',
        )
        east_bedroom.save()
        self.stdout = StringIO()
        self.addCleanup(self.stdout.close)
        self.command = fixtures_build.Command(stdout=self.stdout)

    def test_add_arguments(self) -> None:
        parser = MagicMock(spec=CommandParser)
        self.command.add_arguments(parser)
        parser.add_argument.assert_called()

    @patch(f'{MOD}.get_fixture_home', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.exists', return_value=True)
    @patch(f'{MOD}.os.path.isdir', return_value=True)
    def test_fixture_home(self, *_: NonCallableMock) -> None:
        val = self.command.fixtures_home
        exp = '/foo/bar'
        self.assertEqual(exp, val)

    @patch(f'{MOD}.get_fixture_home', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.exists', return_value=True)
    @patch(f'{MOD}.os.path.isdir', return_value=False)
    def test_fixture_home_raises(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            _ = self.command.fixtures_home

    @patch(f'{MOD}.get_fixture_home', return_value='/foo/bar')
    @patch(f'{MOD}.os.path.exists', return_value=False)
    @patch(f'{MOD}.os.makedirs', return_value=False)
    def test_fixture_mkdir(self, *args: NonCallableMock) -> None:
        makedirs = args[0]
        val = self.command.fixtures_home
        exp = '/foo/bar'
        self.assertEqual(exp, val)
        makedirs.assert_called_once()

    @patch(f'{MOD}.os.listdir', return_value=('one',))
    def test_no_wipe(
            self,
            listdir: NonCallableMock,
    ) -> None:
        options = {'wipe': False, 'verbose': 0, 'dry_run': False}
        with patch(f'{MOD}.Command.fixtures_home', '/foo/bar'):
            self.command.wipe(options)
        listdir.assert_not_called()

    @patch(f'{MOD}.os.listdir', return_value=('one',))
    @patch(f'{MOD}.os.path.isdir', return_value=True)
    @patch(f'{MOD}.shutil.rmtree', return_value=True)
    @patch(f'{MOD}.os.path.exists', return_value=True)
    @patch(f'{MOD}.os.rmdir', return_value=True)
    def test_wipe_dir(
            self,
            rmdir: NonCallableMock,
            exists: NonCallableMock,
            rmtree: NonCallableMock,
            isdir: NonCallableMock,
            listdir: NonCallableMock,
    ) -> None:
        options = {'wipe': True, 'verbose': 0, 'dry_run': False}
        with patch(f'{MOD}.Command.fixtures_home', '/foo/bar'):
            self.command.wipe(options)
        listdir.assert_called_once_with('/foo/bar')
        isdir.assert_called_once_with('/foo/bar/one')
        rmtree.assert_called_once_with('/foo/bar/one')
        exists.assert_called_once_with('/foo/bar/one')
        rmdir.assert_called_once_with('/foo/bar/one')

    @patch(f'{MOD}.os.listdir', return_value=('one',))
    @patch(f'{MOD}.os.path.isdir', return_value=False)
    @patch(f'{MOD}.os.unlink', return_value=None)
    def test_wipe_file_one(
            self,
            unlink: NonCallableMock,
            isdir: NonCallableMock,
            listdir: NonCallableMock,
    ) -> None:
        options = {'wipe': True, 'verbose': 2, 'dry_run': False}
        with patch(f'{MOD}.Command.fixtures_home', '/foo/bar'):
            self.command.wipe(options)
        listdir.assert_called_once_with('/foo/bar')
        isdir.assert_called_once_with('/foo/bar/one')
        unlink.assert_called_once_with('/foo/bar/one')

    @patch(f'{MOD}.os.listdir', return_value=('one',))
    @patch(f'{MOD}.os.path.isdir', return_value=False)
    @patch(f'{MOD}.os.unlink', return_value=None)
    def test_wipe_file_two(
            self,
            unlink: NonCallableMock,
            isdir: NonCallableMock,
            listdir: NonCallableMock,
    ) -> None:
        options = {'wipe': True, 'verbose': 2, 'dry_run': True}
        with patch(f'{MOD}.Command.fixtures_home', '/foo/bar'):
            self.command.wipe(options)
        listdir.assert_called_once_with('/foo/bar')
        isdir.assert_called_once_with('/foo/bar/one')
        unlink.assert_called_once_with('/foo/bar/one')

    @patch(f'{MOD}.os.listdir', return_value=('one',))
    @patch(f'{MOD}.os.path.isdir', return_value=False)
    @patch(f'{MOD}.os.unlink', return_value=None)
    def test_wipe_file_three(
            self,
            unlink: NonCallableMock,
            isdir: NonCallableMock,
            listdir: NonCallableMock,
    ) -> None:
        options = {'wipe': True, 'verbose': 0, 'dry_run': True}
        with patch(f'{MOD}.Command.fixtures_home', '/foo/bar'):
            self.command.wipe(options)
        listdir.assert_called_once_with('/foo/bar')
        isdir.assert_called_once_with('/foo/bar/one')
        unlink.assert_called_once_with('/foo/bar/one')

    FOO = fixtures_build.FixtureMeta(
        num=0,
        app='foo',
        model='foo',
        filename='0000__foo__foo.json',
        path='/fixtures/0000__foo__foo.json',
        args=('dumpdata', 'foo'),
        options=fixtures_build.Options(
            exclude=()
        )
    )
    BAR = fixtures_build.FixtureMeta(
        num=1,
        app='bar',
        model='bar',
        filename='0001__bar__bar.json',
        path='/fixtures/0001__bar__bar.json',
        args=('dumpdata', 'foo'),
        options=fixtures_build.Options(
            exclude=('foo',)
        )
    )

    FIXTURES = (FOO, BAR)

    @patch(f'{MOD}.Command.wipe', return_value=None)
    @patch(f'{MOD}.each_fixture_meta', return_value=FIXTURES)
    @patch(f'{MOD}.os.path.isfile', side_effect=(True, False))
    @patch(f'{MOD}.os.unlink', return_value=None)
    @patch(f'{MOD}.call_command', return_value=None)
    def test_handle_one(
            self,
            call_command_: NonCallableMock,
            unlink: NonCallableMock,
            is_file: NonCallableMock,
            each_fixture_meta: NonCallableMock,
            wipe: NonCallableMock,
    ) -> None:
        options = {
            'wipe': True,
            'verbose': 0,
            'dry_run': False,
            'force': True,
        }
        spin = MagicMock(spec=fixtures_build.Spin)
        open_ = mock_open()
        with patch(f'{MOD}.Command.fixtures_home', '/fixtures'):
            with patch(f'{MOD}.Spin', spin):
                with patch(f'{MOD}.open', open_):
                    self.command.handle(**options)
        wipe.assert_called_once()
        each_fixture_meta.assert_called_once()
        is_file.assert_called()
        unlink.assert_called_once()
        call_command_.assert_called()

    @patch(f'{MOD}.Command.wipe', return_value=None)
    @patch(f'{MOD}.each_fixture_meta', return_value=FIXTURES)
    @patch(f'{MOD}.os.path.isfile', side_effect=(True, False))
    @patch(f'{MOD}.os.unlink', return_value=None)
    @patch(f'{MOD}.call_command', return_value=None)
    def test_handle_two(
            self,
            call_command_: NonCallableMock,
            unlink: NonCallableMock,
            is_file: NonCallableMock,
            each_fixture_meta: NonCallableMock,
            wipe: NonCallableMock,
    ) -> None:
        options = {
            'wipe': True,
            'verbose': 0,
            'dry_run': True,
            'force': True,
        }
        spin = MagicMock(spec=fixtures_build.Spin)
        open_ = mock_open()
        with patch(f'{MOD}.Command.fixtures_home', '/fixtures'):
            with patch(f'{MOD}.Spin', spin):
                with patch(f'{MOD}.open', open_):
                    self.command.handle(**options)
        wipe.assert_called_once()
        each_fixture_meta.assert_called_once()
        is_file.assert_called()
        unlink.assert_not_called()
        call_command_.assert_not_called()

    @patch(f'{MOD}.Command.wipe', return_value=None)
    @patch(f'{MOD}.each_fixture_meta', return_value=FIXTURES)
    @patch(f'{MOD}.os.path.isfile', side_effect=(True, False))
    @patch(f'{MOD}.os.unlink', return_value=None)
    @patch(f'{MOD}.call_command', return_value=None)
    def test_handle_three(
            self,
            call_command_: NonCallableMock,
            unlink: NonCallableMock,
            is_file: NonCallableMock,
            each_fixture_meta: NonCallableMock,
            wipe: NonCallableMock,
    ) -> None:
        options = {
            'wipe': True,
            'verbose': 0,
            'dry_run': False,
            'force': False,
        }
        spin = MagicMock(spec=fixtures_build.Spin)
        open_ = mock_open()
        with patch(f'{MOD}.Command.fixtures_home', '/fixtures'):
            with patch(f'{MOD}.Spin', spin):
                with patch(f'{MOD}.open', open_):
                    self.command.handle(**options)
        wipe.assert_called_once()
        each_fixture_meta.assert_called_once()
        is_file.assert_called()
        unlink.assert_not_called()
        call_command_.assert_called()


# def rm_fixture_home() -> None:
#     shutil.rmtree(settings.FIXTURE_HOME)
#
#
# class TestCommandFunctional(TestCase):
#
#     def setUp(self) -> None:
#         building = models.Building(
#             type=1,
#             name='main'
#         )
#         building.save()
#         basement = models.Floor(
#             building=building,
#             number=0,
#             name='Basement'
#         )
#         basement.save()
#         laundry_room = models.Room(
#             floor=basement,
#             name='Laundry Room',
#         )
#         laundry_room.save()
#         first_floor = models.Floor(
#             building=building,
#             number=1,
#             name='First Floor'
#         )
#         first_floor.save()
#         living_room = models.Room(
#             floor=first_floor,
#             name='Living Room',
#         )
#         living_room.save()
#         kitchen = models.Room(
#             floor=first_floor,
#             name='Kitchen',
#         )
#         kitchen.save()
#         dining_room = models.Room(
#             floor=first_floor,
#             name='Dining Room',
#         )
#         dining_room.save()
#         half_bathroom = models.Room(
#             floor=first_floor,
#             name='Half Bathroom',
#         )
#         half_bathroom.save()
#         second_floor = models.Floor(
#             building=building,
#             number=2,
#             name='Second Floor'
#         )
#         second_floor.save()
#         master_bedroom = models.Room.objects.create(
#             floor=second_floor,
#             name='Master Bedroom',
#         )
#         master_bedroom.save()
#         master_bathroom = models.Room(
#             floor=second_floor,
#             name='Master Bathroom',
#         )
#         master_bathroom.save()
#         west_bedroom = models.Room(
#             floor=second_floor,
#             name='West Bedroom',
#         )
#         west_bedroom.save()
#         east_bedroom = models.Room(
#             floor=second_floor,
#             name='East Bedroom',
#         )
#         east_bedroom.save()
#         self.stdout = StringIO()
#         self.addCleanup(self.stdout.close)
#         # self.addCleanup(rm_fixture_home)
#
#     def test_fixtures_build(self) -> None:
#         call_command('fixtures_build', stdout=self.stdout, force=True)
