
from unittest.mock import (
    NonCallableMock,
    patch,
)

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from flanj.conf.permissions import (
    validate_permissions_settings,
    BUILTIN_SENTINELS,
)

MOD = 'flanj.conf.permissions'
MOD1 = 'flanj.conf.permissions.ContentType.objects'
MOD2 = 'flanj.conf.permissions.Permission.objects'
ContentTypeDoesNotExist = ContentType.DoesNotExist
PermissionDoesNotExist = Permission.DoesNotExist


# noinspection PyMethodMayBeStatic
class Filter:
    def exists(self):
        return False


_filter = Filter()


class TestValidatePermissionsSettings(TestCase):

    def test_settings_val_type_error(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': 0
        }
        with self.assertRaises(TypeError):
            validate_permissions_settings(**kwargs)

    def test_settings_val_type_error_string(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': 'test'
        }
        with self.assertRaises(TypeError):
            validate_permissions_settings(**kwargs)

    def test_settings_val_row_type_error(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [5]
        }
        with self.assertRaises(TypeError):
            validate_permissions_settings(**kwargs)

    def test_settings_val_row_value_error_permission_key(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [
                {'x': 1}
            ]
        }
        with self.assertRaises(ValueError):
            validate_permissions_settings(**kwargs)

    def test_settings_val_row_type_error_permission_key(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [
                {'frontend_id': '1'}
            ]
        }
        with self.assertRaises(TypeError):
            validate_permissions_settings(**kwargs)

    def test_settings_val_row_value_error_permission_key_empty(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [
                {'permission_name': ''}
            ]
        }
        with self.assertRaises(ValueError):
            validate_permissions_settings(**kwargs)

    @patch(f'{MOD1}.get_by_natural_key', side_effect=ContentTypeDoesNotExist)
    def test_settings_val_row_content_type_value_error(
            self,
            *_: NonCallableMock,
    ) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [
                {
                    'frontend_id': 1,
                    'permission_name': 'some role',
                    'permission_codename': 'some_role',
                    'content_type_app_label': 'foo',
                    'content_type_model': 'bar'
                }
            ]
        }
        with self.assertRaises(ValueError):
            validate_permissions_settings(**kwargs)

    def test_settings_frontend_id_duplicate(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [
                {
                    'frontend_id': 1,
                    'permission_name': 'some role 1',
                    'permission_codename': 'some_role_1',
                    'content_type_app_label': 'foo',
                    'content_type_model': 'bar'
                },
                {
                    'frontend_id': 1,
                    'permission_name': 'some role',
                    'permission_codename': 'some_role',
                    'content_type_app_label': 'foo',
                    'content_type_model': 'bar'
                },
            ]
        }
        with self.assertRaises(ValueError):
            validate_permissions_settings(**kwargs)

    def test_settings_permission_name_duplicate(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [
                {
                    'frontend_id': 1,
                    'permission_name': 'some role 1',
                    'permission_codename': 'some_role_1',
                    'content_type_app_label': 'foo',
                    'content_type_model': 'bar'
                },
                {
                    'frontend_id': 2,
                    'permission_name': 'some role 1',
                    'permission_codename': 'some_role_2',
                    'content_type_app_label': 'foo',
                    'content_type_model': 'bar'
                },
            ]
        }
        with self.assertRaises(ValueError):
            validate_permissions_settings(**kwargs)

    def test_settings_permission_codename_duplicate(self) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [
                {
                    'frontend_id': 1,
                    'permission_name': 'some role 1',
                    'permission_codename': 'some_role_1',
                    'content_type_app_label': 'foo',
                    'content_type_model': 'bar'
                },
                {
                    'frontend_id': 2,
                    'permission_name': 'some role 2',
                    'permission_codename': 'some_role_1',
                    'content_type_app_label': 'foo',
                    'content_type_model': 'bar'
                },
            ]
        }
        with self.assertRaises(ValueError):
            validate_permissions_settings(**kwargs)

    @patch(f'{MOD1}.get_by_natural_key', return_value=None)
    @patch(f'{MOD2}.filter', return_value=_filter)
    def test_settings_permissions_builtin_permissions(
            self,
            *_: NonCallableMock,
    ) -> None:
        kwargs = {
            'settings_key': 'PERMS',
            'settings_val': [
                {
                    'frontend_id': 1,
                    'permission_name': BUILTIN_SENTINELS[-1],
                    'permission_codename': 'some_role_1',
                    'content_type_app_label': 'foo',
                    'content_type_model': 'bar'
                },
            ]
        }
        with self.assertRaises(ValueError):
            validate_permissions_settings(**kwargs)
