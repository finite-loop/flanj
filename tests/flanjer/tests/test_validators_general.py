# noinspection PyPep8Naming
from decimal import Decimal as D
from django.test import TestCase
from django.core.exceptions import ValidationError
from flanj.validators import (
    DecimalValidator,
    EmailValidator,
    MaxLengthValidator,
    MaxValueValidator,
    MinLengthValidator,
    MinValueValidator,
    TimeZoneValidator,
)


class TestGeneralValidators(TestCase):

    def test_decimal_validator(self) -> None:
        validate = DecimalValidator(3, 1)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate(D('1.1')))
        self.assertIsNone(validate(D('0.1')))
        self.assertIsNone(validate(D('.1')))

    def test_decimal_validator_error_message(self) -> None:
        validate = DecimalValidator(3, 1)
        vals = (
            D('1.111'),
            D('111.1'),
            D('.111'),
        )
        for val in vals:
            with self.assertRaises(ValidationError) as cm:
                validate(val)
            e = cm.exception

            self.assertGreater(len(e.messages), 0)

            for msg in e.messages:
                self.assertGreater(len(msg.strip()), 0)

    def test_max_length_validator(self) -> None:
        validate = MaxLengthValidator(3)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate(''))
        self.assertIsNone(validate('abc'))

    def test_max_length_validator_error_message(self) -> None:
        validate = MaxLengthValidator(3)

        with self.assertRaises(ValidationError) as cm:
            # noinspection SpellCheckingInspection
            validate('abcd')

        e = cm.exception
        self.assertGreater(len(e.messages), 0)

        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_max_value_validator(self) -> None:
        validate = MaxValueValidator(5)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate(4))
        self.assertIsNone(validate(-5))

    def test_max_value_validator_error_message(self) -> None:
        validate = MaxValueValidator(5)
        with self.assertRaises(ValidationError) as cm:
            validate(6)

        e = cm.exception
        self.assertGreater(len(e.messages), 0)

        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_max_value_validator_negative_numbers(self) -> None:
        validate = MaxValueValidator(-2)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate(-3))
        self.assertIsNone(validate(-4))

        with self.assertRaises(ValidationError):
            validate(-1)

    def test_min_length_validator(self) -> None:
        validate = MinLengthValidator(3)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate('abc'))
        with self.assertRaises(ValidationError):
            validate('')

    def test_min_length_validator_error_message(self) -> None:
        validate = MinLengthValidator(3)

        with self.assertRaises(ValidationError) as cm:
            validate('ab')

        e = cm.exception
        self.assertGreater(len(e.messages), 0)

        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_min_value_validator(self) -> None:
        validate = MinValueValidator(2)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate(2))
        self.assertIsNone(validate(3))

    def test_min_value_validator_error_message(self) -> None:
        validate = MinValueValidator(2)
        with self.assertRaises(ValidationError) as cm:
            validate(1)

        e = cm.exception
        self.assertGreater(len(e.messages), 0)

        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_min_value_validator_negative_numbers(self) -> None:
        validate = MinValueValidator(-3)
        self.assertTrue(callable(validate))
        self.assertIsNone(validate(-2))
        self.assertIsNone(validate(-1))

        with self.assertRaises(ValidationError):
            validate(-4)

    def test_email_validator(self) -> None:
        validate = EmailValidator()
        self.assertTrue(callable(validate))
        self.assertIsNone(validate('test@localhost'))

        with self.assertRaises(ValidationError):
            validate('an email address')

    def test_email_validator_error_message(self) -> None:
        validate = EmailValidator(whitelist=[])

        with self.assertRaises(ValidationError) as cm:
            validate('test@localhost')

        e = cm.exception
        self.assertGreater(len(e.messages), 0)

        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_timezone_validator_message(self) -> None:
        validate = TimeZoneValidator()
        with self.assertRaises(ValidationError) as cm:
            validate('foo-bar')

        e = cm.exception
        self.assertGreater(len(e.messages), 0)

        for msg in e.messages:
            self.assertGreater(len(msg.strip()), 0)

    def test_timezone_validator(self) -> None:
        validate = TimeZoneValidator(
            message='invalid tz name',
            code='invalid_tz_name'
        )
        self.assertTrue(callable(validate))
        self.assertIsNone(validate('America/New_York'))
