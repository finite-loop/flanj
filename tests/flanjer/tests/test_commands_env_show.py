from io import StringIO
from unittest.mock import (
    NonCallableMock,
    patch,
)


from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase
from flanj.management.commands import env_show


MOD = 'flanj.management.commands.env_show'


class TestCommandsEnvShow(TestCase):

    def setUp(self) -> None:
        self.command: env_show.Command = env_show.Command()

    def test_functional(self) -> None:
        out = StringIO()
        call_command('env_show', stdout=out)
        self.assertIn("DOTENV VALS", out.getvalue())
        self.assertIn("BOOL_TRUE='true'", out.getvalue())

    @patch(f'{MOD}.os.environ.get', return_value='')
    def test_settings_module(self, *_: NonCallableMock) -> None:
        with self.assertRaises(CommandError):
            # noinspection PyStatementEffect
            self.command.settings_module
