import os
from unittest.mock import (
    patch,
    Mock,
    NonCallableMock,
    mock_open,
)
from django.test import TestCase

from git import InvalidGitRepositoryError
import pytz
from flanj.env import manifests

TIME_ZONE = 'America/New_York'
TIME_ZONE_OBJ = pytz.timezone(TIME_ZONE)

DIR = os.path.dirname(os.path.abspath(__file__))
TEST_HOME = os.path.abspath(os.path.join(DIR, '..', '..'))
PROJECT_NAME = 'flanjer'
PROJECT_HOME = os.path.dirname(TEST_HOME)
TOML_FILE = os.path.join(TEST_HOME, 'manifest.toml')


MOD = 'flanj.env.manifests'
MANIFEST = Mock(manifests.Manifest)


class TestDirUsableFunctional(TestCase):

    def test_not_absolute_path(self) -> None:
        arg = 'foo/bar'
        exp = False
        got = manifests._dir_usable(arg)
        msg = (
            f'\n\n'
            f'manifests._dir_usable({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    def test_not_directory(self) -> None:
        arg = TOML_FILE
        exp = False
        got = manifests._dir_usable(arg)
        msg = (
            f'\n\n'
            f'manifests._dir_usable({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    def test_not_home(self) -> None:
        arg = os.path.expanduser('~')
        exp = False
        got = manifests._dir_usable(arg)
        msg = (
            f'\n\n'
            f'manifests._dir_usable({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)


class TestDirUsableUnit(TestCase):

    def setUp(self) -> None:
        patcher = patch(
            'flanj.env.manifests.os.path.isabs',
            return_value=True,
        )
        self.os_path_isabs = patcher.start()
        self.addCleanup(patcher.stop)

        patcher = patch(
            'flanj.env.manifests.os.path.isdir',
            return_value=True,
        )
        self.os_path_isabs = patcher.start()
        self.addCleanup(patcher.stop)

        patcher = patch(
            'flanj.env.manifests._HOME',
            '/home/foo',
        )
        self.os_path_isabs = patcher.start()
        self.addCleanup(patcher.stop)

        patcher = patch(
            'flanj.env.manifests.os.path.sep',
            '/',
        )
        self.os_path_isabs = patcher.start()
        self.addCleanup(patcher.stop)

    def test_three_sub_directories(self) -> None:
        arg = '/etc/foo/bar'
        exp = True

        got = manifests._dir_usable(arg)
        msg = (
            f'\n\n'
            f'manifests._dir_usable({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    def test_two_sub_directories(self) -> None:
        arg = '/etc/blah'
        exp = False

        got = manifests._dir_usable(arg)
        msg = (
            f'\n\n'
            f'manifests._dir_usable({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    def test_one_sub_directory(self) -> None:
        arg = '/foo'
        exp = False

        got = manifests._dir_usable(arg)
        msg = (
            f'\n\n'
            f'manifests._dir_usable({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    def test_system_root_directory(self) -> None:
        arg = '/'
        exp = False

        got = manifests._dir_usable(arg)
        msg = (
            f'\n\n'
            f'manifests._dir_usable({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)


class TestIsTomlFile(TestCase):

    @patch(f'{MOD}.os.path.isfile', return_value=True)
    @patch(f'{MOD}.open', mock_open())
    @patch(f'{MOD}.toml.load', return_value={'a', 1})
    def test_true(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar.toml'
        exp = True

        with patch(
            'flanj.env.manifests.os.path.isfile',
            return_value=True,
        ):
            with patch(
                'flanj.env.manifests.open',
                mock_open(),
            ):
                with patch(
                    'flanj.env.manifests.toml.load',
                    return_value={'a', 1},
                ):
                    got = manifests._is_toml_file(arg)
        msg = (
            f'\n\n'
            f'manifests._is_toml_file({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    @patch(f'{MOD}.os.path.isfile', return_value=True)
    @patch(f'{MOD}.open', mock_open())
    @patch(f'{MOD}.toml.load', return_value={})
    def test_empty_data_false(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar.toml'
        exp = False
        got = manifests._is_toml_file(arg)
        msg = (
            f'\n\n'
            f'manifests._is_toml_file({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    @patch(f'{MOD}.os.path.isfile', return_value=False)
    def test_false(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar.toml'
        exp = False
        got = manifests._is_toml_file(arg)
        msg = (
            f'\n\n'
            f'manifests._is_toml_file({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)


class TestIsGitRepo(TestCase):

    @patch(f'{MOD}.os.path.basename', return_value='.git')
    @patch(f'{MOD}.os.path.dirname', return_value='/foo/bar/.git')
    @patch(f'{MOD}.os.path.isdir', return_value=True)
    @patch(f'{MOD}._Repo', side_effect=InvalidGitRepositoryError())
    def test_false(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar/.git'
        exp = False
        got = manifests._is_git_repo(arg)
        msg = (
            f'\n\n'
            f'manifests._is_git_repo({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    @patch(f'{MOD}.os.path.basename', return_value='.git')
    @patch(f'{MOD}.os.path.dirname', return_value='/foo/bar/.git')
    @patch(f'{MOD}.os.path.isdir', return_value=True)
    @patch(f'{MOD}._Repo', return_value=None)
    def test_true(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar/.git'
        exp = True
        got = manifests._is_git_repo(arg)
        msg = (
            f'\n\n'
            f'manifests._is_git_repo({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)


class TestPathType(TestCase):

    def test_empty_path(self) -> None:
        arg = None
        exp = '', ''

        got = manifests._path_type(arg)
        msg = (
            f'\n\n'
            f'manifests._path_type({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    @patch(f'{MOD}.os.path.isabs', return_value=False)
    def test_non_absolute(self, *_: NonCallableMock) -> None:
        arg = 'foo/bar'
        exp = '', ''
        got = manifests._path_type(arg)
        msg = (
            f'\n\n'
            f'manifests._path_type({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    @patch(f'{MOD}.os.path.isdir', return_value=False)
    @patch(f'{MOD}._is_toml_file', return_value=True)
    def test_toml(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar/manifests.toml'
        exp = arg, 'toml'
        got = manifests._path_type(arg)
        msg = (
            f'\n\n'
            f'manifests._path_type({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)
        return None

    @patch(f'{MOD}.os.path.isabs', return_value=True)
    @patch(f'{MOD}.os.path.isdir', return_value=False)
    @patch(f'{MOD}._is_toml_file', return_value=False)
    @patch(f'{MOD}.os.path.dirname', return_value='/foo/bar')
    @patch(f'{MOD}._dir_usable', return_value=True)
    @patch(f'{MOD}.os.path.join', return_value='/foo/bar/.git')
    @patch(f'{MOD}._is_git_repo', return_value=True)
    def test_missing_toml_use_git(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar/manifests.toml'
        exp = '/foo/bar/.git', 'git'
        got = manifests._path_type(arg)
        msg = (
            f'\n\n'
            f'manifests._path_type({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    @patch(f'{MOD}.os.path.isabs', return_value=True)
    @patch(f'{MOD}.os.path.isdir', return_value=True)
    @patch(f'{MOD}._dir_usable', return_value=True)
    @patch(f'{MOD}.os.path.join', side_effect=[
        '/foo/bar/.git',
        '/foo/bar/manifest.toml',
    ])
    @patch(f'{MOD}._is_git_repo', return_value=False)
    @patch(f'{MOD}.os.path.isfile', return_value=True)
    @patch(f'{MOD}._is_toml_file', return_value=True)
    def test_toml_in_dir(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar'
        exp = '/foo/bar/manifest.toml', 'toml'
        got = manifests._path_type(arg)
        msg = (
            f'\n\n'
            f'manifests._path_type({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    @patch(f'{MOD}.os.path.isabs', return_value=True)
    @patch(f'{MOD}.os.path.isdir', return_value=True)
    @patch(f'{MOD}._dir_usable', side_effect=[True, True])
    @patch(f'{MOD}.os.path.join', side_effect=[
        '/foo/bar/foo/.git',
        '/foo/bar/foo/manifest.toml',
        '/foo/bar/.git',
    ])
    @patch(f'{MOD}._is_git_repo', side_effect=[False, True])
    @patch(f'{MOD}.os.path.isfile', return_value=True)
    @patch(f'{MOD}._is_toml_file', return_value=False)
    @patch(f'{MOD}.os.path.dirname', return_value='/foo/bar')
    def test_git_in_parent_dir(self, *_: NonCallableMock) -> None:
        arg = '/foo/bar/foo'
        exp = '/foo/bar/.git', 'git'
        got = manifests._path_type(arg)
        msg = (
            f'\n\n'
            f'manifests._path_type({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)

    @patch(f'{MOD}.os.path.isabs', return_value=True)
    @patch(f'{MOD}.os.path.isdir', return_value=True)
    @patch(f'{MOD}._dir_usable', return_value=False)
    def test_dir_unusable(self, *_: NonCallableMock) -> None:
        arg = '/foo'
        exp = '', ''
        got = manifests._path_type(arg)
        msg = (
            f'\n\n'
            f'manifests._path_type({arg!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertEqual(got, exp, msg=msg)


class TestBuildManifests(TestCase):

    @patch(f'{MOD}._path_type', return_value=('/foo/bar/.git', 'git'))
    @patch(f'{MOD}.as_tzinfo', return_value=pytz.UTC)
    @patch(f'{MOD}._build_manifest_from_git', return_value=MANIFEST)
    def test_git_basic(self, *_: NonCallableMock) -> None:
        project_name = 'foobar'
        path = '/foo/bar'
        time_zone = None
        exp = MANIFEST
        got = manifests.build_manifest(
            project_name,
            path=path,
            time_zone=time_zone
        )
        msg = (
            f'\n\n'
            f'manifests.build_manifest({project_name!r}, path={path!r} '
            f'time_zone={time_zone!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertIsInstance(got, manifests.Manifest, msg=msg)

    @patch(f'{MOD}._path_type', return_value=('/foo/bar/blah.toml', 'toml'))
    @patch(f'{MOD}.as_tzinfo', return_value=pytz.UTC)
    @patch(f'{MOD}._build_manifest_from_file', return_value=MANIFEST)
    def test_toml_basic(self, *_: NonCallableMock) -> None:
        project_name = 'foobar'
        path = '/foo/bar'
        time_zone = None
        exp = MANIFEST
        got = manifests.build_manifest(
            project_name,
            path=path,
            time_zone=time_zone
        )
        msg = (
            f'\n\n'
            f'manifests.build_manifest({project_name!r}, path={path!r} '
            f'time_zone={time_zone!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}'
        )
        self.assertIsInstance(got, manifests.Manifest, msg=msg)

    @patch(f'{MOD}._path_type', return_value=('', ''))
    @patch(f'{MOD}.as_tzinfo', return_value=pytz.UTC)
    @patch(f'{MOD}._build_manifest_default', return_value=MANIFEST)
    def test_default_basic(self, *_: NonCallableMock) -> None:
        project_name = 'foobar'
        path = '/foo/bar'
        time_zone = None
        exp = MANIFEST
        got = manifests.build_manifest(
            project_name,
            path=path,
            time_zone=time_zone
        )
        msg = (
            f'\n\n'
            f'manifests.build_manifest({project_name!r}, path={path!r} '
            f'time_zone={time_zone!r})\n\n'
            f'got:  {got!r}\n\n'
            f'expected: {exp!r}\n\n'
        )
        self.assertIsInstance(got, manifests.Manifest, msg=msg)


